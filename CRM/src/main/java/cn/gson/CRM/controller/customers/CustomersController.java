package cn.gson.CRM.controller.customers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.gson.CRM.model.dao.customers.AddressDao;
import cn.gson.CRM.model.dao.customers.CustomersDao;
import cn.gson.CRM.model.dao.customers.TalkRecordDao;
import cn.gson.CRM.model.entity.Address;
import cn.gson.CRM.model.entity.Customer;
import cn.gson.CRM.model.entity.Order;
import cn.gson.CRM.model.entity.TalkRecord;
import cn.gson.CRM.model.mapper.CustomersMapper;
@Controller
@RequestMapping("/customers")
public class CustomersController {
	
	@Autowired
	CustomersMapper cm;
	
	@Autowired
	CustomersDao cd;
	
	@Autowired
	AddressDao ad;
	
	@Autowired
	TalkRecordDao trd;
	
	/**
	 * 客户管理界面
	 * @return
	 */
	@RequestMapping
	public String index(){
		return "customers/customers";
	}
	
	/**
	 * 交往记录界面
	 * @return
	 */
	@RequestMapping("talkrecord")
	public String talkIndex(){
		return "customers/talkrecord";
	}
	
	/**
	 * 客户订单表单
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping("orderform/{id}")
	public String orderForm(@PathVariable Long id, Model model){
		model.addAttribute("customersId",id);
		return "customers/customerorder";
	}
	
	/**
	 * 客户订单list
	 * @param id
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping("orders/{id}")
	@ResponseBody
	public Map<String, Object> getCustomerOrders(@PathVariable Long id,
			@RequestParam(defaultValue = "1")int page, int rows){
		
		Map<String,Object> orderdata = new HashMap<>();
		PageHelper.startPage(page, rows);
		List<Order> orders = cm.getOrders(id);
		PageInfo<Order> info = new PageInfo<>(orders);
		
		orderdata.put("rows", info.getList());
		orderdata.put("total", info.getTotal());
		
		return orderdata;
		
	}
	
	/**
	 * 交往记录表单
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping("talkrecord/form{id}")
	public String talkForm(@PathVariable Long id, Model model){
		model.addAttribute("customersId",id);
		return "customers/talkrecordform";
	}
	
	/**
	 * 查看交往记录详情
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping("talkrecord/select{id}")
	public String selectTalk(@PathVariable Long id, Model model){
		model.addAttribute("record",trd.findOne(id));
		return "customers/talkrecordform";
	}
	
	/**
	 * 添加交往记录
	 * @param cid
	 * @param record
	 * @param br
	 * @return
	 */
	@PostMapping("talkrecord/save")
	@ResponseBody
	@Transactional
	public JSONObject saveTalk(@RequestParam Long cid, @Valid TalkRecord record, BindingResult br){
		JSONObject result = new JSONObject();
		
		if(br.hasErrors()){
			result.put("err", "保存失败！");
		}else{
			record.setCustomer(cd.findOne(cid));
			trd.save(record);
			result.put("success", true);
		}
		
		return result;
	}
	
	/**
	 * 交往记录列表
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping("talkrecord/list")
	@ResponseBody
	public Map<String,Object> talkRecordList(@RequestParam(defaultValue = "1")int page,
			int rows,Customer customer){
		
		Map<String,Object> data = new HashMap<>();
		PageHelper.startPage(page, rows);
		List<TalkRecord> record = cm.getRecords(customer);
		PageInfo<TalkRecord> info = new PageInfo<>(record);
		
		data.put("rows", info.getList());
		data.put("total", info.getTotal());
		
		return data;
		
	}
	
	/**
	 * 收货地址表单
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping("addrform/{id}")
	public String addrForm(@PathVariable Long id, Model model){
		model.addAttribute("customersId",id);
		return "customers/addressfrom";
	}
	
	/**
	 * 收货地址list
	 * @param id
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping("addrlist/{id}")
	@ResponseBody
	public Map<String, Object> getAddrs(@PathVariable Long id,
			@RequestParam(defaultValue = "1")int page, int rows){
		
		Map<String,Object> addrdata = new HashMap<>();
		PageHelper.startPage(page, rows);
		List<Address> addrs = cm.getAddrs(id);
		PageInfo<Address> info = new PageInfo<>(addrs);
		
		addrdata.put("rows", info.getList());
		addrdata.put("total", info.getTotal());
		
		return addrdata;
		
	}
	
	/**
	 * 修改默认地址
	 * @param cid
	 * @param id
	 * @param checked
	 * @return
	 */
	@GetMapping("change/{id}")
	@ResponseBody
	@Transactional
	public String change(@RequestParam Long cid, @PathVariable Long id, Boolean checked){
		
		if(checked){
			ad.updateMoren(cd.findOne(cid));
			ad.updateMoren(id, checked);
			return "true";
		}else{
			ad.updateMoren(id, checked);
			return "true";
		}
		
	}
	
	/**
	 * 保存收货地址
	 * @param cid
	 * @param address
	 * @param br
	 * @return
	 */
	@PostMapping("saveaddr")
	@ResponseBody
	@Transactional
	public JSONObject saveAddr(@RequestParam Long cid, @Valid Address address, BindingResult br){
		JSONObject result = new JSONObject();
		
		if(br.hasErrors()){
			result.put("err", "保存失败！");
		}else{
			address.setMoren(false);
			address.setCustomer(cd.findOne(cid));
			ad.save(address);
			result.put("success", true);
		}
		return result;
	}
	
	/**
	 * 删除收货地址
	 * @param id
	 * @return
	 */
	@GetMapping("deleteaddr/{id}")
	@ResponseBody
	@Transactional
	public String deleteAddr(@PathVariable Long id) {
		ad.delete(id);
		return "true";
	}
	
	/**
	 * 编辑收货地址
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping("editaddr/{id}")
	@ResponseBody
	public JSONObject editAddr(@PathVariable Long id, Model model){
		JSONObject result = new JSONObject();
		result.put("addr", ad.findOne(id));
		return result;
	}
	
	/**
	 * 删除客户
	 * @param id
	 * @return
	 */
	@GetMapping("delete/{id}")
	@ResponseBody
	@Transactional
	public String delete(@PathVariable Long id) {
		cd.delete(id);
		return "true";
	}
	
	/**
	 * 编辑客户表单
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping({ "form", "form/{id}" })
	public String form(@PathVariable(required = false)Long id, Model model){
		if (id != null) {
			model.addAttribute("customer", cd.findOne(id));
		}
		return "customers/customerform";
	}
	
	/**
	 * 客户表单
	 * @param page
	 * @param rows
	 * @return
	 */
	@RequestMapping("list")
	@ResponseBody
	public Map<String,Object> list(@RequestParam(defaultValue = "1")int page,
			int rows, Customer customer){
		
		Map<String,Object> data = new HashMap<>();
		PageHelper.startPage(page, rows);
		List<Customer> customers = cm.getCustomers(customer);
		PageInfo<Customer> info = new PageInfo<>(customers);
		
		data.put("rows", info.getList());
		data.put("total", info.getTotal());
		
		return data;
		
	}
	
	/**
	 * 查找客户dam
	 * @return
	 */
	@PostMapping("selectList")
	@ResponseBody
	public List<Map<String, Object>> selectList(){
		List<Map<String, Object>> list = new ArrayList<>();
		Iterable<Customer> iterable =  cd.findAll();
		iterable.forEach(customer -> {
			Map<String,Object> data = new HashMap<>();
			data.put("text", customer.getName());
			data.put("id", customer.getId());
			list.add(data);
		});
		return list;
	}
	
	/**
	 * 保存客户
	 * @param customer
	 * @param br
	 * @return
	 */
	@PostMapping("save")
	@ResponseBody
	@Transactional
	public JSONObject save(@Valid Customer customer,BindingResult br){
		JSONObject result = new JSONObject();
		if(br.hasErrors()){
			result.put("err", "保存失败！");
		}else{
			cd.save(customer);
			result.put("success", true);
		}
		return result;
	}
	
}
