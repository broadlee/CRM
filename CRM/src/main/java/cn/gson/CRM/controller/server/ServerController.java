package cn.gson.CRM.controller.server;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.gson.CRM.model.dao.customers.CustomersDao;
import cn.gson.CRM.model.dao.permission.MemberDao;
import cn.gson.CRM.model.dao.server.ServerDao;
import cn.gson.CRM.model.entity.Server;
import cn.gson.CRM.model.entity.Server.State;
import cn.gson.CRM.model.mapper.ServerMapper;

@Controller
@RequestMapping("/server")
public class ServerController {

	@Autowired
	ServerDao serverDao;
	
	@Autowired
	CustomersDao customersDao;
	
	@Autowired
	MemberDao memberDao;
	
	@Autowired
	ServerMapper serverMapper;
	
	@GetMapping
	public String index(){
		return "server/server";
	}
	
	@RequestMapping({ "form", "form/{id}" })
	public String form(@PathVariable(required = false)Long id,Model model){
		if (id != null) {
			model.addAttribute("server", serverDao.findOne(id));
		}
		return "server/form";
	}
	
	@PostMapping("save")
	@ResponseBody
	@Transactional
	public JSONObject save(@Valid Server server,BindingResult br){
		JSONObject result = new JSONObject();
		if(br.hasErrors()){
			result.put("err", "保存失败！");
		}else{
			if(server.getDealer().getId() == null){
				server.setDealer(null);
				server.setState(State.valueOf("NEW"));
			} else if(server.getAnswer() != "" && server.getResult() == ""){
				server.setDealDate(new Date());
				server.setState(State.valueOf("DEALED"));
			} else if(server.getResult() != "" && server.getSatis() >= 3){
				server.setState(State.valueOf("SAVED"));
			} else{
				server.setAnswer("");
				server.setState(State.valueOf("ASSIGNED"));
			}
			serverDao.save(server);
			result.put("success", true);
		}
		return result;
	}
	
	@RequestMapping("list")
	@ResponseBody
	public Map<String,Object> list(Server server,@RequestParam(defaultValue = "1")int page,int rows){
		Map<String,Object> data = new HashMap<>();
		
		PageHelper.startPage(page, rows);
		List<Server> servers = serverMapper.getServer(server);
		PageInfo<Server> pageInfo = new PageInfo<Server>(servers);

		data.put("rows", servers);
		data.put("total", pageInfo.getTotal());
		
		return data;
	}
	
	@GetMapping("delete/{id}")
	@ResponseBody
	public String delete(@PathVariable Long id) {
		serverDao.delete(id);
		return "true";
	}
}
