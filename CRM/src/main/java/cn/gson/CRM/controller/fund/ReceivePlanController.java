package cn.gson.CRM.controller.fund;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.gson.CRM.model.dao.fund.ReceivePlanDao;
import cn.gson.CRM.model.entity.ReceivePlan;

@Controller
@RequestMapping("fund/receivePlan")
public class ReceivePlanController {

	@Autowired
	ReceivePlanDao receivePlanDao;
	
	@GetMapping
	public String index(){
		return "fund/receivePlan";
	}
	
	@RequestMapping("list")
	@ResponseBody
	public Map<String, Object> list(ReceivePlan receivePlan,@RequestParam(defaultValue = "1")int page,int rows){
		Map<String, Object> map = new HashMap<String, Object>();
		PageRequest pr = new PageRequest(page - 1, rows, Direction.ASC, "id");
		Page<ReceivePlan> pages = null; 
		if(receivePlan.getOrders() != null){
			pages = receivePlanDao.findAllByOrdersOrderId(receivePlan.getOrders().getOrderId(),pr);
		} else if(receivePlan.getRepair() != null){
			pages = receivePlanDao.findAllByRepairRepairId(receivePlan.getRepair().getRepairId(),pr);
		} else {
			pages = receivePlanDao.findAll(pr);
		}
		map.put("total", pages.getTotalElements());
		map.put("rows", pages.getContent());
		return map;
	}
}
