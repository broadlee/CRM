package cn.gson.CRM.controller.return_;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.gson.CRM.model.dao.return_.ReturnDao;
import cn.gson.CRM.model.dao.return_.ReturnDetailDao;
import cn.gson.CRM.model.entity.Return;

/**
* @author 邓安民:
*退货记录控制类
*/
@Controller
@RequestMapping("return/return")
public class ReturnController {
	@Autowired
	private ReturnDao returnDao;
	@Autowired
	private ReturnDetailDao returnDetailDao;
	@RequestMapping
	public void index(){
	}
	@ResponseBody
	@GetMapping("list")
	public Map<String, Object> getReturn(@RequestParam(defaultValue="1") Integer page,//当前页
			@RequestParam(defaultValue="8") Integer rows,//页记录数
			@RequestParam(defaultValue="id") String sort,//排序属性
			@RequestParam(defaultValue="desc") String order,//排序类型
			String where//条件
			){
		
			Sort sort2;
			if(order.equals("desc")){
				sort2=new Sort(Direction.DESC,sort);
			}else{
				sort2=new Sort(Direction.ASC,sort);
			}			
			Pageable pageable= new PageRequest(page-1, rows,sort2);	
			Page<Return> returns;
			if(!StringUtils.isEmpty(where)){
				returns=returnDao.selectByWhere(where, pageable);	
			}else{
				returns=returnDao.findAll(pageable);
			}
			
			Map<String, Object> map=new HashMap<>();
			map.put("total", returns.getTotalElements());
			map.put("rows", returns.getContent());
		    return map;	
	}
	@GetMapping("/returnDetail/{id}")
	public String returnDetail(@PathVariable Long id,Model model){
		Return returns=returnDao.findOne(id);
		model.addAttribute("r",returns );
		model.addAttribute("rd", returnDetailDao.findAllByReturn(returns.getId()));
		return "return/returnDetail";
		
	}
}


 