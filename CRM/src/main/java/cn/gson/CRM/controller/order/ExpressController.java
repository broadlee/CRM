package cn.gson.CRM.controller.order;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import cn.gson.CRM.model.dao.order.ExpressCompanyDao;
import cn.gson.CRM.model.entity.ExpressCompany;

/**
* @author 邓安民:
* @version 创建时间：2018年4月17日 下午12:10:54
* 快递公司控制类
*/
@Controller
@RequestMapping("order/express")
public class ExpressController {
	@Autowired
	private ExpressCompanyDao expressCompanyDao;
	@RequestMapping
	public void index(){}
	/**
	 * 分页查询
	 * @param page
	 * @param rows
	 * @param name
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value={"lists","/lists/{name}"},method=RequestMethod.GET)
	public Map<String, Object> lists(@RequestParam(defaultValue="1")Integer page,//当前页
			@RequestParam(defaultValue="8") Integer rows,//页数据量
			@PathVariable(required=false) String name//条件
			){
		Pageable pageable=new PageRequest(page-1, rows,new Sort(Direction.DESC, "id"));
		Page<ExpressCompany> expressCompanies;
		if(!StringUtils.isEmpty(name)){
			expressCompanies=expressCompanyDao.findByWhere(name, pageable);
		}else{
			expressCompanies=expressCompanyDao.findAll(pageable);
		}
		Map<String, Object> map=new HashMap<>();
		map.put("total", expressCompanies.getTotalElements());
		map.put("rows", expressCompanies.getContent());
		return map;
		
	}
	/**
	 * 获取所有的物流公司
	 * @return
	 */
	@ResponseBody
	@GetMapping("/all")
	public Iterable<ExpressCompany> all(){
		return expressCompanyDao.findAll();
	}
	/**
	 * 加载编辑表单
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping({ "form", "form/{id}" })
	public String form(@PathVariable(required = false) Long id, Model model) {
		if (id != null) {
			model.addAttribute("e", expressCompanyDao.findOne(id));
		}
		return "order/expressForm";
	}
	/**
	 * 保存
	 * @param eCompany
	 * @param br
	 * @return
	 */
	@RequestMapping("save")
	@ResponseBody
	@Transactional
	public JSONObject save(@Valid ExpressCompany eCompany, BindingResult br) {
		JSONObject result = new JSONObject();
		if (br.hasErrors()) {
			System.out.println(br);
			result.put("err", "校验失败！");
		} else {
			expressCompanyDao.save(eCompany);
			result.put("success", true);
		}
		return result;
	}
		
	@GetMapping("delete/{id}")
	@ResponseBody
	public String delete(@PathVariable Long id){
		expressCompanyDao.delete(id);	
		return "true";		
	}


}


 