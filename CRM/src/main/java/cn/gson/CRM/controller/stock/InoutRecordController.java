package cn.gson.CRM.controller.stock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.gson.CRM.model.dao.stock.InOutRecordDao;
import cn.gson.CRM.model.entity.InoutRecord;

@Controller
@RequestMapping("stock/inoutRecord")
public class InoutRecordController {
	
	@Autowired
	InOutRecordDao inoutRecordDao;

	@GetMapping
	public String index(){
		return "stock/inoutRecord";
	}
	
	/**
	 * 查询所有出入库单编号及处理人按条件查询
	 * @param value
	 * @param page
	 * @param rows
	 * @return
	 */
	@PostMapping("list")
	@ResponseBody
	public Map<String, Object> init(@RequestParam(required=false)String value,@RequestParam(required=false)String name,@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "10") int rows) {
		PageRequest pr = new PageRequest(page - 1, rows, new Sort(Direction.DESC, "id"));
		Page<InoutRecord> record = null;
		List<?> list = new ArrayList<>();
		
		if(name!=null && value!=null && !value.equals("")){
			if(name.equals("inout.inoutId")){
				record = inoutRecordDao.findByInoutInoutId(pr,value);
			}else if(name.equals("member.account")){
				record = inoutRecordDao.findByMemberNameLike("%"+value+"%",pr);
			}
		}else{
			record = inoutRecordDao.findAll(pr);
		}
		
		Map<String, Object> data = new HashMap<>();
		data.put("rows", record == null ? list : record.getContent());
		data.put("total", record == null ? 0 : record.getTotalElements());
		return data;
	}
}
