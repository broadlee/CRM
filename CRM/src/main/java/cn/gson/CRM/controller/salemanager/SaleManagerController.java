package cn.gson.CRM.controller.salemanager;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;

import cn.gson.CRM.model.dao.salemanage.ChanceDao;
import cn.gson.CRM.model.entity.Chance;
import cn.gson.CRM.model.entity.Customer;
import cn.gson.CRM.model.entity.Member;
import cn.gson.CRM.model.entity.Voc;
import cn.gson.CRM.model.mapper.SaleManagerMapper;
import cn.gson.CRM.service.EntityJSON;
import cn.gson.CRM.service.Result;
import cn.gson.CRM.service.SaleService.ChanceService;
import cn.gson.CRM.service.SaleService.VocService;

@Controller
@RequestMapping("/salemanager")
public class SaleManagerController
{
	@Autowired
	private SaleManagerMapper samM;

	@Autowired
	private ChanceDao chancedao;
	
	@Autowired
	private ChanceService chanceService;
	
	@Autowired
	private VocService vocService;

	@GetMapping
	public String saleManager()
	{
		return "salesmanagement/salemanager";
	}

	@GetMapping("/createsale")
	public String createSale(Model model, @RequestParam(defaultValue = "0") Long chancerow)
	{
		
		List<Customer> customers = samM.getCustomer();

		model.addAttribute("customers", customers);
		

		if (chancerow != 0)
		{
			Chance chance = chancedao.findOne(chancerow);
			
			customers.add(chance.getCustomer());

			model.addAttribute("chance", chance);
		}



		return "salesmanagement/createsales";
	}

	@PostMapping("getchance")
	@ResponseBody
	public EntityJSON<Chance> getChance(Integer page, Integer rows, Chance search, Model model)
	{
		EntityJSON<Chance> chances = new EntityJSON<Chance>();

		PageInfo<Chance> pageInfo = this.chanceService.getChance(page, rows, search);

		chances.setTotal(pageInfo.getTotal());
		chances.setRows(pageInfo.getList());
		model.addAttribute("search", search);

		return chances;
	}

	@PostMapping("addChance")
	@ResponseBody
	public EntityJSON<Chance> addChance(@Valid Chance chance,Long[] voc, BindingResult bind)
	{
		EntityJSON<Chance> result = new EntityJSON<Chance>();

		Boolean add=false;
		if (bind.hasErrors())
		{
			result.setErrors("创建失败！！");
			return result;
		}

		/**
		 * 创建的时候，才需要设置状态和创建人,编号
		 */
		if (chance.getMember1() == null && chance.getState() == null)
		{
			chance.setState(Chance.State.NOASSIGN);
			/**
			 * 创建人，到时由session获取
			 */
			chance.setMember1(new Member(1l));

			result.setInform("创建销售机会");
			
			chance.setQuoted(false);
			
			add = true;
		}
		else
		{
			result.setInform("(" + chance.getId() + ")" + "编辑销售机会|" + chance.getCustomer().getName() + "("
					+ chance.getCustomer().getLinkWay() + ")");
		}
		try
		{
			
			Chance save = chanceService.save(chance);
			
			/**
			 * 更新需求
			 */
			if(add)
				vocService.updateVocChance(save.getId(), voc);
			
			

//			Map<String ,Object> map = new HashMap<>();
//			
//			map.put("voc", voc);
//			map.put("chance", save.getId());
//			
//			this.samM.updateVocChance(map);
			
			
			
		}
		catch (Exception e)
		{
			result.setErrors("创建失败！！");
			e.printStackTrace();
			
			return result;
		}

		return result;

	}

	@PostMapping("/deletechance")
	@ResponseBody
	public EntityJSON<Chance> deletechance(@RequestParam("chances") Long[] chances)
	{
		EntityJSON<Chance> result = new EntityJSON<Chance>();

		try
		{
			List<Chance> deletes = samM.findDeleteChance(chances);
			
			this.chanceService.deleteChance(chances);

			result.setRows(deletes);
		}
		catch (Exception e)
		{
			result.setErrors("删除失败！");
			e.printStackTrace();
			return result;
		}

		return result;
	}

	@PostMapping("/getvoc")
	@ResponseBody
	public EntityJSON<Voc> getVoc(Long customer)
	{
		EntityJSON<Voc> result = new EntityJSON<Voc>();

		List<Voc> vocs = vocService.getVoc(customer);

		if (vocs != null && vocs.size() != 0)
		{
			result.setRows(vocs);
		}
		else
		{
			result.setErrors("该客户无需求！请重选");
		}

		return result;
	}
	
	@GetMapping("/setassaign")
	public String setAssign(Long chanceId,Model model) {
		
		Chance assignChance = chancedao.findOne(chanceId);
		
		model.addAttribute("assignChance", assignChance);
		
		
		return "salesmanagement/setassign";
	}
	@PostMapping("/getMember")
	@ResponseBody
	public List<Map<String ,Object>> getMember(){
		List<Map<String ,Object>> result =new  LinkedList<Map<String,Object>>();
		
		List<Member> assign=samM.getMember();
		
		for (Member member : assign)
		{
			Map<String ,Object> e = new HashMap<>();
			
			e.put("id", member.getId());
			e.put("text", member.getName()+"("+member.getAccount()+")");
			
			result.add(e);
		}
		
		
		return result;
	}
	
	@PostMapping("/doassaign")
	@ResponseBody	
	public EntityJSON<Member> doAssaign(Long memberId,Long chanceId){
		EntityJSON<Member> result = new EntityJSON<Member>(); 
		
		Chance chance = chancedao.findOne(chanceId);
		
		chance.setAssignDate(new Date());
		
		chance.setMember3(new Member(memberId));
		
		chance.setState(Chance.State.YESASSIGN);
		/**
		 * 从session获取指派人
		 */
		chance.setMember2(new Member(1l));
		
		try
		{
			chancedao.save(chance);
			result.setInform("指派成功！！");
		}
		catch (Exception e)
		{
			result.setErrors("指派失败！");
		}
		return result;
	}
	
	@PostMapping("endchance")
	@ResponseBody
	public Result endChance(Long chanceId) {
		
		Result result = new Result();
		
		try
		{
			this.chanceService.endchance(chanceId);
			result.setSuccess(true);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			result.setSuccess(false);
			result.setMsg("操作失败");
		}
		
		return result;
		
		
	}
	

}
