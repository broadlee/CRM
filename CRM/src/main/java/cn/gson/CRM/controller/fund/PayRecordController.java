package cn.gson.CRM.controller.fund;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.gson.CRM.model.dao.fund.PayPlanDao;
import cn.gson.CRM.model.dao.fund.PayRecordDao;
import cn.gson.CRM.model.dao.product.PurchaseDao;
import cn.gson.CRM.model.dao.return_.ReturnDao;
import cn.gson.CRM.model.dao.stock.InOutDao;
import cn.gson.CRM.model.entity.Inout;
import cn.gson.CRM.model.entity.Inout.Type;
import cn.gson.CRM.model.entity.PayRecord;
import cn.gson.CRM.model.entity.Purchase;
import cn.gson.CRM.model.entity.Purchase.State;
import cn.gson.CRM.model.entity.Return;
import cn.gson.CRM.model.mapper.PayRecordMapper;
import cn.gson.CRM.utils.ShareCodeUtil;

@Controller
@RequestMapping("fund/payRecord")
public class PayRecordController {

	@Autowired
	PayRecordDao payRecordDao;
	
	@Autowired
	PayRecordMapper payRecordMapper;
	
	@Autowired
	PurchaseDao purchaseDao;
	
	@Autowired
	ReturnDao returnDao;
	
	@Autowired
	PayPlanDao payPlanDao;
	
	@Autowired
	InOutDao inoutDao;
	
	@GetMapping
	public String index(){
		return "fund/payRecord";
	}
	
	@RequestMapping("list")
	@ResponseBody
	public Map<String, Object> list(PayRecord payRecord,@RequestParam(defaultValue = "1")int page,int rows){
		if(payRecord.getPurchase() != null){
			if(payRecord.getPurchase().getChaseId() == ""){
				payRecord.setPurchase(null);
			}
		}
		
		if(payRecord.getReturns() != null){
			if(payRecord.getReturns().getReturnId() == ""){
				payRecord.setReturns(null);
			}
		}
		Map<String, Object> map = new HashMap<String, Object>();
		PageHelper.startPage(page, rows);
		List<PayRecord> payRecords = payRecordMapper.findAll(payRecord);
		PageInfo<PayRecord> pageInfo = new PageInfo<PayRecord>(payRecords);
		map.put("rows", payRecords);
		map.put("total", pageInfo.getTotal());
		return map;
	}
	
	@PostMapping("save")
	@ResponseBody
	@Transactional
	public JSONObject save(@Valid PayRecord payRecord,Long oId,Long rId,Long planId,BindingResult br){
		Purchase purchase = null;
		Return return1 = null;
		if(oId != null){
			purchase = purchaseDao.findOne(oId);
			if(purchase != null){
				payRecord.setPurchase(purchase);
			}
		} 
		
		if(rId != null){
			return1 = returnDao.findOne(rId);
			if(return1 != null){
				payRecord.setReturns(return1);
			}
		}
		
		JSONObject result = new JSONObject();
		if(br.hasErrors()){
			result.put("err", "保存失败！");
		}else{
			PayRecord saved = payRecordDao.save(payRecord);
			if(saved != null){
				switch (saved.getType().toString()) {
					case "PURCHASE":
						//判断如果付款计划的类型为采购，则修改订采购单状态为已支付
						purchase.setState(State.valueOf("PAYED"));
						purchaseDao.save(purchase);
						
						//添加入库单
						Inout inout = new Inout();
						inout.setInoutId(ShareCodeUtil.getCharAndNumr());
						inout.setPurchase(purchase);
						inout.setState(cn.gson.CRM.model.entity.Inout.State.valueOf("NODEAL"));
						inout.setType(Type.valueOf("COME"));
						inoutDao.save(inout);
						break;
					case "RETURN":
						//判断如果付款计划的类型为退货款，则修改退货单状态为已完成
						return1.setState(cn.gson.CRM.model.entity.Return.State.valueOf("TUED"));
						returnDao.save(return1);
						break;
					default:
						break;
				}
			}
			result.put("success", true);
			if(result.getBooleanValue("success")){
				payPlanDao.delete(planId);
			};
		}

		return result;
	}
}
