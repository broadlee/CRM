package cn.gson.CRM.controller.permission;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.apache.commons.codec.digest.DigestUtils;
import org.omg.CosNaming.NamingContextExtPackage.StringNameHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;

import cn.gson.CRM.model.dao.permission.MemberDao;
import cn.gson.CRM.model.dao.permission.RoleDao;
import cn.gson.CRM.model.entity.Member;
import cn.gson.CRM.model.entity.Role;

@Controller
@RequestMapping("permission/member")
public class MemberController {

	@Autowired
	MemberDao memberDao;

	@Autowired
	RoleDao roleDao;

	@RequestMapping
	public void index() {

	}
	
	@PostMapping("list")
	@ResponseBody
	public Map<String, Object> list(@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "20") int rows) {
		PageRequest pr = new PageRequest(page - 1, rows, new Sort(Direction.DESC, "id"));
		Page<Member> members = memberDao.findAll(pr);
		Map<String, Object> data = new HashMap<>();
		data.put("rows", members.getContent());
		data.put("total", members.getTotalElements());
		return data;
	}

	@RequestMapping({ "form", "form/{id}" })
	public String form(@PathVariable(required = false) Long id, Model model) {
		if (id != null) {
			model.addAttribute("r", memberDao.findOne(id));
		}
		List<Role> roles = roleDao.findByEnable(true);
		model.addAttribute("roles", roles);
		return "permission/member/form";
	}

	@RequestMapping("save")
	@ResponseBody
	@Transactional
	public JSONObject save(@Valid Member member, Long[] role, BindingResult br) {
		JSONObject result = new JSONObject();
		if (br.hasErrors()) {
			System.out.println(br);
			result.put("err", "参数传递错误");
		} else {
			if (role != null && role.length > 0) {
				List<Role> roles = new ArrayList<>();
				for (Long rid : role) {
					roles.add(new Role(rid));
				}
				member.setRoles(roles);
			}

			if (member.getId() == null) {
				// 保存密码
				member.setPassword(DigestUtils.md2Hex(member.getPassword()));
			}else {
				Member m = memberDao.findOne(member.getId());
				member.setPassword(m.getPassword());
			}

			memberDao.save(member);
			result.put("success", true);
		}
		return result;
	}

	@GetMapping("delete/{id}")
	@ResponseBody
	public String delete(@PathVariable Long id) {
		memberDao.delete(id);
		return "true";
	}

	@PostMapping("check")
	@ResponseBody
	public Boolean delete(String account) {
		int count = memberDao.countByAccount(account);
		return count == 0;
	}
	
	@GetMapping("info")
	public String toUserInfo(){
		return "permission/member/info";
	}
	@GetMapping("pass_form")
	public String pass_form(){
		return "permission/member/pass_form";
	}
	
	@PostMapping("resetPass")
	@ResponseBody
	public String resetPass(Long id,String oldPass,String newPass){
		Member member=memberDao.findOne(id);
		if(member!=null){
			if(member.getPassword().equals(oldPass)){
				member.setPassword(newPass);
				memberDao.save(member);
				return "true";
			}else{
				return "err";
			}
		}
		return "false";
	}

}
