package cn.gson.CRM.controller.product;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.support.spring.annotation.ResponseJSONP;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.gson.CRM.model.dao.product.ProductDao;
import cn.gson.CRM.model.dao.product.SupplierDao;
import cn.gson.CRM.model.entity.Product;
import cn.gson.CRM.model.entity.Supplier;
import cn.gson.CRM.model.mapper.ProductMapper;
import cn.gson.CRM.service.product.UploadImg;

@Controller
@RequestMapping("product")
public class ProductController {

	@Autowired
	ProductDao proDao;
	
	@Autowired
	SupplierDao suppDao;
	
	@Autowired
	ProductMapper proMapper;
	
	@Autowired
	private UploadImg uploadImg;
	
	@RequestMapping
	public String init(){
		return "product/product";
	}
	
	@PostMapping("list")
	@ResponseJSONP
	public Map<String, Object> list(Product product ,@RequestParam(defaultValue="1") int page,
			@RequestParam(defaultValue="10") int rows){
		PageHelper.startPage(page, rows);
		List<Product> products = proMapper.searchProduct(product);
		PageInfo<Product> pageInfo = new PageInfo<Product>(products);
		
		Map<String, Object> data = new HashMap<>();
		data.put("rows", products);
		data.put("total", pageInfo.getTotal());
		return data;
	}
	
	//修改
	@RequestMapping({ "form", "form/{id}" })
	public String form(@PathVariable(required = false) Long id, Model model) {
		if (id != null) {
			model.addAttribute("pro", proDao.findOne(id));
		}
		return "product/proForm";
	}
	
	
	//保存
	@RequestMapping("save")
	@ResponseBody
	@Transactional
	public JSONObject save(Product product,Long suppId,
			BindingResult br) {
		System.out.println(product);
		
		JSONObject result = new JSONObject();
		if (br.hasErrors()) {
			System.out.println(br);
			result.put("err", "校验失败！");
		} else {
			if(suppId!=null ){
				product.setSupplier(suppDao.findOne(suppId));
				System.out.println(product);
				proDao.save(product);
				result.put("success", true);
			}
		}
		return result;
	}
	
	@GetMapping("data")
	@ResponseJSONP
	public Iterable<Product> datalist(Product product){
		Iterable<Product> products = proMapper.searchProduct(product);
		return products;
	}
	
	@PostMapping("upload")
	@ResponseBody
	public Result upload(@RequestParam(value = "upload_file", required = false) CommonsMultipartFile f
			,HttpServletRequest request
			) {
		System.out.println("OriginalFilename:"+f.getOriginalFilename()+",ContentType:{}"+ f.getContentType());
		try {
			String imgName = uploadImg.save(f,request);
			
			return new Result(true, "", imgName);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "文件上传失败！"+e.getMessage(), "");
		}
	}
	
	/*
	 * 删除
	 */
	@GetMapping("delete/{id}")
	@ResponseBody
	@Transactional
	public String delete(@PathVariable Long id) {
		proDao.delete(id);
		return "true";
	}
	
	/**
	 * 查找产品
	 * @return
	 */
	@PostMapping("selectList")
	@ResponseBody
	public List<Map<String, Object>> selectList(){
		List<Map<String, Object>> list = new ArrayList<>();
		Iterable<Product> iterable =  proDao.findAll();
		iterable.forEach(product -> {
			Map<String,Object> data = new HashMap<>();
			data.put("text", product.getName());
			data.put("id", product.getId());
			list.add(data);
		});
		return list;
	}
	
	class Result{
		boolean success = false;
		String msg;
		String file_path;
		public Result() {
		}

		public boolean isSuccess() {
			return success;
		}

		public void setSuccess(boolean success) {
			this.success = success;
		}

		public String getMsg() {
			return msg;
		}

		public void setMsg(String msg) {
			this.msg = msg;
		}

		public String getFile_path() {
			return file_path;
		}

		public void setFile_path(String file_path) {
			this.file_path = file_path;
		}

		public Result(boolean success, String msg, String file_path) {
			super();
			this.success = success;
			this.msg = msg;
			this.file_path = file_path;
		}
	}
}
