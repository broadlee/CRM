package cn.gson.CRM.controller.reception;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.pagehelper.PageInfo;

import cn.gson.CRM.model.dao.customers.CustomersDao;
import cn.gson.CRM.model.dao.voc.VocDao;
import cn.gson.CRM.model.entity.Customer;
import cn.gson.CRM.model.entity.Customer.LifeTime;
import cn.gson.CRM.model.entity.PageMsg;
import cn.gson.CRM.model.dao.product.ProductDao;
import cn.gson.CRM.model.entity.Product;
import cn.gson.CRM.model.entity.Voc;
import cn.gson.CRM.model.mapper.ProductMapper;

@Controller
@RequestMapping("/reception")
public class ReceptionController {

	@Autowired
	ProductMapper productMapper;
	
	@Autowired
	ProductDao productDao;
	
	@Autowired
	CustomersDao customersDao;
	
	@Autowired
	VocDao vocDao;

	@RequestMapping
	public String index(Model model) {
		model.addAttribute("count", productDao.count());
		return "reception/reception";
	}
	
	@RequestMapping("form/{id}")
	public String form(@PathVariable(required=false)Long id,Model model){
		model.addAttribute("oneProduct", productDao.findOne(id));
		return "reception/form";
	}
	
	/**
	 * 分页显示产品
	 * @param page
	 * @param pageSize
	 * @param model
	 * @return
	 */
	@GetMapping("list")
	public String list(@RequestParam(required=false)int page,@RequestParam(required=false)int pageSize,Model model){
		PageMsg pageMsg = new PageMsg();
		pageMsg.setPage(page);
		pageMsg.setPageSize(pageSize);
		pageMsg.setStartPage((pageMsg.getPage()-1)*pageMsg.getPageSize()+1);
		
		List<Product> product = productMapper.selectPage(pageMsg);
		PageInfo<Product> info = new PageInfo<>(product);
		model.addAttribute("rows", info);
		return "reception/list";
	}
	
	/**
	 * 提交用户及需求
	 */
	@PostMapping("/userForm")
	public void userForm(@Valid Customer customer,@RequestParam(required=false)String numArray){
		//保存用户
		Customer cust = customersDao.findByNameAndLinkWay(customer.getName(),customer.getLinkWay());
		System.out.println("1"+cust);
		if(cust==null){
			customer.setLifeTime(LifeTime.valueOf("LATENT"));
			customer.setCreateDate(new Date());
			customersDao.save(customer);
		}
		
		if(numArray!=null && numArray.length()>0){
			//保存该用户的需求
			numArray = numArray.substring(0,numArray.length()-1);
			String[] strs = numArray.split(",");
			for (String str : strs) {
				Voc voc = new Voc();
				voc.setProduct(productDao.findOne(Long.parseLong(str.substring(0, str.lastIndexOf(".")))));
				voc.setNumber(Integer.parseInt(str.substring(str.lastIndexOf(".")+1,str.length())));
				if(cust==null){
					System.out.println(customer+"。");
					voc.setCustomer(customer);
				}else{
					System.out.println(cust);
					voc.setCustomer(cust);
				}
				vocDao.save(voc);
			}
		}
	}
	
}