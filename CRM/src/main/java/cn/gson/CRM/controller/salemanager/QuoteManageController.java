package cn.gson.CRM.controller.salemanager;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;

import cn.gson.CRM.model.entity.Chance;
import cn.gson.CRM.model.entity.Order;
import cn.gson.CRM.model.entity.QuoDetail;
import cn.gson.CRM.model.entity.Quotation;
import cn.gson.CRM.service.EntityJSON;
import cn.gson.CRM.service.Result;
import cn.gson.CRM.service.SaleService.ChanceService;
import cn.gson.CRM.service.SaleService.QuotationService;

@Controller
@RequestMapping("/quote")
public class QuoteManageController
{
	@Autowired
	private ChanceService chanceService;

	@Autowired
	private QuotationService quoSer;
	


	@GetMapping
	public String quote()
	{

		return "salesmanagement/quotation";
	}

	@GetMapping("createpage")
	public String createPage()
	{

		return "salesmanagement/createquotation";
	}

	@PostMapping("getchance")
	@ResponseBody
	public List<Map<String, Object>> getChance()
	{
		/**
		 * 获取机会id 和客户信息
		 */
		List<Map<String, Object>> chance = chanceService.getChance();

		return chance;
	}

	@PostMapping("createquote")
	@ResponseBody
	public EntityJSON<Quotation> createQuotation(Quotation quote)
	{

		EntityJSON<Quotation> result = new EntityJSON<Quotation>();

		try
		{
			quoSer.createQuotation(quote);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			result.setErrors("创建失败");
		}
		return result;
	}

	@PostMapping("getquotation")
	@ResponseBody
	public EntityJSON<Quotation> getQuotation(Integer page, Integer rows, Quotation search)
	{
		EntityJSON<Quotation> result = new EntityJSON<Quotation>();

		PageInfo<Quotation> pageinfo = quoSer.getQuotation(page, rows, search);

		result.setTotal(pageinfo.getTotal());
		result.setRows(pageinfo.getList());

		return result;
	}

	@PostMapping("getquodetail")
	@ResponseBody
	public EntityJSON<QuoDetail> getQuoDetail(Integer page, Integer rows, Long quotation)
	{
		EntityJSON<QuoDetail> result = new EntityJSON<QuoDetail>();

		PageInfo<QuoDetail> pageinfo = quoSer.getQuoDetail(page, rows, quotation);

		result.setTotal(pageinfo.getTotal());
		result.setRows(pageinfo.getList());

		return result;
	}

	@PostMapping("deletequotation")
	@ResponseBody
	public EntityJSON<Quotation> deleteQuotation(Long[] quotation)
	{
		EntityJSON<Quotation> result = new EntityJSON<Quotation>();

		try
		{
			quoSer.deleteQutation(quotation);

		}
		catch (Exception e)
		{
			result.setErrors("删除失败");
			e.printStackTrace();
		}

		return result;
	}

	@PostMapping("editdetail")
	@ResponseBody
	public EntityJSON<QuoDetail> editDetail(Double cost, Integer count, Long detailId)
	{
		EntityJSON<QuoDetail> result = new EntityJSON<QuoDetail>();

		quoSer.updateCostAndCont(cost, count, detailId);
		
		Long quotationId = quoSer.getQuotationId(detailId);
		
		Double total = quoSer.changeTotal(quotationId);
		
		result.setRetur(total);

		return result;
	}

	@GetMapping("adddetail")
	public String adddetail(Long quoteid, Model model)
	{
		model.addAttribute("quoteid", quoteid);
		return "salesmanagement/adddetail";

	}

	@PostMapping("getProduct")
	@ResponseBody
	public List<Map<String, Object>> getProduct(Long quotation)
	{

		List<Map<String, Object>> products = quoSer.getProduct(quotation);

		return products;
	}
	@PostMapping("saveDetail")
	@ResponseBody
	public EntityJSON<QuoDetail> saveDetail(QuoDetail quo)
	{
		EntityJSON<QuoDetail> result = new EntityJSON<QuoDetail>();

		
		try
		{
			QuoDetail save = quoSer.saveDetail(quo);
			Double total = quoSer.changeTotal(save.getQuotation().getId());
			result.setRetur(total);
		}
		catch (Exception e)
		{
			result.setErrors("保存失败！");
			e.printStackTrace();
		}

		return result;
	}
	
	@PostMapping("deletedetail")
	@ResponseBody
	public EntityJSON<QuoDetail> deleteDetail(Long[] detailId)
	{
		EntityJSON<QuoDetail> result = new EntityJSON<QuoDetail>();

		
		try
		{
			Long quotationId =0l;
			if(detailId.length>0) {
				quotationId = quoSer.getQuotationId(detailId[0]);
			}
			
			this.quoSer.deleteDetail(detailId);
			
			Double total = quoSer.changeTotal(quotationId);
			
			result.setRetur(total);
			
		}
		catch (Exception e)
		{
			result.setErrors("删除失败！");
			e.printStackTrace();
		}

		return result;
	}
	
	@GetMapping("create_order")
	public String create_order(Long quoteId,Model model) {
		
		Long chanceId =  this.quoSer.getChanceIdByQuoId(quoteId);
		
		List<Chance> chance = chanceService.getChance(new Chance(chanceId));
		
		Quotation quo = this.quoSer.findOneQuote(quoteId);
		
		model.addAttribute("chance", chance.get(0));
		
		model.addAttribute("quo", quo);
		
		
		return "salesmanagement/addOrder";
		
	}
	
	@PostMapping("createOrder")
	@ResponseBody
	public Result createOrder(Order order,Long quoteId,Long chanceId) {
		Result result = new Result();
		
		try
		{
			Order saveOrder = this.quoSer.saveOrder(order);
			
			this.quoSer.saveOrderDetail(saveOrder,quoteId);
			
			chanceService.setQouteAndPlanState(chanceId);
			
			

			
			
			result.setSuccess(true);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			result.setSuccess(false);
			result.setMsg("保存失败");
		}
		
		return result;
	}
	
	@PostMapping("checkSuccess")
	@ResponseBody
	public Result checkSuccess(Long quoteId) {
		Result result = new Result();
		
		Long chanceId =  this.quoSer.getChanceIdByQuoId(quoteId);
		
		List<Chance> chance = chanceService.getChance(new Chance(chanceId));
		
		if(chance.get(0).getState().equals(Chance.State.SUCCESS)) {
			result.setSuccess(true);
			result.setMsg("该报价单已经生成订单");
			return result;
		}else if(chance.get(0).getState().equals(Chance.State.FAILED)){
			result.setMsg("该机会开始失败");
			result.setSuccess(true);
			return result;
		}
		
		
		
		
		return result;
	}
}
