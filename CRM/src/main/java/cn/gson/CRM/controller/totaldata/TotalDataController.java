package cn.gson.CRM.controller.totaldata;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.gson.CRM.model.dao.customers.CustomersDao;
import cn.gson.CRM.model.dao.order.OrderDao;
import cn.gson.CRM.model.dao.salemanage.ChanceDao;
import cn.gson.CRM.model.dao.server.ServerDao;
import cn.gson.CRM.model.entity.Chance.State;
import cn.gson.CRM.model.entity.Customer.LifeTime;
import cn.gson.CRM.model.entity.Server.Type;
import cn.gson.CRM.model.mapper.OrderMapper;
import cn.gson.CRM.model.vo.CustomerDataVo;
import cn.gson.CRM.model.vo.CustomerTotalVo;
import cn.gson.CRM.model.vo.GoodsPaihangDatoaVo;
import cn.gson.CRM.model.vo.MemberPhData;
import cn.gson.CRM.model.vo.MemberTotalVo;
import cn.gson.CRM.model.vo.OrderTotalVo;
import cn.gson.CRM.model.vo.ProductTotalVo;
import cn.gson.CRM.model.vo.SaleTotalVo;

/**
* @author 邓安民:
* @version 创建时间：2018年5月9日 下午12:59:16
* 数据统计
*/
@Controller
@RequestMapping("/dataTotal")
public class TotalDataController {
	
	@Autowired
	private ChanceDao chanceDao;
	
	@Autowired
	private OrderDao orderDao;
	
	@Autowired
	private OrderMapper orderMapper;
	
	@Autowired
	private CustomersDao customerDao;
	
	@Autowired
	private ServerDao serviceDao;
	
	/**
	 * 跳转到基础数据统计
	 * @return
	 */
	@GetMapping("/saleTotal")
	public String toSale(){
		return "dataTotal/saleTotal";
		
	}
	
	/**
	 * 跳转到各类排行统计
	 * @return
	 */
	@GetMapping("/paihang")
	public String toPaihang(){
		return "dataTotal/paihang";
		
	}
	
	/**
	 * 销售机会统计
	 * @param year
	 * @return
	 */
	@GetMapping("/getSaleData")
	@ResponseBody
	public List<SaleTotalVo> getSaleData(@PathVariable(required=false)String year){
		//取得当前的系统时间
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy");
		year=year==null?sdf.format(new Date()):year;
		List<SaleTotalVo> list=new ArrayList<>();
		//获取三种数据
		List<Object[]> data1=chanceDao.getTotalData(year);
		System.out.println(data1.size()+data1.toString());
		List<BigInteger> dataList1=new ArrayList<>();
		for(int i=1;i<=12;i++){
			dataList1.add(BigInteger.valueOf(0));
		}
		
		data1.forEach( d->{
			int m=(int) d[0];
			dataList1.set(m-1, (BigInteger) d[1]);
		});
		
		List<Object[]> data2=chanceDao.getTotalData(year,State.SUCCESS.toString());
		List<BigInteger> dataList2=new ArrayList<>();
		for(int i=1;i<=12;i++){
			dataList2.add(BigInteger.valueOf(0));
		}
		data2.forEach( d->{
			int m=(int) d[0];
			dataList2.set(m-1, (BigInteger) d[1]);
		});
		
		List<Object[]> data3=chanceDao.getTotalData(year,State.FAILED.toString());
		List<BigInteger> dataList3=new ArrayList<>();
		for(int i=1;i<=12;i++){
			dataList3.add(BigInteger.valueOf(0));
		}
		data3.forEach( d->{
			int m=(int) d[0];
			dataList3.set(m-1, (BigInteger) d[1]);
		});
		
		SaleTotalVo all=new SaleTotalVo(year,"all",dataList1) ;
		SaleTotalVo success=new SaleTotalVo(year,"success",dataList2) ;
		SaleTotalVo failed=new SaleTotalVo(year,"failed",dataList3) ;
		list.add(all);
		list.add(success);
		list.add(failed);
		return list;		
	}
	
	/**
	 * 统计消费前15的客户排行
	 * @return
	 */
	@GetMapping("/getCustomerTotal")
	@ResponseBody
	public List<CustomerDataVo> getCustomerTotal(){
		Pageable pageable=new PageRequest(0, 15);
		//获取所有
		List<CustomerTotalVo> all=orderDao.totalByCustomerM(pageable);
		List<String> allname=new ArrayList<>();
		List<BigDecimal> allData=new ArrayList<>();
		all.forEach(a->{
			allname.add(a.getName());
			allData.add(a.getData());
		});
		CustomerDataVo cVo1=new CustomerDataVo("all", allname, allData);
		//获取今年
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy");
		String year=sdf.format(new Date());
		List<CustomerTotalVo> tyear=orderMapper.totalByCustomerM(year);
		List<String> tyearname=new ArrayList<>();
		List<BigDecimal> tyearData=new ArrayList<>();
		tyear.forEach(ty->{
			tyearname.add(ty.getName());
			tyearData.add(ty.getData());
		});
		CustomerDataVo cVo2=new CustomerDataVo(year, tyearname, tyearData);
		System.out.println(tyear.toString());
		List<CustomerDataVo> list=new ArrayList<>();
		list.add(cVo1);
		list.add(cVo2);
		return list;		
	}
	
	/**
	 * 获取商品前10销售额排行
	 * @return
	 */
	@GetMapping("/getProductPaihang")
	@ResponseBody
	public List<GoodsPaihangDatoaVo> getProductPaihang(){
		//获取前10的商品
		Pageable pageable=new PageRequest(0, 10);
		List<ProductTotalVo> phData = orderDao.getGoodsPhData(pageable);
		
		//获取所有名字和金额
		List<String> allname = new ArrayList<>();
		List<BigDecimal> allData = new ArrayList<>();
		phData.forEach(ph -> {
			allname.add(ph.getName());
			allData.add(ph.getNumber());
		});
		
		GoodsPaihangDatoaVo datoaVo = new GoodsPaihangDatoaVo(allname,allData);
		
		List<GoodsPaihangDatoaVo> list = new ArrayList<>();
		list.add(datoaVo);
		
		return list;		
	}
	
	/**
	 * 获取用户开发成功客户前10排行
	 * @return
	 */
	@GetMapping("/getMemberPaihang")
	@ResponseBody
	public List<MemberPhData> getMemberPaihang(){
		//获取前10的商品
		Pageable pageable=new PageRequest(0, 10);
		List<MemberTotalVo> phData = chanceDao.getMemberPhData(pageable);
		
		//获取所有名字和金额
		List<String> allname = new ArrayList<>();
		List<Long> allData = new ArrayList<>();
		phData.forEach(ph -> {
			allname.add(ph.getName());
			allData.add(ph.getData());
		});
		
		MemberPhData datoaVo = new MemberPhData(allname,allData);
		
		List<MemberPhData> list = new ArrayList<>();
		list.add(datoaVo);
		
		return list;		
	}
	
	/**
	 * 月份订单不同状态统计
	 * @param year
	 * @return
	 */
	@GetMapping("/getOrderData")
	@ResponseBody
	public List<OrderTotalVo> getOrderData(@PathVariable(required=false)String year){
		
		//取得当前的系统时间年份
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
		year = year==null?sdf.format(new Date()):year;
		List<OrderTotalVo> list = new ArrayList<>();
		
		//获取每月订单数据-NOPAY
		List<Object[]> nopay = orderDao.getOrderData(year,cn.gson.CRM.model.entity.Order.State.XIA.toString());
		List<BigInteger> nopayList = new ArrayList<>();
		for(int i=1;i<=12;i++){
			nopayList.add(BigInteger.valueOf(0));
		}
		nopay.forEach( d->{
			int m = (int) d[0];
			nopayList.set(m-1, (BigInteger) d[1]);
		});
		
		//获取每月订单数据-XIA
		List<Object[]> xia = orderDao.getOrderData(year,cn.gson.CRM.model.entity.Order.State.PAY.toString());
		List<BigInteger> xiaList = new ArrayList<>();
		for(int i=1;i<=12;i++){
			xiaList.add(BigInteger.valueOf(0));
		}
		xia.forEach( d->{
			int m = (int) d[0];
			xiaList.set(m-1, (BigInteger) d[1]);
		});
		
		//获取每月订单数据-SEND
		List<Object[]> send = orderDao.getOrderData(year,cn.gson.CRM.model.entity.Order.State.SEND.toString());
		List<BigInteger> sendList = new ArrayList<>();
		for(int i=1;i<=12;i++){
			sendList.add(BigInteger.valueOf(0));
		}
		send.forEach( d->{
			int m = (int) d[0];
			sendList.set(m-1, (BigInteger) d[1]);
		});
		
		//获取每月订单数据-DONE
		List<Object[]> done = orderDao.getOrderData(year,cn.gson.CRM.model.entity.Order.State.DONE.toString());
		List<BigInteger> doneList = new ArrayList<>();
		for(int i=1;i<=12;i++){
			doneList.add(BigInteger.valueOf(0));
		}
		done.forEach( d->{
			int m = (int) d[0];
			doneList.set(m-1, (BigInteger) d[1]);
		});
		
		//获取每月订单数据-CANCEL
		List<Object[]> cancel = orderDao.getOrderData(year,cn.gson.CRM.model.entity.Order.State.CANCEL.toString());
		List<BigInteger> cancelList = new ArrayList<>();
		for(int i=1;i<=12;i++){
			cancelList.add(BigInteger.valueOf(0));
		}
		cancel.forEach( d->{
			int m = (int) d[0];
			cancelList.set(m-1, (BigInteger) d[1]);
		});
		
		OrderTotalVo nopayAll = new OrderTotalVo(year,"NOPAY",nopayList);
		OrderTotalVo xiaAll = new OrderTotalVo(year,"XIA",xiaList);
		OrderTotalVo sendAll = new OrderTotalVo(year,"SEND",sendList);
		OrderTotalVo doneAll = new OrderTotalVo(year,"DONE",doneList);
		OrderTotalVo cancelAll = new OrderTotalVo(year,"CANCEL",cancelList);
		
		list.add(nopayAll);
		list.add(xiaAll);
		list.add(sendAll);
		list.add(doneAll);
		list.add(cancelAll);
		return list;		
	}
	
	/**
	 * 月份客户不同状态统计
	 * @param year
	 * @return
	 */
	@GetMapping("/getCustomerData")
	@ResponseBody
	public List<OrderTotalVo> getCustomerData(@PathVariable(required=false)String year){
		
		//取得当前的系统时间年份
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
		year = year==null?sdf.format(new Date()):year;
		List<OrderTotalVo> list = new ArrayList<>();
		
		//获取每月客户数量-NO
		List<Object[]> no = customerDao.getCustomerData(year,LifeTime.NO.toString());
		List<BigInteger> nopayList = new ArrayList<>();
		for(int i=1;i<=12;i++){
			nopayList.add(BigInteger.valueOf(0));
		}
		no.forEach( d->{
			int m = (int) d[0];
			nopayList.set(m-1, (BigInteger) d[1]);
		});
		
		//获取每月客户数量-LATENT
		List<Object[]> latent = customerDao.getCustomerData(year,LifeTime.LATENT.toString());
		List<BigInteger> xiaList = new ArrayList<>();
		for(int i=1;i<=12;i++){
			xiaList.add(BigInteger.valueOf(0));
		}
		latent.forEach( d->{
			int m = (int) d[0];
			xiaList.set(m-1, (BigInteger) d[1]);
		});
		
		//获取每月客户数量-SIGN
		List<Object[]> sign = customerDao.getCustomerData(year,LifeTime.SIGN.toString());
		List<BigInteger> sendList = new ArrayList<>();
		for(int i=1;i<=12;i++){
			sendList.add(BigInteger.valueOf(0));
		}
		sign.forEach( d->{
			int m = (int) d[0];
			sendList.set(m-1, (BigInteger) d[1]);
		});
		
		//获取每月客户数量-REPEAT
		List<Object[]> repeat = customerDao.getCustomerData(year,LifeTime.REPEAT.toString());
		List<BigInteger> doneList = new ArrayList<>();
		for(int i=1;i<=12;i++){
			doneList.add(BigInteger.valueOf(0));
		}
		repeat.forEach( d->{
			int m = (int) d[0];
			doneList.set(m-1, (BigInteger) d[1]);
		});
		
		//获取每月客户数量-LOSE
		List<Object[]> lose = customerDao.getCustomerData(year,LifeTime.LOSE.toString());
		List<BigInteger> cancelList = new ArrayList<>();
		for(int i=1;i<=12;i++){
			cancelList.add(BigInteger.valueOf(0));
		}
		lose.forEach( d->{
			int m = (int) d[0];
			cancelList.set(m-1, (BigInteger) d[1]);
		});
		
		OrderTotalVo nopayAll = new OrderTotalVo(year,"NO",nopayList);
		OrderTotalVo xiaAll = new OrderTotalVo(year,"LATENT",xiaList);
		OrderTotalVo sendAll = new OrderTotalVo(year,"SIGN",sendList);
		OrderTotalVo doneAll = new OrderTotalVo(year,"REPEAT",doneList);
		OrderTotalVo cancelAll = new OrderTotalVo(year,"LOSE",cancelList);
		
		list.add(nopayAll);
		list.add(xiaAll);
		list.add(sendAll);
		list.add(doneAll);
		list.add(cancelAll);
		return list;		
	}

	/**
	 * 月份服务不同状态统计
	 * @param year
	 * @return
	 */
	@GetMapping("/getServiceData")
	@ResponseBody
	public List<OrderTotalVo> getServiceData(@PathVariable(required=false)String year){
		
		//取得当前的系统时间年份
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
		year = year==null?sdf.format(new Date()):year;
		List<OrderTotalVo> list = new ArrayList<>();
		
		//获取每月服务数量-ASK
		List<Object[]> ask = serviceDao.getServiceData(year,Type.ASK.toString());
		List<BigInteger> nopayList = new ArrayList<>();
		for(int i=1;i<=12;i++){
			nopayList.add(BigInteger.valueOf(0));
		}
		ask.forEach( d->{
			int m = (int) d[0];
			nopayList.set(m-1, (BigInteger) d[1]);
		});
		
		//获取每月服务数量-COMPLAIN
		List<Object[]> complain = serviceDao.getServiceData(year,Type.COMPLAIN.toString());
		List<BigInteger> xiaList = new ArrayList<>();
		for(int i=1;i<=12;i++){
			xiaList.add(BigInteger.valueOf(0));
		}
		complain.forEach( d->{
			int m = (int) d[0];
			xiaList.set(m-1, (BigInteger) d[1]);
		});
		
		//获取每月服务数量-ADVICE
		List<Object[]> advice = serviceDao.getServiceData(year,Type.ADVICE.toString());
		List<BigInteger> sendList = new ArrayList<>();
		for(int i=1;i<=12;i++){
			sendList.add(BigInteger.valueOf(0));
		}
		advice.forEach( d->{
			int m = (int) d[0];
			sendList.set(m-1, (BigInteger) d[1]);
		});
		
		OrderTotalVo nopayAll = new OrderTotalVo(year,"ASK",nopayList);
		OrderTotalVo xiaAll = new OrderTotalVo(year,"COMPLAIN",xiaList);
		OrderTotalVo sendAll = new OrderTotalVo(year,"ADVICE",sendList);
		
		list.add(nopayAll);
		list.add(sendAll);
		list.add(xiaAll);
		return list;		
	}
	
}


 