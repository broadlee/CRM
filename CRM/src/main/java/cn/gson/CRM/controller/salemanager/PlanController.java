package cn.gson.CRM.controller.salemanager;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;

import cn.gson.CRM.model.entity.Chance;
import cn.gson.CRM.model.entity.Plan;
import cn.gson.CRM.service.EntityJSON;
import cn.gson.CRM.service.SaleService.ChanceService;
import cn.gson.CRM.service.SaleService.PlanService;

@Controller
@RequestMapping("/plan")
public class PlanController
{
	@Autowired
	private ChanceService chanceService;
	
	@Autowired
	private PlanService planService;
	
	@GetMapping
	public String indexPlan() {
		return "salesmanagement/plan";
	}
	
	@GetMapping("/createplan")
	public String createPlan(Long planid, Model model) {
		
		if(planid!= null) {
			Plan plan = planService.findOne(planid);
			
			
			model.addAttribute("plan", plan);
		}
		
		return "salesmanagement/createplan";
	}
	
	@PostMapping("getchance")
	@ResponseBody
	public List<Map<String, Object>> getChance(Long chanceid)
	{
		/**
		 * 获取机会id 和客户信息
		 */
		List<Map<String, Object>> chance = chanceService.getChanceByPlan(chanceid);
		
		return chance;
	}
	@PostMapping("getOneChance")
	@ResponseBody
	public Chance getOneChance(Long chanceid) {
		Chance chance = chanceService.getOneChance(chanceid);
		
		return chance;
	}
	
	
	@PostMapping("createOpenPlan")
	@ResponseBody
	public EntityJSON<Plan> createOpenPlan(Plan plan){
		EntityJSON<Plan> result = new EntityJSON<Plan>();
		
		try
		{
			planService.createOpenPlan(plan);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			result.setErrors("创建失败");
		}
		
		
		
		return result;
	}
	
	@PostMapping("getPlan")
	@ResponseBody
	public EntityJSON<Plan> getPlan(Integer page, Integer rows, Plan search){
		EntityJSON<Plan> result = new EntityJSON<Plan>();
		
		PageInfo<Plan> pageInfo= planService.getPlan(page,rows,search);
		
		result.setTotal(pageInfo.getTotal());
		result.setRows(pageInfo.getList());
		
		return result;
	}
	
	@PostMapping("deleteplan")
	@ResponseBody
	public EntityJSON<Plan> deleteplan(Long[] planId){
		EntityJSON<Plan> result = new EntityJSON<Plan>();
		
		
		try
		{
			planService.deletePlan(planId);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			result.setErrors("删除失败");
		}
		
		
		
		return result;
	}
	
	@PostMapping("finishplan")
	@ResponseBody
	public EntityJSON<Plan> finishPlan(Long planId,String state){
		EntityJSON<Plan> result = new EntityJSON<Plan>();
		
		
		try
		{
			planService.finishPlan(planId,state);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			result.setErrors("删除失败");
		}
		
		
		
		return result;
	}
	
	
	
}
