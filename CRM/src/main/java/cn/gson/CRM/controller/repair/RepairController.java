package cn.gson.CRM.controller.repair;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.gson.CRM.model.dao.repair.RepairDao;
import cn.gson.CRM.model.entity.Repair;
import cn.gson.CRM.model.entity.Repair.Process;
import cn.gson.CRM.model.mapper.RepairMapper;
import cn.gson.CRM.utils.ShareCodeUtil;

@RequestMapping("/repair")
@Controller
public class RepairController {

	@Autowired
	RepairDao repairDao;
	
	@Autowired
	RepairMapper repairMapper;
	
	@RequestMapping
	public String index(){
		return "repair/repair";
	}
	
	@RequestMapping({ "form", "form/{id}" })
	public String form(@PathVariable(required = false)Long id,Model model){
		if (id != null) {
			model.addAttribute("repair", repairDao.findOne(id));
		}
		return "repair/form";
	}
	
	@RequestMapping("list")
	@ResponseBody
	public Map<String, Object> list(Repair repair,@RequestParam(defaultValue = "1")int page,int rows){
		Map<String, Object> map = new HashMap<>();
		PageHelper.startPage(page, rows);
		List<Repair> repairs = repairMapper.findAll(repair);
		PageInfo<Repair> pageInfo = new PageInfo<Repair>(repairs);
		map.put("rows", repairs);
		map.put("total", pageInfo.getTotal());
 		return map;
	}
	
	@PostMapping("save")
	@ResponseBody
	@Transactional
	public JSONObject save(@Valid Repair repair,BindingResult br){
		JSONObject result = new JSONObject();
		if(br.hasErrors()){
			result.put("err", "保存失败！");
		}else{
			if(repair.getRepairId() == null){
				repair.setRepairId(ShareCodeUtil.getCharAndNumr());
			}
			if(repair.getBackResult() != null && repair.getBackResult() != ""){
				if(repair.getMoney() != null){
					repair.setProcess(Process.PEPAIRING);
				} else {
					repair.setProcess(Process.UNCONFIRM);
				}
			}
			repairDao.save(repair);
			result.put("success", true);
		}
		return result;
	}
	
	@GetMapping("delete/{id}")
	@ResponseBody
	public String delete(@PathVariable Long id) {
		repairDao.delete(id);
		return "true";
	}
	
	@RequestMapping("repairDetails/{id}")
	public String repairDetails(@PathVariable Long id,Model model){
		if (id != null) {
			model.addAttribute("repair", repairDao.findOne(id));
		}
		return "repair/repairDetails";
	}
}
