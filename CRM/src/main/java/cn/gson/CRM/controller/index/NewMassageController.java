package cn.gson.CRM.controller.index;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;

import cn.gson.CRM.model.entity.NewMessage;
import cn.gson.CRM.model.mapper.MessageMapper;

@Controller
@RequestMapping("/message")
public class NewMassageController {
	
	@Autowired
	private MessageMapper mm;
	
	@RequestMapping("list")
	@ResponseBody
	public JSONObject list(Model model){
		JSONObject result = new JSONObject();
		
		List<NewMessage> messages = mm.getMessage();
		
		result.put("message", messages);
		
		return result;
	} 
	/*@RequestMapping("list")
	public String list(Model model){
		List<NewMessage> messages = mm.getMessage();
		System.out.println(messages);
		model.addAttribute("message",messages);
		return "indexInside/message";
	}
*/
}
