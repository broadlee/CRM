package cn.gson.CRM.controller.fund;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.gson.CRM.model.dao.fund.ReceivePlanDao;
import cn.gson.CRM.model.dao.fund.ReceiveRecordDao;
import cn.gson.CRM.model.dao.order.OrderDao;
import cn.gson.CRM.model.dao.repair.RepairDao;
import cn.gson.CRM.model.dao.stock.InOutDao;
import cn.gson.CRM.model.entity.Inout;
import cn.gson.CRM.model.entity.Inout.Type;
import cn.gson.CRM.model.entity.Order;
import cn.gson.CRM.model.entity.Order.State;
import cn.gson.CRM.model.entity.ReceiveRecord;
import cn.gson.CRM.model.entity.Repair;
import cn.gson.CRM.model.entity.Repair.Process;
import cn.gson.CRM.model.mapper.ReceiveRecordMapper;
import cn.gson.CRM.utils.ShareCodeUtil;

@Controller
@RequestMapping("fund/receiveRecord")
public class ReceiveRecordController {

	@Autowired
	ReceiveRecordMapper receiveRecordMapper;
	
	@Autowired
	ReceiveRecordDao receiveRecordDao;
	
	@Autowired
	OrderDao orderDao;
	
	@Autowired
	RepairDao repairDao;
	
	@Autowired
	ReceivePlanDao recievePlanDao;
	
	@Autowired
	InOutDao inoutDao;
	
	@GetMapping
	public String index(){
		return "fund/receiveRecord";
	}
	
	@RequestMapping("list")
	@ResponseBody
	public Map<String, Object> list(ReceiveRecord receiveRecord,@RequestParam(defaultValue = "1")int page,int rows){
		if(receiveRecord.getOrders() != null){
			if(receiveRecord.getOrders().getOrderId() == ""){
				receiveRecord.setOrders(null);
			}
		}
		
		if(receiveRecord.getRepair() != null){
			if(receiveRecord.getRepair().getRepairId() == ""){
				receiveRecord.setRepair(null);
			}
		}
		Map<String, Object> map = new HashMap<String, Object>();
		PageHelper.startPage(page, rows);
		List<ReceiveRecord> receiveRecords = receiveRecordMapper.findAll(receiveRecord);
		PageInfo<ReceiveRecord> pageInfo = new PageInfo<ReceiveRecord>(receiveRecords);
		map.put("rows", receiveRecords);
		map.put("total", pageInfo.getTotal());
		return map;
	}
	
	@PostMapping("save")
	@ResponseBody
	@Transactional
	public JSONObject save(@Valid ReceiveRecord receiveRecord,Long oId,Long rId,Long planId,BindingResult br){
		System.out.println(receiveRecord);
		Order order = null;
		Repair repair = null;
		if(oId != null){
			order = orderDao.findOne(oId);
			if(order != null){
				receiveRecord.setOrders(order);
			}
		} 
		
		if(rId != null){
			repair = repairDao.findOne(rId);
			if(repair != null){
				receiveRecord.setRepair(repair);
			}
		}
		
		JSONObject result = new JSONObject();
		if(br.hasErrors()){
			result.put("err", "保存失败！");
		}else{
			ReceiveRecord saved = receiveRecordDao.save(receiveRecord);
			if(saved != null){
				switch (saved.getType().toString()) {
					case "SUBMONEY":
						//判断如果回款计划的类型为订金，则修改订单状态为已支付
						order.setState(State.valueOf("PAY"));
						orderDao.save(order);
						
						//生成出库单
						Inout inout = new Inout();
						inout.setOrders(order);
						inout.setState(cn.gson.CRM.model.entity.Inout.State.valueOf("NODEAL"));
						inout.setType(Type.valueOf("GO"));
						inout.setInoutId(ShareCodeUtil.getCharAndNumr());
						inoutDao.save(inout);
						break;
					case "PATMENT":
						//判断如果回款计划的类型为订单剩余回款，则修改订单状态为已完成
						order.setState(State.valueOf("DONE"));
						orderDao.save(order);
						break;
					case "REPAIR":
						//判断如果回款计划的类型为维修，则修改维修单状态为已交付
						repair.setProcess(Process.valueOf("HANDLE"));
						repairDao.save(repair);
						break;
					default:
						break;
				}
			}
			result.put("success", true);
			if(result.getBooleanValue("success")){
				recievePlanDao.delete(planId);
			};
		}

		return result;
	}
	
	
}
