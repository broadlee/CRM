package cn.gson.CRM.controller.stock;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.gson.CRM.model.dao.order.OrderDao;
import cn.gson.CRM.model.dao.order.OrderDetailsDao;
import cn.gson.CRM.model.dao.product.ProductDao;
import cn.gson.CRM.model.dao.product.PurchaseDao;
import cn.gson.CRM.model.dao.stock.InOutDao;
import cn.gson.CRM.model.dao.stock.InOutRecordDao;
import cn.gson.CRM.model.entity.Inout;
import cn.gson.CRM.model.entity.Inout.State;
import cn.gson.CRM.model.entity.InoutRecord;
import cn.gson.CRM.model.entity.InoutRecord.Flag;
import cn.gson.CRM.model.entity.Order;
import cn.gson.CRM.model.entity.OrderDetail;
import cn.gson.CRM.model.entity.Product;

@Controller
@RequestMapping("stock/inoutApply")
public class InoutApplyController {

	@Autowired
	InOutDao inOutDao;

	@Autowired
	OrderDao orderDao;

	@Autowired
	OrderDetailsDao orderDetailsDao;
	
	@Autowired
	ProductDao productDao;
	
	@Autowired
	PurchaseDao purchaseDao;
	
	@Autowired
	InOutRecordDao inoutRecordDao;

	@GetMapping
	public String index() {
		return "stock/inoutApply";
	}

	/**
	 * 查询所有出入库订单及按条件精确查询
	 * 
	 * @param value
	 * @param page
	 * @param rows
	 * @return
	 */
	@PostMapping("list")
	@ResponseBody
	public Map<String, Object> init(Inout inout,@RequestParam(required=false)Long rowid,
			@RequestParam(required=false)String rowtype,
			@RequestParam(defaultValue = "1") int page,
			@RequestParam(defaultValue = "10") int rows
	){
		PageRequest pr = new PageRequest(page - 1, rows, new Sort(Direction.DESC, "id"));
		Page<Inout> inOut = null;
		List<?> list = new ArrayList<>();
		
		/**
		 * 根据出入库单id修改出入库状态
		 */
		if(rowid!=null){
			Inout io = inOutDao.findOne(rowid);
			if(io.getState().equals(State.NODEAL)){
				if(rowtype.equals("pass")){
					//添加出/入库记录
					InoutRecord inoutRecord = new InoutRecord();
					inoutRecord.setInout(io);
					inoutRecord.setDate(new Date());
					if(io.getOrders()!=null){
						inoutRecord.setFlag(Flag.OUT);
					}else if(io.getPurchase()!=null){
						inoutRecord.setFlag(Flag.IN);
					}
					//inoutRecord.setMember(member);
					inoutRecordDao.save(inoutRecord);
					
					//修改出入库单的状态
					io.setState(State.DEAL);
					inOutDao.save(io);
					
					//查询订单详情中订购的数量
					Order order = orderDao.findByOrderId(io.getOrders().getOrderId());
					OrderDetail detail = orderDetailsDao.findByOrders(order);
					
					//修改产品库存
					Product product = productDao.findOne(detail.getProduct().getId());
					if(io.getOrders()!=null){
						product.setStock(product.getStock()-detail.getCount());
					}else if(io.getPurchase()!=null){
						product.setStock(product.getStock()+detail.getCount());
					}
					productDao.save(product);
				
				}else{
					//修改出入库单的状态
					io.setState(State.REJECT);
					inOutDao.save(io);
				}
			}
		}
		/**
		 * 按条件查询 根据订单详情id查询 根据采购单id查询 根据状态查询
		 */
		if(inout.getOrders()!=null && inout.getOrders().getOrderId()!=""){
			if (inout.getState()==null) {
				inOut = inOutDao.findByOrderId(inout.getOrders().getOrderId(), pr);
			}else{
				inOut = inOutDao.findByOrdersOrderIdAndState(inout.getOrders().getOrderId(),inout.getState(), pr);
			}
		}else if(inout.getPurchase()!=null && inout.getPurchase().getChaseId()!=""){
			if (inout.getState()==null) {
				inOut = inOutDao.findByPurchaseId(inout.getPurchase().getChaseId(), pr);
			}else{
				inOut = inOutDao.findByPurchaseChaseIdAndState(inout.getPurchase().getChaseId(),inout.getState(), pr);
			}
		}else if(inout.getState()!=null){
			inOut = inOutDao.findByState(pr,inout.getState());
		}else {
			inOut = inOutDao.findAll(pr);
		}

		Map<String, Object> data = new HashMap<>();
		data.put("rows", inOut == null ? list : inOut.getContent());
		data.put("total", inOut == null ? 0 : inOut.getTotalElements());
		return data;
	}
}
