package cn.gson.CRM.converter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.core.convert.converter.Converter;

public class DateConverter 
	implements Converter<String, Date> {

	@Override
	public Date convert(String source) {
		System.out.println("正在转换中……");
		//ͨ通过正则表达式判断是否是正确的日期格式 \d{4}-\d{1,2}-\d{1,2}
		Pattern p = Pattern.compile("\\d{4}-\\d{1,2}-\\d{1,2} \\d{1,2}:\\d{1,2}:\\d{1,2}");
		Matcher m = p.matcher(source);
		if(m.matches()){
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			try {
				Date date = df.parse(source);
				return date;
			} catch (ParseException e) {
				throw new IllegalArgumentException("转换有错");
			}
		}else{
			throw new IllegalArgumentException("日期格式不正确");
		}
	}

}
