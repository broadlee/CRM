package cn.gson.CRM.service;



public class Result
{
	private boolean success;
	
	private String msg;
	
	private String file_path;
	

	public Result()
	{
		super();
		// TODO Auto-generated constructor stub
	}

	public Result(boolean success, String msg, String file_path)
	{
		super();
		this.success = success;
		this.msg = msg;
		this.file_path = file_path;
	}



	public boolean isSuccess()
	{
		return success;
	}

	public void setSuccess(boolean success)
	{
		this.success = success;
	}

	public String getMsg()
	{
		return msg;
	}

	public void setMsg(String msg)
	{
		this.msg = msg;
	}

	public String getFile_path()
	{
		return file_path;
	}

	public void setFile_path(String file_path)
	{
		this.file_path = file_path;
	}


}
