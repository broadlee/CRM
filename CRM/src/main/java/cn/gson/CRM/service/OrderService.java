package cn.gson.CRM.service;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gson.CRM.model.dao.order.OrderDao;
import cn.gson.CRM.model.entity.Order;
import cn.gson.CRM.model.entity.Order.State;

/**
* @author 邓安民:
* @version 创建时间：2018年4月11日 下午6:51:55
* 订单服务类
*/

@Service
@Transactional
public class OrderService {
	@Autowired
	public OrderDao orderDao;
	public Map<String, Object> finAllOrders(Pageable page){
		Page<Order> orders= orderDao.selectByPage(page);
	
		Map< String, Object> map=new HashMap<>();
		map.put("total", orders.getTotalElements());
		map.put("rows", orders.getContent());
		return map;
	}
	public Map< String, Object> finAllOrdersByWhere(State state,String theme,Pageable page){
		Page<Order> orders= orderDao.selectByPageWhere(state,theme,page);
		
		Map< String, Object> map=new HashMap<>();
		map.put("total", orders.getTotalElements());
		map.put("rows", orders.getContent());
		return map;
	}
	public Map< String, Object> finAllOrdersByTheme(String theme,Pageable page){
		Page<Order> orders= orderDao.selectByPageTheme(theme,page);
		
		Map< String, Object> map=new HashMap<>();
		map.put("total", orders.getTotalElements());
		map.put("rows", orders.getContent());
		return map;
	}
	public Map< String, Object> finAllOrdersByState(State state,Pageable page){
		Page<Order> orders= orderDao.selectByPageState(state,page);
		
		Map< String, Object> map=new HashMap<>();
		map.put("total", orders.getTotalElements());
		map.put("rows", orders.getContent());
		return map;
	}



}


 