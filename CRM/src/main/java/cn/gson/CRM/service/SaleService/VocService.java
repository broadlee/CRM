package cn.gson.CRM.service.SaleService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gson.CRM.model.dao.voc.VocDao;
import cn.gson.CRM.model.entity.Voc;
import cn.gson.CRM.model.mapper.SaleManagerMapper;

@Service
public class VocService
{
	@Autowired
	private VocDao vocdao;

	@Autowired
	private SaleManagerMapper samM;

	@Transactional
	public void updateVocChance(Long chance, Long[] vocId)
	{

		for (Long long1 : vocId)
		{
			vocdao.updateVocChance(chance, long1);
		}

	}

	public Long deleteByChance(Long[] chances)
	{

		Long sum = 0l;
		for (Long long1 : chances)
		{
			sum += vocdao.deleteByChance(long1);
		}
		return sum;
	}

	public List<Voc> getVoc(Long customer)
	{
		List<Voc> vocs = samM.getVoc(customer);

		return vocs;
	}

	public List<Voc> getVocByChance(Long chance)
	{
		List<Voc> vocs = samM.getVocByChance(chance);

		return vocs;
	}

}
