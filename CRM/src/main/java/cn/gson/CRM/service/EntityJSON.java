package cn.gson.CRM.service;

import java.util.List;

public class EntityJSON<T>
{
	private Long total;
	
	private List<T> rows;
	
	private String errors;
	
	private String inform;
	
	private Object retur;

	public Object getRetur()
	{
		return retur;
	}

	public void setRetur(Object retur)
	{
		this.retur = retur;
	}

	public String getInform()
	{
		return inform;
	}

	public void setInform(String inform)
	{
		this.inform = inform;
	}

	public String getErrors()
	{
		return errors;
	}

	public void setErrors(String errors)
	{
		this.errors = errors;
	}

	public Long getTotal()
	{
		return total;
	}

	public void setTotal(Long total)
	{
		this.total = total;
	}

	public List<T> getRows()
	{
		return rows;
	}

	public void setRows(List<T> rows)
	{
		this.rows = rows;
	}

	@Override
	public String toString()
	{
		return "EntityJSON [total=" + total + ", rows=" + rows + "]";
	}

	public EntityJSON(Long total, List<T> rows)
	{
		super();
		this.total = total;
		this.rows = rows;
	}

	public EntityJSON()
	{
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
