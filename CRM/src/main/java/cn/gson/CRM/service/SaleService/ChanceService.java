package cn.gson.CRM.service.SaleService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cn.gson.CRM.model.dao.salemanage.ChanceDao;
import cn.gson.CRM.model.entity.Chance;
import cn.gson.CRM.model.entity.Plan;
import cn.gson.CRM.model.mapper.SaleManagerMapper;

@Service
public class ChanceService
{

	@Autowired
	private SaleManagerMapper samM;

	@Autowired
	private ChanceDao chancedao;

	@Autowired
	private VocService vocService;
	
	@Autowired
	private PlanService planser;

	@Transactional
	public Chance save(Chance chance)
	{

		return chancedao.save(chance);

	}

	public int updateChanceQuote(Long chanceId)
	{
		return chancedao.updateChanceQuote(chanceId);

	}

	/**
	 * 获取所有的Chance
	 * 
	 * @param search
	 * @return
	 * @author pshc
	 */
	public List<Chance> getChance(Chance search)
	{

		List<Chance> chance = samM.getChance(search);

		return chance;
	}

	/**
	 * 获取分页的Chance
	 * 
	 * @param page
	 * @param rows
	 * @param search
	 * @return
	 * @author pshc
	 */
	public PageInfo<Chance> getChance(Integer page, Integer rows, Chance search)
	{
		PageHelper.startPage(page, rows);

		List<Chance> chance = getChance(search);

		PageInfo<Chance> pageInfo = new PageInfo<Chance>(chance);

		return pageInfo;

	}

	/**
	 * 删除机会，同时删除机会对应的需求
	 * 
	 * @param chances
	 * @author pshc
	 */
	@Transactional
	public void deleteChance(Long[] chances)
	{
		vocService.deleteByChance(chances);

		for (Long chance : chances)
		{
			this.chancedao.delete(chance);
		}
	}

	public List<Map<String, Object>> getChance()
	{
		ArrayList<Map<String, Object>> result = new ArrayList<Map<String, Object>>();

		List<Chance> chances = this.getChance(new Chance());

		for (Chance chance : chances)
		{
			
			if (!chance.isQuoted()&&chance.getState()!=Chance.State.FAILED)
			{
				Map<String, Object> map = new HashMap<String, Object>();

				map.put("id", chance.getId());
				map.put("text", chance.getCustomer().getName() + "(" + chance.getCustomer().getLinkWay() + ")");

				result.add(map);
			}

		}

		return result;
	}

	public void setQuotedTo0(Long chance_id)
	{
		chancedao.setQuotedTo0(chance_id);
	}

	public List<Map<String, Object>> getChanceByPlan(Long chanceid)
	{
		ArrayList<Map<String, Object>> result = new ArrayList<Map<String, Object>>();

		List<Chance> chances = this.getChance(new Chance());

		for (Chance chance : chances)
		{
			if (((!chance.isPlan())||chance.getId().equals(chanceid))&&chance.getState()!=Chance.State.FAILED)
			{
				Map<String, Object> map = new HashMap<String, Object>();

				map.put("id", chance.getId());
				map.put("text", chance.getCustomer().getName() + "(" + chance.getCustomer().getLinkWay() + ")");

				result.add(map);
			}

		}

		return result;
	}

	public Chance getOneChance(Long chanceid)
	{
		Chance chance = samM.getOneChance(chanceid);
		return chance;
	}
	
	public void updateChancePlan(Long id)
	{
		chancedao.updateChancePlan(id);
		
	}

	public void endchance(Long chanceId)
	{
		this.samM.endchance(chanceId);
		
	}

	public void successChance(Long chanceId)
	{
		this.samM.successChance(chanceId);
		
	}
	@Transactional
	public void setQouteAndPlanState(Long chanceId)
	{
		this.successChance(chanceId);
		
		planser.finishPlanByChance(chanceId, Plan.State.END);
		
	}

}
