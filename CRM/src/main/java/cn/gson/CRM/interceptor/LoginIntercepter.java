package cn.gson.CRM.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
public class LoginIntercepter extends HandlerInterceptorAdapter {
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		//System.out.println("正在检查是否登陆……");
		// 判断 session 中的用户信息是否存在
		if (request.getSession().getAttribute("member") == null) {
			// 没有登陆
			request.getSession().setAttribute("err", "你还没登录,请先登录！");
			response.sendRedirect(request.getContextPath()+"/login");
			return false;
		} 
		return true;

	}
}
