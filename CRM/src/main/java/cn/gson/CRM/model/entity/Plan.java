package cn.gson.CRM.model.entity;

import java.util.Date;

import javax.persistence.*;

/**
 * 开发计划实体类
 * @author Administrator
 *
 */
@Entity
@Table(name = "plan_")
public class Plan {

	/**
	 * 主键自增id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * 对应报价单
	 */
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "q_id")
	private Quotation quotation;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "chance_id")
	private Chance chance;
	
	/**
	 * 计划时间
	 */
	@Column(nullable = false)
	private Date planDate;
	
	/**
	 * 计划内容
	 */
	@Column(nullable = false)
	private String content;
	
	public Chance getChance()
	{
		return chance;
	}

	public void setChance(Chance chance)
	{
		this.chance = chance;
	}

	/**
	 * 开发状态
	 */
	@Enumerated(EnumType.STRING)
	private State state;
	
	public enum State {
		/**
		 * 执行中
		 */
		ING,
		/**
		 * 已结束
		 */
		END
	}

	public Plan() {}

	public Plan(Long id, Quotation quotation, Date planDate, String content, State state) {
		super();
		this.id = id;
		this.quotation = quotation;
		this.planDate = planDate;
		this.content = content;
		this.state = state;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Quotation getQuotation() {
		return quotation;
	}

	public void setQuotation(Quotation quotation) {
		this.quotation = quotation;
	}



	public Date getPlanDate()
	{
		return planDate;
	}

	public void setPlanDate(Date planDate) {
		this.planDate = planDate;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "Plan [id=" + id + ", planDate=" + planDate + ", content=" + content + ", state=" + state + "]";
	}

	public Plan(Long id)
	{
		super();
		this.id = id;
	}
	
}
