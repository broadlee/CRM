package cn.gson.CRM.model.entity;

import java.util.Date;

import javax.persistence.*;

/**
 * 客户实体类
 * @author Administrator
 *
 */
@Entity
@Table(name="customer_")
public class Customer {

	/**
	 * 主键自增id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * 客户姓名
	 */
	@Column(nullable = false)
	private String name;
	
	/**
	 * 联系人姓名
	 */
	@Column(nullable = false)
	private String linkMan;
	
	/**
	 * 联系人联系方式
	 */
	@Column(nullable = false)
	private String linkWay;
	
	/**
	 * 生命周期
	 */
	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private LifeTime lifeTime;
	
	public enum LifeTime {
		/**
		 * 暂无需求
		 */
		NO,
		/**
		 * 潜在客户 
		 */
		LATENT,
		/**
		 * 签约客户
		 */
		SIGN,
		/**
		 * 重复购买
		 */
		REPEAT,
		/**
		 * 失效
		 */
		LOSE
	}
	
	/**
	 * 创建日期
	 */
	private Date createDate;
	
	/**
	 * 地址
	 */
	private String location;
	
	 /**
	  * 电子邮箱
	  */
	private String email;
	
	/**
	 * 所属用户
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="m_id",  nullable = true) 
	private Member member;

	public Customer() {}

	public Customer(Long id, String name, String linkMan, String linkWay, LifeTime lifeTime, Date createDate,
			String location, String email, Member member) {
		super();
		this.id = id;
		this.name = name;
		this.linkMan = linkMan;
		this.linkWay = linkWay;
		this.lifeTime = lifeTime;
		this.createDate = createDate;
		this.location = location;
		this.email = email;
		this.member = member;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLinkMan() {
		return linkMan;
	}

	public void setLinkMan(String linkMan) {
		this.linkMan = linkMan;
	}

	public String getLinkWay() {
		return linkWay;
	}

	public void setLinkWay(String linkWay) {
		this.linkWay = linkWay;
	}

	public LifeTime getLifeTime() {
		return lifeTime;
	}

	public void setLifeTime(LifeTime lifeTime) {
		this.lifeTime = lifeTime;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", name=" + name + ", linkMan=" + linkMan + ", linkWay=" + linkWay + ", lifeTime="
				+ lifeTime + ", createDate=" + createDate + ", location=" + location + ", email=" + email + "]";
	}
}