package cn.gson.CRM.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
* @author 邓安民:
* @version 创建时间：2018年4月17日 上午11:25:23
* 快递公司实体类
*/
@Entity
@Table(name="express_company")
public class ExpressCompany {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	/**
	 * 公司名称
	 */
	@Column(nullable=false,length=50)
	private String name;
	
	/**
	 * 标识
	 */
	@Column(name="identify_",nullable=false)
	private String identify;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdentify() {
		return identify;
	}

	public void setIdentify(String identify) {
		this.identify = identify;
	}

	public ExpressCompany() {
		// TODO Auto-generated constructor stub
	}

	public ExpressCompany(Long id,String name, String identify) {
		super();
		this.id=id;
		this.name = name;
		this.identify = identify;
	}

	@Override
	public String toString() {
		return "ExpressCompany [id=" + id + ", name=" + name + ", identify=" + identify + "]";
	}
	
	
}


 