package cn.gson.CRM.model.entity;

import java.util.Date;

import javax.persistence.*;

/**
 * 维修单实体类
 * @author Administrator
 *
 */
@Entity
@Table(name = "repair_")
public class Repair {

	/**
	 * 主键自增id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * 维修单ID
	 */
	private String repairId;
	
	/**
	 * 客户
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "c_id")
	private Customer customer;
	
	/**
	 * 维修人
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "m_id")
	private Member member;
	
	/**
	 * 对应产品
	 */
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "p_id")
	private Product product;
	
	/**
	 * 主题
	 */
	@Column(nullable = false)
	private String theme;
	
	/**
	 * 接单时间
	 */
	private Date date = new Date();
	
	/**
	 * 费用
	 */
	private Double money;
	
	/**
	 * 维修进度
	 */
	@Enumerated(EnumType.STRING)
	private Process process;
	
	public enum Process {
		/**
		 * 待检测
		 */
		UNCHECK,
		/**
		 * 待客户确认
		 */
		UNCONFIRM,
		/**
		 * 维修中
		 */
		PEPAIRING,
		/**
		 * 已交付
		 */
		HANDLE
	}
	
	/**
	 * 检测结果详情
	 */
	private String backResult;

	public Repair() {}

	public Repair(String repairId,Long id, Customer customer, Member member, String theme, Date date, Double money, Process process) {
		super();
		this.repairId = repairId;
		this.id = id;
		this.customer = customer;
		this.member = member;
		this.theme = theme;
		this.date = date;
		this.money = money;
		this.process = process;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Double getMoney() {
		return money;
	}

	public void setMoney(Double money) {
		this.money = money;
	}

	public Process getProcess() {
		return process;
	}

	public void setProcess(Process process) {
		this.process = process;
	}

	public String getRepairId() {
		return repairId;
	}

	public void setRepairId(String repairId) {
		this.repairId = repairId;
	}
	
	public void setProduct(Product product) {
		this.product = product;
	}
	
	public Product getProduct() {
		return product;
	}
	
	public void setBackResult(String backResult) {
		this.backResult = backResult;
	}
	
	public String getBackResult() {
		return backResult;
	}
	
	@Override
	public String toString() {
		return "Repair [id=" + id + ", theme=" + theme + ", date=" + date + ", money=" + money + ", process=" + process
				+ "]";
	}
	
}
