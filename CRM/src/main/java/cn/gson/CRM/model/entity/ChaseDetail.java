package cn.gson.CRM.model.entity;

import javax.persistence.*;

/**
 * 采购单详情实体类
 * @author Administrator
 *
 */
/**
 * 关联的采购单
 */
@Entity
@Table(name = "chaseDetail_")
public class ChaseDetail {

	/**
	 * 主键自增id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pu_id")
	private Purchase purchase;

	/**
	 * 供应商
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "s_id", nullable = true)
	private Supplier supplier;

	/**
	 * 产品名称
	 */
	@Column(nullable = false)
	private String name;

	/**
	 * 采购价
	 */
	@Column(nullable = false)
	private Double costPrice;


	/**
	 * 数量
	 */
	private Integer count;

	/**
	 * 产品图片
	 */
	private String image;

	/**
	 * 产品状态
	 */
	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private State state;

	private Double money;

	public enum State {
		/**
		 * 未入库
		 */
		NOSTORAGE,
		/**
		 * 已入库
		 */
		STORAGED
	}

	public ChaseDetail() {
	}
	
	public ChaseDetail(Long id, Purchase purchase, Supplier supplier, String name, Double costPrice, Integer count,
			String image, State state, Double money) {
		super();
		this.id = id;
		this.purchase = purchase;
		this.supplier = supplier;
		this.name = name;
		this.costPrice = costPrice;
		this.count = count;
		this.image = image;
		this.state = state;
		this.money = money;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Purchase getPurchase() {
		return purchase;
	}

	public void setPurchase(Purchase purchase) {
		this.purchase = purchase;
	}

	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(Double costPrice) {
		this.costPrice = costPrice;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public Double getMoney() {
		return money;
	}
	//不能设置金额，只能通过数量和价格
	public void setMoney(Double money) {
		if (this.costPrice != null && this.count != null){
			this.money = this.costPrice * this.count;
		}else
			this.money = money;
	}

	@Override
	public String toString() {
		return "ChaseDetail [id=" + id + ", name=" + name + ", costPrice=" + costPrice + ", count=" + count + ", image="
				+ image + ", state=" + state + ", money=" + money + "]";
	}
}