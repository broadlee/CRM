package cn.gson.CRM.model.dao.voc;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import cn.gson.CRM.model.entity.Voc;

public interface VocDao extends PagingAndSortingRepository<Voc, Long> {
	
	
	/**
	 * 更新销售机会
	 * @param chance
	 * @param vocId
	 * @return
	 * @author pshc
	 */
	@Modifying 
	@Query("update Voc vo set vo.chance.id=?1,vo.deals=1 where vo.id=?2")
	int updateVocChance(Long chance,Long vocId);
	
	/**
	 * 
	 * @param chance
	 * @return
	 * @author pshc
	 */
	@Modifying
	@Query("delete from Voc where chance.id=?1")
	int deleteByChance(Long chance);

}
