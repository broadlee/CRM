package cn.gson.CRM.model.mapper;

import java.util.List;

import cn.gson.CRM.model.entity.PayRecord;

public interface PayRecordMapper {

	List<PayRecord> findAll(PayRecord payRecord);
}
