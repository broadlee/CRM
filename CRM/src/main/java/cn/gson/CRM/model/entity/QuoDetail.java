package cn.gson.CRM.model.entity;

import javax.persistence.*;

/**
 * 报价单详情表
 * @author Administrator
 *
 */
@Entity
@Table(name = "quoDetail_")
public class QuoDetail {

	/**
	 * 主键自增id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * 对应报价单
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "qu_id", nullable = false)
	private Quotation quotation;
	
	/**
	 * 产品
	 */
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "p_id")
	private Product product;
	
	/**
	 * 折扣
	 */
	@Column(nullable = false)
	private Double cost;
	
	/**
	 * 数量
	 */
	@Column(nullable = false)
	private Integer count;

	public QuoDetail() {}

	public QuoDetail(Long id, Quotation quotation, Product product, Double cost, Integer count) {
		super();
		this.id = id;
		this.quotation = quotation;
		this.cost = cost;
		this.count = count;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Quotation getQuotation() {
		return quotation;
	}

	public void setQuotation(Quotation quotation) {
		this.quotation = quotation;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "QuoDetail [id=" + id + ", cost=" + cost + ", count=" + count + "]";
	}
	
}
