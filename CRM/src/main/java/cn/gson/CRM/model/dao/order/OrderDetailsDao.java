package cn.gson.CRM.model.dao.order;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import cn.gson.CRM.model.entity.Order;
import cn.gson.CRM.model.entity.OrderDetail;
import cn.gson.CRM.model.entity.Product;
import cn.gson.CRM.model.vo.OrderDetailVo;

/**
* @author 邓安民:
* @version 创建时间：2018年4月12日 下午9:41:07
* 类说明
*/
public interface OrderDetailsDao extends JpaRepository<OrderDetail,Long>{
	/**
	 * 查询对应的订单
	 * @return
	 */
	@Query("select new cn.gson.CRM.model.vo.OrderDetailVo(od.id,od.orders,od.product,"
			+ "od.cost,od.count,od.subtotal,od.state) from OrderDetail od where od.orders.id=?1")
	public List<OrderDetailVo> selectOrderAndDetails(Long id);

	public OrderDetail findByOrders(Order order);

	public List<OrderDetail> findByProduct(Product product);
	
	public List<OrderDetail> findByOrdersId(Long id);
	/**
	 * 统计某一订单的总金额
	 */
	@Query("select sum(od.subtotal) from OrderDetail od where od.orders.id=?1")
	public BigDecimal getTotalByOrder(Long o_id);
	
	/**
	 * 判断是否重复
	 */
	@Query("select od from OrderDetail od where od.orders.id=?1 and od.product.id=?2")
	public List<OrderDetail> ifSameProduct(Long o_id,Long p_id);
	

}


 