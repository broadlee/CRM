package cn.gson.CRM.model.entity;

import javax.persistence.*;

/**
 * 供应商实体类
 * @author Administrator
 *
 */
@Entity
@Table(name = "supplier_")
public class Supplier {

	/**
	 * 主键自增id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * 供应商姓名
	 */
	@Column(nullable = false)
	private String name;
	
	/**
	 * 联系人姓名
	 */
	@Column(nullable = false)
	private String linkMan;
		
	/**
	 * 联系人联系方式
	 */
	@Column(nullable = false)
	private String linkWay;
	
	/**
	 * 地址
	 */
	private String location;
	
	/**
	 * 描述
	 */
	private String describes;

	public Supplier() {}

	public Supplier(Long id, String name, String linkMan, String linkWay, String location, String describes) {
		super();
		this.id = id;
		this.name = name;
		this.linkMan = linkMan;
		this.linkWay = linkWay;
		this.location = location;
		this.describes = describes;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLinkMan() {
		return linkMan;
	}

	public void setLinkMan(String linkMan) {
		this.linkMan = linkMan;
	}

	public String getLinkWay() {
		return linkWay;
	}

	public void setLinkWay(String linkWay) {
		this.linkWay = linkWay;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDescribes() {
		return describes;
	}

	public void setDescribes(String describes) {
		this.describes = describes;
	}

	@Override
	public String toString() {
		return "Supplier [id=" + id + ", name=" + name + ", linkMan=" + linkMan + ", linkWay=" + linkWay + ", location="
				+ location + ", describes=" + describes + "]";
	}
	
}
