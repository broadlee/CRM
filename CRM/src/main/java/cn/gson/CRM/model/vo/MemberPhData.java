package cn.gson.CRM.model.vo;

import java.util.List;

public class MemberPhData {

	private List<String> names;
	
	private List<Long> totals;
	
	public MemberPhData() {
		// TODO Auto-generated constructor stub
	}

	public MemberPhData(List<String> names, List<Long> totals) {
		super();
		this.names = names;
		this.totals = totals;
	}

	public List<String> getNames() {
		return names;
	}

	public void setNames(List<String> names) {
		this.names = names;
	}

	public List<Long> getTotals() {
		return totals;
	}

	public void setTotals(List<Long> totals) {
		this.totals = totals;
	}
	
}
