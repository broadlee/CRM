package cn.gson.CRM.model.dao.fund;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.CRM.model.entity.PayRecord;

public interface PayRecordDao extends PagingAndSortingRepository<PayRecord, Long>{

	
	Page<PayRecord> findAllByReturnsReturnId(String returnId, Pageable pr);

	Page<PayRecord> findAllByPurchaseChaseId(String chaseId, Pageable pr);
	//本月
	//SELECT sum(money) FROM payRecord_ p WHERE DATE_FORMAT(p.date , '%Y%m' ) = DATE_FORMAT( CURDATE( ) , '%Y%m' )
	//上个月
	@Query(value = "SELECT sum(money) FROM PayRecord p WHERE PERIOD_DIFF( date_format( now( ) , '%Y%m' ) , date_format( p.date, '%Y%m' ) ) =1")
	Double countByState();
}
