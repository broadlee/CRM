package cn.gson.CRM.model.mapper;

import java.util.List;
import java.util.Map;

import cn.gson.CRM.model.entity.Plan;

public interface PlanMapper
{

	List<Plan> getPlan(Plan search);

	void deletePlanById(Long planid);

	void updateChancePlan(Long chanceId);

	Long getChanceId(Long planid);

	void finishPlan(Map<String, Object> param);

	void finishPlanByChance(Map<String, Object> param);
}
