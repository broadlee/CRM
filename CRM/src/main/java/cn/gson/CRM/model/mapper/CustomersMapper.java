package cn.gson.CRM.model.mapper;

import java.util.List;

import cn.gson.CRM.model.entity.Address;
import cn.gson.CRM.model.entity.Customer;
import cn.gson.CRM.model.entity.Order;
import cn.gson.CRM.model.entity.TalkRecord;

public interface CustomersMapper {
	
	List<Customer> getCustomers(Customer customer);
	
	List<Address> getAddrs(Long id);
	
	List<TalkRecord> getRecords(Customer customer);
	
	List<Order> getOrders(Long id);

}
