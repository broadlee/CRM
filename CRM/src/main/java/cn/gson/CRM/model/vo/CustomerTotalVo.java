package cn.gson.CRM.model.vo;

import java.math.BigDecimal;

/**
* @author 邓安民:
* @version 创建时间：2018年5月11日 下午2:03:39
* 客户消费统计vo
*/
public class CustomerTotalVo {
	private String name;
	private BigDecimal data;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public BigDecimal getData() {
		return data;
	}
	public void setData(BigDecimal data) {
		this.data = data;
	}
	public CustomerTotalVo(String name, BigDecimal data) {
		super();
		this.name = name;
		this.data = data;
	}
	@Override
	public String toString() {
		return "CustomerTotalVo [name=" + name + ", data=" + data + "]";
	}
	
}


 