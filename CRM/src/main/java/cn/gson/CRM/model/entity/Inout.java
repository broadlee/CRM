package cn.gson.CRM.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * 出入库单实体类
 * @author Administrator
 *
 */
@Entity
@Table(name = "inout_")
public class Inout {

	/**
	 * 主键自增id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * 出入库单编号
	 */
	@Column(nullable = false)
	private String inoutId;
	
	/**
	 * 关联订单
	 */
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "o_id",nullable = true)
	private Order orders;
	
	/**
	 * 关联采购单
	 */
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pu_id",nullable = true)
	private Purchase purchase;
	
	/**
	 * 状态
	 */
	@Enumerated(EnumType.STRING)
	private State state;
	
	public enum State {
		/**
		 * 未处理 
		 */
		NODEAL,
		/**
		 * 已通过
		 */
		DEAL,
		/**
		 * 已拒绝
		 */
		REJECT
	}
	
	/**
	 * 出入库标识
	 */
	@Enumerated(EnumType.STRING)
	public Type type;
	
	public enum Type {
		/**
		 * 入库
		 */
		COME,
		/**
		 * 出库
		 */
		GO
	}
	

	public Inout() {}

	public Inout(Long id,String inoutId, Order orders, Purchase purchase, State state) {
		super();
		this.id = id;
		this.orders = orders;
		this.purchase = purchase;
		this.state = state;
		this.inoutId = inoutId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Order getOrders() {
		return orders;
	}

	public void setOrders(Order orders) {
		this.orders = orders;
	}

	public Purchase getPurchase() {
		return purchase;
	}

	public void setPurchase(Purchase purchase) {
		this.purchase = purchase;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public void setInoutId(String inoutId) {
		this.inoutId = inoutId;
	}
	
	public String getInoutId() {
		return inoutId;
	}
	
	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Inout [id=" + id + ", inoutId=" + inoutId + ", state=" + state + "]";
	}
	
}
