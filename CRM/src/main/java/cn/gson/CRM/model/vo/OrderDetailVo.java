package cn.gson.CRM.model.vo;


import java.math.BigDecimal;

import cn.gson.CRM.model.entity.Order;
import cn.gson.CRM.model.entity.Product;

/**
 * 订单详情转换对象 
 * @author Administrator
 *
 */
public class OrderDetailVo {

	/**
	 * 主键自增id
	 */
	private Long id;

	/**
	 * 对应订单
	 */

	private Order orders;

	private Product product;
	
	/**
	 * 订单折扣
	 */
	private Double cost;
	
	/**
	 * 数量
	 */
	private Integer count;

	/**
	 * 小计
	 */
	private BigDecimal subtotal;
	/**
	 * 状态
	 */
	private Integer state;


	public OrderDetailVo() {}

	public OrderDetailVo(Long id, Order orders, Product product, Double cost, Integer count, BigDecimal subtotal,
			Integer state) {
		super();
		this.id = id;
		this.orders = orders;
		this.product = product;
		this.cost = cost;
		this.count = count;
		this.subtotal = subtotal;
		this.state = state;
	}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Order getOrders() {
		return orders;
	}

	public void setOrders(Order orders) {
		this.orders = orders;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public BigDecimal getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	@Override
	public String toString() {
		return "OrderDetailVo [id=" + id + ", orders=" + orders + ", product=" + product + ", cost=" + cost + ", count="
				+ count + ", subtotal=" + subtotal + ", state=" + state + "]";
	}
	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}





	






}
