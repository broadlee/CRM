package cn.gson.CRM.model.dao.server;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import cn.gson.CRM.model.entity.Server;

public interface ServerDao extends PagingAndSortingRepository<Server, Long>{

	@Query(value = "select count(s) from Server s where s.state != 'SAVED'")
	Integer countByState();
	
	@Query(value="SELECT MONTH (createDate),count(*) FROM server_  WHERE "
			+"YEAR (createDate) = ?1  and type = ?2 GROUP BY MONTH (createDate) order by MONTH (createDate) asc",
			nativeQuery=true)
	List<Object[]>  getServiceData(String year,String state );
}
