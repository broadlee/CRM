package cn.gson.CRM.model.dao.customers;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import cn.gson.CRM.model.entity.Address;
import cn.gson.CRM.model.entity.Customer;

public interface AddressDao extends CrudRepository<Address, Long> {
	
	@Query("UPDATE Address a SET a.moren = FALSE WHERE a.moren = TRUE AND a.customer = ?1")
	@Modifying
	int updateMoren(Customer customer);
	
	@Query("UPDATE Address a SET a.moren = ?2 WHERE a.id = ?1")
	@Modifying
	int updateMoren(Long id, Boolean checked);
	
	@Query("from Address a where a.customer.id=:id")
	Iterable<Address> findByCustomer(@Param("id") Long id);

}
