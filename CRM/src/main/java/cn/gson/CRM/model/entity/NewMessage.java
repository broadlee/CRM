package cn.gson.CRM.model.entity;

import javax.persistence.*;

@Entity
@Table(name = "message_")
public class NewMessage {
	
	/**
	 * 主键自增id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * 对应订单
	 */
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "o_id",nullable = true)
	private Order order;
	
	/**
	 * 对应回款
	 */
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "r_id",nullable = true)
	private ReceiveRecord receiveRecord;
	
	/**
	 * 消息类型
	 */
	private String type;

	public NewMessage() { }

	public NewMessage(Long id, Order order, ReceiveRecord receiveRecord, String type) {
		super();
		this.id = id;
		this.order = order;
		this.receiveRecord = receiveRecord;
		this.type = type;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public ReceiveRecord getReceiveRecord() {
		return receiveRecord;
	}

	public void setReceiveRecord(ReceiveRecord receiveRecord) {
		this.receiveRecord = receiveRecord;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Message [id=" + id + ", order=" + order + ", receiveRecord=" + receiveRecord + ", type=" + type + "]";
	}
	
}
