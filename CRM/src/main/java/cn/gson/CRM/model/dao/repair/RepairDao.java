package cn.gson.CRM.model.dao.repair;

import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.CRM.model.entity.Repair;

public interface RepairDao extends PagingAndSortingRepository<Repair, Long>{

}
