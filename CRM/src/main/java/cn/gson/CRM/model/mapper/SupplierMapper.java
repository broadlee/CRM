package cn.gson.CRM.model.mapper;

import java.util.List;

import cn.gson.CRM.model.entity.Supplier;

public interface SupplierMapper {

	List<Supplier> searchSupplier(Supplier supplier);
}
