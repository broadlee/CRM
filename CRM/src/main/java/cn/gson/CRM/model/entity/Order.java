package cn.gson.CRM.model.entity;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * 订单实体类
 * @author Administrator
 *
 */
@Entity
@Table(name = "order_")
public class Order {

	/**
	 * 主键自增id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * 所属客户
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "c_id")
	private Customer customer;
	
	/**
	 * 销售机会
	 */
 	@OneToOne(fetch = FetchType.EAGER)
 	@JoinColumn(name = "ch_id",nullable = true)
 	private Chance chance;
	
	
	
	/**
	 * 订单创建人，用户
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "m_id",nullable = true)
	private Member member;
	/**
	 * 物流公司
	 */
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="e_id")
	private ExpressCompany expressCompany;
	/**
	 * 订单编号
	 */
	@Column(nullable = false)
	private String orderId;
	
	/**
	 * 主题
	 */
	@Column(nullable = false)
	private String theme;
	
	/**
	 * 下单时间
	 */
	private Date date = new Date();
	
	/**
	 * 总金额
	 */
	@Column(precision=2)
	private BigDecimal total;
	
	/**
	 * 备注
	 */
	private String remark;
	
	/**
	 * 订单状态
	 */
	@Enumerated(EnumType.STRING)
	private State state;
	
	/**
	 * 物流单号
	 */
	private String shipment;
	/**
	 * 收货地址
	 */
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="a_id")
	private  Address address;

	public enum State{
		/**
		 * 已下单   
		 */
		XIA,
		/**
		 * 已付款
		 */
		PAY,
		/**
		 * 已发货
		 */
		SEND,
		/**
		 * 已完成
		 */
		DONE,
		/**
		 * 已取消
		 */
		CANCEL
				
	}


	public Order() {}



	public Order(Long id, Customer customer, Chance chance, Member member, ExpressCompany expressCompany,
			String orderId, String theme, Date date, BigDecimal total, String remark, State state, String shipment,
			Address address) {
		super();
		this.id = id;
		this.customer = customer;
		this.chance = chance;
		this.member = member;
		this.expressCompany = expressCompany;
		this.orderId = orderId;
		this.theme = theme;
		this.date = date;
		this.total = total;
		this.remark = remark;
		this.state = state;
		this.shipment = shipment;
		this.address = address;
	}


	public Address getAddress() {
		return address;
	}


	public void setAddress(Address address) {
		this.address = address;
	}

	public ExpressCompany getExpressCompany() {
		return expressCompany;
	}

	public void setExpressCompany(ExpressCompany expressCompany) {
		this.expressCompany = expressCompany;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}
	
	public String getShipment() {
		return shipment;
	}
	
	public void setShipment(String shipment) {
		this.shipment = shipment;
	}
	
	public void setChance(Chance chance) {
		this.chance = chance;
	}
	
	public Chance getChance() {
		return chance;
	}







	@Override
	public String toString() {
		return "Order [id=" + id + ", customer=" + customer + ", chance=" + chance + ", member=" + member
				+ ", expressCompany=" + expressCompany + ", orderId=" + orderId + ", theme=" + theme + ", date=" + date
				+ ", total=" + total + ", remark=" + remark + ", state=" + state + ", shipment=" + shipment + "]";
	}


	
	

	
}
