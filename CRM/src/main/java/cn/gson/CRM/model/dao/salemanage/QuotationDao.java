package cn.gson.CRM.model.dao.salemanage;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import cn.gson.CRM.model.entity.Quotation;

public interface QuotationDao extends CrudRepository<Quotation, Long>
{
	@Query("select quo from Quotation quo where quo.id=?1")
	Quotation findOneByid(Long long1);
	
}
