package cn.gson.CRM.model.entity;

import java.util.Date;

import javax.persistence.*;

/**
 * 付款计划实体类
 * @author Administrator
 *
 */
@Entity
@Table(name = "payPlan_")
public class PayPlan {

	/**
	 * 主键自增id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * 退货单
	 */
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "re_id")
	private Return returns;
	
	/**
	 * 采购单
	 */
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pu_id")
	private Purchase purchase;
	/**
	 * 订单
	 */
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "o_id",nullable=true)
	private Order order;
	
	/**
	 * 计划付款金额
	 */
	private Double money;
	
	/**
	 * 最晚付款时间
	 */
	private Date lateTime;
	
	/**
	 * 创建日期
	 */
	private Date createDate;
	
	/**
	 * 付款类型
	 */
	@Enumerated(EnumType.STRING)
	private Type type;
	
	public enum Type {
		/**
		 * 采购 
		 */
		PURCHASE,
		/**
		 * 退货款
		 */
		RETURN,
		/**
		 * 订单取消
		 */
		CANCEL
	}

	public PayPlan() {}

	public PayPlan(Long id, Return returns, Double money, Date lateTime, Date createDate) {
		super();
		this.id = id;
		this.returns = returns;
		this.money = money;
		this.lateTime = lateTime;
		this.createDate = createDate;
	}
	
	public PayPlan(Order order, Double money, Date lateTime, Date createDate, Type type) {
		super();
		this.order = order;
		this.money = money;
		this.lateTime = lateTime;
		this.createDate = createDate;
		this.type = type;
	}
	

	public PayPlan(Return returns, Double money, Date lateTime, Date createDate, Type type) {
		super();
		this.returns = returns;
		this.money = money;
		this.lateTime = lateTime;
		this.createDate = createDate;
		this.type = type;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Return getReturns() {
		return returns;
	}

	public void setReturns(Return returns) {
		this.returns = returns;
	}

	public Double getMoney() {
		return money;
	}

	public void setMoney(Double money) {
		this.money = money;
	}

	public Date getLateTime() {
		return lateTime;
	}

	public void setLateTime(Date lateTime) {
		this.lateTime = lateTime;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	public void setPurchase(Purchase purchase) {
		this.purchase = purchase;
	}
	
	public Purchase getPurchase() {
		return purchase;
	}
	
	public void setType(Type type) {
		this.type = type;
	}
	
	public Type getType() {
		return type;
	}
	

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	@Override
	public String toString() {
		return "PayPlan [id=" + id + ", returns=" + returns + ", purchase=" + purchase + ", order=" + order + ", money="
				+ money + ", lateTime=" + lateTime + ", createDate=" + createDate + ", type=" + type + "]";
	}

	
}
