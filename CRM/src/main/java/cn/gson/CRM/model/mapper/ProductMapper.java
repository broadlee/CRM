package cn.gson.CRM.model.mapper;

import java.util.List;

import cn.gson.CRM.model.entity.PageMsg;
import cn.gson.CRM.model.entity.Product;

public interface ProductMapper {
	
	List<Product> selectAll();
	List<Product> selectPage(PageMsg pageMsg);
	
	/*
	 * 搜索产品
	 */
	List<Product> searchProduct(Product product);
	
}
