package cn.gson.CRM.model.mapper;

import java.util.List;

import cn.gson.CRM.model.entity.ReceiveRecord;

public interface ReceiveRecordMapper {

	List<ReceiveRecord> findAll(ReceiveRecord receiveRecord);
}
