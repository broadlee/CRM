package cn.gson.CRM.model.dao.salemanage;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import cn.gson.CRM.model.entity.Plan;

public interface PlanDao extends CrudRepository<Plan, Long>
{
	@Modifying
	@Query("update Plan plan set plan.quotation.id=?2 where plan.chance.id=?1")
	void updatePlanQuo(Long chanceid, Long quoId);

}
