package cn.gson.CRM.model.dao.permission;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import cn.gson.CRM.model.entity.Resource;


public interface ResourceDao extends CrudRepository<Resource, Long> {

	List<Resource> findByParentIsNullAndEnable(boolean b);

	List<Resource> findByParentAndEnable(Resource resource, boolean b);


}
