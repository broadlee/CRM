package cn.gson.CRM.model.dao.stock;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.CRM.model.entity.Inout.State;
import cn.gson.CRM.model.entity.InoutRecord;
import cn.gson.CRM.model.entity.Order;

public interface InOutRecordDao extends PagingAndSortingRepository<InoutRecord, Long> {

	Page<InoutRecord> findByInoutInoutId(Pageable pr, String value);

	@Query("select i from InoutRecord i where i.member.name like ?1")
	Page<InoutRecord> findByMemberNameLike(String string, Pageable pr);

	@Query("select i from InoutRecord i where i.inout.orders = ?1 and i.inout.state =?2")
	InoutRecord findByInoutOrdersAndInoutState(Order order, State deal);


}
