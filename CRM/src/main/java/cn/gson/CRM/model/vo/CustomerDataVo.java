package cn.gson.CRM.model.vo;

import java.math.BigDecimal;
import java.util.List;

/**
* @author 邓安民:
* @version 创建时间：2018年5月11日 下午3:16:34
* 消费排行统计数据封装
*/
public class CustomerDataVo {
	private String type;
	private List<String> names;
	private List<BigDecimal> data;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<String> getNames() {
		return names;
	}
	public void setNames(List<String> names) {
		this.names = names;
	}
	public List<BigDecimal> getData() {
		return data;
	}
	public void setData(List<BigDecimal> data) {
		this.data = data;
	}
	public CustomerDataVo(String type, List<String> names, List<BigDecimal> data) {
		super();
		this.type = type;
		this.names = names;
		this.data = data;
	}
	@Override
	public String toString() {
		return "CustomerDataVo [type=" + type + ", names=" + names + ", data=" + data + "]";
	}
	
}


 