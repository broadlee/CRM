package cn.gson.CRM.model.mapper;

import java.util.List;

import cn.gson.CRM.model.entity.Server;

public interface ServerMapper {
	List<Server> getServer(Server server);
}
