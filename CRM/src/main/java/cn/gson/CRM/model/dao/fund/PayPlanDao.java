package cn.gson.CRM.model.dao.fund;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.CRM.model.entity.PayPlan;

public interface PayPlanDao extends PagingAndSortingRepository<PayPlan, Long>{

	@Query("select p from PayPlan p where p.returns.returnId = ?1")
	Page<PayPlan> findAllByReturnsId(String id,Pageable pr);

	@Query("select p from PayPlan p where p.purchase.chaseId = ?1")
	Page<PayPlan> findAllByPurchaseId(String chaseId, Pageable pr);

	@Query("select count(*) from PayPlan p")
	int countByState();
}
