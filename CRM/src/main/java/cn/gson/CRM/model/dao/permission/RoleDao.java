package cn.gson.CRM.model.dao.permission;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.CRM.model.entity.Role;

public interface RoleDao extends PagingAndSortingRepository<Role, Long> {

	List<Role> findByEnable(boolean b);

}
