package cn.gson.CRM.model.entity;

import java.util.Date;

import javax.persistence.*;

/**
 * 回款记录实体类
 * @author Administrator
 *
 */
@Entity
@Table(name = "receiveRecord_")
public class ReceiveRecord {

	/**
	 * 主键自增id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * 对应订单
	 */
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "o_id",nullable = true)
	private Order orders;
	
	/**
	 * 对应维修单
	 */
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "rep_id",nullable = true)
	private Repair repair;
	
	/**
	 * 回款金额
	 */
	@Column(precision=2)
	private Double money;
	
	/**
	 * 回款类型
	 */
	@Enumerated(EnumType.STRING)
	private Type type;
	
	public enum Type {
		/**
		 * 订金   
		 */
		SUBMONEY,
		/**
		 * 回款
		 */
		PATMENT,
		/**
		 * 维修
		 */
		REPAIR
	}
	
	/**
	 * 回款时间
	 */
	private Date date = new Date();
	
	/**
	 * 是否开发票,默认是
	 */
	private Boolean draw = true;

	public ReceiveRecord() {}
	
	public ReceiveRecord(Long id, Order orders, Repair repair, Double money, Type type, Date date, Boolean draw) {
		super();
		this.id = id;
		this.orders = orders;
		this.repair = repair;
		this.money = money;
		this.type = type;
		this.date = date;
		this.draw = draw;
	}
	public ReceiveRecord(Order orders,Double money, Type type, Date date, Boolean draw) {
		super();
		this.orders = orders;
		this.money = money;
		this.type = type;
		this.date = date;
		this.draw = draw;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Order getOrders() {
		return orders;
	}

	public void setOrders(Order orders) {
		this.orders = orders;
	}

	public Repair getRepair() {
		return repair;
	}

	public void setRepair(Repair repair) {
		this.repair = repair;
	}

	public Double getMoney() {
		return money;
	}

	public void setMoney(Double money) {
		this.money = money;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Boolean getDraw() {
		return draw;
	}

	public void setDraw(Boolean draw) {
		this.draw = draw;
	}

	@Override
	public String toString() {
		return "ReceiveRecord [id=" + id + ", money=" + money + ", type=" + type + ", date=" + date + ", draw=" + draw
				+ "]";
	}
}
