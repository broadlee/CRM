package cn.gson.CRM.model.entity;

import java.util.List;

import javax.persistence.*;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 角色实体类
 * @author Administrator
 *
 */
@Entity
@Table(name = "role_")
public class Role {
	
	/**
	 * 主键自增id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/**
	 * 角色名称
	 */
	@Column(name = "role_name", unique = true)
	private String roleName;
	
	/**
	 * 拥有资源
	 */
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(
			name="role_resource",
			joinColumns= @JoinColumn(name="role_id"),
			inverseJoinColumns = @JoinColumn(name="resource_id")
	)
	@JSONField(serialize=false)//禁止json序列化
	private List<Resource> resources;
	
	/**
	 * 资源是否可用
	 */
	private Boolean enable = false;

	public Role() {}
	
	public Role(Long rid) {
		this.id = rid;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public List<Resource> getResources() {
		return resources;
	}

	public void setResources(List<Resource> resources) {
		this.resources = resources;
	}

	public Boolean getEnable() {
		return enable;
	}

	public void setEnable(Boolean enable) {
		this.enable = enable;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((enable == null) ? 0 : enable.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Role other = (Role) obj;
		if (enable == null) {
			if (other.enable != null)
				return false;
		} else if (!enable.equals(other.enable))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Role [id=" + id + ", roleName=" + roleName + ", enable=" + enable + "]";
	}
}
