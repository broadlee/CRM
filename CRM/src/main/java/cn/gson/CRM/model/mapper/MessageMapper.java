package cn.gson.CRM.model.mapper;

import java.util.List;

import cn.gson.CRM.model.entity.NewMessage;


public interface MessageMapper {
	
	List<NewMessage> getMessage();
	
}
