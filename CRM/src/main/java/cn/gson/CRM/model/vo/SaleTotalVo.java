package cn.gson.CRM.model.vo;
import java.math.BigInteger;
/**
* @author 邓安民:
* @version 创建时间：2018年5月9日 下午4:51:15
* 销售机会统计数据转换类
*/
import java.util.List;
public class SaleTotalVo {
	private String year;
	private String type;
	private List<BigInteger> data;
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<BigInteger> getData() {
		return data;
	}
	public void setData(List<BigInteger> data) {
		this.data = data;
	}
	public SaleTotalVo(String year, String type, List<BigInteger> data) {
		super();
		this.year = year;
		this.type = type;
		this.data = data;
	}
	@Override
	public String toString() {
		return "SaleTotalVo [year=" + year + ", type=" + type + ", data=" + data + "]";
	}



}


 