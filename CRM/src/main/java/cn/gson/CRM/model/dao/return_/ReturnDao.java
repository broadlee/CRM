package cn.gson.CRM.model.dao.return_;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import cn.gson.CRM.model.entity.Return;

/**
* @author 邓安民:
* @version 创建时间：2018年4月19日 下午12:21:02
* 退货dao
*/
public interface ReturnDao extends JpaRepository<Return, Long> {

	Page<Return> findAll(Pageable pageable);
	
	@Query("from Return r where r.returnId like %:text% or r.orders.theme like %:text% or r.orders.orderId like %:text% or"
			+ " r.orders.customer.name like %:text%")
	Page<Return> selectByWhere(@Param("text") String text,Pageable pageable);
	
	Return findByOrdersId(Long id);
	

}


 