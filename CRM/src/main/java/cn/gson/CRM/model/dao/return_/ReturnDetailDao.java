package cn.gson.CRM.model.dao.return_;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import cn.gson.CRM.model.entity.ReturnDetail;

/**
* @author 邓安民:
* @version 创建时间：2018年4月19日 下午12:22:26
* 类说明
*/
public interface ReturnDetailDao extends JpaRepository<ReturnDetail, Long> {
	@Query("from ReturnDetail rd where rd.returns.id=:id")
	List<ReturnDetail> findAllByReturn(@Param("id")Long id);
	
	/**
	 * 统计某一退货单的总金额
	 */
	@Query("select sum(rd.subtotal) from ReturnDetail rd where rd.returns.id=?1")
	public BigDecimal getTotalByReturn(Long r_id);

}


 