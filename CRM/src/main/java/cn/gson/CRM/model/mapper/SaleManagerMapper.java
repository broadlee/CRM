package cn.gson.CRM.model.mapper;

import java.util.List;
import java.util.Map;

import cn.gson.CRM.model.entity.Chance;
import cn.gson.CRM.model.entity.Customer;
import cn.gson.CRM.model.entity.Member;
import cn.gson.CRM.model.entity.Voc;

public interface SaleManagerMapper
{
	List<Chance> getChance(Chance chance);
	
	List<Customer> getCustomer();
	
	int deleteChance(Long[] id);

	List<Voc> getVoc(Long customer);

	List<Chance> findDeleteChance(Long[] chances);

	List<Member> getMember();
	
	int updateVocChance(Map<String ,Object> map);
	
	
	List<Voc> getVocByChance(Long chance);

	Chance getOneChance(Long chanceid);

	void endchance(Long chanceId);

	void successChance(Long chanceId);
}
