package cn.gson.CRM.model.dao.product;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import cn.gson.CRM.model.entity.ChaseDetail;

public interface ChaseDetailDao extends PagingAndSortingRepository<ChaseDetail, Long>{

	@Query("SELECT c FROM ChaseDetail c JOIN c.purchase p WHERE p.id = :id")
	List<ChaseDetail> findById(@Param("id") Long id);

	ChaseDetail findByName(String name);
}
