package cn.gson.CRM.model.entity;

import java.util.Date;

import javax.persistence.*;

/**
 * 报价单实体类
 * @author Administrator
 *
 */
@Entity
@Table(name = "quotation_")
public class Quotation {

	/**
	 * 主键自增id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * 对应销售机会
	 */
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ch_id",nullable = false)
	private Chance chance;
	
	/**
	 * 报价单编号
	 */
	@Column(nullable = false, unique = true, length = 64)
	private String quoId;
	
	/**
	 * 主题
	 */
	@Column(nullable = false)
	private String theme;
	
	/**
	 * 创建日期
	 */
	private Date createDate = new Date();
	
	/**
	 * 总金额
	 */
	private Double total;
	
	public Quotation() {}
	
	public Quotation(Long id, Chance chance, String quoId, String theme, Date createDate, Double total) {
		super();
		this.id = id;
		this.chance = chance;
		this.quoId = quoId;
		this.theme = theme;
		this.createDate = createDate;
		this.total = total;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Chance getChance() {
		return chance;
	}

	public void setChance(Chance chance) {
		this.chance = chance;
	}

	public String getQuoId() {
		return quoId;
	}

	public void setQuoId(String quoId) {
		this.quoId = quoId;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Quotation(Long id)
	{
		super();
		this.id = id;
	}

	@Override
	public String toString() {
		return "Quotation [id=" + id + ", quoId=" + quoId + ", theme=" + theme + ", createDate=" + createDate
				+ ", total=" + total + "]";
	}
	
}
