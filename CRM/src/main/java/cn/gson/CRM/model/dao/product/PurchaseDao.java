package cn.gson.CRM.model.dao.product;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.CRM.model.entity.Purchase;
import cn.gson.CRM.model.entity.Purchase.State;

public interface PurchaseDao extends PagingAndSortingRepository<Purchase, Long>{


	Purchase findByChaseId(String value);

	//通过状态查询
	@Query("select count(*) from Purchase p where p.state=?1")
	int findByCount(State state);
	
	//未完成采购
	@Query("select count(*) from Purchase p where p.state!= 'PAYED'")
	int countByState();
}
