package cn.gson.CRM.model.entity;

import javax.persistence.*;

/**
 * 产品实体类
 * @author Administrator
 *
 */
@Entity
@Table(name = "product_")
public class Product {

	/**
	 * 主键自增id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * 供应商
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="s_id",  nullable = true)
	private Supplier supplier;
	
	/**
	 * 产品名称
	 */
	@Column(nullable = false)
	private String name;
	
	/**
	 * 成本价
	 */
	@Column(nullable = false)
	private Double costPrice;
	
	/**
	 * 销售价
	 */
	@Column(nullable = false)
	private Double sellPrice;
	
	/**
	 * 库存
	 */
	private Integer stock;
	
	/**
	 * 产品图片
	 */
	private String image;
	
	/**
	 * 产品状态
	 */
	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private State state;
	
	public enum State{
		/**
		 * 正常
		 */
		NORMAL,
		/**
		 * 缺货
		 */
		LACK,
		/**
		 * 停售
		 */
		HALT
	}

	public Product() {}

	public Product(Long id, Supplier supplier, String name, Double costPrice, Double sellPrice, Integer stock,
			String image, State state) {
		super();
		this.id = id;
		this.supplier = supplier;
		this.name = name;
		this.costPrice = costPrice;
		this.sellPrice = sellPrice;
		this.stock = stock;
		this.image = image;
		this.state = state;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(Double costPrice) {
		this.costPrice = costPrice;
	}

	public Double getSellPrice() {
		return sellPrice;
	}

	public void setSellPrice(Double sellPrice) {
		this.sellPrice = sellPrice;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", costPrice=" + costPrice + ", sellPrice=" + sellPrice
				+ ", stock=" + stock + ", image=" + image + ", state=" + state + "]";
	}
	
}
