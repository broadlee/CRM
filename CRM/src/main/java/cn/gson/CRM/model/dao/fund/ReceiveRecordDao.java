package cn.gson.CRM.model.dao.fund;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.CRM.model.entity.ReceiveRecord;
import cn.gson.CRM.model.entity.ReceiveRecord.Type;

public interface ReceiveRecordDao extends PagingAndSortingRepository<ReceiveRecord, Long>{

	@Query(value = "SELECT sum(money) FROM ReceiveRecord r WHERE PERIOD_DIFF( date_format( now( ) , '%Y%m' ) , date_format( r.date, '%Y%m' ) ) =1")
	Double countByState();
	ReceiveRecord findByOrdersIdAndType(Long id,Type type);
}
