package cn.gson.CRM.model.entity;

public class PageMsg {

	private int page;//页码
	private int pageSize;//页大小
	private int startPage;//起始页记录
	
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	
	public int getStartPage() {
		return startPage;
	}
	public void setStartPage(int startPage) {
		this.startPage = startPage;
	}
	@Override
	public String toString() {
		return "PageMsg [page=" + page + ", pageSize=" + pageSize + ", startPage=" + startPage + "]";
	}
}
