package cn.gson.CRM.model.entity;

import java.util.Date;

import javax.persistence.*;

/**
 * 销售机会实体类
 * @author Administrator
 *
 */
@Entity
@Table(name = "chance_")
public class Chance {

	/**
	 * 主键自增id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * 销售机会对应客户
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "c_id", nullable = false)
	private Customer customer;
	
	/**
	 * 创建人ID
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "m_id1", nullable = false)
	private Member member1;
	
	/**
	 * 指派人ID
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "m_id2", nullable = true)
	private Member member2;
	
	/**
	 * 被指派人ID
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "m_id3", nullable = true)
	private Member member3;
	
	/**
	 * 概要
	 */
	@Column(nullable = false)
	private String summary;
	
	/**
	 * 机会详情
	 */
	@Column(nullable = false)
	private String chanceInfo;
	
	/**
	 * 成功率
	 */
	@Column(nullable = false)
	private String success;
	
	/**
	 * 状态
	 */
	@Enumerated(EnumType.STRING)
	private State state;
	
	public enum State {
		/**
		 * 未指派  
		 */
		NOASSIGN,
		/**
		 * 已指派
		 */
		YESASSIGN,
		/**
		 * 开发成功
		 */
		SUCCESS,
		/**
		 * 开发失败
		 */
		FAILED
	}
	
	/**
	 * 创建时间
	 */
	private Date createDate = new Date();
	
	/**
	 * 指派时间
	 */
	private Date assignDate;
	
	/**
	 * 是否已经创建销售机会
	 */
	private boolean quoted;
	
	/**
	 * 是否已经创建了开发计划
	 */
	private boolean plan;

	public Chance() {}

	public Chance(Long id, Customer customer, Member member1, Member member2, Member member3, String summary,
			String chanceInfo, String success, State state, Date createDate, Date assignDate) {
		super();
		this.id = id;
		this.customer = customer;
		this.member1 = member1;
		this.member2 = member2;
		this.member3 = member3;
		this.summary = summary;
		this.chanceInfo = chanceInfo;
		this.success = success;
		this.state = state;
		this.createDate = createDate;
		this.assignDate = assignDate;
	}

	public boolean isPlan()
	{
		return plan;
	}

	public void setPlan(boolean plan)
	{
		this.plan = plan;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Member getMember1() {
		return member1;
	}

	public void setMember1(Member member1) {
		this.member1 = member1;
	}

	public Member getMember2() {
		return member2;
	}

	public void setMember2(Member member2) {
		this.member2 = member2;
	}

	public Member getMember3() {
		return member3;
	}

	public void setMember3(Member member3) {
		this.member3 = member3;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getChanceInfo() {
		return chanceInfo;
	}

	public void setChanceInfo(String chanceInfo) {
		this.chanceInfo = chanceInfo;
	}

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getAssignDate() {
		return assignDate;
	}

	public void setAssignDate(Date assignDate) {
		this.assignDate = assignDate;
	}

	public Chance(Long id)
	{
		super();
		this.id = id;
	}

	@Override
	public String toString() {
		return "Chance [id=" + id + ", summary=" + summary + ", chanceInfo=" + chanceInfo + ", success=" + success
				+ ", state=" + state + ", createDate=" + createDate + ", assignDate=" + assignDate + "]";
	}

	public boolean isQuoted()
	{
		return quoted;
	}

	public void setQuoted(boolean quoted)
	{
		this.quoted = quoted;
	}



	
	
}
