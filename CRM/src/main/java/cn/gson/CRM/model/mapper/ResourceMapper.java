package cn.gson.CRM.model.mapper;

import java.util.List;

import cn.gson.CRM.model.entity.Resource;


public interface ResourceMapper {

	List<Resource> selectRoot();

	List<Resource> selectChildren(Long long1);

}
