package cn.gson.CRM.model.mapper;

import java.util.List;

import cn.gson.CRM.model.entity.Purchase;

public interface PurchaseMapper {

	List<Purchase> searchPurchase(Purchase purchase);

}
