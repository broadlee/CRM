package cn.gson.CRM.model.entity;

import java.util.Date;

import javax.persistence.*;

/**
 * 回款计划实体类
 * @author Administrator
 *
 */
@Entity
@Table(name = "receivePlan_")
public class ReceivePlan {

	/**
	 * 主键自增id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * 订单
	 */
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "o_id")
	private Order orders;
	
	/**
	 * 维修单
	 */
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "rep_id")
	private Repair repair;
	
	/**
	 * 回款计划类型
	 */
	@Enumerated(EnumType.STRING)
	private Type type;
	
	public enum Type {
		/**
		 * 订金   
		 */
		SUBMONEY,
		/**
		 * 回款
		 */
		PATMENT,
		/**
		 * 维修
		 */
		REPAIR
	}
	
	/**
	 * 计划回款金额
	 */
	private Double money;
	
	/**
	 * 最晚回款时间
	 */
	private Date lateTime;
	
	/**
	 * 创建日期
	 */
	private Date createDate;

	public ReceivePlan() {}

	public ReceivePlan(Long id, Order orders, Double money, Date lateTime, Date createDate) {
		super();
		this.id = id;
		this.orders = orders;
		this.money = money;
		this.lateTime = lateTime;
		this.createDate = createDate;
	}
	
	public ReceivePlan( Order orders, Double money, Date lateTime, Date createDate) {
		super();
		this.orders = orders;
		this.money = money;
		this.lateTime = lateTime;
		this.createDate = createDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Order getOrders() {
		return orders;
	}

	public void setOrders(Order orders) {
		this.orders = orders;
	}

	public Double getMoney() {
		return money;
	}

	public void setMoney(Double money) {
		this.money = money;
	}

	public Date getLateTime() {
		return lateTime;
	}

	public void setLateTime(Date lateTime) {
		this.lateTime = lateTime;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	public void setRepair(Repair repair) {
		this.repair = repair;
	}
	
	public Repair getRepair() {
		return repair;
	}
	
	public void setType(Type type) {
		this.type = type;
	}
	
	public Type getType() {
		return type;
	}

	@Override
	public String toString() {
		return "ReceivePlan [id=" + id + ", money=" + money + ", lateTime=" + lateTime + ", createDate=" + createDate
				+ "]";
	}
	
}
