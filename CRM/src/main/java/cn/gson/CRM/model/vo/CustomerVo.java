package cn.gson.CRM.model.vo;

import java.util.Date;

import cn.gson.CRM.model.entity.Customer.LifeTime;
/**
* @author 邓安民:
* @version 创建时间：2018年4月28日 下午10:41:14
* 客户转换类
*/
public class CustomerVo {


	private Long id;
	
	/**
	 * 客户姓名
	 */
	private String name;
	/**
	 * 联系人
	 */
	private String linkMan;
	
	/**
	 * 联系方式
	 */

	private String linkWay;
	
	/**
	 * 生命周期
	 */
	private LifeTime lifeTime;
	
	
	/**
	 * 创建日期
	 */
	private Date createDate;
	
	/**
	 * 地址
	 */
	private String location;
	
	 /**
	  * 电子邮箱
	  */
	private String email;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLinkMan() {
		return linkMan;
	}

	public void setLinkMan(String linkMan) {
		this.linkMan = linkMan;
	}

	public String getLinkWay() {
		return linkWay;
	}

	public void setLinkWay(String linkWay) {
		this.linkWay = linkWay;
	}

	public LifeTime getLifeTime() {
		return lifeTime;
	}

	public void setLifeTime(LifeTime lifeTime) {
		this.lifeTime = lifeTime;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	public CustomerVo() {
		// TODO Auto-generated constructor stub
	}

	public CustomerVo(Long id, String name, String linkMan, String linkWay, LifeTime lifeTime, Date createDate,
			String location, String email) {
		super();
		this.id = id;
		this.name = name;
		this.linkMan = linkMan;
		this.linkWay = linkWay;
		this.lifeTime = lifeTime;
		this.createDate = createDate;
		this.location = location;
		this.email = email;
	}
	
}