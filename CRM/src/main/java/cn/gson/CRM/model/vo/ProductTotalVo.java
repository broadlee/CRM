package cn.gson.CRM.model.vo;

import java.math.BigDecimal;

public class ProductTotalVo {

	private String name;
	private BigDecimal number;
	
	public ProductTotalVo() {
	}
	
	public ProductTotalVo(String name, BigDecimal number) {
		super();
		this.name = name;
		this.number = number;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public BigDecimal getNumber() {
		return number;
	}
	public void setNumber(BigDecimal number) {
		this.number = number;
	}
	
}
