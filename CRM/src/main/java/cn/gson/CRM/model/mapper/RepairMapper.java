package cn.gson.CRM.model.mapper;

import java.util.List;

import cn.gson.CRM.model.entity.Repair;

public interface RepairMapper {

	List<Repair> findAll(Repair repair);
}
