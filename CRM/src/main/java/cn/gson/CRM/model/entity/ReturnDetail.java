package cn.gson.CRM.model.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * 退货单详情实体类
 * @author Administrator
 *
 */
@Entity
@Table(name = "returnDetail_")
public class ReturnDetail {

	/**
	 * 主键自增id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * 关联退货单
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "re_id")
	private Return returns;
	
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "p_id",nullable = false)
	private Product product;
	
	/**
	 * 订单折扣
	 */
	private Double cost;
	
	/**
	 * 数量
	 */
	private Integer count;

	/**
	 * 小计
	 */
	@Column(length=15,precision=2,nullable=false)
	private BigDecimal subtotal;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Return getReturns() {
		return returns;
	}

	public void setReturns(Return returns) {
		this.returns = returns;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public BigDecimal getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}
	public ReturnDetail() {
		// TODO Auto-generated constructor stub
	}
	public ReturnDetail(Long id, Return returns, Product product, Double cost, Integer count, BigDecimal subtotal) {
		super();
		this.id = id;
		this.returns = returns;
		this.product = product;
		this.cost = cost;
		this.count = count;
		this.subtotal = subtotal;
	}
	public ReturnDetail(Return returns, Product product, Double cost, Integer count, BigDecimal subtotal) {
		super();
		this.returns = returns;
		this.product = product;
		this.cost = cost;
		this.count = count;
		this.subtotal = subtotal;
	}

	@Override
	public String toString() {
		return "ReturnDetail [id=" + id + ", returns=" + returns + ", product=" + product + ", cost=" + cost
				+ ", count=" + count + ", subtotal=" + subtotal + "]";
	}
	
}
