package cn.gson.CRM.model.dao.index;

import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.CRM.model.entity.NewMessage;

public interface MessageDao extends PagingAndSortingRepository<NewMessage, Long> {
	
	

}
