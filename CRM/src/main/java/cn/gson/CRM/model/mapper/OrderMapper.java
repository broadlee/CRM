package cn.gson.CRM.model.mapper;

import java.util.List;
import cn.gson.CRM.model.vo.CustomerTotalVo;
import cn.gson.CRM.model.vo.OrderTotalVo;

/**
* @author 邓安民:
* @version 创建时间：2018年5月11日 下午4:53:11
* 订单mapper
*/
public interface OrderMapper {
	
	/**
	 * 统计今年客户消费金额前15
	 * 
	 */
	public List<CustomerTotalVo> totalByCustomerM(String year);
	/**
	 * 统计今天的订单成交量
	 * @param year
	 * @return
	 */
	public List<OrderTotalVo> countcountByMonth(String year);

}


 