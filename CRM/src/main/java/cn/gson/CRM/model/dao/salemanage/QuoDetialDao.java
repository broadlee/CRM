package cn.gson.CRM.model.dao.salemanage;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import cn.gson.CRM.model.entity.QuoDetail;

public interface QuoDetialDao extends CrudRepository<QuoDetail, Long>
{
	@Modifying
	@Query("delete from QuoDetail qd where qd.quotation.id=?1")
	int deleteQuoDetail(Long quotation);

	
	@Modifying
	@Query("update QuoDetail quo set quo.cost=?1,quo.count=?2 where quo.id=?3")
	void updateCostAndCont(Double cost,Integer count,Long detailId);
}
