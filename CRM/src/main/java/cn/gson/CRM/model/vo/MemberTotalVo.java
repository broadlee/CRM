package cn.gson.CRM.model.vo;

public class MemberTotalVo {
	private String name;
	private Long data;
	
	public MemberTotalVo() {
		// TODO Auto-generated constructor stub
	}
	public MemberTotalVo(String name, Long data) {
		this.name = name;
		this.data = data;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getData() {
		return data;
	}
	public void setData(Long data) {
		this.data = data;
	}
	
	@Override
	public String toString() {
		return "MemberTotalVo [name=" + name + ", data=" + data + "]";
	}
}
