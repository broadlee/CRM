package cn.gson.CRM.model.dao.stock;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.CRM.model.entity.Inout;
import cn.gson.CRM.model.entity.Inout.State;

public interface InOutDao extends PagingAndSortingRepository<Inout, Long> {

	@Query("select o from Inout o where o.orders.orderId = ?1")
	Page<Inout> findByOrderId(String orderId,Pageable pr);

	@Query("select o from Inout o where o.purchase.chaseId = ?1")
	Page<Inout> findByPurchaseId(String chaseId,Pageable pr);

	Page<Inout> findByState(Pageable pr, State state);

	Page<Inout> findByPurchaseChaseIdAndState(String chaseId, State state, Pageable pr);

	Page<Inout> findByOrdersOrderIdAndState(String orderId, State state, Pageable pr);

	List<Inout> findByOrdersIdAndState(Long id, State deal, Pageable pr);

	/**
	 * 为处理出库单
	 * @return
	 */
	@Query("select count(*) from Inout o where o.type = 'GO' and o.state = 'NODEAL'")
	int findOutNodeal();
	
	/**
	 * 为处理入库单
	 * @return
	 */
	@Query("select count(*) from Inout o where o.type = 'COME' and o.state = 'NODEAL'")
	int findInNodeal();
	
	/**
	 * 已完成入库单
	 * @return
	 */
	@Query("select count(*) from Inout o where o.type = 'COME' and o.state = 'DEAL'")
	int findInCount();
	
	/**
	 * 已完成出库单
	 * @return
	 */
	@Query("select count(*) from Inout o where o.type = 'GO' and o.state = 'DEAL'")
	int findOutCount();
	
	/**
	 * 已拒绝出库单
	 * @return
	 */
	@Query("select count(*) from Inout o where o.type = 'GO' and o.state = 'REJECT'")
	int findOutReject();
	
	/**
	 * 已拒绝入库单
	 * @return
	 */
	@Query("select count(*) from Inout o where o.type = 'COME' and o.state = 'REJECT'")
	int findInReject();
	
}
