package cn.gson.CRM.model.entity;

import java.util.Date;

import javax.persistence.*;

/**
 * 付款记录实体类
 * @author Administrator
 *
 */
@Entity
@Table(name = "payRecord_")
public class PayRecord {

	/**
	 * 主键自增id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * 对应采购单
	 */
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pu_id",nullable = true)
	private Purchase purchase;
	
	/**
	 * 对应退货单
	 */
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "re_id",nullable = true)
	private Return returns;
	
	/**
	 * 付款金额
	 */
	private Double money;
	
	/**
	 * 付款类型
	 */
	@Enumerated(EnumType.STRING)
	private Type type;
	
	public enum Type {
		/**
		 * 采购 
		 */
		PURCHASE,
		/**
		 * 退货款
		 */
		RETURN
	}
	
	/**
	 * 付款时间
	 */
	private Date date = new Date();
	
	/**
	 * 是否开发票,默认是
	 */
	private Boolean draw = true;

	public PayRecord() {}

	public PayRecord(Long id, Purchase purchase, Return returns, Double money, Type type, Date date, Boolean draw) {
		super();
		this.id = id;
		this.purchase = purchase;
		this.returns = returns;
		this.money = money;
		this.type = type;
		this.date = date;
		this.draw = draw;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Purchase getPurchase() {
		return purchase;
	}

	public void setPurchase(Purchase purchase) {
		this.purchase = purchase;
	}

	public Return getReturns() {
		return returns;
	}

	public void setReturns(Return returns) {
		this.returns = returns;
	}

	public Double getMoney() {
		return money;
	}

	public void setMoney(Double money) {
		this.money = money;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Boolean getDraw() {
		return draw;
	}

	public void setDraw(Boolean draw) {
		this.draw = draw;
	}

	@Override
	public String toString() {
		return "PayRecord [id=" + id + ", money=" + money + ", type=" + type + ", date=" + date + ", draw=" + draw
				+ "]";
	}
}
