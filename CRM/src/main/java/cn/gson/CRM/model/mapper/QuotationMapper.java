package cn.gson.CRM.model.mapper;

import java.util.List;
import java.util.Map;

import cn.gson.CRM.model.entity.Product;
import cn.gson.CRM.model.entity.QuoDetail;
import cn.gson.CRM.model.entity.Quotation;

public interface QuotationMapper
{
	List<Quotation> getQuotation(Quotation quotation);

	List<QuoDetail> getQuoDetail(Long quotation);

	Long findOneByid(Long quotation);

	List<Product> getProductOutquo(Long quotation);

	Long changeTotal(Map<String, Object> map);

	Long getQuotationId(Long detailId);

	Long getQuoIdByChanceId(Long chanceid);

	Long getChanceIdByQuoId(Long quoteId);
}
