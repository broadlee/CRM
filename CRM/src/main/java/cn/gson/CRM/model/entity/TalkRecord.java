package cn.gson.CRM.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 与客户交谈记录实体类
 * @author Administrator
 *
 */
@Entity
@Table(name = "talkRecord_")
public class TalkRecord {

	/**
	 * 主键自增id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * 所属客户
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "c_id", nullable = false)
	private Customer customer;
	
	/**
	 * 交谈日期
	 */
	@Column(nullable = false)
	private Date date;
	
	/**
	 * 交谈地点
	 */
	@Column(nullable = false)
	private String place;
	
	/**
	 * 交谈概要
	 */
	@Column(nullable = false)
	private String summary;
	
	/**
	 * 交谈具体
	 */
	@Column(nullable = false)
	private String detail;

	public TalkRecord() {}

	public TalkRecord(Long id, Customer customer, Date date, String place, String summary, String detail) {
		super();
		this.id = id;
		this.customer = customer;
		this.date = date;
		this.place = place;
		this.summary = summary;
		this.detail = detail;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	@Override
	public String toString() {
		return "TalkRecord [id=" + id + ", date=" + date + ", place=" + place + ", summary=" + summary + ", detail="
				+ detail + "]";
	}
	
}
