package cn.gson.CRM.model.dao.order;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import cn.gson.CRM.model.entity.ExpressCompany;

/**
* @author 邓安民:
* @version 创建时间：2018年4月17日 下午12:45:03
* 类说明
*/
public interface ExpressCompanyDao extends JpaRepository<ExpressCompany, Long>{
	@Query("from ExpressCompany e where e.name like %:where%  or e.identify like  %:where% ")
	Page<ExpressCompany> findByWhere(@Param("where") String where,Pageable pageable);

}


 