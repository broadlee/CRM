package cn.gson.CRM.model.entity;

import java.util.Date;

import javax.persistence.*;

/**
 * 采购单实体类
 * @author Administrator
 *
 */
@Entity
@Table(name = "purchase_")
public class Purchase {
	
	/**
	 * 主键自增id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * 创建人
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "m_id")
	private Member member;
	
	/**
	 * 采购单号
	 */
	@Column(nullable = false)
	private String chaseId;
	
	/**
	 * 采购主题
	 */
	@Column(nullable = false)
	private String theme;
	
	/**
	 * 采购时间
	 */
	private Date date = new Date();
	
	/**
	 * 总金额
	 */
	private Double total;
	
	/**
	 * 最晚到货时间
	 */
	private Date lateTime;
	
	/**
	 * 采购单状态
	 */
	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private State state;
	
	public enum State {
		/**
		 * 未付款
		 */
		NOPAY,
		/**
		 * 已付款
		 */
		PAYED,
		/**
		 * 无产品
		 */
		NODETAIL
	}
	
	public Purchase() {}
	
	public Purchase(Long id, Member member, String chaseId, String theme, Date date, Double total,
			Date lateTime, State state) {
		super();
		this.id = id;
		this.member = member;
		this.chaseId = chaseId;
		this.theme = theme;
		this.date = date;
		this.total = total;
		this.lateTime = lateTime;
		this.state = state;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public String getChaseId() {
		return chaseId;
	}

	public void setChaseId(String chaseId) {
		this.chaseId = chaseId;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Date getLateTime() {
		return lateTime;
	}

	public void setLateTime(Date lateTime) {
		this.lateTime = lateTime;
	}
	
	public void setState(State state) {
		this.state = state;
	}
	
	public State getState() {
		return state;
	}

	@Override
	public String toString() {
		return "Purchase [id=" + id + ", chaseId=" + chaseId + ", theme=" + theme + ", date=" + date + ", total="
				+ total + ", lateTime=" + lateTime + ", state=" + state + "]";
	}

}
