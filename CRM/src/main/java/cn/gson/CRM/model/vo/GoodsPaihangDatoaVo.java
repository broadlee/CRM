package cn.gson.CRM.model.vo;

import java.math.BigDecimal;
import java.util.List;

public class GoodsPaihangDatoaVo {

	private List<String> names;
	
	private List<BigDecimal> totals;
	
	public GoodsPaihangDatoaVo() {}

	public GoodsPaihangDatoaVo(List<String> names, List<BigDecimal> totals) {
		this.names = names;
		this.totals = totals;
	}

	public List<String> getNames() {
		return names;
	}

	public void setNames(List<String> names) {
		this.names = names;
	}

	public List<BigDecimal> getTotals() {
		return totals;
	}

	public void setTotals(List<BigDecimal> totals) {
		this.totals = totals;
	}

}
