package cn.gson.CRM.model.dao.product;

import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.CRM.model.entity.Product;

public interface ProductDao extends PagingAndSortingRepository<Product, Long> {

	Product findByName(String name);

}
