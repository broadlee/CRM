package cn.gson.CRM.model.entity;

import javax.persistence.*;

/**
 * 收货信息实体类
 * @author Administrator
 *
 */
@Entity
@Table(name = "address_")
public class Address {

	/**
	 * 主键自增id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
		
	/**
	 * 所属客户
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "c_id", nullable = false)
	private Customer customer; 
	
	/**
	 * 收货人姓名
	 */
	@Column(nullable = false)
	private String receiver;
	
	/**
	 * 收货地址
	 */
	@Column(nullable = false)
	private String address;
	
	/**
	 * 收货人联系方式
	 */
	@Column(nullable = false)
	private String concact;
	
	/**
	 * 邮编
	 */
	@Column(nullable = false)
	private String codes;
	
	/**
	 * 是否默认地址
	 */
	private Boolean moren;

	public Address() {}

	public Address(Long id, Customer customer, String receiver, String address, String concact, String codes,
			Boolean moren) {
		super();
		this.id = id;
		this.customer = customer;
		this.receiver = receiver;
		this.address = address;
		this.concact = concact;
		this.codes = codes;
		this.moren = moren;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getConcact() {
		return concact;
	}

	public void setConcact(String concact) {
		this.concact = concact;
	}

	public String getCodes() {
		return codes;
	}

	public void setCodes(String codes) {
		this.codes = codes;
	}

	public Boolean getMoren() {
		return moren;
	}

	public void setMoren(Boolean moren) {
		this.moren = moren;
	}

	@Override
	public String toString() {
		return "Address [id=" + id + ", receiver=" + receiver + ", address=" + address + ", concact=" + concact
				+ ", codes=" + codes + ", moren=" + moren + "]";
	}
	
}
