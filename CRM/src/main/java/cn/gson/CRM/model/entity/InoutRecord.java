package cn.gson.CRM.model.entity;

import java.util.Date;

import javax.persistence.*;

/**
 * 出入库记录实体类
 * @author Administrator
 *
 */
@Entity
@Table(name = "inoutRecord_")
public class InoutRecord {
	
	/**
	 * 主键自增id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * 对应出入单
	 */
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "io_id")
	private Inout inout;
	
	/**
	 * 出入单处理人
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "m_id")
	private Member member;
	
	/**
	 * 出入库时间
	 */
	private Date date = new Date();
	
	/**
	 * 出入库标记
	 */
	public Flag flag;
	
	public enum Flag {
		/**
		 * 出库 
		 */
		OUT,
		/**
		 * 入库
		 */
		IN
	}
	
	/**
	 * 备注
	 */
	private String remark;

	public InoutRecord() {}

	public InoutRecord(Long id, Inout inout, Member member, Date date, Flag flag, String remark) {
		super();
		this.id = id;
		this.inout = inout;
		this.member = member;
		this.date = date;
		this.flag = flag;
		this.remark = remark;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Inout getInout() {
		return inout;
	}

	public void setInout(Inout inout) {
		this.inout = inout;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Flag getFlag() {
		return flag;
	}

	public void setFlag(Flag flag) {
		this.flag = flag;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "InoutRecord [id=" + id + ", date=" + date + ", flag=" + flag + ", remark=" + remark + "]";
	}
	
}
