package cn.gson.CRM.model.dao.fund;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.CRM.model.entity.ReceivePlan;

public interface ReceivePlanDao extends PagingAndSortingRepository<ReceivePlan, Long>{

	Page<ReceivePlan> findAllByOrdersOrderId(String orderId, Pageable pr);

	Page<ReceivePlan> findAllByRepairRepairId(String repairId, Pageable pr);
	
	ReceivePlan findAllByOrdersId(Long id);
	
	@Query("select count(p) from ReceivePlan p ")
	int countByState();

}
