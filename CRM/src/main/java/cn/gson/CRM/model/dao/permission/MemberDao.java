package cn.gson.CRM.model.dao.permission;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.CRM.model.entity.Member;

public interface MemberDao extends PagingAndSortingRepository<Member, Long>{

	List<Member> findByNameLike(String name);

	int countByAccount(String account);

	Member findOneByAccount(String account);

}
