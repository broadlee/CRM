package cn.gson.CRM.model.entity;

import java.util.Date;

import javax.persistence.*;

/**
 * 退货单实体类
 * @author Administrator
 *
 */
@Entity
@Table(name = "return_")
public class Return {
	
	/**
	 * 主键自增id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * 关联订单
	 */
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "o_id")
	private Order orders;
	
	/**
	 * 退货单号
	 */
	@Column(nullable = false)
	private String returnId;
	
	/**
	 * 退货时间
	 */
	private Date date = new Date(); 
	
	/**
	 * 应退款
	 */
	private Double returned;
	
	/**
	 * 已退款
	 */
	private Double refuned;
	
	/**
	 * 状态
	 */
	@Enumerated(EnumType.STRING)
	private State state;
	
	public enum State {
		/**
		 * 退货中 
		 */
		TUING,
		/**
		 * 已完成
		 */
		TUED
	}

	public Return() {}

	public Return(Long id, Order orders, String returnId, Date date, Double returned, Double refuned, State state) {
		super();
		this.id = id;
		this.orders = orders;
		this.returnId = returnId;
		this.date = date;
		this.returned = returned;
		this.refuned = refuned;
		this.state = state;
	}
	public Return(Order orders, String returnId, Date date, Double returned, Double refuned, State state) {
		super();
		this.orders = orders;
		this.returnId = returnId;
		this.date = date;
		this.returned = returned;
		this.refuned = refuned;
		this.state = state;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Order getOrders() {
		return orders;
	}

	public void setOrders(Order orders) {
		this.orders = orders;
	}

	public String getReturnId() {
		return returnId;
	}

	public void setReturnId(String returnId) {
		this.returnId = returnId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Double getReturned() {
		return returned;
	}

	public void setReturned(Double returned) {
		this.returned = returned;
	}

	public Double getRefuned() {
		return refuned;
	}

	public void setRefuned(Double refuned) {
		this.refuned = refuned;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "Return [id=" + id + ", returnId=" + returnId + ", date=" + date + ", returned=" + returned
				+ ", refuned=" + refuned + ", state=" + state + "]";
	}
	
}
