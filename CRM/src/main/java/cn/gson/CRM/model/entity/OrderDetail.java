package cn.gson.CRM.model.entity;

import java.math.BigDecimal;
import javax.persistence.*;

/**
 * 订单详情表
 * @author Administrator
 *
 */
@Entity
@Table(name = "orderDetail_")
public class OrderDetail {

	/**
	 * 主键自增id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/**
	 * 对应订单
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "o_id",nullable = false)
	private Order orders;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "p_id",nullable = false)
	private Product product;
	
	/**
	 * 订单折扣
	 */
	private Double cost;
	
	/**
	 * 数量
	 */
	private Integer count;

	/**
	 * 小计
	 */
	@Column(precision=2,nullable=false)
	private BigDecimal subtotal;
	
	/**
	 * 状态
	 * 1,正常
	 * 0,退货
	 */
	@Column(precision=0,length=3)
	private Integer state;



	public OrderDetail() {}



	


	public OrderDetail(Long id, Order orders, Product product, Double cost, Integer count, BigDecimal subtotal,
			Integer state) {
		super();
		this.id = id;
		this.orders = orders;
		this.product = product;
		this.cost = cost;
		this.count = count;
		this.subtotal = subtotal;
		this.state = state;
	}






	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Order getOrders() {
		return orders;
	}

	public void setOrders(Order orders) {
		this.orders = orders;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}
	



	@Override
	public String toString() {
		return "OrderDetail [id=" + id + ", orders=" + orders + ", product=" + product + ", cost=" + cost + ", count="
				+ count + ", subtotal=" + subtotal + "]";
	}



	public BigDecimal getSubtotal() {
		return subtotal;
	}



	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}



	public Integer getState() {
		return state;
	}



	public void setState(Integer state) {
		this.state = state;
	}



}
