package cn.gson.CRM.model.entity;

import java.util.Date;

import javax.persistence.*;

/**
 * 开发失败记录实体类
 * @author Administrator
 *
 */
@Entity
@Table(name = "failRecord_")
public class FailRecord {

	/**
	 * 主键自增id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * 对应销售机会
	 */
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ch_id", nullable = false)
	private Chance chance;
	
	/**
	 * 失败原因
	 */
	private String reason;
	
	/**
	 * 失败时间
	 */
	private Date failTime = new Date();
	
	public FailRecord() {}

	public FailRecord(Long id, Chance chance, String reason, Date failTime) {
		super();
		this.id = id;
		this.chance = chance;
		this.reason = reason;
		this.failTime = failTime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Chance getChance() {
		return chance;
	}

	public void setChance(Chance chance) {
		this.chance = chance;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Date getFailTime() {
		return failTime;
	}

	public void setFailTime(Date failTime) {
		this.failTime = failTime;
	}

	@Override
	public String toString() {
		return "FailRecord [id=" + id + ", reason=" + reason + ", failTime=" + failTime + "]";
	}
	
}
