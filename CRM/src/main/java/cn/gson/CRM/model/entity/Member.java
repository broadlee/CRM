package cn.gson.CRM.model.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 * 用户实体类（后台操作者）
 * @author Administrator
 *
 */
@Entity
@Table(name = "member_")
public class Member {

	/**
	 * 主键自增id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/**
	 * 账号
	 */
	@Column(nullable = false, unique = true, length = 64)
	private String account;

	/**
	 * 姓名
	 */
	private String name;
	
	/**
	 * 密码
	 */
	@Column(nullable = false)
	private String password;
	
	/**
	 * 拥有角色
	 */
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(
			name="member_role",
			joinColumns=@JoinColumn(name="member_id"),
			inverseJoinColumns=@JoinColumn(name="role_id")
	)
	private List<Role> roles;

	/**
	 * 用户是否可用
	 */
	private Boolean enable = true;
	
	/**
	 * 创建日期
	 */
	private Date date = new Date();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public Boolean getEnable() {
		return enable;
	}

	public void setEnable(Boolean enable) {
		this.enable = enable;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "Member [id=" + id + ", account=" + account + ", password=" + password + ", enable=" + enable + ", date="
				+ date + "]";
	}

	public Member(Long id)
	{
		super();
		this.id = id;
	}

	public Member()
	{
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
