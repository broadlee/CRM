package cn.gson.CRM.model.dao.stock;



import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.CRM.model.entity.Product;

public interface StockDao extends PagingAndSortingRepository<Product, Long> {

	Page<Product> findByNameLike(Pageable pr,String name);
}
