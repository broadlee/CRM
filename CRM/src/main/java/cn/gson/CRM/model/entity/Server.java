package cn.gson.CRM.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 服务管理实体类
 * @author Administrator
 *
 */
@Entity
@Table(name = "server_")
public class Server {

	/**
	 * 主键自增id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * 关联客户
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "c_id")
	private Customer customer;
	
	/**
	 * 创建人
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "m_id1")
	private Member creator;
	
	/**
	 * 处理人
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "m_id2",nullable = true)
	private Member dealer;
	
	/**
	 * 问题描述
	 */
	@Column(nullable = false)
	private String question;
	
	/**
	 * 解决方案
	 */
	private String answer;
	
	/**
	 * 满意度星数
	 */
	private Integer satis;
	
	/**
	 * 服务类型
	 */
	@Enumerated(EnumType.STRING)
	private Type type;
	
	public enum Type {
		/**
		 * 咨询  
		 */
		ASK,
		/**
		 * 投诉
		 */
		COMPLAIN,
		/**
		 * 建议
		 */
		ADVICE
	}
	
	/**
	 * 创建时间
	 */
	private Date createDate = new Date();
	
	/**
	 * 分配时间
	 */
	private Date assignDate;
	
	/**
	 * 处理时间
	 */
	private Date dealDate;
	
	/**
	 * 状态
	 */
	@Enumerated(EnumType.STRING)
	private State state;
	
	public enum State {
		/**
		 * 新创建  
		 */
		NEW,
		/**已分配
		 * 
		 */
		ASSIGNED,
		/**
		 * 已处理
		 */
		DEALED,
		/**
		 * 已归档
		 */
		SAVED
	}
	
	/**
	 * 处理结果
	 */
	private String result;

	public Server() {}

	public Server(Long id, Customer customer, Member creator, Member dealer, String question, String answer,
			Integer satis, Type type, Date createDate, Date assignDate, Date dealDate, State state, String result) {
		super();
		this.id = id;
		this.customer = customer;
		this.creator = creator;
		this.dealer = dealer;
		this.question = question;
		this.answer = answer;
		this.satis = satis;
		this.type = type;
		this.createDate = createDate;
		this.assignDate = assignDate;
		this.dealDate = dealDate;
		this.state = state;
		this.result = result;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Member getCreator() {
		return creator;
	}

	public void setCreator(Member creator) {
		this.creator = creator;
	}

	public Member getDealer() {
		return dealer;
	}

	public void setDealer(Member dealer) {
		this.dealer = dealer;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public Integer getSatis() {
		return satis;
	}

	public void setSatis(Integer satis) {
		this.satis = satis;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getAssignDate() {
		return assignDate;
	}

	public void setAssignDate(Date assignDate) {
		this.assignDate = assignDate;
	}

	public Date getDealDate() {
		return dealDate;
	}

	public void setDealDate(Date dealDate) {
		this.dealDate = dealDate;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "Server [id=" + id + ", question=" + question + ", answer=" + answer + ", satis=" + satis + ", type="
				+ type + ", createDate=" + createDate + ", assignDate=" + assignDate + ", dealDate=" + dealDate
				+ ", state=" + state + ", result=" + result + "]";
	}
	
}
