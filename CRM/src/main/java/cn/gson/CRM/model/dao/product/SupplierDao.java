package cn.gson.CRM.model.dao.product;

import org.springframework.data.repository.PagingAndSortingRepository;

import cn.gson.CRM.model.entity.Supplier;

public interface SupplierDao extends PagingAndSortingRepository<Supplier, Long>{

}
