package cn.gson.CRM.model.vo;

import java.math.BigInteger;
import java.util.List;

public class OrderTotalVo {

	private String year;
	
	private String type;
	
	private List<BigInteger> data;

	public OrderTotalVo() {}
	
	public OrderTotalVo(String year, String type, List<BigInteger> data) {
		this.year = year;
		this.type = type;
		this.data = data;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<BigInteger> getData() {
		return data;
	}

	public void setData(List<BigInteger> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "OrderTotalVo [year=" + year + ", type=" + type + ", data=" + data + "]";
	}
	
}
