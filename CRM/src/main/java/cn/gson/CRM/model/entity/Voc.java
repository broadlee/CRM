package cn.gson.CRM.model.entity;

import javax.persistence.*;

/**
 * 客户需求实体类
 * @author Administrator
 *
 */
@Entity
@Table(name = "voc_")
public class Voc {

	/**
	 * 主键自增id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	/**
	 * 关联客户
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "c_id")
	private Customer customer;
	
	/**
	 * 产品关联
	 */
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "p_id")
	private Product product;
	
	/**
	 * 数量
	 */
	private Integer number;
	
	/**
	 * 备注
	 */
	private String note;
	
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "chance_id")
	private Chance chance;
	
	public Chance getChance()
	{
		return chance;
	}

	public void setChance(Chance chance)
	{
		this.chance = chance;
	}

	/**
	 * 是否做处理
	 */
	private Boolean deals = false;

	public Voc() {}

	public Voc(Long id, Customer customer, String note, Boolean deals,Product product, Integer number) {
		super();
		this.id = id;
		this.customer = customer;
		this.note = note;
		this.deals = deals;
		this.product = product;
		this.number = number;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public void setDeals(Boolean deals) {
		this.deals = deals;
	}
	
	public Boolean getDeals() {
		return deals;
	}
	
	public void setProduct(Product product) {
		this.product = product;
	}
	
	public Product getProduct() {
		return product;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	@Override
	public String toString()
	{
		return "Voc [id=" + id + ", product=" + product + ", number=" + number + ", chance=" + chance + ", deals="
				+ deals + "]";
	}

	
}
