package cn.gson.CRM.model.mapper;

import java.util.List;

import cn.gson.CRM.model.entity.Inout;

public interface InoutMapper {
	
	List<Inout> selectIn(String name);
	
	List<Inout> selectOut(String name);

}
