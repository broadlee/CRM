package cn.gson.CRM.utils;

/**
 * 
 * @author 验证码接口
 *
 */
public interface Constants {
	/**
	 * 后台账号session存储的键值
	 */
	String USER_SESSION_KEY = "user";
	/**
	 * 验证码的session存储键值
	 */
	String CAPTCHA_CODE_SESSION_KEY = "captcha_code";
}
