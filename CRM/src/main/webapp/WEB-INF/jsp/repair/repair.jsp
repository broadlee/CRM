<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<table id="repairGird"></table>
<div id="repairToolbar">
    <a class="easyui-linkbutton add" iconCls="icon-add" plain="true">添加维修单</a>
    <a class="easyui-linkbutton edit" iconCls="icon-edit" plain="true">编辑维修单</a>
    <a class="easyui-linkbutton check" iconCls="icon-add" plain="true">反馈检测结果</a>
    <a class="easyui-linkbutton confirm" iconCls="icon-ok" plain="true">客户已确认</a>
    <a class="easyui-linkbutton delete" iconCls="icon-remove" plain="true">删除维修单</a>
    <a class="easyui-linkbutton see" iconCls="icon-details" plain="true">查看维修单详情</a>
    <input class="easyui-searchbox" menu='#searchitem' id="search" data-options="prompt:'请输入关键字',searcher:doSearch"/>
	<div id="searchitem">
		<div data-options="name:'customer.name',iconCls:'icon-ok'">客户名</div>
		<div data-options="name:'product.name',iconCls:'icon-ok'">产品名</div>
		<div data-options="name:'member.name',iconCls:'icon-ok'">维修人名</div>
	</div>
	<input id="selectProcess" name="process" class="easyui-combobox" editable="false" panelHeight="auto" style="width:100px;"/>
	<a href="javascript:void(0)" class="easyui-linkbutton all"
		iconCls="icon-reload" plain="true">刷新</a> 
</div>
<script src="static/js/repair/repair.js"></script>
	