<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
<link href="static/css/server/server.css" rel="stylesheet" type="text/css" /> 

<form id="repairForm" method="post" style="margin:0;padding:10px 30px">
	<input type="hidden" name="id" value="${repair.id}"/>
	<c:if test="${not empty repair.repairId}">
		<div style="margin-bottom:10px">
	        <input id="repairId" value="${repair.repairId}" class="easyui-textbox" name="repairId" style="width:99.5%;" required="true" editable="false" panelHeight="auto" label="维修单编号" labelAlign="center"/>
	    </div>
    </c:if>
    <div style="margin-bottom:10px">
        <input value="${repair.customer.id}" class="easyui-combobox" id="selectCus" name="customer.id" style="width:99.5%;" required="true" panelHeight="auto" label="客户" labelAlign="center"/>
    </div>
     <div style="margin-bottom:10px" id="special">
    	<input id="product" required="true" name="product.id" class="easyui-combobox" multiline="true" label="产品" style="width:99.5%" value="${repair.product.id}" labelAlign="center">
    </div>
     <div style="margin-bottom:10px" id="special">
    	<input id="theme" required="true" name="theme" class="easyui-textbox" multiline="true" label="维修主题" style="width:99.5%" value="${repair.theme}" labelAlign="center">
    </div>
    <div style="margin-bottom:10px">
    	<c:if test="${repair.process == 'UNCHECK' || repair.process == null}">
        	<input id="process" name="process" class="easyui-textbox" readonly="true" label="进度" style="width:49%" value="待检测" labelAlign="center">
        </c:if>
        <c:if test="${repair.process == 'UNCONFIRM'}">
        	<input id="process" name="process" class="easyui-textbox" readonly="true" label="进度" style="width:49%" value="待客户确认" labelAlign="center">
        </c:if>
        <c:if test="${repair.process == 'PEPAIRING'}">
        	<input id="process" name="process" class="easyui-textbox" readonly="true" label="进度" style="width:49%" value="维修中" labelAlign="center">
        </c:if>
        <c:if test="${repair.process == 'HANDLE'}">
        	<input id="process" name="process" class="easyui-textbox" readonly="true" label="进度" style="width:49%" value="已交付" labelAlign="center">
        </c:if>
        <fmt:formatDate value="${repair.date}" pattern="yyyy-MM-dd hh:mm:ss" var="createDate"/>
    	<input id="createDate" name="date" class="easyui-textbox" readonly="true" label="创建时间" style="width:49%" value="${createDate}" labelAlign="center">
    </div>
    <div style="margin-bottom:10px" id="special" >
    	<input id="selectMem" required name="member.id" class="easyui-combobox" label="被分配接单人" style="width:49%" value="${repair.member.id}" labelAlign="center" panelHeight="auto">
    	<input id="money" name="money" class="easyui-textbox" multiline="true" label="费用" style="width:49%" value="${repair.money}" labelAlign="center">
    </div>
    <div style="margin-bottom:10px" id="special" >
    	<input id="backResult" name="backResult" class="easyui-textbox" label="检测结果详情" style="width:99.5%" value="${repair.backResult}" labelAlign="center" panelHeight="auto">
    </div>
</form>
<script src="static/js/repair/form.js"></script>