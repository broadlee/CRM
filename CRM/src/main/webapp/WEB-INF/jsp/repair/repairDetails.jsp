<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="repairDetails" style="padding: 10px 20px;font-weight: bold; ">
	<input type="hidden" value="${repair.id}" name="repair.id" id="repairId" />
	<div style="margin:2px 0px;"><label>维修单编号：</label><span>${repair.repairId}</span></div>
	<div style="margin:2px 0px;"><label>维修主题：</label><span>${repair.theme }</span></div>
	<div style="margin:2px 0px;"><label>客户：</label><span>${repair.customer.name }</span></div>
	<div style="margin:2px 0px;"><label>产品：</label><span>${repair.product.name }</span></div>
	<div style="margin:2px 0px;"><label>处理人：</label><span>${repair.member.name }</span></div>
	<div style="margin:2px 0px;"><label>创建时间：</label><span>${repair.date }</span></div>
	<div style="margin:2px 0px;"><label>检测结果：</label><span>${repair.backResult }</span></div>
	<div style="margin:2px 0px;">
	<label>进度：</label>
	<span>
		<c:if test="${repair.process=='UNCHECK' }">
			待检测
		</c:if>
		<c:if test="${repair.state=='UNCONFIRM' }">
			待客户确认
		</c:if>
		<c:if test="${repair.state=='UNCONFIRM' }">
			维修中
		</c:if>
		<c:if test="${repair.state=='HANDLE' }">
			已交付
		</c:if>
	</span>
	</div>
	<div style="margin:2px 0px;"><label>费用：</label><span>${repair.money }</span></div>
</div>
