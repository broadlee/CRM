<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>前台【客户提交的需求模块】 内容：展示产品列表、提交需求表单</title>
<jsp:include page="/WEB-INF/jsp/easyuilink.jsp"></jsp:include>
<script type="text/javascript" src="static/js/reception/reception.js"></script>
<script type="text/javascript" src="static/js/reception/order.js"></script>
<link href="static/css/reception/reception.css" rel="stylesheet" />
<link rel="stylesheet" href="static/iconfont/iconfont.css">
</head>
<body class="easyui-layout">
	<div data-options="region:'east',split:true" title="客户需求"
		style="width: 400px;">
		<div class="easyui-panel" title="客户信息"
			style="width: 100%; max-width: 400px; padding: 30px 60px;">
			<form id="userForm" method="post">
				<div style="margin-bottom: 20px">
					<input class="easyui-textbox" name="name" style="width: 100%"
						data-options="label:'客户姓名:',required:true">
				</div>
				<div style="margin-bottom: 20px">
					<input class="easyui-textbox" name="linkMan" style="width: 100%"
						data-options="label:'联系人姓名:',required:true">
				</div>
				<div style="margin-bottom: 20px">
					<input class="easyui-numberbox" name="linkWay" style="width: 100%"
						data-options="label:'联系方式:',required:true" 
						onKeyUp="value=value.replace(/[^/d|chun]/g,'')">
				</div>
			</form>
			<div style="text-align: center;">
				<a href="javascript:void(0)" class="easyui-linkbutton"
					onclick="submitForm()" style="width: 80px">保存</a> <a
					href="javascript:void(0)" class="easyui-linkbutton"
					onclick="clearForm()" style="width: 80px">清除</a>
			</div>
		</div>
		<table id="orderDg" title="订购产品"></table>
	</div>

	<div data-options="region:'center',title:'产品列表',iconCls:'icon-ok'">
		<div id="content" class="easyui-panel" data-options="href:'reception/list?page=1&pageSize=10'"></div>
		<div id="pp" class="easyui-pagination" style="border: 1px solid #ccc;"
			data-options="pageSize:10,total:${count }"></div>
		</div>

</body>
</html>