<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<form id="receptionForm" style="margin: 40px;">
	<input id="id" type="hidden" name="id" value="${oneProduct.id }" />
	<div style="margin: 20px">
		<input id="name" disabled="disabled" class="easyui-textbox" name="name" style="width: 60%"
			data-options="label:'产品名称:',required:true"
			value="${oneProduct.name }">
	</div>
	<div style="margin: 20px">
		<input id="num" class="easyui-numberspinner" name="num" style="width: 60%;" value="1"
			data-options="label:'产品数量:',min:1,editable:true">
	</div>
</form>