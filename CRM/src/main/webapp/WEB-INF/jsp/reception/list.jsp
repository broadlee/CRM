<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="static/js/reception/reception.js"></script>
<c:forEach items="${rows.list }" var="row">
	<ul class="show-product"  style="display: inline-block;margin: 19px">
		<input type="hidden" name="id" value="${row.id }" />
		<li class="show-product-url" name="image"><img height='200'
			width="200" class="reception-img" alt="${row.image }"
			src="${row.image }"></li>
		<li class="show-product-name" name="name" ><span>${row.name }</span></li>
		<li class="show-product-like">
			<a href="javascript:void(0)" class="open-form" style="color: #333333;"> 
				<i class="iconfont icon-favorite"></i>
			</a>
		</li>
	</ul>
</c:forEach>