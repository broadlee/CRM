<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>    
<div style="margin:0;padding: 10px 30px;font-weight: bold; ">
<table style="width: 100%">
	<tr>
		<td><div style="margin:2px 0px;"><label>采购单编号：</label><span>${pur.id}</span></div>
		</td>
		<td><div style="margin:2px 0px;"><label>主题：</label><span>${pur.theme }</span></div>
		</td>
	</tr>
	<tr>
		<td><div style="margin:2px 0px;"><label>采购人：</label><span>${pur.member.name }</span></div>
		</td>
		<td><div style="margin:2px 0px;"><label>总金额：</label><span>${pur.total } ￥</span></div>
		</td>
	</tr>
	<tr>
		<td><div style="margin:2px 0px;">
		<label>采购单状态：</label>
		<span>
			<c:if test="${pur.state=='NOPAY' }">
				未付款
			</c:if>
			<c:if test="${pur.state=='PAYED' }">
				已付款
			</c:if>
			<c:if test="${pur.state=='NODETAIL' }">
				无产品
			</c:if>
		</span>
		</div>
		</td>
		<td><div style="margin:2px 0px;"><label>采购时间：</label><span>${pur.date }</span></div>
		</td>
	</tr>
	<tr>
		<td><div style="margin:2px 0px;"><label>最晚到货时间：</label><span>${pur.lateTime }</span></div>
		</td>
	</tr>
</table>
</div>
    
    <table id="chaseDetailGrid" ></table>
    
    <div id="chaseDetailToolbar">
    <a class="easyui-linkbutton add" iconCls="icon-add" plain="true">添加采购产品</a>
    <a class="easyui-linkbutton edit" iconCls="icon-edit" plain="true">编辑采购产品</a>
    <a class="easyui-linkbutton delete" iconCls="icon-remove" plain="true">删除采购产品</a>
				<select id="product-purchase"
				class="easyui-combogrid" label="已有产品:" style="width:300px;"
				data-options="    
			            panelWidth:'auto', 
			            prompt:'选择产品',  
			            idField:'id', 
			            iconCls:'icon-search',
			            textField:'name',
			            method:'get',
			            url:'product/data',   
			            columns:[[    
			                {field:'name',title:'姓名',width:100},
			                {title:'供应商',field:'supplier',width:100,align:'center',
					        	formatter:function(value,row,index){
					        		return value.name;
					        }},
					        {title:'成本价',field:'costPrice',width:50,formatter:function(value,row,index){
				        		return value+' ￥';
				       		}}   
			            ]]    
			        "></select>
	</div>    
    <script type="text/javascript">
    	$(function(){
    		var chaseDetailGrid;
    		var states = {NOSTORAGE:"未入库",STORAGED:"已入库"};
    		chaseDetailGrid=$("#chaseDetailGrid").datagrid({
				border:false,
				url:'purchase/detail/list/'+'${pur.id}',
				singleSelect:true,
				rownumbers:true,
				fitColumns:true,
				//onLoadSuccess: compute,//加载完毕后执行计算  
				height:400,
				/* 显示分页工具栏 */
				pagination:true,
				/* 奇偶行使用不同背景色 */
				/* striped:true, */
				idField:'id',
			    toolbar:"#chaseDetailToolbar",
			    columns:[[
			    	{title:'产品名',field:'name',width:50},
			        {title:'产品图片',field:'image',width:50,
			        	formatter:function(value,row,index){
			        		return '<a id="pimg" href="javascript:void(0);" onclick="bigImg('+"'"+ value +"'"+ ');"><img src="' + value + '" alt="无图片" title="点击查看大图" style="height:38px;width:50px;"></a>';
			        }},
			        {title:'供应商',field:'supplier',width:100,align:'center',
			        	formatter:function(value,row,index){
			        		return value.name;
			        }},
			        {title:'状态',field:'state',width:50,formatter:function(value,row,index){
		        		return states[value];
		      		}},
			        {title:'数量',field:'count',width:50},
			        {title:'成本价',field:'costPrice',width:50,formatter:function(value,row,index){
		        		return "￥"+value;
		       		}},
			        {title:'采购小计',field:'money',width:50,formatter:function(value,row,index){
		        		return "￥"+value;
		       		}}
			    ]]
			});
    		
/*     		function compute() {//计算函数  
    		     var rows = chaseDetailGrid.datagrid('getRows')//获取当前的数据行  
    		     var count = 0 ,cost = 0 ,money = 0;
    		     for (var i = 0; i < rows.length; i++) {  
    		    	 count += rows[i]['count']; 
    		    	 cost += rows[i]['costPrice'];
    		    	 money += rows[i]['money'];
    		     }
    		     chaseDetailGrid.datagrid('appendRow', { name: '<b>统计：</b>',costPrice: cost,money: money,
    		    	 count: count,state: '',supplier: ''}); 
    		} */
    		
    		$("#product-purchase").combogrid({  
    		    onSelect: function (index, row) {  
    		    	if(row){
    					var dialog = $("<div/>").dialog({
    						title: "编辑产品",
    						width: 450,
    						height: 400,
    						modal:true,
    						href:'purchase/detail/product/'+'${pur.id}/'+row.id,
    						onClose: function(){
    							//关闭窗户后，立即销毁窗口
    							$(this).dialog("destroy");
    						},
    						buttons:[
    							{
    								iconCls:"icon-ok",
    								text:"保存",
    								handler:function(){

    									var uploader = $("#uploader");
    									var form = $("#productForm");
    									var file = $("#upimg").get(0).files[0];
    									console.log(file);
    									
    									if(file){
    										var formData = new FormData();
    										formData.append("upload_file",file);
    										$.ajax({
    											url:"product/upload",
    											type:"POST",
    											data:formData,
    											contentType:false,
    											processData:false,
    											success:function(data){
    												if(data.success){
    													form.append('<input type="hidden" name="image" value="'+data.file_path+'">');
    													//校验表单数据
    													if(form.form("validate")){
    														$.post("purchase/detail/save",form.serialize(),function(rs){
    															if(rs.err){
    																alert(rs.err);
    															}else{
    																//关闭当前窗口
    																dialog.dialog("close");
    																chaseDetailGrid.datagrid("reload");
    															}
    														});
    													}
    												}else{
    													alert(data.msg);
    												}
    											}
    										});
    									}else{
    										form.append('<input type="hidden" name="image" value="static/image/product/null.png">');
    										//校验表单数据
    										if(form.form("validate")){
    											$.post("purchase/detail/save",form.serialize(),function(rs){
    												if(rs.err){
    													alert(rs.err);
    												}else{
    													//关闭当前窗口
    													dialog.dialog("close");
    													chaseDetailGrid.datagrid("reload");
    												}
    											});
    										}
    										}
    									}
    								},
    								{
    									iconCls:"icon-cancel",
    									text:"取消",
    									handler:function(){
    										dialog.dialog("close");
    									}
    								}
    							]
    					});
    				}else{
    					alert("请选择要操作的行");
    				}
    		        $("#product-purchase").combogrid('setValue', row.name);
    		    }
    		});
    		
			$("#chaseDetailToolbar").on("click","a.add",function(){
				var dialog = $("<div/>").dialog({
					title: "采购新产品",
					width: 450,
					height: 400,
					modal:true,
					href:'purchase/detail/form/'+'${pur.id}',
					onClose: function(){
						//关闭窗户后，立即销毁窗口
						$(this).dialog("destroy");
					},
					buttons:[
						{
							iconCls:"icon-ok",
							text:"保存",
							handler:function(){

								var uploader = $("#uploader");
								var form = $("#productForm");
								var file = $("#upimg").get(0).files[0];
								console.log(file);
								
								if(file){
									var formData = new FormData();
									formData.append("upload_file",file);
									$.ajax({
										url:"product/upload",
										type:"POST",
										data:formData,
										contentType:false,
										processData:false,
										success:function(data){
											if(data.success){
												form.append('<input type="hidden" name="image" value="'+data.file_path+'">');
												//校验表单数据
												if(form.form("validate")){
													$.post("purchase/detail/save",form.serialize(),function(rs){
														if(rs.err){
															alert(rs.err);
														}else{
															//关闭当前窗口
															dialog.dialog("close");
															chaseDetailGrid.datagrid("reload");
														}
													});
												}
											}else{
												alert(data.msg);
											}
										}
									});
								}else{
									form.append('<input type="hidden" name="image" value="static/image/product/null.png">');
									//校验表单数据
									if(form.form("validate")){
										$.post("purchase/detail/save",form.serialize(),function(rs){
											if(rs.err){
												alert(rs.err);
											}else{
												//关闭当前窗口
												dialog.dialog("close");
												chaseDetailGrid.datagrid("reload");
											}
										});
									}
								}
							}
						},
						{
							iconCls:"icon-cancel",
							text:"取消",
							handler:function(){
								dialog.dialog("close");
							}
						}
					]
				});
			}).on("click","a.edit",function(){
				var row = chaseDetailGrid.datagrid("getSelected");
				if(row){
					var dialog = $("<div/>").dialog({
						title: "编辑产品",
						width: 450,
						height: 400,
						modal:true,
						href:'purchase/detail/form/'+'${pur.id}/'+row.id,
						onClose: function(){
							//关闭窗户后，立即销毁窗口
							$(this).dialog("destroy");
						},
						buttons:[
							{
								iconCls:"icon-ok",
								text:"保存",
								handler:function(){

									var uploader = $("#uploader");
									var form = $("#productForm");
									var file = $("#upimg").get(0).files[0];
									console.log(file);
									
									if(file){
										var formData = new FormData();
										formData.append("upload_file",file);
										$.ajax({
											url:"product/upload",
											type:"POST",
											data:formData,
											contentType:false,
											processData:false,
											success:function(data){
												if(data.success){
													form.append('<input type="hidden" name="image" value="'+data.file_path+'">');
													//校验表单数据
													if(form.form("validate")){
														$.post("purchase/detail/save",form.serialize(),function(rs){
															if(rs.err){
																alert(rs.err);
															}else{
																//关闭当前窗口
																dialog.dialog("close");
																chaseDetailGrid.datagrid("reload");
															}
														});
													}
												}else{
													alert(data.msg);
												}
											}
										});
									}else{
										form.append('<input type="hidden" name="image" value="static/image/product/null.png">');
										//校验表单数据
										if(form.form("validate")){
											$.post("purchase/detail/save",form.serialize(),function(rs){
												if(rs.err){
													alert(rs.err);
												}else{
													//关闭当前窗口
													dialog.dialog("close");
													chaseDetailGrid.datagrid("reload");
												}
											});
										}
										}
									}
								},
								{
									iconCls:"icon-cancel",
									text:"取消",
									handler:function(){
										dialog.dialog("close");
									}
								}
							]
					});
				}else{
					alert("请选择要操作的行");
				}
			}).on("click","a.delete",function(){
				var row = chaseDetailGrid.datagrid("getSelected");
				if(row){
					$.messager.confirm('删除提示', '确认删除此记录?', function(r){
		                if (r){
		                    $.get("purchase/detail/delete/"+row.id,function(){
		                    	chaseDetailGrid.datagrid("reload");
		                    }).error(function(){
		                    		$.messager.alert("提示","系统错误,删除失败!");
		                    });
		                }
		            });
				}else{
					alert("请选择要操作的行");
				}
			});
			
			$('#chaseDetailSearch').searchbox({
				searcher : function(value, name) {
					alert(value + "," + name)
				},
				width : 300,
				menu : '#proSitem',
				prompt : '请输入查找关键字'
			});
    	});
    	function bigImg(value){
    		var dialog = $("<div/>").dialog({
    			width:400,
    			height:441,
    			border:false,
    			modal:true,
    			noheader:true,
    			maximizable:true,
    			revert:true,
    			onClick:function(){
    				dialog.dialog("close");
    			},
    			onClose: function(){
    				//关闭窗户后，立即销毁窗口
    				$(this).dialog("destroy");
    			},

    			content:'<img alt="加载失败" title="点击图片关闭大图" style="text-align: center;width:400px;height:400px" src="' + value + '" >'
    		});
    		dialog.click(function(){
    			dialog.dialog("close");
    		});
    	}
    </script>