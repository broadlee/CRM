<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>    
<form id="productForm" method="post" style="margin:0;padding:10px 30px">
	<input type="hidden" name="id" value="${pro.id }"/>
	<c:if test="${not empty pur }">
		<input type="hidden" name="pid" value="${pur.id }"/>
	</c:if>
    <div style="margin-bottom:10px">
		<input name="name" class="easyui-textbox" required="true" label="产品名:" style="width:350px" value="${pro.name }">
    </div>
    
    <div style="margin-bottom:10px">
        <select name="state"  class="easyui-combobox" 
        editable="false" panelHeight="auto" label="状态:" style="width:350px" 
        value="${pro.state }">
        <c:choose>
        	<c:when test="${not empty pur }">
        		<option value="NOSTORAGE">未入库</option>
    			<option value="STORAGED">已入库</option>
        	</c:when>
        	<c:otherwise>
        		<option value="NORMAL">正常</option>
    			<option value="LACK">缺货</option>
    			<option value="HALT">停售</option>
        	</c:otherwise>
        </c:choose>
        </select>
    </div>
    <div style="margin-bottom:10px;width:380px" >
				<select id="product-supp"
				class="easyui-combogrid" style="width:300px;"
				data-options="    
			            panelWidth:'auto', 
			            prompt:'选择供应商',  
			            idField:'id', 
			            iconCls:'icon-search',
			            textField:'name',
			            method:'get',
			            fitColumns:true, 	             
			            url:'purchase/supp/data',   
			            columns:[[    
			                {field:'name',title:'姓名',width:100},
			                {field:'location',title:'所在地',width:100}  
			            ]]    
			        "></select><br>
			        <input id="pro-supp1"
				label="供应商:" editable="false" style="width:350px;"
				required="true" class="easyui-textbox" value="${pro.supplier.name }"> 
				<input id="pro-supp2" name="suppId" type="hidden" value="${pro.supplier.id }">
    </div>
    <div style="margin-bottom:10px">
    	<c:choose>
    		<c:when test="${not empty pur }">
    			<input name="count" class="easyui-numberbox" label="数量:" style="width:350px" value="${pro.count }">
    		</c:when>
    		<c:otherwise>
        		<input name="stock" class="easyui-numberbox" label="数量:" style="width:350px" value="${pro.stock }">
       		</c:otherwise>
        </c:choose>
    </div>
	<div style="margin-bottom:10px">
        <input name="costPrice" class="easyui-textbox" required="true" label="成本价:" style="width:350px" value="${pro.costPrice }">
    </div>
    <c:if test="${ empty pur }">
    <div style="margin-bottom:10px">
        <input name="sellPrice" class="easyui-textbox" required="true" label="销售价:" style="width:350px" value="${pro.sellPrice }">
    </div>
    </c:if>
    <div class="upload" style="margin-bottom:10px">
		<div class="btn uploadBtn" id="uploader">
			<label class="textbox-label textbox-label-before" style="text-align: left; height: 25px; line-height: 25px;">产品图片:</label>
			<input id="upimg" accept="image/*" class="file" type="file" />
			<c:if test="${not empty pro }">
				<br><br><span style="color: red;">选择图片上传会覆盖原图片</span>
			</c:if>
		</div>
		<div id="show" style="text-align: center;">
		<c:choose>
			<c:when test="${ not empty pro && pro.image != 'static/image/product/null.png' }">
				<span >${pro.image.split("/")[3] }</span>
				<img alt="加载失败" src='${pro.image }' style="width:350px;">
			</c:when>
			<c:otherwise><span >无图片</span></c:otherwise>
		</c:choose>
		</div>
	</div>
</form>
<script>
$(function() {
	var upimg = document.querySelector('#upimg');
	upimg.addEventListener('change', function(e){
	    var files = this.files;
	    if(files.length){
	    	// 对文件进行处理，下面会讲解checkFile()会做什么
	        checkFile(this.files);
	    }
	});
	
	function checkFile(files){
		var file = files[0];
		var reader = new FileReader();
		// show表示<div id='show'></div>，用来展示图片预览的
	    // onload是异步操作
		reader.onload = function(e){
			show.innerHTML = '<img src="'+e.target.result+'" style="width:350px;" alt="img">';
		}
		reader.readAsDataURL(file);
	}
	
	$("#product-supp").combogrid({  
	    onSelect: function (index, row) {  
	        $("#pro-supp2").val(row.id);//隐藏域的值
	        $("#pro-supp1").textbox("setText",row.name);
	        $("#product-supp").combogrid('setValue', ''); 
	    }
	});
	
});
</script>