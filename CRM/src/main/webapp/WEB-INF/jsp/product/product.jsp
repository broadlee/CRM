<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<table id="productGird" ></table>

<div id="proToolbar">
    <a class="easyui-linkbutton add" iconCls="icon-add" plain="true">新增</a>
    <a class="easyui-linkbutton edit" iconCls="icon-edit" plain="true">编辑</a>
    <a class="easyui-linkbutton delete" iconCls="icon-remove" plain="true">删除</a>
    <input id="productSear"></input>
	<div id="pp" style="width:100px">
	    <div data-options="name:'name',iconCls:'icon-ok'">产品名</div>
	    <div data-options="name:'supplier.name'">供应商</div>
	</div>
    <input id="productState" name="state" class="easyui-combobox" editable="false" panelHeight="auto" style="width:100px;" />
    <a href="javascript:void(0)" class="easyui-linkbutton all"
		iconCls="icon-reload" plain="true">刷新</a> 
</div>

<script type="text/javascript"
	src="static/js/product/product.js"></script>