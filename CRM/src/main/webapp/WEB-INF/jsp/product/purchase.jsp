<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<table id="purchaseGird" ></table>
<div id="purchaseToolbar">
    <a class="easyui-linkbutton add" iconCls="icon-add" plain="true">添加采购单</a>
    <a class="easyui-linkbutton edit" iconCls="icon-edit" plain="true">编辑采购单</a>
    <a class="easyui-linkbutton look" iconCls="icon-search" plain="true">查看采购详情</a>
    <a class="easyui-linkbutton editDetail" iconCls="icon-print" plain="true">编辑采购详情</a>
	<input id="purchaseSearch"></input>
	<div id="purSearchitem" style="width: 120px">
		<div data-options="name:'theme',iconCls:'icon-ok'">采购主题</div>
		<div data-options="name:'chaseId',iconCls:'icon-ok'">采购单号</div>
		<div data-options="name:'member.name',iconCls:'icon-ok'">创建人</div>
	</div>
	<input id="purchaseState" name="state" class="easyui-combobox" editable="false" panelHeight="auto" style="width:100px;" />
	<a href="javascript:void(0)" class="easyui-linkbutton all"
		iconCls="icon-reload" plain="true">刷新</a> 
		<span style="color: gray;">注：已付款无法编辑</span>
</div>

<script type="text/javascript"
	src="static/js/product/purchase.js"></script>