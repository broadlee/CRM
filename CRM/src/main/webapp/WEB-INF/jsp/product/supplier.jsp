<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<table id="supplierGird" ></table>
<div id="supplierToolbar">
	<a class="easyui-linkbutton add" iconCls="icon-add" plain="true">添加供应商</a>
	<a class="easyui-linkbutton edit" iconCls="icon-edit" plain="true">编辑供应商</a>
    <a class="easyui-linkbutton delete" iconCls="icon-remove" plain="true">删除供应商</a>
	<input class="easyui-searchbox" menu='#searchitem' id="suppSearch" data-options="prompt:'请输入关键字',width:250,searcher:doSearch"/>
	<div id="suppSearchitem">
		<div data-options="name:'name',iconCls:'icon-ok'">供应商名</div>
		<div data-options="name:'linkMan',iconCls:'icon-ok'">联系人</div>
		<div data-options="name:'linkWay',iconCls:'icon-ok'">联系人电话或方式</div>
		<div data-options="name:'location',iconCls:'icon-ok'">地址</div>
		<div data-options="name:'describes',iconCls:'icon-ok'">描述</div>
	</div>
	<a href="javascript:void(0)" class="easyui-linkbutton all"
		iconCls="icon-reload" plain="true">刷新</a> 
</div>
<script type="text/javascript" src="static/js/product/supplier.js"></script>