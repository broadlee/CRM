<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>    
<form id="supplierForm" method="post" style="margin:0;padding:10px 30px">
	<div style="margin-bottom:10px">
		<input type="hidden" name="id" value="${r.id }"/>
	</div>
	<div style="margin-bottom:10px">
        <input name="describes" class="easyui-textbox" label="描述:" style="width:350px" value="${r.describes }">
    </div>
    <div style="margin-bottom:10px">
    	<input name="name" class="easyui-textbox" label="供应商名:" style="width:350px" value="${r.name }">
    </div>
    <div style="margin-bottom:10px">
    	<input name="linkMan" class="easyui-textbox" label="联系人:" style="width:350px" value="${r.linkMan }">
    </div>
    <div style="margin-bottom:10px">
    	<input name="linkWay" class="easyui-textbox" label="联系人联系方式:" style="width:350px" value="${r.linkWay }">
    </div>
    <div style="margin-bottom:10px">
        <input name="location" class="easyui-textbox" label="地址:" style="width:350px" value="${r.location }">
    </div>
   
    
</form>