<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="purchaseDetails" style="padding: 10px 20px;font-weight: bold; ">
	<div style="margin:2px 0px;"><label>采购单编号：</label><span>${pur.id}</span></div>
	<div style="margin:2px 0px;"><label>主题：</label><span>${pur.theme }</span></div>
	<div style="margin:2px 0px;"><label>采购人：</label><span>${pur.member.name }</span></div>
	<div style="margin:2px 0px;"><label>总金额：</label><span>￥${pur.total }</span></div>
	<div style="margin:2px 0px;">
	<label>采购单状态：</label>
	<span>
		<c:if test="${pur.state=='NOPAY' }">
			未付款
		</c:if>
		<c:if test="${pur.state=='PAYED' }">
			已付款
		</c:if>
		<c:if test="${pur.state=='NODETAIL' }">
			无产品
		</c:if>
	</span>
	</div>
	<div style="margin:2px 0px;"><label>采购时间：</label><span>${pur.date }</span></div>
	<div style="margin:2px 0px;"><label>最晚到货时间：</label><span>${pur.lateTime }</span></div>
	<div style="border-bottom:1px solid gray"></div>

	<table width="100%">
		<tr>
		<td>产品名</td>
		<td>供应商</td>
		<td>数量</td>
		<td>成本价</td>
		<td>小计</td>
		</tr>
		<c:forEach items="${rd}" var="item">
		<tr>
			<td>${item.name}</td>
			<td>${item.supplier.name}</td>			
			<td>${item.count }</td>
			<td>￥${item.costPrice}</td>
			<td>${item.money}</td>
		</tr>
		</c:forEach>
	</table>
	<div style="border-bottom:1px solid gray"></div>
	<table width="100%">
	<tr>
		<td width="83%">总金额：</td>
		<td>￥${pur.total }</td>
	</tr>
	</table>
</div>
