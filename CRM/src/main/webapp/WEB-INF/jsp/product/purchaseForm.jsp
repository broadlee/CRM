<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>    
<form id="purchaseForm" method="post" style="margin:0;padding:10px 30px">

	<div style="margin-bottom:10px">
		<input type="hidden" name="id" value="${pur.id }">
	</div>
	
    <div style="margin-bottom:10px">
    	<input name="theme" class="easyui-textbox" required="true" label="采购主题:" style="width:350px" value="${pur.theme }">
    </div>
    <div style="margin-bottom:10px">
    	<input name="chaseId" class="easyui-textbox" required="true" label="采购单号:" style="width:350px" value="${pur.chaseId }">
    </div>
    <div style="margin-bottom:10px">
        <select name="mbId" class="easyui-combobox" editable="false" panelHeight="auto" label="采购人:" style="width:350px">
        	<c:forEach items="${mbs }" var="mb">
       			<option  ${pur.member.id==mb.id?'selected':'' } value="${mb.id }">${mb.account }</option>
       		</c:forEach>
        </select>
    </div>
    
    <div style="margin-bottom:10px">
        <input name="total" class="easyui-textbox" readonly="true" label="总金额:" style="width:350px" value="${pur.total }">
    </div>
    <div style="margin-bottom:10px">
        <select name="state"  class="easyui-combobox" 
        editable="false" panelHeight="auto" label="状态:" style="width:350px" 
        value="${pur.state }">
       			<option value="NODETAIL">无产品</option>
       			<option value="NOPAY">未付款</option>
       			<option value="PAYED">已付款</option>
        </select>
    </div>
    <div style="margin-bottom:10px">
		<input name="date" class="easyui-datetimebox" required="true" label="采购时间:" style="width:350px" value="${pur.date }">
    </div>
    
    <div style="margin-bottom:10px">
    	<input name="lateTime" class="easyui-datetimebox" required="true" label="最晚到货时间:" style="width:350px" value="${pur.lateTime }">
    </div>
    
</form>