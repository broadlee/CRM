<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
<form id="customerForm" method="post" style="margin: 0; padding: 10px 30px">
	<input type="hidden" name="id" value="${customer.id }"/>
	<div style="margin: 20px 0 20px 25px">
		<input name="name" class="easyui-textbox" required="true"
			label="客户"  style="width: 280px" value="${customer.name }">
	</div>
	<div style="margin:0 0 20px 25px">
		<input name="linkMan" class="easyui-textbox" required="true"
			label="联系人"  style="width: 280px" value="${customer.linkMan }">
	</div>
	<div style="margin:0 0 20px 25px">
		<input name="linkWay" class="easyui-textbox" required="true"
			label="联系方式"  style="width: 280px" value="${customer.linkWay }">
	</div>
	<div style="margin:0 0 20px 25px">
		<input name="email" class="easyui-textbox" required="true"
			label="邮箱"  style="width: 280px" value="${customer.email }">
	</div>
	<div style="margin:0 0 20px 25px">
		<input name="location" class="easyui-textbox" required="true"
			label="地址"  style="width: 280px" value="${customer.location }">
	</div>
	<div style="margin:0 0 20px 25px">
		<input name="createDate" class="easyui-textbox" required="true"
			label="创建日期" readonly="readonly"  style="width: 280px"
			value='<fmt:formatDate pattern="yyyy-MM-dd hh:mm:ss" value="${customer.createDate }"/>'>
	</div>
	<div style="margin:0 0 20px 25px">
		<input name="lifeTime" class="easyui-textbox" required="true"
			label="生命周期" readonly="readonly"  style="width: 280px"
			value="${customer.lifeTime }">
	</div>
</form>
