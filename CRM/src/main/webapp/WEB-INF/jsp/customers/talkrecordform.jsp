<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<form id="ralkRecordForm" method="post" style="margin: 0; padding: 10px 30px">

	<input type="hidden" name="cid" value="${customersId }"/>
	<input type="hidden" name="id" value="${record.id }"/>
	<div style="margin: 20px 0 25px 60px">
		<input name="place" class="easyui-textbox" required="true" \
			label="交谈地址" labelWidth="70px"  style="width: 250px"
			data-options="<c:if test="${record.place !=null }">value:'${record.place }',readonly:true</c:if>">
	</div>
	<div style="margin: 20px 0 25px 60px">
		<input class="easyui-datetimebox" name="date" data-options="
				editable:false,
				required:true,<c:if test="${record.date !=null }">value:'${record.date }',readonly:true</c:if>"
				label="交谈日期" labelWidth="70px"  style="width: 250px">
	</div>
	<div style="margin: 20px 0 25px 60px">
		<input name="summary" class="easyui-textbox" required="true"
			label="交谈概要" labelWidth="70px"  style="width: 300px"
			data-options="<c:if test="${record.summary !=null }">value:'${record.summary }',readonly:true</c:if>">
	</div>
	<div style="margin: 20px 0 20px 60px">
		<input name="detail" class="easyui-textbox" required="true"
			label="交谈具体" labelWidth="70px" style="width:300px;height:80px;line-height:70px;" multiline="true"
			data-options="<c:if test="${record.detail !=null }">value:'${record.detail }',readonly:true</c:if>">
	</div>
</form>