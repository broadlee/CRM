<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<table id="customers">
	
</table>
<div id="customersToolsBar">
	<input id="customerSearch"></input>
	<div id="searchitem" style="width: 120px">
		<div data-options="name:'name'">客户名称</div>
	</div>
	<select id="lifeTime" class="easyui-combobox"
				labelWidth="80" panelHeight="auto" name="lifeTime" style="width: 180px;"
				label="生命周期:" data-options="editable:false,onChange:getCustomersByLifeTime">
					<option value="">(空)</option>
					<option value="NO">暂无需求</option>
					<option value="LATENT">潜在客户</option>
					<option value="SIGN">签约客户</option>
					<option value="REPEAT">重复购买</option>
					<option value="LOSE">失效</option>
	</select>
    <a class="easyui-linkbutton edit" iconCls="icon-edit" plain="true">编辑客户信息</a>
    <a class="easyui-linkbutton add" iconCls="icon-add" plain="true">添加交往记录</a>
    <a class="easyui-linkbutton search" iconCls="icon-search" plain="true">查看历史订单</a>
    <a class="easyui-linkbutton delete" iconCls="icon-remove" plain="true">删除</a>
    <a class="easyui-linkbutton reload" iconCls="icon-reload" plain="true">刷新</a>
</div>

<link href="static/css/customers/customerform.css" rel="stylesheet" type="text/css" />
<script src="static/js/customers/customers.js"></script>