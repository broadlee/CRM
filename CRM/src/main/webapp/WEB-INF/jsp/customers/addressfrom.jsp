<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<script type="text/javascript">

	function switchBtn(value, row, index){
		var switchbtn = "<input name='moren' class='switchBtn' data-cid='${customersId}' data-id='"+row.id+"' data-options='checked:"+value+"' />";
		return switchbtn;
	}
	
</script>
<script src="static/js/customers/address.js"></script>
<form id="addressForm" method="post" style="padding-left:50px;">
	<input type="hidden" name="cid" value="${customersId}"/>
	<input id="addrid" type="hidden" name="id" value="${addr.id }"/>
	<div style="margin: 20px 0 10px 0;display: inline-block;">
		<input id="receiver" name="receiver" class="easyui-textbox" required="true"
			label="收货人" labelWidth="70px" labelAlign="center" style="width: 280px" 
			value="${addr.receiver }">
	</div>
	<div style="margin: 20px 0 10px 50px;display: inline-block;">
		<input name="concact" class="easyui-textbox" required="true"
			label="联系方式" labelWidth="70px" labelAlign="center" style="width: 280px" 
			value="${addr.concact }">
	
	</div>
	<div style="margin: 20px 0 10px 0;display: inline-block;">
		<input name="codes" class="easyui-textbox" required="true"
			label="邮编" labelWidth="70px" labelAlign="center" style="width: 280px;margin-right: 30px;" 
			value="${addr.codes }">
	</div>
	<div style="margin: 20px 0 10px 50px;display: inline-block;">
		<input name="address" class="easyui-textbox" required="true" label="收货地址"
		 	labelWidth="70px" labelAlign="center" style="width:280px;height:60px;" 
		 	multiline="true" value="${addr.address }">
	</div>
</form>
<div class="easyui-tabs" border="false" style="width:100%;">
	<div title="所有收货地址">
		<div id="editBar">
			<a class="easyui-linkbutton edit" iconCls="icon-edit" plain="true">编辑</a>
			<a class="easyui-linkbutton delete" iconCls="icon-remove" plain="true">删除</a>
		</div>
		<table id="addrTable" style="width:798px;height:365px;" class="easyui-datagrid" data-options="
						idField:'id',
						url:'customers/addrlist/${customersId} ',
						textField:'name',
						toolbar: '#editBar',
						singleSelect: true,
						border:false,
						pagination:true,
						fitColumns: true,
						onLoadSuccess:onload,
						rownumbers: true,
						autoRowHeight: true">
			<thead>
				<tr>
					<th style="width:80px;" data-options="field:'receiver'">收货人</th>
					<th style="width:140px;" data-options="field:'concact'">联系方式</th>
					<th style="width:100px;" data-options="field:'codes'">邮编</th>
					<th style="width:360px;" data-options="field:'address'">收货地址</th>
					<th style="width:100px;text-align:center;" data-options="field:'moren',formatter:switchBtn">默认开关</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
