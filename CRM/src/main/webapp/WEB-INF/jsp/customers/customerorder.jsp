<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<script src="static/js/customers/customerorder.js"></script>
<script type="text/javascript">

	var state = {XIA:"已下单", PAY:"已付款", SEND:"已发货", DONE:"已完成", CANCEL:"已取消"}
	
	function orderState(value, row, index){
		return state[value];
	}
	
</script>
<div class="easyui-tabs" border="false" style="width:100%;">
	<div title="所有订单">
		<div id="editBar">
			<a class="easyui-linkbutton search" iconCls="icon-search" plain="true">查看明细</a>
		</div>
		<table id="orderTable" style="width:100%;height:500px;"  class="easyui-datagrid" data-options="
						idField:'id',
						url:'customers/orders/${customersId} ',
						textField:'name',
						toolbar: '#editBar',
						singleSelect: true,
						border:false,
						pagination:true,
						fitColumns: true,
						rownumbers: true,
						autoRowHeight: true">
			<thead>
				<tr>
					<th style="width:15%;" data-options="field:'orderId'">订单编号</th>
					<th style="width:15%;" data-options="field:'theme'">主题</th>
					<th style="width:7%;" data-options="field:'member',
						formatter:function(value,row,index){
						return value.name;
						}">创建人</th>
					<th style="width:18%;" data-options="field:'date'">下单时间</th>
					<th style="width:10%;" data-options="field:'state',formatter:orderState">订单状态</th>
					<th style="width:10%;" data-options="field:'total'">总金额</th>
					<th style="width:15%;" data-options="field:'shipment'">物流单号</th>
					<th style="width:10%;" data-options="field:'remark'">备注</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
