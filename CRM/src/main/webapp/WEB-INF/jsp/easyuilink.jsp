<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<base href="${basePath}">
<link href="static/easyui/themes/icon.css" rel="stylesheet" /> 
<link href="static/insdep/insdep.easyui.min.css" rel="stylesheet" />
<link href="static/insdep/icon.css" rel="stylesheet" />
<link href="static/insdep/iconfont.css" rel="stylesheet" />
<script type="text/javascript" src="static/easyui/jquery.min.js"></script>
<script type="text/javascript" src="static/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="static/easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="static/js/timeFormat.js"></script>
<script type="text/javascript" src="static/insdep/insdep.extend.min.js"></script>
<link media="all" rel="stylesheet" type="text/css" href="static/simditor/simditor.css" />
<script type="text/javascript" src="static/simditor/module.js"></script>
<script type="text/javascript" src="static/simditor/uploader.js"></script>
<script type="text/javascript" src="static/simditor/hotkeys.js"></script>
<script type="text/javascript" src="static/simditor/simditor.js"></script>
<script type="text/javascript" src="https://www.jeasyui.com/easyui/datagrid-detailview.js"></script>
<script type="text/javascript" src="https://www.jeasyui.com/easyui/datagrid-scrollview.js"></script>