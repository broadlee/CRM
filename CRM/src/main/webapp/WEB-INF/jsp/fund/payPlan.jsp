<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<table id="payPlanGird"></table>
<div id="payPlanToolbar">
    <input id="search"></input>
	<div id="searchitem" style="width: 120px">
		<div data-options="name:'returns.returnId',iconCls:'icon-ok'">退货单编号</div>
		<div data-options="name:'purchase.chaseId',iconCls:'icon-ok'">采购单编号</div>
	</div>
	 <a class="easyui-linkbutton submit" iconCls="icon-ok" plain="true">确认付款</a>
</div>
<script src="static/js/fund/payPlan.js"></script>