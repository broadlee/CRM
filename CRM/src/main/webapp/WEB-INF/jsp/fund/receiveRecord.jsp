<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<table id="receiveRecordGird"></table>
<div id="receiveRecordToolbar">
    <input class="easyui-searchbox" id="search" data-options="prompt:'请输入关键字',searcher:doSearch" menu='#searchitem' />
	<div id="searchitem" style="width: 120px">
		<div data-options="name:'orders.orderId',iconCls:'icon-ok'">订单编号</div>
		<div data-options="name:'repair.repairId',iconCls:'icon-ok'">维修单编号</div>
	</div>
	<input id="selectType" name="type" class="easyui-combobox" editable="false" panelHeight="auto" style="width:100px;"/>
    <a class="easyui-linkbutton detail" iconCls="icon-details" plain="true">查看详情</a>
</div>
<script src="static/js/fund/receiveRecord.js"></script>