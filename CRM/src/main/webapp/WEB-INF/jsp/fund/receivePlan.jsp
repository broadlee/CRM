<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<table id="receivePlanGird"></table>
<div id="receivePlanToolbar">
    <input id="search"></input>
	<div id="searchitem" style="width: 120px">
		<div data-options="name:'orders.orderId',iconCls:'icon-ok'">订单编号</div>
		<div data-options="name:'repair.repairId',iconCls:'icon-ok'">维修单编号</div>
	</div>
	<a class="easyui-linkbutton submit" iconCls="icon-ok" plain="true">确认回款</a>
</div>
<script src="static/js/fund/receivePlan.js"></script>