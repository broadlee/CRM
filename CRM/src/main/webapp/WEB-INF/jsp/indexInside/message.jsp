<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- <meta http-equiv="refresh" content="1"> -->
<script type="text/javascript">

function newMessage(msgdata){
	for(var index in msgdata){
		
		var msgs = msgdata[index];
		
		for(var index in msgs){
			var msg = msgs[index];	
			var type = msg.type;
			var date,theme,name,money;
			if(msg.order == null){
				date = msg.receiveRecord.date;
				theme = "";
				name = "";
				money = "¥" + msg.receiveRecord.money;
			}else{
				date = msg.order.date;
				theme = msg.order.theme;
				name = msg.order.member.name;
				money = "";
			}
			var _msgul = $("#message");
			var _msgli = $("<li>").appendTo(_msgul);
			var _msgp1 = $("<p>",{'id':'centerTitle'}).text("业务消息："+type).appendTo(_msgli);
			var _msgp2 = $("<p>",{'id':'centerTime'}).text(date).appendTo(_msgli);
			var _msgp3 = $("<p>",{'id':'centerMan'}).text(type+" "+name+" "+money+" "+theme).appendTo(_msgli);
		}
	}
}

var getting = {

        url:'message/list',

        dataType:'json',

        success:function(res) {
			
        	newMessage(res);
        	
        }
}
$.ajax(getting);
/* window.setInterval(function(){$.ajax(getting)},1000); */
</script>
<li id="waiLi">
	<p id="headTitle">新 消 息</p>
	<ul id="message">
		<!-- <li>
			<p id="centerTitle">业务消息：新订单</p>
			<p id="centerTime">04-20 15:31</p>
			<p id="centerMan">新订单 李博 ¥ 5000.0</p>
		</li> -->
	</ul>
</li>