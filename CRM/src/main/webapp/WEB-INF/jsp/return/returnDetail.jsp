<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div id="dam-orderDetails" style="padding: 10px 20px;font-weight: bold; ">
	<div style="margin:2px 0px;"><label>退货单号：</label><span>${r.returnId}</span></div>
	<div style="margin:2px 0px;"><label>退货时间：</label><span>${r.date}</span></div>
	<div style="margin:2px 0px;"><label>应退金额：</label><span>￥${r.returned}</span></div>
	<div style="margin:2px 0px;"><label>已退金额：</label><span>￥${r.refuned}</span></div>
	<div style="margin:2px 0px;">
		<label>状态：</label>
		<span>
		${r.state=="TUING"?"退货中":"已完成" }
		</span>
		</div>
	<div style="border-bottom:1px solid gray"></div>
	<div style="margin:2px 0px;"><label>订单编号：</label><span>${r.orders.id }</span></div>
	<div style="margin:2px 0px;"><label>主题：</label><span>${r.orders.theme }</span></div>
	<div style="margin:2px 0px;"><label>创建人：</label><span>${r.orders.member.name }</span></div>
	<div style="margin:2px 0px;"><label>客户：</label><span>${r.orders.customer.name }</span></div>
	<div style="margin:2px 0px;"><label>下单时间：</label><span>${r.orders.date }</span></div>

	<div style="border-bottom:1px solid gray"></div>
	<div style="margin:2px 0px;">
		<table width="100%">
			<tr>
			<td>退货产品</td>
			<td>单价</td>
			<td>数量</td>
			<td>折扣</td>
			<td>小计</td>

			</tr>
			<c:forEach items="${rd}" var="item">
			<tr>
				<td>${item.product.name}</td>
				<td>￥${item.product.sellPrice}</td>			
				<td>${item.count }</td>
				<td>${item.cost }</td>
				<td>￥${item.subtotal}</td>
			</tr>
			</c:forEach>
		</table>
	</div>
	<div style="border-bottom:1px solid gray"></div>
	<div>
		<table width="100%">
			<tr>
				<td width="76%">总计</td>
				<td>￥${r.returned}</td>
			</tr>
		</table>
	</div>
	
</div>
