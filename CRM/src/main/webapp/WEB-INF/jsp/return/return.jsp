<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<script type="text/javascript">
function returnSearch(value,name){ 

	//alert("name:"+name+"---value"+value);
	 $("#returnDataGrid").datagrid({
		 url:'return/return/list?where='+value
	 });
} 
</script>
<table id="returnDataGrid" class="easyui-datagird"></table>
<div id="returnToolBar" style="padding:10px 10px">

   	<input id="orderCancelSearch" class="easyui-searchbox" style="width:500px" 
	data-options="searcher:returnSearch,prompt:'退货编号/订单编号/订单主题/客户',Width:300, menu:'#returnSearchM'"/>
	<div id="returnSearchM"> 
		<div data-options="name:'where'">条件搜索</div>  
	</div> 
    <a class="easyui-linkbutton delete" iconCls="icon-remove" plain="true">删除</a>
    <a class="easyui-linkbutton customerInfo" iconCls="icon-customerInfo" plain="true">客户信息</a>	
    <a class='easyui-linkbutton returnDetails' iconCls="icon-details" plain="true">明细</a> 
    <a class="easyui-linkbutton" onclick="javascript:$('#returnDataGrid').datagrid('reload')"
	  iconCls="icon-reload" plain="true">刷新</a> 
</div>
<script src="static/js/return/return.js"></script>