<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<table id="applyGird"></table>
<div id="applyToolbar">
    <a id="pass" class="easyui-linkbutton agree" iconCls="icon-ok" plain="true">确认出/入库</a>
    <a id="refuse" class="easyui-linkbutton reject" iconCls="icon-no" plain="true">拒绝出/入库</a>
    <input id="search"></input>
	<div id="searchitem" style="width: 120px">
		<div data-options="name:'orders.orderId',iconCls:'icon-ok'">订单编号</div>
		<div data-options="name:'purchase.chaseId',iconCls:'icon-ok'">采购单编号</div>
	</div>
	<input id="selectState" name="state" class="easyui-combobox" editable="false" panelHeight="auto" style="width:100px;"/>
        
</div>
<script src="static/js/stock/inoutApply.js"></script>