<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<style type="text/css">
    #indexRemind div table{
     margin: 0 20px;
     }
</style>
<div id="indexRemind" class="easyui-tabs" data-options="border:false,tabHeight:45,justified:true,tools: '#RemindTools'">   
   	<div title="客户" style="padding:20px;display:none;">   
		<table style="width=100%">
        	<tr style="width=100%;height:50px">
        		<td><img alt="待处理客户" src="static/image/product/job.png" width="60%"><br>待处理客户</td>
        		<td>
					<c:if test="${not empty ctNO}">
						<a style="color: black;font-size: 20px;font: bolder;" title="待处理客户">${ctNO}</a>
					</c:if>
				</td>
        		<td><img alt="潜在客户" src="static/image/product/job.png" width="60%"><br>潜在客户</td>
        		<td>
					<c:if test="${not empty ctLATENT}">
						<a style="color: black;font-size: 20px;font: bolder;" title="潜在客户">${ctLATENT}</a>
					</c:if>
				</td>
			</tr>
			<tr style="width=100%;height:50px">
				<td><img alt="下单客户" src="static/image/product/job.png" width="60%"><br>下单客户</td>
        		<td>
					<c:if test="${not empty ctSIGN}">
						<a style="color: black;font-size: 20px;font: bolder;" title="下单客户">${ctSIGN}</a>
					</c:if>
				</td>
				<td><img alt="重复购买" src="static/image/product/job.png" width="60%"><br>重复购买</td>
        		<td>
					<c:if test="${not empty ctREPEAT}">
						<a style="color: black;font-size: 20px;font: bolder;" title="重复购买">${ctREPEAT}</a>
					</c:if>
				</td>
        	</tr>
        	<tr style="width=100%;height:50px">
				<td><img alt="今天客户满周年" src="static/image/product/job.png" width="60%"><br>今天客户满周年</td>
        		<td>
					<c:if test="${not empty ctTodayCount}">
						<a style="color: black;font-size: 20px;font: bolder;" title="今天客户满周年">${ctTodayCount}</a>
					</c:if>
				</td>
				<td><img alt="本月客户满周年" src="static/image/product/job.png" width="60%"><br>本月客户满周年</td>
        		<td>
					<c:if test="${not empty ctMonCount}">
						<a style="color: black;font-size: 20px;font: bolder;" title="本月客户满周年">${ctMonCount}</a>
					</c:if>
				</td>
        	</tr>
        </table>  
    </div>
   	<div title="采购" style="padding:20px;display:none;width:100%;height:100%;">  
        <table style="width=100%">
        	<tr style="width=100%;height:50px">
        		<td><img alt="未添加" src="static/image/product/job.png" width="70%"><br>未添加</td>
        		<td><c:if test="${not empty purNodetail}">
						<a style="color: black;font-size: 20px;font: bolder;" title="未添加">${purNodetail}</a>
					</c:if>
        		</td>
        		<td><img alt="未付款" src="static/image/product/job.png" width="70%"><br>未付款</td>
        		<td><c:if test="${not empty purNopay}">
						<a style="color: black;font-size: 20px;font: bolder;" title="未付款">${purNopay}</a>
					</c:if>
				</td>
        	</tr>
        	<tr style="width=100%;height:50px">
        		<td><img alt="未完成采购单" src="static/image/product/job.png" width="70%"><br>未完成采购单</td>
        		<td>
					<c:if test="${not empty purCount}">
						<a style="color: black;font-size: 20px;font: bolder;" title="未完成采购单">${purCount}</a>
					</c:if>
				</td>
        		<td><img alt="完成采购" src="static/image/product/job.png" width="70%"><br>完成采购</td>
        		<td>
					<c:if test="${not empty purPayed}">
						<a style="color: black;font-size: 20px;font: bolder;" title="完成采购">${purPayed}</a>
					</c:if>
				</td>
        	</tr>
        	<tr style="width=100%;height:50px">
        		<td><img alt="计划付款" src="static/image/product/job.png" width="70%"><br>计划付款</td>
        		<td>
					<c:if test="${not empty payPlanCount}">
						<a style="color: black;font-size: 20px;font: bolder;" title="计划付款">${payPlanCount}</a>
					</c:if>
				</td>
        		<td><img alt="回款计划" src="static/image/product/job.png" width="70%"><br>回款计划</td>
        		<td>
					<c:if test="${not empty receivePlanCount}">
						<a style="color: black;font-size: 20px;font: bolder;" title="回款计划">${receivePlanCount}</a>
					</c:if>
				</td>
        	</tr>
        </table>   
    </div>   
    <div title="订单" style="overflow:auto;padding:20px;display:none;">   
		<table style="width=100%">
        	<tr style="width=100%;height:50px">
        		<td><img alt="待付款" src="static/image/product/job.png" width="70%"><br>待付款</td>
        		<td><c:if test="${not empty orderXIA}">
						<a style="color: black;font-size: 20px;font: bolder;" title="待付款">${orderXIA}</a>
					</c:if>
        		</td>
        		<td><img alt="待发货" src="static/image/product/job.png" width="70%"><br>待发货</td>
        		<td><c:if test="${not empty orderPAY}">
						<a style="color: black;font-size: 20px;font: bolder;" title="待发货">${orderPAY}</a>
					</c:if>
				</td>
        	</tr>
        	<tr style="width=100%;height:50px">
        		<td><img alt="待确认收货" src="static/image/product/job.png" width="70%"><br>待确认收货</td>
        		<td>
					<c:if test="${not empty orderSEND}">
						<a style="color: black;font-size: 20px;font: bolder;" title="待确认收货">${orderSEND}</a>
					</c:if>
				</td>
        		<td><img alt="上月完成订单" src="static/image/product/job.png" width="70%"><br>上月完成订单</td>
        		<td>
					<c:if test="${not empty orderLastMonth}">
						<a style="color: black;font-size: 20px;font: bolder;" title="上月完成订单">${orderLastMonth}</a>
					</c:if>
				</td>
        	</tr>
        	<tr style="width=100%;height:50px">
        		<td><img alt="本月完成订单" src="static/image/product/job.png" width="70%"><br>本月完成订单</td>
        		<td>
					<c:if test="${not empty orderMonth}">
						<a style="color: black;font-size: 20px;font: bolder;" title="本月完成订单">${orderMonth}</a>
					</c:if>
				</td>
        	</tr>
        </table>
    </div>   
    <div title="财务" style="padding:20px;display:none;">   
		<table style="width=100%">
        	<tr style="width=100%;height:50px">
        		<td><img alt="计划付款" src="static/image/product/job.png" width="70%"><br>计划付款</td>
        		<td>
					<c:if test="${not empty payPlanCount}">
						<a style="color: black;font-size: 20px;font: bolder;" title="计划付款">${payPlanCount}</a>
					</c:if>
				</td>
        		<td><img alt="回款计划" src="static/image/product/job.png" width="70%"><br>回款计划</td>
        		<td>
					<c:if test="${not empty receivePlanCount}">
						<a style="color: black;font-size: 20px;font: bolder;" title="回款计划">${receivePlanCount}</a>
					</c:if>
				</td>
			</tr>
			<tr style="width=100%;height:50px">
				<td><img alt="本月付款" src="static/image/product/job.png" width="70%"><br>本月付款</td>
        		<td>
					<c:if test="${not empty paySum}">
						<a style="color: black;font-size: 20px;font: bolder;" title="本月付款">${paySum}</a>
					</c:if>
				</td>
				<td><img alt="本月回款" src="static/image/product/job.png" width="70%"><br>本月回款</td>
        		<td>
					<c:if test="${not empty paySum}">
						<a style="color: black;font-size: 20px;font: bolder;" title="本月回款">${paySum}</a>
					</c:if>
				</td>
        	</tr>
        </table>  
    </div>
    <div title="库存" style="padding:20px;display:none;">   
		<table style="width=100%">
        	<tr style="width=100%;height:50px">
        		<td><img alt="待确认出库" src="static/image/product/job.png" width="70%"><br>待确认出库</td>
        		<td>
					<c:if test="${not empty outNodeal}">
						<a style="color: black;font-size: 20px;font: bolder;" title="待确认出库">${outNodeal}</a>
					</c:if>
				</td>
        		<td><img alt="待确认入库" src="static/image/product/job.png" width="70%"><br>待确认入库</td>
        		<td>
					<c:if test="${not empty inNodeal}">
						<a style="color: black;font-size: 20px;font: bolder;" title="待确认入库">${inNodeal}</a>
					</c:if>
				</td>
			</tr>
			<tr style="width=100%;height:50px">
				<td><img alt="入库单" src="static/image/product/job.png" width="70%"><br>入库单</td>
        		<td>
					<c:if test="${not empty inCount}">
						<a style="color: black;font-size: 20px;font: bolder;" title="入库单">${inCount}</a>
					</c:if>
				</td>
				<td><img alt="出库单" src="static/image/product/job.png" width="70%"><br>出库单</td>
        		<td>
					<c:if test="${not empty outCount}">
						<a style="color: black;font-size: 20px;font: bolder;" title="出库单">${outCount}</a>
					</c:if>
				</td>
        	</tr>
        	<tr style="width=100%;height:50px">
        		<td><img alt="拒绝出库" src="static/image/product/job.png" width="70%"><br>拒绝出库</td>
        		<td>
					<c:if test="${not empty outReject}">
						<a style="color: black;font-size: 20px;font: bolder;" title="拒绝出库">${outReject}</a>
					</c:if>
				</td>
				<td><img alt="拒绝入库" src="static/image/product/job.png" width="70%"><br>拒绝入库</td>
        		<td>
					<c:if test="${not empty inReject}">
						<a style="color: black;font-size: 20px;font: bolder;" title="拒绝入库">${inReject}</a>
					</c:if>
				</td>
			</tr>
        </table>  
    </div>
</div>
<div id="RemindTools" >
	<a title="刷新" href="#" class="easyui-linkbutton" plain="true" iconCls="icon-reload"></a>
</div>
<script>
$(function() {
	//刷新
	$("#RemindTools").click(function(){
		var tab = $('#indexRemind').tabs('getSelected');
		tab.panel('refresh');
	});
	
	$("#indexRemind").on("click","div table tr td",function(){
		//跳转到新增订单页面
		if ($('#tabs').tabs("exists", "采购单")) {
			$('#tabs').tabs("select", "采购单")
		} else {
			$('#tabs').tabs('add', {
				title : "采购单",
				href : "purchase",
				closable : true
			});
		}
	});
});
</script>