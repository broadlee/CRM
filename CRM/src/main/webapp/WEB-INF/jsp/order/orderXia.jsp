<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript"> 

		function orderXiaSearch(value,name){ 

			//alert("name:"+name+"---value"+value);
			 $("#orderXiaGird").datagrid({
				 url:'order/order/lists?theme='+value+'&state=XIA'
			 });
		} 
</script> 

<table id="orderXiaGird"></table>
<div id="orderXiaToolbar" style="padding:5px 5px;" >
	<input id="orderXiaSearch" class="easyui-searchbox" style="width:400px" 
	data-options="searcher:orderXiaSearch,prompt:'订单编号/主题/备注/客户',menu:'#orderXiaSearchM',Width:300"/>
	<div id="orderXiaSearchM" > 
		<div data-options="name:'XIA'">已下单</div>  
	</div> 
    <a class="easyui-linkbutton add" iconCls="icon-add" plain="true">新增</a>
    <a class="easyui-linkbutton edit" iconCls="icon-edit" plain="true">编辑订单</a>
    <a class="easyui-linkbutton editDetail" iconCls="icon-detailEdit" plain="true">编辑订单详情 </a>
    <a class="easyui-linkbutton cancel" iconCls="icon-cancel" plain="true">取消</a>
    <a class="easyui-linkbutton customerInfo" iconCls="icon-customerInfo" plain="true">客户信息</a>		
    <a class='easyui-linkbutton orderDetails' iconCls="icon-details" plain="true">明细</a> 
    <a class="easyui-linkbutton" onclick="javascript:$('#orderXiaGird').datagrid('reload')"
	  iconCls="icon-reload" plain="true">刷新</a> 
</div>	  


<script src="static/js/order/orderXia.js"></script>
