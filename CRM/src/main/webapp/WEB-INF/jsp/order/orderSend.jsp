<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript"> 

		function orderSendSearch(value,name){ 

			//alert("name:"+name+"---value"+value);
			 $("#orderSendGird").datagrid({
				 url:'order/order/lists?theme='+value+'&state=SEND'
			 });
		} 
</script> 

<table id="orderSendGird"></table>
<div id="orderSendToolbar" style="padding:5px 5px;" >
	<input id="orderSendSearch" class="easyui-searchbox" style="width:400px" 
	data-options="searcher:orderSendSearch,prompt:'订单编号/主题/备注/客户',menu:'#orderSendSearchM',Width:300"/>
	<div id="orderSendSearchM" > 
		<div data-options="name:'SEND'">已发货</div>  
	</div> 
    <a class="easyui-linkbutton add" iconCls="icon-add" plain="true">新增</a>
    <a class="easyui-linkbutton edit" iconCls="icon-edit" plain="true">编辑物流单号</a>
    <a class="easyui-linkbutton customerInfo" iconCls="icon-customerInfo" plain="true">客户信息</a>		
    <a class='easyui-linkbutton orderDetails' iconCls="icon-details" plain="true">明细</a> 
    <a class='easyui-linkbutton orderShipment' iconCls="icon-wuliu" plain="true">物流信息</a>
    <a class="easyui-linkbutton" onclick="javascript:$('#orderSendGird').datagrid('reload')"
	  iconCls="icon-reload" plain="true">刷新</a> 
</div>	  


<script src="static/js/order/orderSend.js"></script>
