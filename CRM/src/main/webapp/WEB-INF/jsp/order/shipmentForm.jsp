<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<form id="orderShipmentForm" action="post" style="padding:5px;" >
	<div><input type="hidden" name="id" value="${o.id }"></div>
	<div style="padding:5px 10px;"><input class="easyui-combobox" style="width:60%" name="expressCompany.id" required="true" label="公司名称" value="${o.expressCompany.id }"
	 data-options="method:'get',
		 editable:false,
		 valueField:'id',
		 textField:'name',
		 url:'order/express/all'
	"></div>
	<div style="padding:5px 10px;"><input class="easyui-textbox" style="width:80%"  name="shipment" required="true" label="快递单号" value="${o.shipment }"></div>
</form>