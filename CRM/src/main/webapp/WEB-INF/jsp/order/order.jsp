<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript"> 
	$(function(){
		$(".orderLink").click(function() {
			var url = $(this).attr("url");
			var title = $(this).text().trim();		
			//先判断选项卡是否存在，存在，激活就行了
			 if ($("#orderTabs").tabs("exists",title)) {
					$("#orderTabs").tabs("select", title);
					return;
			}
			//不存在，则打开
			//添加新选项卡
			var config = {
				title : title,
				href : url,
				closable : true
			};
			$("#orderTabs").tabs("add", config);
	});
});


		function qq(value,name){ 

			//如果搜索全部则赋值为空
			if(name=="all"){
			   name="";
			}
			//alert("name:"+name+"---value"+value);
			 $("#orderGird").datagrid({
				 url:'order/order/lists?theme='+value+'&state='+name
			 });
		} 
</script> 
<style>
.m, .m li {
	list-style: none;
	margin: 0px;
	padding-left: 2px;
}
</style>
<div class="easyui-layout" data-options="fit:true">   
            <div data-options="region:'west'" title="订单列表" style="width:120px">
	            <div class="easyui-accordion" data-options="fit:true, border:false">
		
			
						<ul class="m">
							<li><a url="" text="全部订单"
								class="orderLink easyui-linkbutton"
								data-options="plain:true,iconCls:'icon-all'">
								
								</a>
							</li>
							<li>
								<a url="order/order/xia" text="已下单" class="orderLink easyui-linkbutton"
								data-options="plain:true,iconCls:'icon-xia'">
								</a>
							</li>
							<li>
								<a url="order/order/pay" text="已付款" class="orderLink easyui-linkbutton"
								data-options="plain:true,iconCls:'icon-pay'">
								</a>
							</li>
							<li>
								<a url="order/order/send" text="已发货" class="orderLink easyui-linkbutton"
								data-options="plain:true,iconCls:'icon-send'">
								</a>
							</li>
							<li>
								<a url="order/order/done" text="已完成" class="orderLink easyui-linkbutton"
								data-options="plain:true,iconCls:'icon-ok'">
								</a>
							</li>
							<li>
								<a url="order/order/cancel" text="已取消" class="orderLink easyui-linkbutton"
								data-options="plain:true,iconCls:'icon-cancel'">
								</a>
							</li>
							
						</ul>		
	            </div> 
            </div>
            <div data-options="region:'center'" border="false">
            	<div id="orderTabs" class="easyui-tabs"
					data-options="
		    		fit:true, border:false
		    		">
					<div data-options="title:'全部订单'" style="width:100%;" fit="true" >
				            <table id="orderGird"></table>
							<div id="orderToolbar" style="padding:5px 5px;" >
								<input id="orderSearch" class="easyui-searchbox" style="width:400px" 
								data-options="searcher:qq,prompt:'订单编号/主题/备注/客户',Width:300,menu:'#orderStateSearch'"/>
									<div id="orderStateSearch"> 
										<div data-options="name:'all'">所有订单</div> 
										<div data-options="name:'XIA'">已下单</div> 
										<div data-options="name:'PAY'">已付款</div> 
										<div data-options="name:'SEND'">已发货</div> 
										<div data-options="name:'DONE'">已完成</div>
										<div data-options="name:'CANCEL'">已取消</div>  
									</div> 
								    <a class="easyui-linkbutton add" iconCls="icon-add" plain="true">新增</a>
									<!--  <a class="easyui-linkbutton delete" iconCls="icon-remove" plain="true">删除</a> -->	
									<a class="easyui-linkbutton customerInfo" iconCls="icon-customerInfo" plain="true">客户信息</a>							    
								    <a class='easyui-linkbutton orderDetails' iconCls="icon-details" plain="true">明细</a> 
								    <a class='easyui-linkbutton orderShipment' iconCls="icon-wuliu" plain="true">物流信息</a>
								    <a class="easyui-linkbutton" onclick="javascript:$('#orderGird').datagrid('reload')"
		  							  iconCls="icon-reload" plain="true">刷新</a> 
							</div>	  
					</div>
				</div> 	    
            </div>   
</div>   

<script src="static/js/order/order.js"></script>
