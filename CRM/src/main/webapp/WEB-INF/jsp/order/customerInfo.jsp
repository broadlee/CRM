<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<style>
 #customerInfo td {
 padding:10px;
 padding-left:20px;
 font-weight: bold;
 
 }
</style>
<table id="customerInfo" style="width:100%;" cellpadding="20">
	<tbody>
		<tr>
			<td>客户</td>
			<td>${c.name }</td>
		</tr>
		<tr>
			<td>联系人</td>
			<td>${c.linkMan }</td>
		</tr>
		<tr>
			<td>联系方式</td>
			<td>${c.linkWay }</td>
		</tr>
		<tr>
			<td>生命周期</td>
			<td>
				<c:choose>
					<c:when test="${c.lifeTime=='NO' }">
						暂无需求
					</c:when>
					<c:when test="${c.lifeTime=='LATENT'}">
						潜在客户
					</c:when>
					<c:when test="${c.lifeTime=='SIGN'}">
						签约客户
					</c:when>
					<c:when test="${c.lifeTime=='REPEAT' }">
						重复购买
					</c:when>
					<c:when test="${c.lifeTime=='LOSE' }">
						失效
					</c:when>
					<c:otherwise>
						暂无登记
					</c:otherwise>
				</c:choose>
		
			</td>
		</tr>
		<tr>
			<td>地址</td>
			<td>${c.location }</td>
		</tr>
		<tr>
			<td>邮箱</td>
			<td>${c.email }</td>
		</tr>
	</tbody>
</table>