<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="dam-orderDetails" style="padding: 10px 20px;font-weight: bold; ">
	<div style="margin:2px 0px;"><label>订单编号：</label><span>${o.orderId}</span></div>
	<div style="margin:2px 0px;"><label>主题：</label><span>${o.theme }</span></div>
	<div style="margin:2px 0px;"><label>创建人：</label><span>${o.member.name }</span></div>
	<div style="margin:2px 0px;"><label>客户：</label><span>${o.customer.name }</span></div>
	<div style="margin:2px 0px;"><label>下单时间：</label><span>${o.date }</span></div>
	<div style="margin:2px 0px;">
	<label>订单状态：</label>
	<span>
		<c:if test="${o.state=='XIA' }">
			已下单
		</c:if>
		<c:if test="${o.state=='PAY' }">
			已付款
		</c:if>
		<c:if test="${o.state=='SEND' }">
			已发货
		</c:if>
		<c:if test="${o.state=='DONE' }">
			已完成
		</c:if>
	</span>
	</div>
	<div style="margin:2px 0px;">
		<label>物流单号：</label> 
		<c:if test="${not empty o.expressCompany}">		
			<span>【${o.expressCompany.name}】</span>
		</c:if>
		<span>${o.shipment }</span>
	</div>
	<div style="margin:2px 0px;"><label>备注：</label><span>${o.remark }</span></div>
	<div style="margin:2px 0px;"><label>总金额：</label><span>￥${o.total }</span></div>
	<div style="margin:2px 0px;"><label>收货人：</label><span style="display:inline-block;width:100px;">${o.address.receiver }</span><span style="display:inline-block;">${o.address.concact }</span></div>
	<div style="margin:2px 0px;"><label>收货地址：</label><span>${o.address.address }</span></div>
	<div style="border-bottom:1px solid gray"></div>
	<div style="margin:2px 0px;">
		<table width="100%">
			<tr>
			<td>产品</td>
			<td>单价</td>
			<td>数量</td>
			<td>折扣</td>
			<td>小计</td>
			<td>状态</td>

			</tr>
			<c:forEach items="${od}" var="item">
			<tr>
				<td>${item.product.name}</td>
				<td>￥${item.product.sellPrice}</td>			
				<td>${item.count }</td>
				<td>${item.cost }</td>
				<td>￥${item.subtotal}</td>
<<<<<<< HEAD
				<td>${item.state==0?"退货":"正常"}</td>
=======
				<td>${item.state==0?"退货":"正常"}</td>
>>>>>>> refs/remotes/origin/lb
			</tr>
			</c:forEach>
		</table>
	</div>
	<div style="border-bottom:1px solid gray"></div>
	<div>
		<table width="100%">
			<tr>
				<td width="76%">总计</td>
				<td>￥${o.total }</td>
			</tr>
		</table>
	</div>
</div>
