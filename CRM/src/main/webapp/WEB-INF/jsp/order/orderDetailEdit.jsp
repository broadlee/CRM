<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<script type="text/javascript" src="static/js/order/orderDetailEdit.js"></script>
<input id="orderDetailOrderEdit" type="hidden" value="${order}">
<table id="orderDetailEditGird" class="easyui-datagrid" data-options="
		fit:true,
		border:false,
		fitColumns:true,
		width:'100%',
		rownumbers:true,
		singleSelect:true,
	    url:'order/order/detailList/'+${order },
	    idField:'id',
	    method:'get',
	    toolbar:'#orderDetailEditToolbar',
	    onLoadSuccess:function(data){
	    	var data=data.rows;
	    	if(data==''){
	    		$.messager.alert('提示','该订单还没有添加产品!');
	    	}
	    }

"> 
<thead>
	<tr>
		<th data-options="field:'product',width:200,align:'center',formatter:function(value,row,index){
		        	return row.product? row.product.name:value;
		        }">产品名称</th>
		<th data-options="field:'price',width:220,align:'center',formatter:function(value,row,index){
		        	return row.product? row.product.sellPrice:value;
		        }">单价/元</th>        
		<th data-options="field:'count',align:'center',width:220">数量</th>        
		<th data-options="field:'cost',align:'center',width:220">折扣</th>        
		<th data-options="field:'subtotal',align:'center',width:220">小计/元</th>   
	</tr> 
</thead>
<tbody></tbody>    
</table>
<div id="orderDetailEditToolbar" style="padding:5px 5px;">
	
    <a class="easyui-linkbutton add" iconCls="icon-add" plain="true">新增</a>
    <a class="easyui-linkbutton edit" iconCls="icon-edit" plain="true">编辑</a>
    <a class="easyui-linkbutton delete" iconCls="icon-remove" plain="true">删除</a>	
    <a class="easyui-linkbutton" onclick="javascript:$('#orderDetailEditGird').datagrid('reload')"
    iconCls="icon-reload" plain="true">刷新</a> 
</div>	  
