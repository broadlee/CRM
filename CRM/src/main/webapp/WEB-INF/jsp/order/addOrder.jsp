<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<style>
#order-form tr td {
	padding: 5px 20px;
}
#dg .datagrid-body{
		height:500px;
	}
</style>

<script type="text/javascript">
 $(function(){
	 var selectRow=null;
	 var artChanged;
	 $("#order-c").combogrid({  
		    onChange: function (newValue, oldValue) {  
		        artChanged = true;//记录是否有改变（当手动输入时发生) 
		    }, 
		    onSelect: function (index, row) {  
		        selectRow = row;
		        $("#order-customer2").val(row.id);//隐藏域的值
		        $("#order-customer1").textbox("setText",row.name);
		        $("#order-c").combogrid('setValue', ''); 
		        //加载收获地址
		        $("#order-address").combogrid({
		        	url:"order/order/addressList/"+row.id,
		        	onLoadSuccess:function(data){//加载成功后设计默认地址
		        		data=data.rows;
		        		if(data!=""){		        		
			        		//alert(JSON.stringify(data));
			        		for(var i=0;i<data.length;i++){
			        			if(data[i].moren){
			        				//设计选中
			        				$('#order-address').combogrid('grid').datagrid('selectRecord',data[i].id);
			        			}
			        		}
			        	}else{
			        		$.messager.alert("提示","该客户还没有添加收货地址，请先添加收货地址！");
			        		}
		        	}
		        });
		    
		   	
		    }
		});
	$('#order-address').combogrid({
		onShowPanel:function(){
			 if( $("#order-customer2").val()==""){
				 //隐藏面板
				 $(this).combogrid("hidePanel");
				 $.messager.alert("提示","请先选择客户");
				
			 }
		}
	});
 	
 
 	$("#order-state").combobox({
 		onBeforeLoad: function(param){//判断是否显示物流单号
 			if($(this).combobox('getValue')!="SEND"){
 				$("#order-wuliu").hide();
 			}
 		
 		},
 		 onChange: function (newValue, oldValue) {  
		        if(newValue=="SEND"){
		        	$("#order-wuliu").show();
		        }else{
		        	$("#order-wuliu").hide();
		        }
		    },
 	});
 	//限制只能输入小数
 	$.extend($.fn.textbox.defaults.rules, {  
 	 number : {  
         validator : function(value, param) {  
             return /\b(0(\.\d{1,2})?)|1\b/.test(value);  
         },  
         message : "请输入0-1之间的小数" 
     }
 	});
 
 
 });
</script>
<script type="text/javascript" src="static/js/order/edatagrid.js"></script>

<div class="easyui-panel" title="填写订单信息"  style="overflow-x: hidden;overflow-y: auto;"
		border="false" fit="true" >
<form id="orderForm" action="/order/order/save" method="post">
	<table id="order-form" width="90%" border="0" style="padding: 6px;">
		<tr>
			<td width="50%" colspan="2">
				 <input type="hidden"name="member.id" value="${member.id }"/>
				  <input " label="创建人" editable="false"
				  	value="${member.name }" 
					 style="width: 30%;" required="true"
					class="easyui-textbox" label="主题">
			</td>
		</tr>
		<tr>
			<td width="50%" colspan="2">
				 <input name="theme" label="主题"
					prompt="请输入主题内容" style="width: 80%;" required="true"
					class="easyui-textbox" label="主题">
			</td>
		</tr>
		<tr>
			<td width="50%" colspan="2">
				<input id="order-customer1"
				label="客户" editable="false" style="width: 40%;"
				required="true" class="easyui-textbox"> 
				<input id="order-customer2" name="customer.id" type="hidden">
				
				<select id="order-c"
				class="easyui-combogrid" style="width: 350px;"
				data-options="    
			            panelWidth:'auto', 
			            prompt:'选择客户',  
			            idField:'id', 
			            iconCls:'icon-search',
			            textField:'name',
			            method:'get',
			            panelWidth:500,
			            fitColumns:true, 	             
			            url:'order/order/customerList',   
			            columns:[[    
			                {field:'id',title:'ID',width:20},    
			                {field:'name',title:'姓名',width:100},    
			                {field:'linkWay',title:'联系方式',width:120},    
			                {field:'location',title:'所在地',width:100}   
			            ]]    
			        "></select>
			</td>
		</tr>
		<tr>
			<td colspan="2">	
				<select id="order-address" name="address.id"
				class="easyui-combogrid" style="width: 500px;"
				label="收货地址"
				required="true"    
				data-options=" 				
			            panelWidth:'auto', 
			            prompt:'选择地址',  
			            idField:'id', 
			            textField:'address',
			            method:'get',
			            panelWidth:700,
			            editable:false,
			            fitColumns:true, 	              
			            columns:[[    
			                {field:'id',title:'ID',width:60},    
			                {field:'receiver',title:'收货人姓名',width:100},    
			                {field:'address',title:'收货地址',width:120},    
			                {field:'concact',title:'联系方式',width:100},    
			                {field:'codes',title:'邮编',width:100},   
			                {field:'moren',title:'是否默认',width:100,formatter:function(value,row,index){
			                	if(row&&row.moren){
			                	 return '是';
			                	}else{
			                	return '否';
			                	}
			                }},     
			            ]]    
			        "></select>
			</td>
		</tr>
		<tr>
			<td width="30%">
				<select id="order-state" name="state"
					class="easyui-combobox" style="width: 250px;"
					data-options="panelHeight:'auto'" label="订单状态">
						<option value="XIA" selected="true">已下单</option>
						<option value="PAY">已支付</option>
						<option value="SEND">已发货</option>
						<option value="DONE">已完成</option>
				</select>
			</td>
			<td width="50%">
				<div id="order-wuliu">
					<input name="shipment" id="input-wuliu" class="easyui-textbox"
						type="text" prompt="请输入物流单号" style="width: 300px;" label="物流单号">
				</div>
			</td>
		</tr>
		<tr>
			<td width="50%" colspan="2">
				<input name="remark" prompt="备注内容"
					style="width: 60%;" multiline="true" class="easyui-textbox"
					label="备注">
			</td>
		</tr>
		<tr>
			<td width="50%" colspan="2">
				<input id="orderTotal" name="total" value="0.0"
					style="width: 30%;"  class="easyui-textbox"
					editable="false"
					required="true"
					label="总金额/元">
			</td>
		</tr>
	</table>

</form>

<div class="easyui-tabs" border="false" style="width: 100%;overflow-y: auto;">
	<div title="订单详情" style="overflow-y: auto;">
		<div id="plan_tb">
			<a href="javascript:void(0)" class="easyui-linkbutton"
				data-options="iconCls:'icon-add',plain:true" onclick="append('#dg')">添加产品</a>
			<a href="javascript:void(0)" class="easyui-linkbutton"
				data-options="iconCls:'icon-remove',plain:true"
				onclick="removeit('#dg')">删除</a>
		</div>

		<table style="width: 100%;overflow-x:hidden;" id="dg" class="easyui-datagrid"
			data-options="
						idField:'id',
						textField:'name',
						singleSelect:true,
						fitColumns: true, nowrap: false,
						rownumbers: true, toolbar: '#plan_tb',
						onDblClickCell: onClickCell,
						onClickRow:onClickRow,
						onEndEdit: onEndEdit,
						border: false,
						fit:true
					">
			<thead>
				<tr>
					<th data-options="field:'cz',checkbox:true" style="width:3%"></th>
					<th style="width: 30%;"
						data-options="field:'p_id',textField:'product.name', editor: {type: 'combogrid',options:{	
									idField: 'id',
									panelHeight:250,
									textField: 'name',
									required:true,
									editable:false,
									method:'get',
									url:'order/order/productList',    
						            columns:[[    
						                {field:'id',title:'ID',width:'15%'},    
						                {field:'name',title:'名称',width:'30%'},    
						                {field:'sellPrice',title:'价格/元',width:'15%'},    
						                {field:'stock',title:'库存',width:'20%'},
						                {field:'supplier',title:'供应商',width:'20%',formatter:function(value,row,index){
						                 	if(row.supplier!=null){
						                 	 return row.supplier.name;
						                 	}
						                }}       
						            ]],
						            onSelect:function(index, row){//选中的时候统计价格
						            	//判断表格中是否存在这个商品
										
						            	if(addIfProduct(row.id)){
						            		//获取编辑的行
						            		var edit=editGrid.datagrid('getEditingRowIndexs');
						        			editIndex=edit[0];
							          		//获取价格编辑的单元格
							            	var price = editGrid.datagrid('getEditor', {index:editIndex,field:'price'});
							            	//设置编辑的单元格的价格
											$(price.target).textbox('setValue',row.sellPrice);	
											//获取其他编辑的单元格									
							            	var count = editGrid.datagrid('getEditor', {index:editIndex,field:'count'});
							            	var cost = editGrid.datagrid('getEditor', {index:editIndex,field:'cost'});
							            	var subtotal = editGrid.datagrid('getEditor', {index:editIndex,field:'subtotal'});
							            	//获取编辑单元格的文本
							            	var count1=$(count.target).numberspinner('getText');
							            	var cost1=$(cost.target).textbox('getText');
											var price1=$(price.target).textbox('getText');
							            	//计算金额
											var total=parseInt(count1)*parseFloat(cost1)*parseFloat(price1)/10;
											$(subtotal.target).textbox('setValue',total);	
											//统计总金额
											setTotal(total);
						            	}
						            	}
						              }},
						            formatter: function(value,row,index){
						            		var text;
						            		if(value!=''){
						            					//alert(value);
						            					$.ajax({url:'order/order/product',
										 					async: false,//为同步才能先执行里面的代码,不然会执行$.ajax外面的代码
										 					type: 'get',
															data:{id:value},
															success:function(data){																									
															text=data.name;
																		
															}
															});
														return text;
													}else{
														return value;
													}
															
									}
						            
						            ">产品名</th>
					<th style="width: 15%;"
						data-options="field:'price', editor: {type: 'textbox',options:{
									editable:false,
									min: 1,    
									max: 100 
									}}">单价/元</th>
					<th style="width: 12%;"
						data-options="field:'count', editor: {type: 'numberspinner',options:{
									min: 1,    
									max: 100000,
									onChange: function (newValue, oldValue) { 
											//获取编辑的行
						            		var edit=editGrid.datagrid('getEditingRowIndexs');
						        			editIndex=edit[0];
									   		//获取价格编辑的单元格
							            	var price = editGrid.datagrid('getEditor', {index:editIndex,field:'price'});
											//获取其他编辑的单元格									
							            	var count = editGrid.datagrid('getEditor', {index:editIndex,field:'count'});
							            	var cost = editGrid.datagrid('getEditor', {index:editIndex,field:'cost'});
							            	var subtotal = editGrid.datagrid('getEditor', {index:editIndex,field:'subtotal'});
							            	//获取编辑单元格的文本
							            	var count1=$(count.target).numberspinner('getText');
							            	var cost1=$(cost.target).numberspinner('getText');
											var price1=$(price.target).textbox('getText');
							            	//计算金额
											var total=parseInt(count1)*parseFloat(cost1)*parseFloat(price1)/10;
											$(subtotal.target).textbox('setValue',total);
											//统计总金额
											setTotal(total);
											
									}  
									}}">数量</th>
					<th style="width: 15%;"
						data-options="field:'cost', editor: {type: 'numberspinner', options:{
								  min: 0,    
								  max: 10,
								  precision:1,
								  increment:0.1,
								  required:true,
								  onChange: function (newValue, oldValue) { 
								  			//获取编辑的行
						            		var edit=editGrid.datagrid('getEditingRowIndexs');
						        			editIndex=edit[0];
									   		//获取价格编辑的单元格
							            	var price = editGrid.datagrid('getEditor', {index:editIndex,field:'price'});
											//获取其他编辑的单元格									
							            	var count = editGrid.datagrid('getEditor', {index:editIndex,field:'count'});
							            	var cost = editGrid.datagrid('getEditor', {index:editIndex,field:'cost'});
							            	var subtotal = $('#dg').datagrid('getEditor', {index:editIndex,field:'subtotal'});
							            	//获取编辑单元格的文本
							            	var count1=$(count.target).numberspinner('getText');
							            	var cost1=$(cost.target).numberspinner('getText');
											var price1=$(price.target).textbox('getText');
							            	//计算金额
											var total=parseInt(count1)*parseFloat(cost1)*parseFloat(price1)/10;
											$(subtotal.target).textbox('setValue',total);
											//统计总金额
											setTotal(total);
											
									}
								   
								}}">折扣</th>

					<th style="width: 15%;"
						data-options="field:'subtotal', editor: {type: 'textbox',options:{editable:false}}">小计/元</th>

				</tr>
			</thead>

			<tbody>
			</tbody>
		</table>
		<a class="easyui-linkbutton" iconCls="icon-save" onclick="save()"
			plain="true">保存</a>
	</div>
</div>
</div>


