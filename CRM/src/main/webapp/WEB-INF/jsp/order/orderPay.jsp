<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript"> 

		function orderPaySearch(value,name){ 

			//alert("name:"+name+"---value"+value);
			 $("#orderPayGird").datagrid({
				 url:'order/order/lists?theme='+value+'&state=PAY'
			 });
		} 
</script> 

<table id="orderPayGird"></table>
<div id="orderPayToolbar" style="padding:5px 5px;" >
	<input id="orderPaySearch" class="easyui-searchbox" style="width:400px" 
	data-options="searcher:orderPaySearch,prompt:'订单编号/主题/备注/客户',menu:'#orderPaySearchM',Width:300"/>
	<div id="orderPaySearchM" > 
		<div data-options="name:'PAY'">已付款</div>  
	</div> 
    <a class="easyui-linkbutton add" iconCls="icon-add" plain="true">新增</a>
    <a class="easyui-linkbutton cancel" iconCls="icon-cancel" plain="true">取消</a>
    <a class="easyui-linkbutton customerInfo" iconCls="icon-customerInfo" plain="true">客户信息</a>		
    <a class='easyui-linkbutton orderDetails' iconCls="icon-details" plain="true">明细</a> 
    <a class="easyui-linkbutton" onclick="javascript:$('#orderPayGird').datagrid('reload')"
	  iconCls="icon-reload" plain="true">刷新</a> 
</div>	  


<script src="static/js/order/orderPay.js"></script>
