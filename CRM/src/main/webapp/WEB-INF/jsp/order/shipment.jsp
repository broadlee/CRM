<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
	function searchKuaidi(){
		$("#kuaidi100").html("<iframe name='kuaidi100' src='https://www.kuaidi100.com/frame/app/index.html?canvas_pos=600' width='550' height='360' marginwidth='0' marginheight='0' hspace='0' vspace='0' frameborder='0' scrolling='no'></iframe>");
		
	}
</script>
<style>
	#orderShipment{font-size: 12px;}
	#orderShipment ul li{list-style: none;}
	#orderShipment .track-rcol{ border: 1px solid #eee;}
	#orderShipment .track-list{margin: 20px; padding-left: 5px; position: relative;}
	#orderShipment .track-list li{position: relative; padding: 9px 0 0 25px; line-height: 18px; border-left: 1px solid #d9d9d9; color: #999;}
	#orderShipment .track-list li.first{color: red; padding-top: 0; border-left-color: #fff;}
	#orderShipment .track-list li .node-icon{position: absolute; left: -6px; top: 50%; width: 11px; height: 11px; background: url(static/image/order/order-icons.png)  -21px -72px no-repeat;}
	#orderShipment .track-list li.first .node-icon{background-position:0 -72px;}
	#orderShipment .track-list li .time{margin-right: 20px; position: relative; top: 4px; display: inline-block; vertical-align: middle;}
	#orderShipment .track-list li .txt{max-width: 600px; position: relative; top: 4px; display: inline-block; vertical-align: middle;}
	#orderShipment .track-list li.first .time{margin-right: 20px; }
	#orderShipment .track-list li.first .txt{max-width: 600px; }
</style>
<div id="orderShipment">
<div class="track-rcol">
			<div class="track-list">
				<ul>
					<c:if test="${shipment.message=='ok' }" var="r">
						<c:set scope="page" var="i" value="0"></c:set>
						<c:forEach items="${shipment.data }" var="d">
							<c:if test="${i==0 }" var="dd">
								<li class="first">
									<i class="node-icon"></i>
									<span class="time">${d.time }</span>
									<span class="txt">${d.context }</span>
									<span class="txt">来源：${d.location }</span>
								</li>
								<c:set scope="page" var="i" value="${i+1}"></c:set>
							</c:if>
							<c:if test="${not dd }">
								<li>
									<i class="node-icon"></i>
									<span class="time">${d.time }</span>
									<span class="txt">${d.context }</span>
									<span class="txt">来源：${d.location }</span>
								</li>
							</c:if>							
						</c:forEach>
					</c:if>
					<c:if test="${not r }">
					${shipment.message}
					</c:if>
				</ul>
			</div>
		</div>	
</div>
<div style="text-align: center;">
无物流信息？<a class="easyui-linkbutton" onclick="searchKuaidi()" iconCls="icon-search">我要查快递</a>
</div>
<div id="kuaidi100" style="margin:10px 0px;">
</div>

