<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript"> 

		function orderCancelSearch(value,name){ 

			//alert("name:"+name+"---value"+value);
			 $("#orderCancelGird").datagrid({
				 url:'order/order/lists?theme='+value+'&state=CANCEL'
			 });
		} 
</script> 

<table id="orderCancelGird"></table>
<div id="orderCancelToolbar" style="padding:5px 5px;" >
	<input id="orderCancelSearch" class="easyui-searchbox" style="width:400px" 
	data-options="searcher:orderCancelSearch,prompt:'订单编号/主题/备注/客户',menu:'#orderCancelSearchM',Width:300"/>
	<div id="orderCancelSearchM"> 
		<div data-options="name:'CANCEL'">已取消</div>  
	</div> 
    <a class="easyui-linkbutton add" iconCls="icon-add" plain="true">新增</a>
    <a class="easyui-linkbutton delete" iconCls="icon-remove" plain="true">删除</a>
    <a class="easyui-linkbutton customerInfo" iconCls="icon-customerInfo" plain="true">客户信息</a>		
    <a class='easyui-linkbutton orderDetails' iconCls="icon-details" plain="true">明细</a> 
    <a class="easyui-linkbutton" onclick="javascript:$('#orderCancelGird').datagrid('reload')"
	  iconCls="icon-reload" plain="true">刷新</a> 
</div>	  


<script src="static/js/order/orderCancel.js"></script>