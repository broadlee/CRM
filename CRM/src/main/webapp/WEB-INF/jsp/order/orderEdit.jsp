<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<style>
#orderEditForm tr td {
	padding: 5px 20px;
}
</style>
<script type="text/javascript">
 $(function(){
	 var selectRow=null;
	 var artChanged;
	 $("#orderEdit-c").combogrid({  
		    onChange: function (newValue, oldValue) {  
		        artChanged = true;//记录是否有改变（当手动输入时发生) 
		    }, 
		    onSelect: function (index, row) {  
		        selectRow = row;
		        $("#orderEdit-customer2").val(row.id);//隐藏域的值
		        $("#orderEdit-customer1").textbox("setText",row.name);
		        $("#orderEdit-c").combogrid('setValue', ''); 
		        //加载收货地址
		        $("#orderEdit-address").combogrid({
		        	url:"order/order/addressList/"+row.id,
		        	onLoadSuccess:function(data){//加载成功后设计默认地址
		        		data=data.rows;
		        		if(data!=""){		        		
			        		//alert(JSON.stringify(data));
			        		for(var i=0;i<data.length;i++){
			        			if(data[i].moren){
			        				//设计选中
			        				$('#orderEdit-address').combogrid('grid').datagrid('selectRecord',data[i].id);
			        			}
			        		}
			        	}else{
			        		$.messager.alert("提示","该客户还没有添加收货地址，请先添加收货地址！");
			        		}
		        	}
		        });
		    
		   	
		    }
		});
	$('#orderEdit-address').combogrid({
		onShowPanel:function(){
			 if( $("#orderEdit-customer2").val()==""){
				 //隐藏面板
				 $(this).combogrid("hidePanel");
				 $.messager.alert("提示","请先选择客户");
				
			 }
		}
	});
    //加载收获地址
    var customerId=${o.customer.id};
  	var addressId=$("#orderEdit-addressId").val();
	

    $("#orderEdit-address").combogrid({
    	url:"order/order/addressList/"+customerId,
    	onLoadSuccess:function(data){//加载成功后设计地址
    		data=data.rows;	
    		if(data!=""){		        		
        		//alert(JSON.stringify(data));
        		for(var i=0;i<data.length;i++){
        			if(data[i].id==addressId){
        				//设计选中
        				$('#orderEdit-address').combogrid('grid').datagrid('selectRecord',addressId);
        			}
        		}
        	}else{
        		$.messager.alert("提示","该客户还没有添加收货地址，请先添加收货地址！");
        		}
    	}
    }); 
 	
 
 	$("#orderEdit-state").combobox({
 		onBeforeLoad: function(param){//判断是否显示物流单号
 			if($(this).combobox('getValue')!="SEND"){
 				$("#orderEdit-wuliu").hide();
 			}
 		
 		},
 		 onChange: function (newValue, oldValue) {  
		        if(newValue=="SEND"){
		        	$("#orderEdit-wuliu").show();
		        }else{
		        	$("#orderEdit-wuliu").hide();
		        }
		    },
 	});
 	//限制只能输入小数
 	$.extend($.fn.textbox.defaults.rules, {  
 	 number : {  
         validator : function(value, param) {  
             return /\b(0(\.\d{1,2})?)|1\b/.test(value);  
         },  
         message : "请输入0-1之间的小数" 
     }
 	});
	$("#orderGridXiaEdit").datagrid({
 		url:''
 	}); 
 
 
 });
</script>
<div>
<form id="orderEditForm" action="/order/order/edit" method="post">
	<table id="orderEditFormTable" width="90%" border="0" style="padding: 6px;">
		<tr>
			<td width="50%" colspan="2">
				 <input type="hidden"name="member.id" value="${o.member.id }"/>
				  <input " label="创建人" editable="false"
				  	value="${o.member.name }"
					 style="width: 30%;" required="true"
					class="easyui-textbox">
			</td>
		</tr>
		<tr>
			<td width="50%">
				  <input " label="订单编号" name='orderId' editable="false"
				  	value="${o.orderId}"
				  	style="width: 60%;" 
					required="true"
					class="easyui-textbox" >
			</td>
			<td width="50%">
				  <input " label="下单时间" name='date'
				  	value="${o.date}" editable="false"
					 style="width: 70%;" required="true"
					class="easyui-datetimebox" >
			</td>
		</tr>
		<tr>
			<td width="50%" colspan="2">
				<input type="hidden" name="id" value="${o.id }"/>
				 <input name="theme" label="主题"
				 	value="${o.theme }"
					prompt="请输入主题内容" style="width: 80%;" required="true"
					class="easyui-textbox" >
			</td>
		</tr>
		<tr>
			<td width="50%" colspan="2">
				<input id="orderEdit-customer1" value="${o.customer.name }"
				label="客户" editable="false" style="width: 40%;"
				required="true" class="easyui-textbox"> 
				<input id="orderEdit-customer2" name="customer.id" value="${o.customer.id }"type="hidden">
				
				<select id="orderEdit-c"
				class="easyui-combogrid" style="width: 350px;"
				data-options="    
			            panelWidth:'auto', 
			            prompt:'选择客户',  
			            idField:'id', 
			            iconCls:'icon-search',
			            textField:'name',
			            method:'get',
			            panelWidth:500,
			            fitColumns:true, 	             
			            url:'order/order/customerList',   
			            columns:[[    
			                {field:'id',title:'ID',width:20},    
			                {field:'name',title:'姓名',width:100},    
			                {field:'linkWay',title:'联系方式',width:120},    
			                {field:'location',title:'所在地',width:100}   
			            ]]    
			        "></select>
			</td>
		</tr>
		<tr>		
			<td colspan="2">	
				<input id="orderEdit-addressId" type="hidden" value="${o.address.id }">
				<select id="orderEdit-address" name="address.id"
				class="easyui-combogrid" style="width: 500px;"
				label="收货地址"
				required="true"    
				data-options=" 				
			            panelWidth:'auto', 
			            prompt:'选择地址',  
			            idField:'id', 
			            textField:'address',
			            method:'get',
			            panelWidth:700,
			            editable:false,
			            fitColumns:true, 	              
			            columns:[[    
			                {field:'id',title:'ID',width:60},    
			                {field:'receiver',title:'收货人姓名',width:100},    
			                {field:'address',title:'收货地址',width:120},    
			                {field:'concact',title:'联系方式',width:100},    
			                {field:'codes',title:'邮编',width:100},   
			                {field:'moren',title:'是否默认',width:100,formatter:function(value,row,index){
			                	if(row&&row.moren){
			                	 return '是';
			                	}else{
			                	return '否';
			                	}
			                }},     
			            ]]    
			        "></select>
			</td>
		</tr>
		<tr>
			<td width="30%">
				<select id="orderEdit-state" name="state"
					class="easyui-combobox" style="width: 250px;"
					data-options="panelHeight:'auto'" label="订单状态">
						<option value="XIA" selected="true">已下单</option>
						<option value="PAY">已支付</option>
						<option value="SEND">已发货</option>
						<option value="DONE">已完成</option>
				</select>
			</td>
			<td width="50%">
				<div id="orderEdit-wuliu">
					<input name="shipment" id="input-wuliu" class="easyui-textbox"
						type="text" prompt="请输入物流单号" style="width: 300px;" label="物流单号">
				</div>
			</td>
		</tr>
		<tr>
			<td width="80%" colspan="2">
				<input name="remark" prompt="备注内容"
					value="${o.remark }"
					style="width: 80%;" multiline="true" class="easyui-textbox"
					label="备注">
			</td>
		</tr>
		<tr>
			<td width="50%" colspan="2">
				<input id="orderEditTotal" name="total" value="${o.total }"
					style="width: 30%;"  class="easyui-textbox"
					editable="false"
					required="true"
					label="总金额/元">
			</td>
		</tr>
	</table>
</form>
</div>

    