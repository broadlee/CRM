<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<script type="text/javascript">
	var grid=$("#orderReturnGird");
	$("#orderReturnToolbar").on("click","a.returnGood",function(){
		var row = grid.datagrid("getSelected");
		if(row){
			if(row.state!=0){
				$.messager.confirm('退货提示', '真的要退货吗?', function(r){
		            if (r){
		                $.get("order/order/return/"+row.id,function(){
		                		grid.datagrid("reload");
		                }).error(function(){
		                		$.messager.alert("提示","系统错误,操作失败!");
		                });
		            }
		        });
			}else{
				$.messager.alert("提示","该商品已退货，禁止操作!");
			}
		}else{
			$.messager.alert("提示","请选择一个商品!");
		}
	})
</script>
<input id="orderReturnGirdId" type="hidden" value="${order}">
<table id="orderReturnGird" class="easyui-datagrid" data-options="
		fit:true,
		border:false,
		fitColumns:true,
		width:'100%',
		rownumbers:true,
		singleSelect:true,
	    url:'order/order/detailList/'+${order },
	    idField:'id',
	    method:'get',
	    toolbar:'#orderReturnToolbar',
	    onLoadSuccess:function(data){
	    	var data=data.rows;
	    	if(data==''){
	    		$.messager.alert('提示','该订单还没有添加产品!');
	    	}
	    }

"> 
<thead>
	<tr>
		<th data-options="field:'product',width:200,align:'center',formatter:function(value,row,index){
		        	return row.product? row.product.name:value;
		        }">产品名称</th>
		<th data-options="field:'price',width:220,align:'center',formatter:function(value,row,index){
		        	return row.product? row.product.sellPrice:value;
		        }">单价/元</th>        
		<th data-options="field:'count',align:'center',width:220">数量</th>        
		<th data-options="field:'cost',align:'center',width:220">折扣</th>        
		<th data-options="field:'subtotal',align:'center',width:220">小计/元</th> 
		<th data-options="field:'state',align:'center',width:220,formatter:function(value,row,index){
			if(value==0){
				return '退货';
			}else{
				return '正常';
			}},
			styler:function(value,row,index){
				if(value==0){
					return 'color:red';
				}else{
					return 'color:blue';
				}
			}
			">状态</th>     
	</tr> 
</thead>
<tbody></tbody>    
</table>
<div id="orderReturnToolbar" style="padding:5px 5px;">	
    <a class="easyui-linkbutton returnGood" iconCls="icon-tui" plain="true">退货</a>
    <a class="easyui-linkbutton" onclick="javascript:$('#orderReturnGird').datagrid('reload')"
    iconCls="icon-reload" plain="true">刷新</a> 
</div>	  
