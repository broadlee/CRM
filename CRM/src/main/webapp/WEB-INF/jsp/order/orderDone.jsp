<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript"> 

		function orderDoneSearch(value,name){ 

			//alert("name:"+name+"---value"+value);
			 $("#orderDoneGird").datagrid({
				 url:'order/order/lists?theme='+value+'&state=DONE'
			 });
		} 
</script> 

<table id="orderDoneGird"></table>
<div id="orderDoneToolbar" style="padding:5px 5px;">
	<input id="orderDoneSearch" class="easyui-searchbox" style="width:400px" 
	data-options="searcher:orderDoneSearch,prompt:'订单编号/主题/备注/客户',menu:'#orderDoneSearchM',Width:300"/>
	<div id="orderDoneSearchM" > 
		<div data-options="name:'DONE'">已完成</div>  
	</div> 
    <a class="easyui-linkbutton add" iconCls="icon-add" plain="true">新增</a>
    <a class="easyui-linkbutton tui" iconCls="icon-tui" plain="true">退货</a>
    <a class="easyui-linkbutton edit" iconCls="icon-edit" plain="true">编辑物流单号</a>
    <a class="easyui-linkbutton customerInfo" iconCls="icon-customerInfo" plain="true">客户信息</a>		
    <a class='easyui-linkbutton orderDetails' iconCls="icon-details" plain="true">明细</a> 
	<a class='easyui-linkbutton orderShipment' iconCls="icon-wuliu" plain="true">物流信息</a>
    <a class="easyui-linkbutton" onclick="javascript:$('#orderDoneGird').datagrid('reload')"
	  iconCls="icon-reload" plain="true">刷新</a> 
</div>	  


<script src="static/js/order/orderDone.js"></script>
