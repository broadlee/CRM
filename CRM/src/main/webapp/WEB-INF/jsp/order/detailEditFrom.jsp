<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
	//设计总金额
	function detailEditFromSetTotal(){
		var price=$('#detailEditFrom #price').textbox('getValue');
		var count=$('#detailEditFrom #count').numberspinner('getValue'); 
		var cost=$('#detailEditFrom #cost').numberspinner('getValue'); 
		var subtotal=parseFloat(price*count*cost/10);
		$('#detailEditFrom #subtotal').textbox('setValue',subtotal);
		
	}
</script>
<form id="detailEditFrom">
	<c:if test="${not empty od }" >
		<input type="hidden" name="id" value="${od.id }">
	</c:if>
		<input type="hidden" name="orders.id" value="${orderdam.id }">
	<div style="margin:10px 10px;">
		<input name="product.id" value="${not empty od.product.id?od.product.id:'' }" class="easyui-combogrid" label="产品" required="true"
		 data-options="method:'get',
		 editable:false,
		 idField:'id',
		 panelHeight:300,
		 textField:'name',
		 url:'order/order/productList',
		 width:'90%',
		 columns:[[ 
				{field:'id',title:'ID',width:'15%'},    
               {field:'name',title:'名称',width:'30%'},    
               {field:'sellPrice',title:'价格/元',width:'15%'},    
               {field:'stock',title:'库存',width:'20%'},
               {field:'supplier',title:'供应商',width:'20%',formatter:function(value,row,index){
                	if(row.supplier!=null){
                	 return row.supplier.name;
                	}
               }} ]],
         onSelect:function(index, row){
         	$('#detailEditFrom #price').textbox('setValue',row.sellPrice);
         	//设计总金额
         	detailEditFromSetTotal();
         }
               " ></select>
	</div>
	<div style="margin:10px 10px;">
		<input id="price" disabled="disabled"  editable="false"value="${not empty od.product.sellPrice?od.product.sellPrice:'0.00' }" class="easyui-textbox" label="单价/元" style="width:30%;"  > 
		<input id="count" name="count" value="${not empty od.count?od.count:'1' }" class="easyui-numberspinner" label="数量" style="width:40%;"   
        required="required" data-options="min:1,max:100000,
            onChange:function( newValue, oldValue){
            //设计总金额
         	detailEditFromSetTotal();
        } "> 
    </div>
	<div style="margin:10px 10px;">
		<input id="cost" name="cost" value="${not empty od.cost?od.cost:'10.0' }" class="easyui-numberspinner" label="折扣" style="width:60%;"   
        required="required" data-options="min:1,max:10,precision:1,increment:0.1,
            onChange:function( newValue, oldValue){
            //设计总金额
         	detailEditFromSetTotal();
        } "> 
    </div>
	<div style="margin:10px 10px;">
		<input id="subtotal" name="subtotal" value="${not empty od.subtotal?od.subtotal:'0.00'}"class="easyui-textbox" label="小计/元" style="width:80%;"   
        required="required" editable="false"> 
    </div>

</form>