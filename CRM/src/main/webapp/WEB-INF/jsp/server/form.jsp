<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %> 
<link href="static/css/server/server.css" rel="stylesheet" type="text/css" /> 

<form id="serverForm" method="post" style="margin:0;padding:10px 30px">
	<input type="hidden" name="id" value="${server.id}"/>
    <div style="margin-bottom:10px">
        <input value="${server.customer.id}" class="easyui-combobox" id="selectCus" name="customer.id" style="width:99.5%;" required="true" panelHeight="auto" label="客户" labelAlign="center"/>
    </div>
    <div style="margin-bottom:10px">
    	<select id="type" required="true" name="type" class="easyui-combobox" editable="false" panelHeight="auto" label="服务类型" style="width:49%;" labelAlign="center" value="${server.type}">
        	<option value="ASK">咨询</option>
        	<option value="COMPLAIN">投诉</option>
        	<option value="ADVICE">建议</option>
        </select>
        <c:if test="${server.state == 'NEW' || server.state == null}">
        	<input id="state" name="state" class="easyui-textbox" readonly="true" label="状态" style="width:49%" value="新创建" labelAlign="center">
        </c:if>
        <c:if test="${server.state == 'ASSIGNED'}">
        	<input id="state" name="state" class="easyui-textbox" readonly="true" label="状态" style="width:49%" value="已分配" labelAlign="center">
        </c:if>
        <c:if test="${server.state == 'DEALED'}">
        	<input id="state" name="state" class="easyui-textbox" readonly="true" label="状态" style="width:49%" value="已处理" labelAlign="center">
        </c:if>
        <c:if test="${server.state == 'SAVED'}">
        	<input id="state" name="state" class="easyui-textbox" readonly="true" label="状态" style="width:49%" value="已归档" labelAlign="center">
        </c:if>
    </div>
    <div style="margin-bottom:10px" id="special">
    	<input id="question" required="true" name="question" class="easyui-textbox" multiline="true" label="问题描述" style="width:99.5%" value="${server.question}" labelAlign="center">
    </div>
    <div style="margin-bottom:10px">
    	<select name="creator.id" readonly="true" class="easyui-combobox" label="创建人" style="width:49%" labelAlign="center">
    		<option value="${empty server.creator.id ? member.id : server.creator.id}">${empty server.creator.name ? member.name : server.creator.name}</option>
    	</select>
    	<fmt:formatDate value="${server.createDate}" pattern="yyyy-MM-dd hh:mm:ss" var="createDate"/>
    	<input id="createDate" name="createDate" class="easyui-textbox" readonly="true" label="创建时间" style="width:49%" value="${createDate}" labelAlign="center">
    </div>
    <div style="margin-bottom:10px">
    	<input id="selectMem" name="dealer.id" class="easyui-combobox" label="被分配人" style="width:49%" value="${server.dealer.id}" labelAlign="center" panelHeight="auto">
    	<fmt:formatDate value="${server.assignDate}" pattern="yyyy-MM-dd hh:mm:ss" var="assignDate"/>
    	<input id="assignDate" name="assignDate" class="easyui-textbox" readonly="true" label="分配时间" style="width:49%" value="${assignDate}" labelAlign="center">
    </div>
    <div style="margin-bottom:10px" id="special">
    	<input id="answer" name="answer" class="easyui-textbox" multiline="true" label="解决方案" style="width:99.5%" value="${server.answer}" labelAlign="center">
    </div>
    <div style="margin-bottom:10px">
    	<c:if test="${not empty server.answer}">
    		<input id="dealer" disabled="disabled" class="easyui-textbox" readonly="true" label="处理人" style="width:49%" labelAlign="center" value="${server.dealer.name}">
    	</c:if>
    	<c:if test="${empty server.answer}">
    		<input id="dealer" disabled="disabled" class="easyui-textbox" readonly="true" label="处理人" style="width:49%" labelAlign="center" value="">
    	</c:if>
    	<fmt:formatDate value="${server.dealDate}" pattern="yyyy-MM-dd hh:mm:ss" var="dealDate"/>
    	<input id="dealDate" disabled="disabled" name="dealDate" class="easyui-textbox" readonly="true" label="处理时间" style="width:49%" value="${dealDate}" labelAlign="center">
    </div>
     <div style="margin-bottom:10px">
    	<input id="result" name="result" class="easyui-textbox" label="处理结果" style="width:49%" value="${server.result}" labelAlign="center">
    	<input value="${server.satis}" class="easyui-combobox" id="satis" name="satis" style="width:49%;" editable="false" panelHeight="auto" label="满意度" labelAlign="center"/>
    </div>
</form>
<script src="static/js/server/form.js"></script>