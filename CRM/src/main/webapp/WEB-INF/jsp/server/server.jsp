<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  
<table id="serverGird"></table>
<div id="serverToolbar">
	<input id="sessionName" type="hidden" value="${member.name}" />
    <a class="easyui-linkbutton add" iconCls="icon-add" plain="true">创建服务</a>
    <a class="easyui-linkbutton edit" iconCls="icon-edit" plain="true">编辑/分配服务</a>
    <a class="easyui-linkbutton delete" iconCls="icon-remove" plain="true">删除服务</a>
    <a class="easyui-linkbutton deal" iconCls="icon-redo" plain="true">处理服务</a>
    <a class="easyui-linkbutton back" iconCls="icon-man" plain="true">反馈服务</a>
    <a class="easyui-linkbutton see" iconCls="icon-details" plain="true">查看服务详情</a>
    <input class="easyui-searchbox" menu='#searchitem' id="search" data-options="prompt:'请输入关键字',searcher:doSearch"/>
	<div id="searchitem">
		<div data-options="name:'customer.name',iconCls:'icon-ok'">客户名</div>
		<div data-options="name:'creator.name',iconCls:'icon-ok'">创建人名</div>
	</div>
	<input id="selectType" name="type" class="easyui-combobox" editable="false" panelHeight="auto" style="width:100px;"/>
    <input id="selectState" name="state" class="easyui-combobox" editable="false" panelHeight="auto" style="width:100px;" />
	<a href="javascript:void(0)" class="easyui-linkbutton all"
		iconCls="icon-reload" plain="true">刷新</a> 
</div>
<script src="static/js/server/server.js"></script>
	