<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>    
<table id="systemRsource"></table>
<div id="systemRsourceToolbar">
	<c:if test="${identifys.contains('sys:resource:add')}">
	    <a class="easyui-linkbutton add" iconCls="icon-add" plain="true">新增</a>
    </c:if>
    <c:if test="${identifys.contains('sys:resource:edit')}">
	    <a class="easyui-linkbutton edit" iconCls="icon-edit" plain="true">编辑</a>
    </c:if>
    <c:if test="${identifys.contains('sys:resource:delete')}">
	    <a class="easyui-linkbutton delete" iconCls="icon-remove" plain="true">删除</a>
	</c:if>
</div>

<script type="text/javascript" src="static/js/permission/resource.js"></script>