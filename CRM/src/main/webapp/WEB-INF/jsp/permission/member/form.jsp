<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>    
<form id="memberForm" method="post" style="margin:0;padding:10px 30px">
	<input type="hidden" name="id" value="${r.id }"/>

    <div style="margin-bottom:10px">
    		<c:choose>
	    		<c:when test="${r == null}">
	        	<input name="account" class="easyui-textbox" required="true" invalidMessage="帐号已存在" validType="remote['permission/member/check','account']" label="帐号:" style="width:350px">
    			</c:when>
    			<c:otherwise>
    				<input name="account" class="easyui-textbox" readonly="true" label="帐号:" style="width:350px" value="${r.account }">
    			</c:otherwise>
    		</c:choose>
    </div>
    <c:if test="${r == null}">
    <div style="margin-bottom:10px">
        <input name="password" class="easyui-passwordbox" required="true" label="密码:" style="width:350px">
    </div>
    </c:if>
    <div style="margin-bottom:10px">
        <select name="role"  multiple="true" class="easyui-combobox" editable="false" panelHeight="auto" label="角色:" style="width:350px">
        		<c:forEach items="${roles }" var="role">
        			<option ${r.roles.contains(role)?'selected':'' } value="${role.id }">${role.roleName }</option>
        		</c:forEach>
        </select>
    </div>

    <div style="margin-bottom:10px">
    	<label class="textbox-label textbox-label-before" style="text-align: left; height: 25px; line-height: 25px;">状态:</label>
    		<input class="easyui-switchbutton" name="enable" data-options="onText:'启用',offText:'禁用',value:'1',checked:${r==null || r.enable?'true':'false'}">
    </div>
</form>