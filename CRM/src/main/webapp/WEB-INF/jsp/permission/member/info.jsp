<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<style>
	#userInfo-dam tr td{
	padding:10px 20px;
	}
</style> 
<script type="text/javascript">
	function resetPass(){
		var dialog = $("<div/>").dialog({
			title: "重置密码",
			width: 450,
			height: 350,
			modal:true,
			href:'permission/member/pass_form',
			onClose: function(){
				//关闭窗户后，立即销毁窗口
				$(this).dialog("destroy");
			},
			buttons:[{
					iconCls:"icon-ok",
					text:"保存",
					handler:function(){				
						var form = $("#pass_form");
						//校验表单数据
						if(form.form("validate")){
							$.post("permission/member/resetPass",form.serialize(),function(rs){
								if(rs=='true'){
									$.messager.alert('提示','修改成功');
									dialog.dialog("close");
								}else if(rs=='err'){
									$.messager.alert('提示','原密码错误');
								}else{
									$.messager.alert('提示','系统异常，操作失败');
									dialog.dialog("close");
								}
							});
						}
					}
				},
				{
					iconCls:"icon-cancel",
					text:"取消",
					handler:function(){
						dialog.dialog("close");
					}		
			}]

		});
	}
</script>   
<table id="userInfo-dam" style="width:100%;margin:10px 0px;font-weight: bold" cellpadding="60" cellspacing="10">
<tr>
	<td>帐号</td>
	<td> ${member.account }  </td>
</tr>
<tr>
	<td>姓名</td>
	<td> ${member.name }  </td>
</tr>
<tr>
	<td>角色</td>
	<td>  
		<c:forEach items="${member.roles }" var="role">
			[${role.roleName }]
		</c:forEach>
	</td>
</tr>
<tr>
	<td>创建时间</td>
	<td> ${member.date }  </td>
</tr>
</table>
<a onclick="resetPass()" class="easyui-linkbutton editPassword" iconCls="icon-detailEdit" plain="true">重置密码 </a>