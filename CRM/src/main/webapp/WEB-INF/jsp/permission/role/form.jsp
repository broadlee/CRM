<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<form id="roleForm" method="post" style="margin:0;padding:10px 30px">
	<input type="hidden" name="id" value="${r.id }"/>

    <div style="margin-bottom:10px">
        <input name="roleName" class="easyui-textbox" required="true" label="名称:" style="width:350px" value="${r.roleName }">
    </div>

    <div style="margin-bottom:10px">
    	<label class="textbox-label textbox-label-before" style="text-align: left; height: 25px; line-height: 25px;">状态:</label>
    		<input class="easyui-switchbutton" name="enable" data-options="onText:'启用',offText:'禁用',value:'1',checked:${r==null || r.enable?'true':'false'}">
    </div>
</form>