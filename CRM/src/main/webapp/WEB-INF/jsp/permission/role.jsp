<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<table id="roleGird"></table>
<div id="roleToolbar">
	<c:if test="${identifys.contains('sys:role:add')}">
		<a class="easyui-linkbutton add" iconCls="icon-add" plain="true">新增</a>
	</c:if>
	<c:if test="${identifys.contains('sys:role:edit')}">
		<a class="easyui-linkbutton edit" iconCls="icon-edit" plain="true">编辑</a>
	</c:if>
	<c:if test="${identifys.contains('sys:role:delete')}">
		<a class="easyui-linkbutton delete" iconCls="icon-remove" plain="true">删除</a>
	</c:if>
	<c:if test="${identifys.contains('sys:role:grant')}">
		<a class="easyui-linkbutton grant" iconCls="icon-add" plain="true">授权</a>
	</c:if>
</div>

<script src="static/js/permission/role.js"></script>