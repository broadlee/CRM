<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<form id="systemRsourceForm" method="post" style="margin:0;padding:10px 30px">
	<input type="hidden" name="id" value="${r.id }"/>
    <div style="margin-bottom:10px">
		<select name="parent.id"  class="easyui-combotree" style="width:350px;"
		        data-options="url:'permission/resource/parents',value:'${r.parent.id}'" label="父节点:">
		</select>
    </div>
    <div style="margin-bottom:10px">
        <input name="text" class="easyui-textbox" required="true" label="名称:" style="width:350px" value="${r.text }">
    </div>
    <div style="margin-bottom:10px">
        <input name="identify" class="easyui-textbox" required="true" label="标识:" style="width:350px" value="${r.identify }">
    </div>
    <div style="margin-bottom:10px">
    		<select name="type" class="easyui-combobox" editable="false" panelHeight="auto" required="true" label="类型:" value="${r.type }" style="width:350px">
    			<option <c:if test="${r.type=='MENU'}"> selected="true"</c:if>  value="MENU">菜单</option>
    			<option <c:if test="${r.type=='FUNCTION'}"> selected="true"</c:if> value="FUNCTION">功能</option>
    			<option <c:if test="${r.type=='PAGE'}"> selected="true"</c:if> value="PAGE">页面</option>
    		</select>
    </div>
    <div style="margin-bottom:10px">
        <input name="menuHref" class="easyui-textbox" label="菜单链接:" style="width:350px" value="${r.menuHref }">
    </div>
    <div style="margin-bottom:10px">
        <input name="weight" class="easyui-textbox" label="权重:" style="width:350px" value="${r.weight }">
    </div>
    <div style="margin-bottom:10px">
        <input name="urls" class="easyui-textbox" label="资源链接:" multiline="true" style="width:350px;height: 100px;" value="${r.urls }">
    </div>
    <div style="margin-bottom:10px">
    	<label class="textbox-label textbox-label-before" style="text-align: left; height: 25px; line-height: 25px;">状态:</label>
    		<input class="easyui-switchbutton" name="enable" data-options="onText:'启用',offText:'禁用',value:'1',checked:${r==null || r.enable?'true':'false'}">
    </div>
</form>