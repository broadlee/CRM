<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<jsp:include page="/WEB-INF/jsp/easyuilink.jsp"></jsp:include>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>登录-客户关系管理系统</title>
<link href="static/css/login/login.css" rel="stylesheet" />
</head>
<script> 
	$(function(){
		var err = $("#err").val();
		if(err != ""){
			$.messager.alert('提示',err);
		}
	})
</script>
<body>
<div id="wrap">
	<input id="err" type='hidden' value="${err}" />
	<div id="header" style="height:30px;margin-top: 70px;">
		<h1 style="text-align: center;font-size: 30px;
			color: white;letter-spacing: 5px;font-family: '楷体';">
		客户关系管理系统
		</h1>  
	</div>
    <div id="content-wrap" style="margin-top:20px">
    	<div class="space" ></div>
   	  <form action="login" method="post">
   	  	<div class="content">
        <div class="field"><label>账　户：</label><input class="account" required = 'true' name="account" type="text" placeholder="请输入账户"/></div>
		<div class="field"><label>密　码：</label><input class="password" required = 'true' name="password" type="password" placeholder="请输入密码"/><br /></div>
        <div class="field"><label>验证码：</label><input class="checkcode" name="" type="text" placeholder="验证码"/><br /></div>
        <div class="btn"><input name="" type="submit" class="login-btn" value="" /></div>
      </div>
   	  </form>
    </div>
   <!--  <div id="footer" style="color:white;text-align: center;font-size:12px ;
    	margin-top:50px;font-family: '微软雅黑';position: absolute;bottom: 0;left: 37%"> 
    	<p>©Copyright2018-2019  深圳市韬睿科技有限公司  粤ICP备120755981号</p>
    </div> -->
</div>
</body>
</html>

