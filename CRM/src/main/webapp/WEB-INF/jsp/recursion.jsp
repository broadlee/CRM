<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${not empty children }">
	<ul>
		<c:forEach items="${children }" var="item">
				<c:if test="${not empty item.menuHref }" var="rs">
					<li data-options="href:'${item.menuHref}'">
				</c:if>
				<c:if test="${not rs}">
					<li>
				</c:if>
				<span>${item.text }</span>
				<c:set var="children" value="${item.children}" scope="request"/>
				<c:import url="recursion.jsp"/>
			</li>
		</c:forEach>
	</ul>
</c:if>