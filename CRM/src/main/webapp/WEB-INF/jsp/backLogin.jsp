<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="<c:url value="static/easyui/themes/icon.css"/>" rel="stylesheet" /> 
<link href="<c:url value="static/insdep/insdep.easyui.min.css"/>" rel="stylesheet" />
<link href="<c:url value="static/insdep/icon.css"/>" rel="stylesheet" />
<link href="<c:url value="static/insdep/iconfont.css"/>" rel="stylesheet" />
<link href="<c:url value="static/css/backlogin.css"/>" rel="stylesheet" />
<script type="text/javascript" src="<c:url value="static/easyui/jquery.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="static/easyui/jquery.easyui.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="static/easyui/locale/easyui-lang-zh_CN.js"/>"></script>
<script type="text/javascript" src="<c:url value="static/js/timeFormat.js"/>"></script>
<script type="text/javascript" src="<c:url value="static/insdep/insdep.extend.min.js"/>"></script>

<title>CRM-后台管理员登录</title>
</head>

<script> 
/* 	$(function(){
		var err = $("#err").val();
		if(err != ""){
			$.messager.alert('提示',err);
		}
	}); */
	//产生1-10的随机数
	var num=Math.ceil(Math.random()*10);
	num=num%3==0?1:2;
	var url='static/js/login/background'+num+'.js';
	//alert(url);
	// 动态加载js脚本文件       
		<!--引入画布-->
	function loadScript() {                          
	 	var script = document.createElement("script");  
	    script.type = "text/javascript";                
	    script.src =url ; 
	    var body = $("body");
		 if(num==1){
			 //移除background2.js
			 $("script[src='static/js/login/background2.js']").remove();
			 body.append(script);   
	    } 
		 
	} 
</script>
<style>
	html,body{
		margin:0px;
		width:100%;
		height:100%;
		overflow:hidden;
	    background:#000;
	}
	
	#canvas{
		position:absolute;
		width:100%;
		height:100%;
		z-index: 10;
	}
	
	#background {	
		position:absolute;
		width:100%;
		height:100%;
		z-index: 1;
		}
</style>
<body class="bodycss"  onload="loadScript()" >
	<canvas id="canvas" ></canvas>
	<canvas id="background"></canvas>
	<div style="z-index:100;position: absolute;width:100%;height:100%;">
		<center>
		<div id="loginpanel" style="margin-top: 100px;">		
			<div id="title">
				<h3 style="color:white ;font-family:'楷体';letter-spacing: 8px;">客户关系管理系统</h3>
			</div>
			<div style="margin:35px 0px;border-radius:3px;padding:30px 0px;background: #f4f4f547; ">
				<h4 style="color:white;text-align: center;width: 350px;font-family: '楷体' ">用户登录</h4>
				<form action="login" method="post" >
					<div style="margin: 10px 20px 10px 25px;">
			            <input name="account" class="easyui-textbox"  required="required" style="width:300px;height:40px;padding:12px" 
			            data-options="prompt:'Username',value:'100000',iconCls:'icon-man',iconWidth:38">
			        </div>
					<div style="margin: 10px 20px 10px 25px;">
			            <input name="password" required="required"  class="easyui-passwordbox"style="width:300px;height:40px;padding:12px" 
			            data-options="prompt:'Password',value:'123456',iconCls:'icon-lock',iconWidth:38">
			        </div>
					<div style="margin: 10px 20px 10px 25px;">
			            <input name="yanzheng" class="easyui-textbox" 
			            maxlength="4" size="4"
			 		    style="width:177px;height:40px;padding:12px" 
			            data-options="prompt:'验证码',iconCls:'icon-lock',iconWidth:38">
			            <a href="javascript:void(0)" style="color:white"><img style="vertical-align: middle;"alt="验证码图片"  src="captcha" onclick="this.src='captcha?r='+Date.now()" width="120" height="38"></a>
			        </div>
			        <div style="margin: 10px 20px 10px 24px;">
			        	<input type="submit" value="登录" style="padding: 8px 0px; width:300px;font-size: 16px;text-align:center;background:#A291FF;border:none;outline:none;color:#fff;cursor:pointer" />
					</div>
					 <div style="margin: 10px 20px 10px 24px;text-align: center;color:red;font-weight: bold;">
					 	${err }
					 </div>
				</form>
			</div>
		</div>
		<!--  <div id="footer" style="color:white;text-align: center;font-size:12px ;
			margin-top:100px;font-family: '楷体';"> 
	    	<p>©Copyright2018-2019  深圳市韬睿科技有限公司  粤ICP备120755981号</p>
	    </div> -->
	    </center>
	</div>
	
	<script type="text/javascript" src="static/js/login/background2.js"></script>
</body>
</html>