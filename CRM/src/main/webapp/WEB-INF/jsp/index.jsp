<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/WEB-INF/jsp/easyuilink.jsp"></jsp:include>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="icon" type="image/x-icon" href="static/image/bitbug_favicon.ico" />  <!-- 标题小图标 -->
<link href="static/css/index.css" rel="stylesheet" type="text/css" /> 
<link href="static/css/showTips.css" rel="stylesheet" type="text/css" />
<title>CRM</title>
<script
	src="http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=js">
</script>
<style type="text/css">
#showList {
	display: block;
	white-space: nowrap;
	overflow: auto;
	margin: 10px;
}

#showList #waiLi {
	width: 31.5%;
	height: 400px;
	margin: 5px;
	display: inline-block;
	border: 1px solid #CFCFD1;
	box-shadow: 0 0 5px #A291FF;
	vertical-align: top;
}

#showList #waiLi #headTitle {
	padding: 10px;
	background: #A291FF;
	font-size: 13px !important;
	box-shadow: 0 0 5px #A291FF;
	color: #fff;
}

#showList #waiLi ul {
	/* margin: 0 20px; */
	
}

#showList #waiLi ul li {
	border-bottom: 1px dashed #A291FF;
	height: 40px;
	margin-top: 5px;
	line-height: 15px;
	/* cursor: pointer; */
}

#showList #waiLi ul li #centerTitle {
	font-size: 15px !important;
	color: #f60;
	margin-left: 40px;
}

#showList #waiLi ul li #centerTime {
	float: right;
	color: #A291FF;
	margin-top: 8px;
}

#showList #waiLi ul li #centerMan {
	margin-left: 40px;
	margin-top: 8px;
	color: #A291FF;
}

#indexRemind div table tr td {
	width: 25%;
	font-size: 4px;
	color: gray;
}

.m, .m li {
	list-style: none;
	margin: 0px;
	padding-left: 2px;
}

.m li {
	
}
</style>
</head>
<body class="easyui-layout">
	<!-- 布局顶部 -->
	<div data-options="region:'north'" border="false"
		style="height: 48px; padding: 0px; overflow: hidden;">
		<%@include file="header.jsp"%>
	</div>
	<!-- 布局下面 -->
	<div data-options="region:'south'"
		style="height: 20px; color: #6082af;">
		<div style="text-align: center;font-family: '微软雅黑'">
			©2018-2019 韬睿科技版权所有
		    <span style="float: right; margin: 0px 6px;">版本：1.0.0</span>
			<span id="dam-his-header-ip" style="float: left; margin: 0px 6px;">
			</span>
    		<script type="text/javascript">
    				document.getElementById("dam-his-header-ip").innerText="IP地址："+remote_ip_info["province"]+remote_ip_info["city"];
    		</script> 
    	</div>
    	<div style="clear:both;"></div>  	
    </div>

	<!-- 布局左边 -->
	<div data-options="region:'west'" style="width: 200px;" title="导航">
		<!--采用分类菜单-->
		<div class="easyui-accordion" data-options="fit:true, border:false">
			    <c:forEach items="${menus }" var="item">
			  		<div title="${item.text }" data-options="iconCls:'icon-menu'">		
						<ul class="m">
							<c:forEach items="${item.children }"  var="child">
								<li>
									<a url="${child.menuHref}" text="${child.text }"
									class="link easyui-linkbutton"
									data-options="plain:true,iconCls:'icon-add'">
									</a>
								</li>
							</c:forEach>
							
						</ul>
					</div>
				</c:forEach>
		</div>
		<%-- <ul id="navTree">
			<li><span>权限管理</span>
				<ul>
				    <li data-options="href:'permission/member'"><span>用户管理</span></li>
					<li data-options="href:'permission/role'"><span>角色管理</span></li>
					<li data-options="href:'permission/resource'"><span>资源管理</span></li>
				</ul></li>
			<li><span>营销管理</span>
				<ul>
					<li data-options="href:'salemanager'"><span>销售机会管理</span></li>
					<li data-options="href:'quote'"><span>报价单管理</span></li>
					<li data-options="href:'plan'"><span>开发计划</span></li>
				</ul>
			<li><span>采购管理</span>
				<ul>
					<li data-options="href:'purchase'"><span>采购单</span></li>
					<li data-options="href:'purchase/supp'"><span>供应商</span></li>
				</ul>
			</li>
			<li><span>产品管理</span>
				<ul>
					<li data-options="href:'product'"><span>产品</span></li>
				</ul>
			</li>
			<li>
			<span>订单管理</span>
				<ul>
					<li data-options="href:'order/order'">
					<span>订单列表</span>
					</li>
					<li data-options="href:'order/order/toAdd'">
					<span>新增订单</span>
					</li>
					<li data-options="href:'return/return'">
					<span>退货记录</span>
					</li>
					<li data-options="href:'order/express'">
					<span>物流公司</span>
					</li>
				</ul>
			</li>

			<li>
				<span>客户管理</span>
				<ul>
					<li data-options="href:'customers'"><span>客户管理</span></li>
					<li data-options="href:'customers/talkrecord'"><span>交往记录</span></li>
				</ul>
			</li>
			<li><span>库存管理</span>
				<ul>
					<li data-options="href:'stock/stockList'"><span>库存列表</span></li>
					<li data-options="href:'stock/inoutApply'"><span>出/入库单</span></li>
					<li data-options="href:'stock/inoutRecord'"><span>出/入库记录</span></li>
				</ul>
			</li>
			<li><span>款项管理</span>
				<ul>
					<li data-options="href:'fund/payRecord'"><span>付款记录</span></li>
					<li data-options="href:'fund/payPlan'"><span>付款计划</span></li>
					<li data-options="href:'fund/receiveRecord'"><span>回款记录</span></li>
					<li data-options="href:'fund/receivePlan'"><span>回款计划</span></li>
				</ul>
			</li>
			<li><span>服务售后管理</span>
				<ul>
					<li data-options="href:'server'">
						<span>服务管理
							<c:if test="${not empty serverCount}">
								<a title="待处理服务" id="tips">${serverCount}</a>
							</c:if>
						</span>
					</li>
					<li data-options="href:'repair'"><span>售后维修管理</span></li>
				</ul>
			</li>
			<li><span>统计报表</span>
				<ul>
					<li data-options="href:'dataTotal/saleTotal'"><span>基本数据统计</span></li>
					<li data-options="href:'dataTotal/paihang'"><span>各类排行统计</span></li>
					<!-- <li data-options="href:'dataTotal/'"><span>营业统计</span></li> -->
				</ul>
			</li>
		</ul> --%>
	<ul id="navTree">
			<c:forEach items="${menus }" var="item">
				<c:if test="${not empty item.menuHref }" var="rs">
					<li data-options="href:'${item.menuHref}'">
				</c:if>
				<c:if test="${not rs}">
					<li>
				</c:if>
		        <span>${item.text }</span>
		        <c:set var="children" value="${item.children}" scope="request"/>
				<c:import url="recursion.jsp"/>
		    </li>
		    </c:forEach>
		</ul> 
	</div>
	<!-- 布局中间 -->
	<div data-options="region:'center'" border="false">
		<div id="tabs" class="easyui-tabs"
			data-options="fit:true, border:false,tools: '#tools'">
			<div id="showList" data-options="title: '主页面概况'">
				<ul>
					<jsp:include page="/WEB-INF/jsp/indexInside/message.jsp"></jsp:include>
					<li id="waiLi">
						<p id="headTitle">提 醒</p>
							<%@include file="remind.jsp"%>
					</li>
				</ul>
			</div>
		</div>
		<div id="tools"
			style="border: none;border-bottom: 1px solid rgb(149, 184, 231);">
			<a id="close_a" title="关闭当前" class="easyui-linkbutton"
				data-options="iconCls:'icon-clear',plain:true"></a> <a
				id="close_all" title="关闭所有" class="easyui-linkbutton"
				data-options="iconCls:'icon-remove',plain:true"></a>
		</div>
		
	</div>
	<script>
		$(function() {
			//选项卡
			$(".link").click(function() {
				var url = $(this).attr("url");
				var title = $(this).text().trim();//去掉空格
				//先判断选项卡是否存在，存在，激活就行了
				if ($("#tabs").tabs("exists", title)) {
					$("#tabs").tabs("select", title);
					return;
				}
				//不存在，则打开
				//添加新选项卡
				var config = {
					title : title,
					href : url,
					closable : true
				};
				$("#tabs").tabs("add", config);
			});

			$("#navTree").tree({
				lines : true,
				onSelect : function(node) {
					var index = node.text.indexOf("<");
					if(index == -1){
						index = node.text.length; 
					}
					if (node.href) {
						if ($('#tabs').tabs("exists", node.text.substr(0,index))) {
							$('#tabs').tabs("select", node.text.substr(0,index));
						} else {
							$('#tabs').tabs('add', {
								title : node.text.substr(0,index),
								href : node.href,
								closable : true
							});
						}
					}
				}
			})
			//关闭当前的
			$("#close_a").click(function() {
				var tab = $('#tabs').tabs('getSelected');
				var index = $('#tabs').tabs('getTabIndex', tab);
				if (index != 0) {
					$('#tabs').tabs('close', index);
				}
			});

			//关闭所有的
			$("#close_all").click(function() {
				var tabs = $('#tabs').tabs('tabs');
				for (var i = tabs.length - 1; i >= 1; i--) {
					$('#tabs').tabs('close', i);
				}
			});
			
			

		});
	</script>
</body>
</html>








