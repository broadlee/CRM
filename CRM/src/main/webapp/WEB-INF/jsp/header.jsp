<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript">
			function toggleFullScreen() {
				if (!document.fullscreenElement && // alternative standard method
					!document.mozFullScreenElement && !document.webkitFullscreenElement) {// current working methods
					if (document.documentElement.requestFullscreen) {
						document.documentElement.requestFullscreen();
					} else if (document.documentElement.mozRequestFullScreen) {
						document.documentElement.mozRequestFullScreen();
					} else if (document.documentElement.webkitRequestFullscreen) {
						document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
					}
				} else {
					if (document.cancelFullScreen) {
						document.cancelFullScreen();
					} else if (document.mozCancelFullScreen) {
						document.mozCancelFullScreen();
					} else if (document.webkitCancelFullScreen) {
						document.webkitCancelFullScreen();
					}
				}
			}
			
			function prop(){
				$.messager.confirm("确认","真的要退出吗",function(r){
					if(r){
						window.location.href="logout";		
					}	
				});
		
			}
			//查看个人信息
			function userInfoCheck(){
				var dialog = $("<div/>").dialog({
					title: "个人中心",
					width: 450,
					height: 350,
					modal:true,
					href:'permission/member/info',
					onClose: function(){
						//关闭窗户后，立即销毁窗口
						$(this).dialog("destroy");
					}
			
				});
			}
		</script>
<style>
	#personSheZhi #_easyui_textbox_input1{
		background: red;
	}
	.dam-header-menu a:LINK{
		text-decoration: none;
	}
	.dam-header-menu a:VISITED{
		color: white;
	}
	.dam-header-menu a:hover{
		color: red;
	}	
</style>
<link href="static/css/login/style.css" rel="stylesheet" type="text/css" />

<div id="wrap" style="margin: 0px; ">
	<div id="header" class="dam-crm-header"
		style="font-family: '楷体'; color: white;font-weight: bold;">
		<div class="logo fleft"
			style="text-align: center; font-size: 20px;padding:5px;">
			<div>
				<a href="index" 
					style="margin-right: 20px; color: white; font-size:18px; margin-top: 10px;font-family: '楷体'">
					 <img width="38px" height="38px" style="vertical-align: middle;"
					src="static/image/login/logo11.png" /> 客户关系管理系统
				</a>
			</div>
		</div>

		<div  class="dam-header-menu" style="float: right; margin-top:11px; font-size: 14px;">
			<div style="margin-right: 10px;height:30px; padding-top:5px;display:inline-block; font-family: '宋体';font-size: 10px;font-weight: normal;color: #d5f6f9">
					${member.name} 
					<c:forEach items="${member.roles }" var="role">
						[${role.roleName }]
					</c:forEach>
					</div>
			<span style="margin-right: 10px;">
				<input type="text" style="border:1px solid lightgray;border-radius:5px;padding:3px; 
				width:200px;
				outline: none;" placeholder="请输入关键字"/>
			
				<button style="border:1px solid lightgray;border-radius:5px;padding:2px;letter-spacing: 3px;
				cursor:pointer;text-align:center;
				background: #F8F8F8">
				搜索
				</button>		
			</span>
			<a  href="javascript:void(0)" onclick="userInfoCheck()"
				style="margin-right: 10px; color: white; margin-top: 18px;"> <img
				style="vertical-align: middle;"
				src="static/image/login/user.png" /> 个人中心
			</a> 
			<a   href="javascript:void(0)" onclick="toggleFullScreen()"
				style="margin-right: 10px; color: white; margin-top: 18px;"> <img
				style="vertical-align: middle;"
				src="static/image/login/fullScreen.png" /> 全屏
			</a> 
			<a  onclick="return prop();"
				style="margin-right: 10px; color: white; margin-top: 14px;"> <img
				style="vertical-align: middle;"
				src="static/image/login/index-logout.png" /> 注销
			</a>

		</div>

		<div class="clear"></div>
	</div>
<%-- 
	<div id="footer" style="color: white; margin: 0px; padding: 0px;">
		<div style="float: left; margin: 0px 10px;">
			欢迎进入客戶信息管理系統[${member.name}]</div>
		<div id="clock" style="float: right; margin: 0px 10px;"></div>
	</div> --%>
	<!--#footer -->

</div>
<!-- <script type="text/javascript" src="static/js/login/Clock.js"></script>
<SCRIPT type=text/javascript>
		  var clock = new Clock();
		  clock.display(document.getElementById("clock"));
		</SCRIPT> -->
<!--#wrap -->
