<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!--引入echarts  -->
<script type="text/javascript" src="static/js/dataTotal/echarts.js"></script>
<!-- 为 ECharts 准备一个具备大小（宽高）的 DOM -->
<div class="easyui-panel" title="各类排行统计"  style="overflow-x: hidden;overflow-y: auto;"
		border="false" fit="true" >
	<div id="productPaihang" style="width:97%;height:400px;margin:5px;"></div>
	<br>
	<div id="customerTotalBar" style="width:97%;height:400px;margin:5px;overflow-y:auto;"></div>
	<br>
	<div id="memberPaihang" style="width:97%;height:400px;margin:5px;"></div>
	<br>
</div>

<script type="text/javascript">
//获取数据
	var memberPh;
	$.ajax({url:'dataTotal/getMemberPaihang',
		type:"get",
		async:false,
		success:function(data){
			memberPh=data;
	},error:function(){
		alert("图表请求数据失败");
	}
	});

var dom = document.getElementById("memberPaihang");
var myChart = echarts.init(dom);
var app = {};
option = null;
app.title = '用户开发成功客户前10排行';

option = {
    title: {
        text: '用户开发成功客户前10排行',
        subtext: '柱状图'
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis: {
        type: 'value',
        boundaryGap: [0, 0.01]
    },
    yAxis: {
        type: 'category',
        data: memberPh[0].names,
        inverse:true
    },
    series: [
        {
            name: '开发成功数量',
            type: 'bar',
            data: memberPh[0].totals
        } 
    ]
};
;
if (option && typeof option === "object") {
    myChart.setOption(option, true);
}
</script>

<script type="text/javascript">
//获取数据
	var productPh;
	$.ajax({url:'dataTotal/getProductPaihang',
		type:"get",
		async:false,
		success:function(data){
			productPh=data;
	},error:function(){
		alert("图表请求数据失败");
	}
	});

var dom = document.getElementById("productPaihang");
var myChart = echarts.init(dom);
var app = {};
option = null;
app.title = '产品销售额前10排行';

option = {
    title: {
        text: '产品销售额前10排行',
        subtext: '柱状图'
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis: {
        type: 'value',
        boundaryGap: [0, 0.01]
    },
    yAxis: {
        type: 'category',
        data: productPh[0].names,
        inverse:true
    },
    series: [
        {
            name: '销售额',
            type: 'bar',
            data: productPh[0].totals
        } 
    ]
};
;
if (option && typeof option === "object") {
    myChart.setOption(option, true);
}
</script>

 <script type="text/javascript">
//获取数据
	var customerData;
	$.ajax({url:'dataTotal/getCustomerTotal',
		type:"get",
		async:false,
		success:function(data){
		customerData=data;
	},error:function(){
		alert("图表请求数据失败");
	}
	});

var dom = document.getElementById("customerTotalBar");
var myChart = echarts.init(dom);
var app = {};
option = null;
app.title = '客户消费金额前15排行';

option = {
    title: {
        text: '客户消费金额前15排行',
        subtext: '柱状图'
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'shadow'
        }
    },
    legend: {
        data: ['全部年份', customerData[1].type+'年']
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis: {
        type: 'value',
        boundaryGap: [0, 0.01]
    },
    yAxis: {
        type: 'category',
        data: customerData[0].names
    },
    series: [
        {
            name: '全部年份',
            type: 'bar',
            data: customerData[0].data
        },
        {
            name:customerData[1].type+'年' ,
            type: 'bar',
            data: customerData[1].data
        }
    ]
};
;
if (option && typeof option === "object") {
    myChart.setOption(option, true);
}
</script>