<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!--引入echarts  -->
<script type="text/javascript" src="static/js/dataTotal/echarts.js"></script>
<!-- 为 ECharts 准备一个具备大小（宽高）的 DOM -->
<div class="easyui-panel" title="基本数据统计"  style="overflow-x: hidden;overflow-y: auto;"
		border="false" fit="true" >
	<div id="saleTotalBar" style="width:97%;height:400px;margin:5px;"></div>
	<br>
	<div id="orderTotal" style="width:97%;height:400px;margin:5px;"></div>
	<br>
	<div id="customerTotal" style="width:97%;height:400px;margin:5px;"></div>
	<br>
	<div id="serviceTotal" style="width:97%;height:400px;margin:5px;"></div>
	<br>
</div>
<script type="text/javascript">
		//获取数据
		var service;
		$.ajax({url:'dataTotal/getServiceData',
			type:"get",
			async:false,
			success:function(data){
				service=data;
			},error:function(){
				alert("图表请求数据失败");
			}
		});

        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('serviceTotal'));
     // 指定图表的配置项和数据  
        var option = {  
            title: {  
                text: new Date().getFullYear()+"年服务接收数量", 
                subtext:'堆叠柱状图'
            },  
            //鼠标触发提示数量  
            tooltip: {  
                 trigger: "axis"  
            },  
            toolbox: {
                show : true,
                feature : {
                    dataView : {show: true, readOnly: false},
                    magicType : {show: true, type: ['line', 'bar']},
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            legend: {  
                data:["咨询","建议","投诉"]  
            },  
            //x轴显示  
            xAxis: {  
                data: ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月'],  
                splitLine:{  
                	show:false  
                }  
            },  
            //y轴显示  
            yAxis: {  
                splitLine:{  
                	show:false  
                }  
            },  
            series: [  
                 {  
                    name: "咨询",  
                    type: "bar",  
                    stack: "业务",//折叠显示  
                    data: service[0].data
                 },  
                 {  
                     name: "建议",  
                     type: "bar",  
                     stack: "业务",  
                     data: service[1].data
                   
                 },  
                 {  
                     name: "投诉",  
                     type: "bar",  
                     stack: "业务",  
                     data: service[2].data
                 }
             ]  
        };  

        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
</script>


<script type="text/javascript">
		//获取数据
		var customer;
		$.ajax({url:'dataTotal/getCustomerData',
			type:"get",
			async:false,
			success:function(data){
				customer=data;
			},error:function(){
				alert("图表请求数据失败");
			}
		});

        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('customerTotal'));
     // 指定图表的配置项和数据  
        var option = {  
            title: {  
                text: new Date().getFullYear()+"年客户数量", 
                subtext:'堆叠柱状图'
            },  
            //鼠标触发提示数量  
            tooltip: {  
                 trigger: "axis"  
            },  
            toolbox: {
                show : true,
                feature : {
                    dataView : {show: true, readOnly: false},
                    magicType : {show: true, type: ['line', 'bar']},
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            legend: {  
                data:["暂无需求","潜在客户","签约客户","重复购买","失效"]  
            },  
            //x轴显示  
            xAxis: {  
                data: ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月'],  
                splitLine:{  
                	show:false  
                }  
            },  
            //y轴显示  
            yAxis: {  
                splitLine:{  
                	show:false  
                }  
            },  
            series: [  
                 {  
                    name: "暂无需求",  
                    type: "bar",  
                    stack: "业务",//折叠显示  
                    data: customer[0].data
                 },  
                 {  
                     name: "潜在客户",  
                     type: "bar",  
                     stack: "业务",  
                     data: customer[1].data
                   
                 },  
                 {  
                     name: "签约客户",  
                     type: "bar",  
                     stack: "业务",  
                     data: customer[2].data
                 },  
                 {  
                     name: "重复购买",  
                     type: "bar",  
                     stack: "业务",  
                     data: customer[3].data
                    
                 },  
                 {  
                     name: "失效",  
                     type: "bar",  
                     stack: "业务",  
                     data: customer[4].data
                    
                 }  
             ]  
        };  

        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
</script>

<script type="text/javascript">
		//获取数据
		var sale;
		$.ajax({url:'dataTotal/getSaleData',
			type:"get",
			async:false,
			success:function(data){
				sale=data;
			},error:function(){
				alert("图表请求数据失败");
			}
		});

        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('saleTotalBar'));
        // 指定图表的配置项和数据
        var option = {
        	    title : {
        	        text:sale[0].year+'年销售机会数量',
        	        subtext: '柱状/折线图'
        	    },
        	    tooltip : {
        	        trigger: 'axis'
        	    },
        	    legend: {
        	        data:['所有','开发成功','开发失败']
        	    },
        	    toolbox: {
        	        show : true,
        	        feature : {
        	            dataView : {show: true, readOnly: false},
        	            magicType : {show: true, type: ['line', 'bar']},
        	            restore : {show: true},
        	            saveAsImage : {show: true}
        	        }
        	    },
        	    calculable : true,
        	    xAxis : [
        	        {
        	            type : 'category',
        	            data : ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月']
        	        }
        	    ],
        	    yAxis : [
        	        {
        	            type : 'value'
        	        }
        	    ],
        	    series : [
        	        {
        	            name:'所有',
        	            type:'bar',
        	            data:sale[0].data,
        	            markPoint : {
        	                data : [
        	                    {type : 'max', name: '最大值'},
        	                    {type : 'min', name: '最小值'}
        	                ]
        	            }
        	        },
        	        {
        	            'name':'开发成功',
        	            type:'bar',
        	            data:sale[1].data,
        	            markPoint : {
        	                data : [
        	                    {type : 'max', name: '最大值'},
        	                    {type : 'min', name: '最小值'}
        	                ]
        	            }
        	        },
        	        {
        	            name:'开发失败',
        	            type:'bar',
        	            data:sale[2].data,
        	            markPoint : {
        	                data : [
        	                    {type : 'max', name: '最大值'},
        	                    {type : 'min', name: '最小值'}
        	                ]
        	            }
        	        }        	        
        	    ]
        	};

        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
</script>


<script type="text/javascript">
//获取数据
var order;
$.ajax({url:'dataTotal/getOrderData',
	type:"get",
	async:false,
	success:function(data){
		order = data;
	},error:function(){
		alert("图表请求数据失败");
	} 
});

var orderTotal = document.getElementById("orderTotal");
var myChart = echarts.init(orderTotal);  
// 指定图表的配置项和数据  
var option = {  
    title: {  
        text: new Date().getFullYear()+"年订单数量", 
        subtext:'堆叠柱状图'
    },  
    //鼠标触发提示数量  
    tooltip: {  
         trigger: "axis"  
    },  
    toolbox: {
        show : true,
        feature : {
            dataView : {show: true, readOnly: false},
            magicType : {show: true, type: ['line', 'bar']},
            restore : {show: true},
            saveAsImage : {show: true}
        }
    },
    legend: {  
        data:["已下单","已付款","已发货","已完成","取消"]  
    },  
    //x轴显示  
    xAxis: {  
        data: ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月'],  
        splitLine:{  
        	show:false  
        }  
    },  
    //y轴显示  
    yAxis: {  
        splitLine:{  
        	show:false  
        }  
    },  
    series: [  
         {  
            name: "已下单",  
            type: "bar",  
            stack: "业务",//折叠显示  
            data: order[0].data
         },  
         {  
             name: "已付款",  
             type: "bar",  
             stack: "业务",  
             data: order[1].data
           
         },  
         {  
             name: "已发货",  
             type: "bar",  
             stack: "业务",  
             data: order[2].data
         },  
         {  
             name: "已完成",  
             type: "bar",  
             stack: "业务",  
             data: order[3].data
            
         },  
         {  
             name: "已取消",  
             type: "bar",  
             stack: "业务",  
             data: order[4].data
            
         }  
     ]  
};  
  
// 使用刚指定的配置项和数据显示图表。  
myChart.setOption(option);  
</script>