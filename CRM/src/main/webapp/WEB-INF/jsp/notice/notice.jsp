<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
		<form action="" style="width:800px;margin-left:auto;margin-right:auto; ">	
				<textarea id="text1" name="text" style="display:none;"></textarea>
				<div id="editor">
			    </div>
			 	<div style="margin: 10px 0px;text-align: left;">
			    <input type="button" onclick="getHtml()" value="预览">
			    <button>提交</button>
			     <input type="button" onclick="clearEditorDam()" value="清空">
			    </div>
			 
		</form>
		<div id="wangEditorPage"  style="width:800px;margin-left:auto;margin-right:auto; "></div>
	<!-- 注意， 只需要引用 JS，无需引用任何 CSS ！！！-->
	<script type="text/javascript" src="${pageContext.request.contextPath }/static/js/notice/wangEditor.js"></script>
	<script type="text/javascript">
        var E = window.wangEditor;
        var editor = new E('#editor') ;
        // 或者 var editor = new E( document.getElementById('editor') )

        // 配置服务器端地址
        editor.customConfig.uploadImgServer = '${pageContext.request.contextPath}/notice/upload'//富文本上传图片的路径，不上传图片这里可以忽略
        editor.customConfig.uploadImgMaxSize = 3 * 1024 * 1024;
        editor.customConfig.uploadImgMaxLength = 5;    
        editor.customConfig.uploadFileName = 'myFileName';
        editor.customConfig.uploadImgHooks={ 
	        customInsert:function (insertImg, result, editor) {
	                    // 图片上传并返回结果，自定义插入图片的事件（而不是编辑器自动插入图片！！！）
	                    // insertImg 是插入图片的函数，editor 是编辑器对象，result 是服务器端返回的结果
	             
	                    // 举例：假如上传图片成功后，服务器端返回的是 {url:'....'} 这种格式，即可这样插入图片：
	                    var url =result.data;
	                    insertImg(url);
	             
	                    // result 必须是一个 JSON 格式字符串！！！否则报错
	                }
            }
        var $text1 = document.getElementById('text1');
        editor.customConfig.onchange = function (html) {
            // 监控变化，同步更新到 textarea
            $text1.value=html;
        }
        editor.customConfig.debug = location.href.indexOf('wangeditor_debug_mode=1') > 0;
        editor.create();
        function getHtml(){
        	var page=document.getElementById("wangEditorPage");    
        	// 获取编辑器区域完整html代码
            var html = editor.txt.html();
            page.innerHTML=html;
          
        }
        function clearEditorDam(){
        	 editor.txt.clear();
        	 $text1.value="";
        }
       
    </script>
</body>
</html>