<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript"
	src="static/js/salemanagerment/quotation.js"></script>
<link href="static/css/salemanagerment/quotation.css" rel="stylesheet" />

<table id="quotation"></table>

<div id="quotationtoolbar">
	<a href="javascript:void(0)" class="easyui-linkbutton create"
		iconCls="icon-add" plain="true">创建报价单</a> <a href="javascript:void(0)"
		class="easyui-linkbutton delete" iconCls="icon-remove" plain="true">删除报价单</a>
		
	<input id="quotationsearch"></input>
	<div id="quotationsearchitem" style="width: 120px">
		<div data-options="name:'quoId'">报价单编号</div>
		<div data-options="name:'theme'">主题</div>
		<div data-options="name:'chance.id'">销售机会编号</div>
		 <div data-options="name:'chance.customer.name'">销售机会客户名</div>
		 <div data-options="name:'chance.customer.linkWay'">机会客户联系方式</div>
	</div>
	<a href="javascript:void(0)" class="easyui-linkbutton all"
		iconCls="icon-reload" plain="true">刷新</a> 
	<a href="javascript:void(0)" class="easyui-linkbutton create_order"
		iconCls="icon-reload" plain="true">生成订单</a> 

</div>


