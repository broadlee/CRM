<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link href="static/css/salemanagerment/createsales.css" rel="stylesheet" />
<script type="text/javascript" src="static/js/button.js"></script>
<script type="text/javascript"
	src="static/js/salemanagerment/createsales.js"></script>

<form id="createsaleform${chance.id }" class="createsaleform" method="post" style="margin: 20px;">
	<input name="id" type="hidden" value="${chance.id }" />
	
	<c:if test="${chance !=null }">
		<input name="member1.id" type="hidden" value="${chance.member1.id }" />
		<input name="state" type="hidden" value="${chance.state }" />
		<input name="customer.name" type="hidden" value="${chance.customer.name }" />
		<input name="customer.linkWay" type="hidden" value="${chance.customer.linkWay }" />
		<input name="quoted" type="hidden" value="${chance.quoted }">
		<input name="plan" type="hidden" value="${chance.plan }">
	</c:if>

	<table>
		<tr>

			<td style="padding-left: 30px;"><c:choose>
					<c:when test="${chance !=null }">
						<span><h1>编辑销售机会</h1></span>
					</c:when>
					<c:otherwise>
						<span><h1>创建销售机会</h1></span>
					</c:otherwise>

				</c:choose></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td><select id="memberclient${chance.id }" class="easyui-combobox"
				labelWidth="80" name="customer.id" style="width: 300px;" label="客户:" data-id='${chance.id }'
				panelMaxHeight="100" panelHeight="auto" labelAlign="right" 
				data-options="onChange:function(){findvoc(this)},cls:'chanceinput',editable:false,required:true<c:if test="${chance.customer.id !=null }">,value:'${chance.customer.id }',readonly:true</c:if>">
					<option value=""></option>
					<c:forEach var="cus" items="${customers }">
						<option value="${cus.id }">${cus.name }(${cus.linkWay })</option>
					</c:forEach>

			</select></td>

			<td>
				<!-- <select id="assignmember" class="easyui-combobox"
				labelWidth="80" name="member2.id" style="width: 300px;" label="指派人:"
				panelHeight="auto" panelMaxHeight="100" labelAlign="right"
				data-options="cls:'chanceinput',editable:false">
					<option value="">(空)</option>
			</select> -->
			</td>
			<td>
				
			</td>

		</tr>
		<tr>
			<td><input id="successid" name="success"
				class="easyui-numberspinner" label="成功率(%):" labelAlign="right"
				value="0" style="width: 300px;" labelWidth="80"
				data-options="cls:'chanceinput',min:0,max:100,value:'${chance.success }',required:true"></td>


			<td><input class="easyui-datetimebox" name="createDate"
				id="createDate_id" label="创建时间:" labelAlign="right"
				data-options="cls:'chanceinput',required:true,showSeconds:false,editable:false,panelWidth:200,formatter:formatter<c:if test="${chance.createDate !=null }">,value:'${chance.createDate }',readonly:true</c:if>"
				labelWidth="80" style="width: 300px"></td>


			<td>
				<!-- <input class="easyui-datetimebox" name="assignDate"
				label="指派时间:" labelAlign="right"
				data-options="cls:'chanceinput',showSeconds:false,editable:false,panelWidth:200"
				labelWidth="80" style="width: 300px"> -->
			</td>
		</tr>
		<tr>
			<td colspan="3"><input id="summary${chance.id }" name="summary"
				class="easyui-textbox" label="机会概要:" multiline="true"
				style="width: 450px; height: 100px;" labelAlign="right"
				labelWidth="80" value=""
				data-options="cls:'chanceinput',required:true,value:'${chance.summary }'">
				<input id="chanceinfoid${chance.id }" name="chanceInfo" class="easyui-textbox"
				label="机会详情:" multiline="true" style="width: 450px; height: 100px;"
				labelAlign="right" labelWidth="80"
				data-options="cls:'chanceinput',required:true,readonly:true,value:'${chance.chanceInfo }'"></td>



		</tr>
		<tr>
			<td></td>
			<td></td>

			<td><button type="button" class="okcreate" data-id="${chance.id }">
					<c:choose>
						<c:when test="${chance !=null }">
						确认修改
						</c:when>
						<c:otherwise>
						确认创建
						</c:otherwise>

					</c:choose>
				</button></td>

		</tr>
	</table>
</form>

