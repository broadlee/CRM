<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript"
	src="static/js/salemanagerment/plan.js"></script>
<link href="static/css/salemanagerment/plan.css" rel="stylesheet" />

<table id="plan"></table>

<div id="plantoolbar">
	<a href="javascript:void(0)" class="easyui-linkbutton create"
		iconCls="icon-add" plain="true">创建开发计划</a> 
 	<a href="javascript:void(0)"
		class="easyui-linkbutton edit" iconCls="icon-edit" plain="true">修改开发计划</a>
	<a href="javascript:void(0)"
		class="easyui-linkbutton delete" iconCls="icon-remove" plain="true" >删除开发计划</a>
	<a href="javascript:void(0)"
		class="easyui-linkbutton finish" iconCls="icon-redo" plain="true" >结束计划</a>
	<a href="javascript:void(0)"
		class="easyui-linkbutton restart" iconCls="icon-undo" plain="true" >重新开始</a>
		
	<select id="planstate" class="easyui-combobox"
				labelWidth="75" name="state" style="width: 200px;"
				label="计划状态:" panelHeight="auto"
				labelAlign="right" data-options="cls:'chanceinput',editable:false,onChange:stateOnChange">
					<option value="">(空)</option>
					<option value="ING">执行中</option>
					<option value="END">已结束</option>
	</select>
	<a href="javascript:void(0)" class="easyui-linkbutton all"
		iconCls="icon-reload" plain="true">刷新</a>  

</div>
