<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<form id="adddetailform" method="post">
	<input type="hidden" name="quotation.id" value="${quoteid}">
	<div style="margin: 22px;">
		<select class="easyui-combobox"
				labelWidth="80" name="product.id" style="width: 300px;" label="商品选择:" 
				panelMaxHeight="100" panelHeight="auto" labelAlign="right" 
				data-options="cls:'chanceinput',valueField: 'id',
                    textField:'text',editable:false,required:true,url:'quote/getProduct?quotation=${quoteid}'">
			</select>
	</div>
	<div style="margin: 22px;">
			<input name="count"
				class="easyui-numberspinner" label="数量:" labelAlign="right"
				 style="width: 300px;" labelWidth="80"
				data-options="cls:'chanceinput',min:0,required:true">
		
	
	</div>
	<div style="margin: 22px;">
		<input  name="cost"
				class="easyui-numberspinner" label="折扣(%):" labelAlign="right"
				 style="width: 300px;" labelWidth="80"
				data-options="cls:'chanceinput',min:0,max:100,value:100,required:true">
	
	</div>
	

</form>