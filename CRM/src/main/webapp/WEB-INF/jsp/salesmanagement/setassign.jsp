<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<form id="setassignform" method="post">
	<input name="chanceId" type="hidden" value="${assignChance.id}"/>
	<div style="margin-top: 25px">
		<select id="beassignmember" class="easyui-combobox"
				labelWidth="80" name="memberId" style="width: 300px;"
				label="被指派人:" panelHeight="auto" panelMaxHeight="100"
				labelAlign="right" data-options="cls:'chanceinput',required:true,valueField: 'id',
                    textField:'text',editable:false,url:'salemanager/getMember',value:'${assignChance.member3.id }'">
			</select>
	</div>
</form>