<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript">
	function getChance(newv, old) {

		$.post("plan/getOneChance", "chanceid=" + newv, function(result) {
			$("#createDate").html(result.createDate)
			console.log(result.state)
			if (result.state == "NOASSIGN") {
				$("#state").html("未指派 ")
			} else if (result.state == "YESASSIGN") {
				$("#state").html("已指派")
			} else if (result.state == "SUCCESS") {
				$("#state").html("开发成功")
			} else if (result.state == "FAILED") {
				$("#state").html("开发失败")
			}
			$("#success").html(result.success)
			$("#summary").html(result.summary)
			$("#chanceInfo").html(result.chanceInfo)
			$("#cousname").html(result.customer.name)
			$("#linkMan").html(result.customer.linkMan)
			$("#linkWay").html(result.customer.linkWay)
		})

	}
    function Todate(num) { 
        num = num + "";
        var date = "";
        var month = new Array();
        month["Jan"] = 1; month["Feb"] = 2; month["Mar"] = 3; month["Apr"] = 4; month["May"] = 5; month["Jan"] = 6;
        month["Jul"] = 7; month["Aug"] = 8; month["Sep"] = 9; month["Oct"] = 10; month["Nov"] = 11; month["Dec"] = 12;
        var week = new Array();
        week["Mon"] = "一"; week["Tue"] = "二"; week["Wed"] = "三"; week["Thu"] = "四"; week["Fri"] = "五"; week["Sat"] = "六"; week["Sun"] = "日";
        str = num.split(" ");
        date = str[5] + "-";
        date = date + month[str[1]] + "-" + str[2]+" "+str[3];
        return date;
    }
	function onLoadSuccess() {

		$("#chance_plan").combobox("setValue", '${plan.chance.id}')
	}
	function formatter(date) {
		return new Date(date).format("yyyy-MM-dd hh:mm:ss");
	}
	var planContext;
	$(function() {

		Simditor.locale = 'zh-CN';
		toolbar = [ 'title', 'bold', 'italic', 'underline', 'strikethrough',
				'fontScale', '|', 'color', 'ol', 'ul', 'blockquote', 'code',
				'table', '|', 'link', 'hr', 'indent', 'outdent', 'alignment' ];
		planContext = new Simditor({
			textarea : $("#planContext"),
			placeholder : '这里输入文字...',
			toolbar : toolbar
		});
	})
</script>

<form id="planform" method="post">
	<c:if test="${plan!=null }">
		<input name="id" value="${plan.id }" type="hidden">
		<input name="state" value="${plan.state }" type="hidden">
	</c:if>
	<div style="margin: 2% 4%">
		<select id="chance_plan" class="easyui-combobox" labelWidth="120"
			name="chance.id" style="width: 50%;" label="选择销售机会："
			panelHeight="auto" labelAlign="right"
			data-options="cls:'chanceinput',valueField: 'id',onChange:getChance,
                    textField:'text',editable:false,required:true,url:'plan/getchance?chanceid=${plan.chance.id }',onLoadSuccess:onLoadSuccess
                 	<c:if test="${plan!=null }">,readonly:true</c:if>">
		</select>

	</div>
	<div id="planchance">
		<div style="font-weight: bold; font-size: 20px">该机会详情：</div>
		<table id="planchancetable" style="width: 100%">

			<tr>
				<td style="width: 10%" class="themefont">创建时间：</td>
				<td style="width: 15%"><span id="createDate"></span></td>
				<td style="width: 10%" class="themefont">机会状态：</td>
				<td><span id="state"></span></td>
			</tr>
			<tr>
				<td class="themefont">成功率（%）：</td>
				<td><span id="success"></span></td>
				<td class="themefont"></td>
				<td></td>
			</tr>

			<tr>
				<td class="themefont">客户：</td>
				<td><span id="cousname"></span></td>
				<td class="themefont"></td>
				<td></td>

			</tr>
			<tr>
				<td class="themefont">联系人：</td>
				<td><span id="linkMan"></span></td>
				<td class="themefont">机会概况：</td>
				<td><span id="summary"></span></td>
			</tr>
			<tr>

				<td class="themefont">联系方式：</td>
				<td><span id="linkWay"></span></td>
				<td class="themefont">机会详情：</td>
				<td><span id="chanceInfo"></span></td>
			</tr>
		</table>

	</div>
	<div style="margin: 2% 4%">
		<input class="easyui-datetimebox" name="planDate" id="planDate"
			label="计划到期时间:" labelAlign="right" value="<fmt:formatDate value="${plan.planDate }" pattern="yyyy-MM-dd hh:mm:ss" />"
			data-options="required:true,showSeconds:false,editable:false,panelWidth:200,formatter:formatter"
			labelWidth="120" style="width: 50%">
			
	</div>
	




	<div style="margin: 2% 4%">
		<div style="font-weight: bold; font-size: 20px">开发计划的具体内容：</div>
		<textarea id="planContext" name="content">
		${plan.content }
		</textarea>

	</div>



</form>
