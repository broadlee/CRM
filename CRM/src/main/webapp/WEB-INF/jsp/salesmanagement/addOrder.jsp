<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<style>
#order-form tr td {
	padding: 5px 20px;
}
</style>

<form id="quoteOrderForm" method="post">
	<table id="order-form" width="90%" border="0" style="padding: 6px;">
		<tr>
		
		
		
		
			<td width="50%" colspan="2">
				 <input type="hidden"name="member.id" value="${member.id }"/>
				  <input " label="创建人" editable="false"
				  	value="${member.name }" 
					 style="width: 30%;" required="true"
					class="easyui-textbox" label="主题">
				
			</td>
		</tr>
		<tr>
			<td width="50%" colspan="2">
				 <input name="theme" label="主题" value="${quo.theme }"
					prompt="请输入主题内容" style="width: 80%;" required="true"
					class="easyui-textbox" label="主题">
			</td>
		</tr>
		<tr>
			<td width="50%" colspan="2">
				<input id="order-customer1" value="${chance.customer.name }"
				label="客户" editable="false" style="width: 40%;"
				required="true" class="easyui-textbox"> 
				<input name="customer.id" value="${chance.customer.id }" type="hidden">
				<input name="chance.id" value="${chance.id }" type="hidden">
				 <input type="hidden"name="quoteId" value="${quo.id }"/>
				 <input type="hidden"name="chanceId" value="${chance.id }"/> 
			</td>
		</tr>
		<tr>
			<td colspan="2">	
				<select id="order-address" name="address.id"
				class="easyui-combogrid" style="width: 500px;"
				label="收货地址"
				required="true"    
				data-options=" 				
			            panelWidth:'auto',
			            prompt:'选择地址',  
			            idField:'id', 
			            textField:'address',
			            method:'get',
			            panelWidth:700,
			            editable:false,
			            fitColumns:true,
			            url:'order/order/addressList/${chance.customer.id }',
			            onLoadSuccess:function(data){
		        		data=data.rows;
			        		if(data.length==0){		        		
				        		$.messager.alert('提示','该客户还没有添加收货地址，请先添加收货地址！');
				        	}
		        		},	              
			            columns:[[    
			                {field:'id',title:'ID',width:60},    
			                {field:'receiver',title:'收货人姓名',width:100},    
			                {field:'address',title:'收货地址',width:120},    
			                {field:'concact',title:'联系方式',width:100},    
			                {field:'codes',title:'邮编',width:100},   
			                {field:'moren',title:'是否默认',width:100,formatter:function(value,row,index){
			                	if(row&&row.moren){
			                	 return '是';
			                	}else{
			                	return '否';
			                	}
			                }},     
			            ]]    
			        "></select>
			<!--  <script type="text/javascript">
				$("#order-address").combogrid({
		        	url:"order/order/addressList/${chance.customer.id }",
		        	onLoadSuccess:function(data){//加载成功后设计默认地址
		        		data=data.rows;
		        		if(data.length!=0){		        		
			        		for(var i=0;i<data.length;i++){
			        			if(data[i].moren){
			        				$('#order-address').combogrid('grid').datagrid('selectRecord',data[i].id);
			        			}
			        		}
			        	}else{
			        		$.messager.alert("提示","该客户还没有添加收货地址，请先添加收货地址！");
			        		}
		        	}
		        });
			
			</script> -->
			</td>
			
		</tr>
		<tr>
			<td width="30%">
				<select id="order-state" name="state"
					class="easyui-combobox" style="width: 250px;" 
					data-options="panelHeight:'auto'" label="订单状态">
						<option value="XIA" selected="true">已下单</option>
				</select>
			</td>
			<td width="50%">
			</td>
		</tr>
		<tr>
			<td width="50%" colspan="2">
				<input name="remark" prompt="备注内容"
					style="width: 60%;" multiline="true" class="easyui-textbox"
					label="备注">
			</td>
		</tr>
		<tr>
			<td width="50%" colspan="2">
				<input id="orderTotal" name="total" value="${quo.total }"
					style="width: 30%;"  class="easyui-textbox"
					editable="false"
					required="true"
					label="总金额/元">
			</td>
		</tr>
	</table>

</form>

<div class="easyui-tabs" border="false">
	<div title="订单详情" style="overflow-y: auto; height:300px">

		<table style="width: 100%;overflow-x:hidden;" id="dg" class="easyui-datagrid"
			data-options="
						idField:'id',
						textField:'name',
						singleSelect:true,
						fitColumns: true,
						rownumbers: true, 
						pagination : true,
						border: false,
						url:'quote/getquodetail?quotation=${quo.id }',
						fit:true
					">
			<thead>
				<tr>
					<th style="width: 30%;"
						data-options="field:'product',formatter : function(value, row, index) {
											return value.name;
										}">产品名</th>
					<th style="width: 15%;"
						data-options="field:'price',formatter : function(value, row, index) {
											return row.product.sellPrice
										}">单价/元</th>
					<th style="width: 12%;"
						data-options="field:'count'">数量</th>
					<th style="width: 15%;"
						data-options="field:'cost'">折扣</th>

					<th style="width: 15%;"
						data-options="field:'subtotal',
						formatter : function(value, row, index) {
							return row.product.sellPrice*row.count*row.cost*0.01;
					}, editor: {type: 'textbox',options:{editable:false}}">小计/元</th>

				</tr>
			</thead>

			<tbody>
			</tbody>
		</table>
	</div>
</div>



