/**
 * @author pshc
 * 
 */

$(function() {

$('#salemanager').datagrid({
	view: detailview,
	sortName:"id" ,
	sortOrder:"desc",
	pagination : true,
	fitColumns : true,
	border : false,
	fit : true,
	idField : 'id',
	rownumbers : "true",
	url : "salemanager/getchance",
	singleSelect : "true",
	toolbar : "#salemanagertoolbar",
	columns : [ [ {
		field : "ck",
		checkbox : true
	}, {
		field : 'id',
		title : '编号',
		width : 50
	}, {
		field : 'customer',
		title : '客户',
		width : 100,
		formatter : function(value, row, index) {
			return value.name + "(" + value.linkWay + ")";
		}
	}, {
		field : 'member1',
		title : '创建人',
		width : 100,
		formatter : function(value, row, index) {
			return value.name;
		}
	}, {
		field : "createDate",
		title : "创建日期",
		width : 100,
		formatter : function(value, row, index) {
			return new Date(value).format("yyyy-MM-dd hh:mm:ss");
		}
	}, {
		field : 'state',
		title : '机会状态',
		width : 100,
		formatter : function(value, row, index) {

			switch (value) {
			case "NOASSIGN":
				return "未指派";
				break;
			case "YESASSIGN":
				return "已指派";
				break;
			case "SUCCESS":
				return "开发成功";
				break;
			case "FAILED":
				return "开发失败";
				break;
			}
		}
	}, {
		field : 'success',
		title : '成功率(%)',
		width : 100
	}] ],

	checkOnSelect : false,
	selectOnCheck : false, 
	
    detailFormatter:function(index,row){   
    	
        var re='<table style="margin:10px 0px;width:100%;border-collapse:collapse;">'+
        '<tr>' +
        '<td style="border:1px dotted #ccc;padding:5px">' +
        '客户: ' + row.customer.name +'</td>' +
        '<td style="border:1px dotted #ccc;padding:5px">' +
        '客户联系人:' + row.customer.linkMan +'</td>' +
        '<td style="border:1px dotted #ccc;padding:5px">' +
        '客户联系方式:' + row.customer.linkWay +'</td>' +
        '</tr>'+
        '<tr>'+
        '<td style="border:1px dotted #ccc;padding:5px">' +
        '<p>机会概要: ' + row.summary + '</p>' +
        '</td>' +
        '<td colspan="2" style="border:1px dotted #ccc;padding:5px">' +
        '<p>机会详情: ' + row.chanceInfo + '</p>' +
        '</td>' +
        '</tr>';
    	
        if(row.member2){
        	re+='</tr>'+
	        '<td style="border:1px dotted #ccc;padding:5px">' +
	        '指派人: ' + row.member2.name+'('+row.member2.account +')'+'</td>' +
	        '<td style="border:1px dotted #ccc;padding:5px">' +
	        '被指派人:' + row.member3.name+'('+row.member3.account +')'+'</td>' +
	        '<td style="border:1px dotted #ccc;padding:5px">' +
	        '指派时间:' + row.assignDate +'</td>' +
	        '</tr>'+'</table>';
        }else{
        	re+='</table>';
        }
        
        
        return re;
    },
	onClickRow:function(index,row){
		$('#salemanager').datagrid("selectRow",index);
	}


});


$('#search').searchbox({
	searcher : function(value, name) {
		
		var param = {};
		
		param[name]=value;
		
		param["state"]=$("#state").combobox("getValue");
		console.log(param);
		$('#salemanager').datagrid("load",param);
		
	},
	width : 300,
	menu : '#searchitem',
	prompt : '请输入查找关键字'
});

$("#salemanagertoolbar a.add").click(function() {

	if ($('#tabs').tabs("exists", "创建销售机会")) {
		$('#tabs').tabs("select", "创建销售机会")
	} else {
		$('#tabs').tabs('add', {
			title : "创建销售机会",
			href : "salemanager/createsale",
			closable : true
		});
	}
})

$("#salemanagertoolbar a.all").click(function() {
	
	$("#state").combobox("setValue","");
	$("#search").searchbox("setValue","");
	
	$('#salemanager').datagrid("load",{});
})



$("#salemanagertoolbar a.edit")
		.click(
				function() {

					var row = $('#salemanager').datagrid("getSelected");

					if (!row) {
						return;
					}

					var user = row.customer.name + "(" + row.customer.linkWay
							+ ")";

					if (row.state != "NOASSIGN") {
						alertWarn("该机会状态下不可编辑");
						return;
					}

					if ($('#tabs').tabs("exists", "("+row.id+")"+"编辑销售机会|" + user)) {
						$('#tabs').tabs("select", "("+row.id+")"+"编辑销售机会|" + user)
					} else {
						$('#tabs').tabs(
								'add',
								{
									title : "("+row.id+")"+"编辑销售机会|" + user,
									href : "salemanager/createsale?chancerow="
											+ row.id,
									closable : true
								});
					}

				})
				
$("#salemanagertoolbar a.editassign").click(function(){
	var row = $('#salemanager').datagrid("getSelected");

	if (!row) {
		alertWarn("请选择机会！")
		return;
	}
	if(row.state!="YESASSIGN"){
		alertWarn("该机会不可编辑指派！");
		return;
	}
	var wind = $("<div/>").dialog({
		title : '修改指派|'+row.customer.name+"("+row.customer.linkWay+")",
		width : 350,
		height : 160,
		href:"salemanager/setassaign?chanceId="+row.id ,
		modal : true,
		buttons : [ {
			iconCls : "icon-add",
			text:"确认修改指派",
			handler : function() {
				var form = $("#setassignform");
				// 校验表单数据
				if (form.form("validate")) { 
					var serial =  form.serialize()
					wind.dialog("destroy");
					$.post("salemanager/doassaign",serial,function(result){
						if(result.errors){
							alertWarn(result.errors);
						}else{
							alertWarn(result.inform);
							$('#salemanager').datagrid("reload");
						}
						
					})
					
					
				}
			}
		},{
			iconCls : "icon-cancel",
			text : "关闭",
			handler : function() {
				wind.dialog("destroy");
			}
		} ],
		onClose : function() {
			wind.dialog("destroy");
		}
	}).dialog("open");
	
	
	
})	
				
				
$("#salemanagertoolbar a.assign").click(function(){
	
	var row = $('#salemanager').datagrid("getSelected");

	if (!row) {
		alertWarn("请选择机会！")
		return;
	}
	if(row.state!="NOASSIGN"){
		alertWarn("该机会已经指派，请重新选择！")
		return;
	}
	var wind = $("<div/>").dialog({
		title : '指派|'+row.customer.name+"("+row.customer.linkWay+")",
		width : 350,
		height : 160,
		href:"salemanager/setassaign?chanceId="+row.id ,
		modal : true,
		buttons : [ {
			iconCls : "icon-add",
			text:"确认指派",
			handler : function() {
				var form = $("#setassignform");
				// 校验表单数据
				if (form.form("validate")) { 
					var serial = form.serialize();
					wind.dialog("destroy");
					$.post("salemanager/doassaign",serial,function(result){
						if(result.errors){
							alertWarn(result.errors);
						}else{
							alertWarn(result.inform);
							$('#salemanager').datagrid("reload");
						}
						
					})
					
					
				}
			}
		},{
			iconCls : "icon-cancel",
			text : "关闭",
			handler : function() {
				wind.dialog("destroy");
			}
		} ],
		onClose : function() {
			wind.dialog("destroy");
		}
	}).dialog("open");
	
	
	
	
})			
function alertWarn(inform){
	var wind = $("<div/>").dialog({
		title : '警告！',
		width : 220,
		height : 160,
		content : "<div style='margin:25px;font-size:15px;'>"+inform+"</div>",
		modal : true,
		openAnimation : "slide",
		buttons : [ {
			iconCls : "icon-cancel",
			text : "关闭",
			handler : function() {
				wind.dialog("destroy");
			}
		} ],
		onClose : function() {
			wind.dialog("destroy");
		}
	}).dialog("open");
}

$("#salemanagertoolbar .endchange").click(function(){
	var row = $('#salemanager').datagrid("getSelected");

	if (!row) {
		alertWarn("请选择机会！")
		return;
	}
	if(row.state=='SUCCESS'){
		alertWarn("该机会已经开发成功！");
		return;
	}
	
	$.messager.confirm('警告','该操作不可逆,你确定结束吗？',function(r){
	    if (r){
	        var param = {};
	        param.chanceId=row.id;
	    	$.post("salemanager/endchance" ,param,function(result){
	    		if(result.success){
	    			$('#salemanager').datagrid("reload");
	    		}else{
	    			$.messager.alert("警告","操作失败");
	    		}
	    	})
	    	
	    	
	    }
	});
	
	
})



				
$("#salemanagertoolbar a.delete").click(function(){
	
	var rows=$('#salemanager').datagrid("getChecked");
	
	if(!rows.length){
		alertWarn("请选择行！！")
		return;
	}
	var rowid="";
	
	for(var i = 0;i<rows.length;i++){
		rowid+="chances="+rows[i].id+"&";
	}
	
	var wind = $("<div/>").dialog({
		title : '提示！！',
		width : 220,
		height : 137,
		content : "<div style='margin:25px;font-size:15px;'>你确定删除吗？</div>",
		modal : true,
		openAnimation : "slide",
		buttons : [{
			iconCls : "icon-ok",
			text : "确定",
			handler : function() {
				wind.dialog("destroy");
				$.post("salemanager/deletechance",rowid,function(result){
					if(result.errors){
						alertWarn(result.errors);
					}else{
						
						var cus=result.rows
						for ( var row in cus) {
							
							var user = cus[row].customer.name + "(" + cus[row].customer.linkWay
							+ ")";
							if ($('#tabs').tabs("exists", "("+cus[row].id+")"+"编辑销售机会|" + user)) {
								$('#tabs').tabs("close", "("+cus[row].id+")"+"编辑销售机会|" + user)
							}
						}
						$('#salemanager').datagrid("reload");
						
						$('#salemanager').datagrid("uncheckAll");
						$('#salemanager').datagrid("unselectAll");
					}
				})
			}
		}, {
			iconCls : "icon-cancel",
			text : "关闭",
			handler : function() {
				wind.dialog("destroy");
			}
		} ],
		onClose : function() {
			wind.dialog("destroy");
		}
	}).dialog("open");
	
})
})

function selectOnChange(newi,old){
	
	var param = {};
	
	param[$("#search").searchbox("getName")]=$("#search").searchbox("getValue");
	
	param["state"]=newi;
	
	console.log(param)
	
	$('#salemanager').datagrid("load",param);
	
}















