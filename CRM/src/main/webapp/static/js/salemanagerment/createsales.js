/**
 * 
 */
function formatter(date) {
	return new Date(date).format("yyyy-MM-dd hh:mm:ss");
}

function findvoc(select) {
	
	$.post("salemanager/getvoc","customer="+$(select).val(),function(result){
		
		var rows = result.rows;
		

		var rowid="voc=";
		
		
		var simRows = {};
		
		for(var i=0;i<rows.length;i++){
			
			rowid+=rows[i].id+"&voc=";
			
			if(simRows[rows[i].product.name])
			{
				simRows[rows[i].product.name].number+=rows[i].number;
			}else{
				simRows[rows[i].product.name]=rows[i];
			}
		}
		$("#createsaleform").data("rowid",rowid);

		
		
		var info="需求：";
		var sum=0;
		for ( var row in simRows) {
			
			info+="【"+simRows[row].product.name+":"+"(数量："+simRows[row].number+";单价："+simRows[row].product.sellPrice+";总价："+(simRows[row].number*simRows[row].product.sellPrice)+")】；"
			sum+=simRows[row].number*simRows[row].product.sellPrice;
		}
		info+="总金额："+sum+"";
		
		if(result.errors){
			alert(result.errors);
			$("#memberclient"+$(select).data("id")).combobox("clear");
		}
		else
		{
			$("#chanceinfoid"+$(select).data("id")).textbox("setValue",info);
		}
	})
}


$(function() {

	$(".okcreate").unbind("click").click(function() {
		var form = $("#createsaleform"+$(this).data("id"));
		
		// 校验表单数据
		if (form.form("validate")) {
			$(this).attr("disabled","disabled");
			$.post("salemanager/addChance", form.serialize()+"&"+$("#createsaleform").data("rowid"), function(rs) {
				if (rs.errors) {
					alert(rs.errors);
				} else {
					alert("保存成功！！");
					if ($('#tabs').tabs("exists", "销售机会管理")) {
						$('#tabs').tabs("select", "销售机会管理");
						$('#salemanager').datagrid("reload");
					}
					console.log(rs.inform);
					$('#tabs').tabs("close", rs.inform);
					
				}
			});
		}

	})

})