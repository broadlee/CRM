/**
 * 
 */


function stateOnChange(newValue,oldValue) {
	var param ={};
	param.state=newValue;
	
	$('#plan').datagrid("load",param);
	
}

$(function() {
	$('#plan').datagrid({
		view: detailview,
		sortName : "id",
		sortOrder : "desc",
		pagination : true,
		fitColumns : true,
		border : false,
		fit : true,
		idField : 'id',
		rownumbers : "true",
		url : "plan/getPlan",
		singleSelect : "true",
		toolbar : "#plantoolbar",
		columns : [ [ {
			field : "ck",
			checkbox : true
		}, {
			field : 'id',
			title : '编号',
			width : 50
		}, {
			field : 'planDate',
			title : '计划时间',
			width : 100,
			formatter : function(value, row, index) {
				
				var time =new Date(value).getTime()-new Date().getTime();
				if(row.state=='ING'){
					if(time<0){
						return "<span style='color:red'>"+new Date(value).format("yyyy-MM-dd hh:mm:ss")+"(时间已经过期)</span> "
					}else if(time < 48*60*60*1000){
						return "<span style='color:red'>"+new Date(value).format("yyyy-MM-dd hh:mm:ss")+"(即将到期)</span> "
					}else{
						return new Date(value).format("yyyy-MM-dd hh:mm:ss");
					}
				}else{
					return new Date(value).format("yyyy-MM-dd hh:mm:ss");
				}
				
				
				
			}
		}, {
			field : 'state',
			title : '计划状态',
			width : 100,
			formatter : function(value, row, index) {

				switch (value) {
				case "ING":
					return "<span style='color:red'>执行中</span>";
					break;
				case "END":
					return "已结束";
					break;
				}
			}
		}, {
			field : "quotation",
			title : "报价单编号",
			width : 100,
			formatter : function(value, row, index) {
				if(value!=null){
					return value.quoId;
				}
				return ""
			}
			
		}, {
			field : 'chance',
			title : '所属机会',
			width : 100,
			formatter : function(value, row, index) {
				return value.id;
			}
			
		} ] ],

		checkOnSelect : false,
		selectOnCheck : false,

		 detailFormatter:function(index,row){
		    	return "<div style='border:1px solid #ddd;margin:10px 10px;'><h3 style='text-align:center'>计划内容</h3><hr>"+row.content+"</div>"
		 },
		onClickRow : function(index, row) {
			$('#plan').datagrid("selectRow", index);
		}

	});
	
	$("#plantoolbar a.create").click(function(){
	
		var wind = $("<div/>").dialog({
			title : '创建开发计划',
			width : 1200,
			height : 600,
			href:"plan/createplan",
			modal : true,
			buttons : [ {
				iconCls : "icon-add",
				text:"确认创建开发计划",
				handler : function() {
					var form = $("#planform");
					// 校验表单数据
					if (form.form("validate")) { 
						
						var param ={}
						var serial = form
								.serializeArray();
						for ( var i in serial) {
							param[serial[i].name]=serial[i].value
						}
						
						$.post("plan/createOpenPlan",param,function(result){
							if(result.errors){
								$.messager.alert("警告",result.errors,"error");
							}else{
								$('#plan').datagrid("reload");
								wind.dialog("destroy");
							}
						})
					}
				}
			},{
				iconCls : "icon-cancel",
				text : "关闭",
				handler : function() {
					wind.dialog("destroy");
				}
			} ],
			onClose : function() {
				wind.dialog("destroy");
			}
		}).dialog("open");
		
		
		
		
		
		
	})
	
	$("#plantoolbar a.edit").click(function(){
		var row = $('#plan').datagrid("getSelected");

		if (!row) {
			$.messager.alert("警告","请选择行","error");
			return;
		}
		
		if(row.state=="END"){
			$.messager.alert("警告","该计划已经结束！","error");
			return;
		}
		
		var wind = $("<div/>").dialog({
			title : '修改开发计划',
			width : 1200,
			height : 600,
			href:"plan/createplan?planid="+row.id,
			modal : true,
			buttons : [ {
				iconCls : "icon-edit",
				text:"确认修改开发计划",
				handler : function() {
					var form = $("#planform");
					// 校验表单数据
					if (form.form("validate")) { 
						var param ={}
						var serial = form
								.serializeArray();
						for ( var i in serial) {
							param[serial[i].name]=serial[i].value
						}
						
						$.post("plan/createOpenPlan",param,function(result){
							if(result.errors){
								$.messager.alert("警告",result.errors,"error");
							}else{
								$('#plan').datagrid("reload");
								wind.dialog("destroy");
							}
						})
					}
				}
			},{
				iconCls : "icon-cancel",
				text : "关闭",
				handler : function() {
					wind.dialog("destroy");
				}
			} ],
			onClose : function() {
				wind.dialog("destroy");
			}
		}).dialog("open");
		
		
		
		
		
		
	})
	
	$("#plantoolbar a.delete").click(function(){
		var rows=$('#plan').datagrid("getChecked");
		
		if(!rows.length){
			$.messager.alert("警告","请勾选行","error");
			return;
		}
		var rowid="";
		
		for(var i = 0;i<rows.length;i++){
			rowid+="planId="+rows[i].id+"&";
		}
		
		$.messager.confirm('提示', '你确定删除吗？', function(r){
			if (r){
				$.post("plan/deleteplan",rowid,function(result){
					if(result.errors){
						$.messager.alert("警告",result.errors,"error");
					}else{
						$('#plan').datagrid("reload");
						$('#plan').datagrid("uncheckAll");
					}
				})
			}
		});
		
		
		
	})
	
	$("#plantoolbar a.finish").click(function(){
		var row = $('#plan').datagrid("getSelected");

		if (!row) {
			$.messager.alert("警告","请选择行","error");
			return;
		}
		$.messager.confirm('提示', '你确定结束吗？', function(r){
			if (r){
				$.post("plan/finishplan","planId="+row.id+"&state=END",function(result){
					if(result.errors){
						$.messager.alert("警告",result.errors,"error");
					}else{
						$('#plan').datagrid("reload");
					}
				})
			}
		});
		
		
		
	})
	
	
	$("#plantoolbar a.restart").click(function(){
		var row = $('#plan').datagrid("getSelected");

		if (!row) {
			$.messager.alert("警告","请选择行","error");
			return;
		}
		$.messager.confirm('提示', '你确定重新开始吗？', function(r){
			if (r){
				$.post("plan/finishplan","planId="+row.id+"&state=ING",function(result){
					if(result.errors){
						$.messager.alert("警告",result.errors,"error");
					}else{
						$('#plan').datagrid("reload");
					}
				})
			}
		});
		
		
		
	})
	
	
	$("#plantoolbar a.all").click(function(){
		$("#planstate").combobox("setValue","");
		$('#plan').datagrid("load",{});
	})
	
	
	
	
	
	
})