$(function(){
	
	$("#editBar").on("click","a.search",function(){
		var row = $("#orderTable").datagrid("getSelected");
		if(row){
			var dialog = $("<div/>").dialog({
				title: "订单明细",
				minimizable:true,
				maximizable:true,
				width:550,
				height:600,
				modal:true,
				href:'order/order/orderDetails/'+row.id,
				onClose: function(){
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				},
				buttons:[
					{
						iconCls:"icon-ok",
						text:"打印",
						handler:function(){				
				
							var printHtml = document.getElementById("dam-orderDetails").innerHTML;

							var wind = window.open("",'newwindow', 'height=600, width=800, top=100, left=300, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=n o, status=no');

							wind.document.body.innerHTML = printHtml;

							wind.print();
							return false; 
			
			
						}
					},
					{
						iconCls:"icon-cancel",
						text:"取消",
						handler:function(){
							dialog.dialog("close");
						}
					}
				]
			});
		}else{
			$.messager.alert('提示', '请选择一条订单！');
		}
	});
	
});