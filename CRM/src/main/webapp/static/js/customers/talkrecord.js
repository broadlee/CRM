$(function(){
	
	var talkgrid = $("#talkRecord");
	
	talkgrid.datagrid({
		pagination:true,
		fit:true,
		border:false,
		singleSelect: true,
	    url:'customers/talkrecord/list',
		treefield:'text',
		idField:'id',
		toolbar:'#talkRecordToolsBar',
		columns : [ [ {
			field : 'id',
			title : '编号',
			width : 50
		} ,{
			field : 'customer',
			title : '所属客户',
			width : 120,
			formatter: function(value,row,index){
				return value.name;
			}
		} ,{
			field : 'date',
			title : '交谈日期',
			width : 150
		} ,{
			field : 'place',
			title : '交谈地点',
			width : 180
		} ,{
			field : 'summary',
			title : '交谈概要',
			width : 120
		} ,{
			field : 'detail',
			title : '交谈具体',
			width : 180
		}  ] ]
	});
	
	$('#talkSearch').searchbox({
		searcher : function(value, name) {
			var param = {};
			
			param[name]=value;
			
			talkgrid.datagrid("load",param);
		},
		width : 300,
		menu : '#searchitem',
		prompt : '请输入查找关键字'
	});
	
	$("#talkRecordToolsBar").on("click","a.search",function(){
		var row = talkgrid.datagrid("getSelected");
		if(row){
			var talkDialog = $("<div/>").dialog({
				title: "交往记录详情",
				width: 480,
				height: 380,
				modal:true,
				editable:false,
				href:'customers/talkrecord/select'+row.id,
				onClose: function(){
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				}
			});
		}else{
			$.messager.alert('提示', '请选择一条交往记录！');
		}
	});
	
});