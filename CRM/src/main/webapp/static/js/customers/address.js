	
	var gridTable = $("#addrTable");
	
	function onload(){
		$('.switchBtn').switchbutton({
			height: 20,
	        onText:'是',
	        offText:'否',
	        onChange: function(checked){
	        	var id = $(this).data("id");
	        	var cid = $(this).data("cid");
	            $.get("customers/change/"+id,{checked:checked,cid:cid},function(){
	            	gridTable.datagrid("reload");
	           	});
	    	}
	    });
	}

$("#editBar").on('click','a.delete',function(){
		var row = gridTable.datagrid("getSelected");
		if(row){
			$.messager.confirm('删除提示', '确认删除此记录?', function(r){
	            if (r){
	                $.get("customers/deleteaddr/"+row.id,function(){
	                	gridTable.datagrid("reload");
	                }).error(function(){
	                		$.messager.alert("提示","系统错误,删除失败!");
	                });
	            }
	        });
		}else{
			$.messager.alert('提示', '请选择一条收货地址！');
		}
	}).on('click','a.edit',function(){
		var row = gridTable.datagrid("getSelected");
		if(row){
			$.post("customers/editaddr/"+row.id,function(rs){
				$("#addressForm").form('load',{
					id:rs.addr.id,
					receiver:rs.addr.receiver,
					concact:rs.addr.concact,
					codes:rs.addr.codes,
					address:rs.addr.address
				})
			})
		}else{
			$.messager.alert('提示', '请选择一条收货地址！');
		}
	});