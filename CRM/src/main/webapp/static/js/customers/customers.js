$(function(){
	
	/*alert(window.screen.availHeight);*/
	
	var lifetime = {NO:"暂无需求", LATENT:"潜在客户", SIGN:"签约客户", REPEAT:"重复购买", LOSE:"失效"}
	
	var grid = $('#customers');
	
	grid.datagrid({
		pagination:true,
		fit:true,
		border:false,
		singleSelect: true,
	    url:'customers/list',
		treefield:'text',
		idField:'id',
		rownumbers: true,
		toolbar:'#customersToolsBar',
		columns : [ [ {
			field : 'name',
			title : '客户',
			width : 120
		} ,{
			field : 'linkMan',
			title : '联系人姓名',
			width : 100
		} ,{
			field : 'linkWay',
			title : '联系方式',
			width : 120
		} ,{
			field : 'lifeTime',
			title : '生命周期',
			width : 120,
			formatter:function(value,row,index){
				return lifetime[value];
			}
		} ,{
			field : 'createDate',
			title : '创建日期',
			width : 160
		} ,{
			field : 'location',
			title : '地址',
			width : 200
		} ,{
			field : 'email',
			title : '邮箱',
			width : 180
		} ,{
			field : 'edit',
			title : '操作',
			width : 120,
			formatter : function(value, row, index){
				return  "<button onclick='getAllAddr("+row.id+")' type='button' class='cell-button'>查看收货地址</button>";
			}
		}  ] ]
	});
	
	$('#customerSearch').searchbox({
		searcher : function(value, name) {
			
			var param = {};
			
			param[name]=value;
			
			grid.datagrid("load",param);
			
		},
		width : 300,
		menu : '#searchitem',
		prompt : '请输入查找关键字'
	});
	
	$("#customersToolsBar").on("click","a.edit",function(){
		var row = grid.datagrid("getSelected");
		if(row){
			var dialog = $("<div/>").dialog({
				title: "编辑客户信息",
				width: 400,
				height: 495,
				modal:true,
				href:'customers/form/'+row.id,
				onClose: function(){
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				},
				buttons:[
					{
						iconCls:"icon-ok",
						text:"保存",
						handler:function(){
							var form = $("#customerForm");
							//校验表单数据
							if(form.form("validate")){
								$.post("customers/save",form.serialize(),function(rs){
									if(rs.err){
										alert(rs.err);
									}else{
										//关闭当前窗口
										dialog.dialog("close");
										grid.datagrid("reload");
									}
								});
							}
						}
					},
					{
						iconCls:"icon-cancel",
						text:"取消",
						handler:function(){
							dialog.dialog("close");
						}
					}
				]
			});
		}else{
			$.messager.alert('提示', '请选择一个客户！');
		}
	}).on("click","a.delete",function(){
		var row = grid.datagrid("getSelected");
		if(row){
			$.messager.confirm('删除提示', '确认删除此记录?', function(r){
                if (r){
                    $.get("customers/delete/"+row.id,function(){
                    		grid.datagrid("reload");
                    }).error(function(){
                    		$.messager.alert("提示","系统错误,删除失败!");
                    });
                }
            });
		}else{
			$.messager.alert('提示', '请选择一个客户！');
		}
	}).on("click","a.add",function(){
		var row = grid.datagrid("getSelected");
		if(row){
			var talkDialog = $("<div/>").dialog({
				title: "添加交往记录",
				width: 480,
				height: 400,
				modal:true,
				href:'customers/talkrecord/form'+row.id,
				onClose: function(){
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				},
				buttons:[
					{
						iconCls:"icon-ok",
						text:"保存",
						handler:function(){
							var form = $("#ralkRecordForm");
							//校验表单数据
							if(form.form("validate")){
								$.post("customers/talkrecord/save",form.serialize(),function(rs){
									if(rs.err){
										alert(rs.err);
									}else{
										//关闭当前窗口
										talkDialog.dialog("close");
										grid.datagrid("reload");
									}
								});
							}
						}
					},
					{
						iconCls:"icon-cancel",
						text:"取消",
						handler:function(){
							talkDialog.dialog("close");
						}
					}
				]
			});
		}else{
			$.messager.alert('提示', '请选择一个客户！');
		}
	}).on('click','a.reload',function(){
		
		$("#lifeTime").combobox("setValue","");
		$("#customerSearch").searchbox("setValue","");
		grid.datagrid("load",{});
		
	}).on('click','a.search',function(){
		var row = grid.datagrid("getSelected");
		if(row){
			var talkDialog = $("<div/>").dialog({
				title: "用户历史订单",
				width: 900,
				height:587,
				modal:true,
				href:'customers/orderform/'+row.id,
				onClose: function(){
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				}
			});
		}else{
			$.messager.alert('提示', '请选择一个客户！');
		}
	});
});
//查看收货地址
function getAllAddr(id){
	
	var dialog = $("<div/>").dialog({
		title: "客户收货地址",
		width: 800,
		height: 647,
		modal:true,
		href:'customers/addrform/'+id,
		onClose: function(){
			//关闭窗户后，立即销毁窗口
			$(this).dialog("destroy");
		},
		buttons:[
			{
				iconCls:"icon-ok",
				text:"保存",
				handler:function(){
					var form = $("#addressForm");
					//校验表单数据
					if(form.form("validate")){
						$.post("customers/saveaddr",form.serialize(),function(rs){
							if(rs.err){
								alert(rs.err);
							}else{
								$("#addrTable").datagrid("reload");
								$("#addressForm").form('clear');
							}
						});
					}
				}
			},
			{
				iconCls:"icon-cancel",
				text:"取消",
				handler:function(){
					dialog.dialog("close");
				}
			}
		]
	});
}

function getCustomersByLifeTime(value,name){
	var param = {};
	
	param[$("#customerSearch").searchbox("getName")]=$("#customerSearch").searchbox("getValue");

	param["lifeTime"]=value;
	
	$('#customers').datagrid("load",param);
}