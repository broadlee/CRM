$(function(){
	var purStates = {NOPAY:"未付款",PAYED:"已付款",NODETAIL:"无产品"};
	var purchaseGird;
	purchaseGird = $("#purchaseGird").datagrid({
		fit:true,
		border:false,
		singleSelect:true,
		rownumbers:true,
		fitColumns:true,
		/* 显示分页工具栏 */
		pagination:true,
//		onLoadSuccess: compute,//加载完毕后执行计算  
		/* 奇偶行使用不同背景色 */
		/* striped:true, */
	    url:'purchase/list',
	    idField:'id',
	    toolbar:"#purchaseToolbar",
	    columns:[[
	        {title:'编号',field:'id',width:50},
	        {title:'采购主题',field:'theme',width:100},
	        {title:'采购单号',field:'chaseId',width:50},
	        {title:'采购时间',field:'date',width:100},
	        {title:'总金额',field:'total',width:50,formatter:function(value,row,index){
        		return "￥"+value;
       		}},
	        {title:'状态',field:'state',width:50,formatter:function(value,row,index){
        		return purStates[value];
	        }},
	        {title:'采购人',field:'member',width:100,
	        	formatter:function(value,row,index){
	        		return value.account;
	        }},
	        {title:'最晚到货时间',field:'lateTime',width:100},
	    ]]
	});
	
//	function compute() {//计算函数  
//	     var rows = purchaseGird.datagrid('getRows')//获取当前的数据行  
//	     var ptotal = 0;
//	     for (var i = 0; i < rows.length; i++) {  
//	         ptotal += rows[i]['total'];  
//	     }
//	     purchaseGird.datagrid('appendRow', { id: '<b>统计：</b>',theme: '',chaseId: '',
//	    	 date: '',state: '',member: '',lateTime: '',total: ptotal}); 
//	}
	
	$("#purchaseToolbar #purchaseState").combobox({
		valueField: 'id',
		textField: 'text',
		data:[{
		    "id":"",
		    "text":"请选择状态"
		},{
		    "id":"NODETAIL",
		    "text":"无产品"
		},{
		    "id":"NOPAY",
		    "text":"未付款"
		},{
		    "id":"PAYED",
		    "text":"已付款"
		}],
		onChange: function(before,after){
			var value = $("#purchaseToolbar #purchaseSearch").searchbox('getValue');
			var name = $("#purchaseToolbar #purchaseSearch").searchbox('getName');
			doSearch(value,name);
	    }
	});
	
	$("#purchaseToolbar").on("click","a.add",function(){
		var dialog = $("<div/>").dialog({
			title: "添加采购单",
			width: 450,
			height: 400,
			modal:true,
			href:'purchase/form',
			onClose: function(){
				//关闭窗户后，立即销毁窗口
				$(this).dialog("destroy");
			},
			buttons:[
				{
					iconCls:"icon-ok",
					text:"保存",
					handler:function(){
						var form = $("#purchaseForm");
						//校验表单数据
						if(form.form("validate")){
							$.post("purchase/save",form.serialize(),function(rs){
								if(rs.err){
									alert(rs.err);
								}else{
									//关闭当前窗口
									dialog.dialog("close");
									purchaseGird.datagrid("reload");
								}
							});
						}
					}
				},
				{
					iconCls:"icon-cancel",
					text:"取消",
					handler:function(){
						dialog.dialog("close");
					}
				}
			]
		});
	}).on("click","a.edit",function(){
		var row = purchaseGird.datagrid("getSelected");
		if(row){
			var dialog = $("<div/>").dialog({
				title: "编辑采购单",
				width: 450,
				height: 400,
				modal:true,
				href:'purchase/form/'+row.id,
				onClose: function(){
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				},
				buttons:[
					{
						iconCls:"icon-ok",
						text:"保存",
						handler:function(){
							var form = $("#purchaseForm");
							//校验表单数据
							if(form.form("validate")){
								$.post("purchase/save",form.serialize(),function(rs){
									if(rs.err){
										alert(rs.err);
									}else{
										//关闭当前窗口
										dialog.dialog("close");
										purchaseGird.datagrid("reload");
									}
								});
							}
						}
					},
					{
						iconCls:"icon-cancel",
						text:"取消",
						handler:function(){
							dialog.dialog("close");
						}
					}
				]
			});
		}else{
			alert("请选择要操作的行");
		}
	}).on("click","a.editDetail",function(){
		var row = purchaseGird.datagrid("getSelected");
		if(row){
			if(row.state != "PAYED"){
			var dialog = $("<div/>").dialog({
				title: "编辑采购明细",
				width: 1000,
				height: 600,
				closable:true,
				modal:true,
				href:'purchase/detail/'+row.id,
				onClose: function(){
					$.get("purchase/detail/state/"+row.id,function(){
						purchaseGird.datagrid("reload");
					}).error(function(){
                		$.messager.alert("提示","系统错误,更改状态失败!");
                    });
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				},
				buttons:[
					{
						iconCls:"icon-cancel",
						text:"完成",
						handler:function(){
							dialog.dialog("close");
							purchaseGird.datagrid("reload");
						}
					}
				]
			});
		}else{
			alert("已付款无法编辑");
			}
		}else{
			alert("请选择要操作的行");
		}
	}).on("click","a.look",function(){
		var row = purchaseGird.datagrid("getSelected");
		if(row){
			var dialog = $("<div/>").dialog({
				title: "采购单明细",
				minimizable:true,
				maximizable:true,
				width:700,
				height:600,
				modal:true,
				href:'purchase/detail/look/'+row.id,
				onClose: function(){
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				},
				buttons:[
					{
						iconCls:"icon-ok",
						text:"打印",
						handler:function(){				
				
							var printHtml = $("#purchaseDetails").innerHTML;

							var wind = window.open("",'newwindow', 'height=600, width=800, top=100, left=300, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=n o, status=no');

							wind.document.body.innerHTML = printHtml;

							wind.print();
							return false; 
						}
					},
					{
						iconCls:"icon-cancel",
						text:"取消",
						handler:function(){
							dialog.dialog("close");
						}
					}
				]
			});
		
			
		}else{
			alert("请选择要操作的行");
		}
	});
	
	$("#purchaseToolbar a.all").click(function() {
		
		$("#purchaseSearch").searchbox("setValue","");
		
		$('#purchaseGird').datagrid("load",{});
	});
	
	$('#purchaseSearch').searchbox({
		searcher : doSearch,
		width : 300,
		menu : '#purSearchitem',
		prompt : '请输入查找关键字'
	});
});

function doSearch(value,name){
	var param = {};
	param[name] = value;
	param["state"] = $("#purchaseToolbar #purchaseState").combobox("getValue");
	$('#purchaseGird').datagrid('load', param);
}