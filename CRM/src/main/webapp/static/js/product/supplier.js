$(function(){
	
	var supplierGird;
	supplierGird = $("#supplierGird").datagrid({
		fit:true,
		bpurchase:false,
		singleSelect:true,
		rownumbers:true,
		fitColumns:true,
		/* 显示分页工具栏 */
		pagination:true,
		/* 奇偶行使用不同背景色 */
		/* striped:true, */
	    url:'purchase/supp/list',
	    idField:'id',
	    toolbar:"#supplierToolbar",
	    columns:[[
	        {title:'编号',field:'id',width:180},
	        {title:'供应商名',field:'name',width:180},
	        {title:'联系人',field:'linkMan',width:180},
	        {title:'联系人联系方式',field:'linkWay',width:180},
	        {title:'地址',field:'location',width:180},
	        {title:'描述',field:'describes',width:180},
	    ]]
	});
	
	$("#supplierToolbar").on("click","a.add",function(){
		var dialog = $("<div/>").dialog({
			title: "新增供应商",
			width: 450,
			height: 400,
			modal:true,
			href:'purchase/supp/form',
			onClose: function(){
				//关闭窗户后，立即销毁窗口
				$(this).dialog("destroy");
			},
			buttons:[
				{
					iconCls:"icon-ok",
					text:"保存",
					handler:function(){
						var form = $("#supplierForm");
						//校验表单数据
						if(form.form("validate")){
							$.post("purchase/supp/save",form.serialize(),function(rs){
								if(rs.err){
									alert(rs.err);
								}else{
									//关闭当前窗口
									dialog.dialog("close");
									supplierGird.datagrid("reload");
								}
							});
						}
					}
				},
				{
					iconCls:"icon-cancel",
					text:"取消",
					handler:function(){
						dialog.dialog("close");
					}
				}
			]
		});
	}).on("click","a.edit",function(){
		var row = supplierGird.datagrid("getSelected");
		if(row){
			var dialog = $("<div/>").dialog({
				title: "编辑供应商",
				width: 450,
				height: 400,
				modal:true,
				href:'purchase/supp/form/'+row.id,
				onClose: function(){
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				},
				buttons:[
					{
						iconCls:"icon-ok",
						text:"保存",
						handler:function(){
							var form = $("#supplierForm");
							console.log(form.serialize());
							//校验表单数据
							if(form.form("validate")){
								$.post("purchase/supp/save",form.serialize(),function(rs){
									if(rs.err){
										alert(rs.err);
									}else{
										//关闭当前窗口
										dialog.dialog("close");
										supplierGird.datagrid("reload");
									}
								});
							}
						}
					},
					{
						iconCls:"icon-cancel",
						text:"取消",
						handler:function(){
							dialog.dialog("close");
						}
					}
				]
			});
		}else{
			alert("请选择要操作的行");
		}
	}).on("click","a.delete",function(){
		var row = supplierGird.datagrid("getSelected");
		if(row){
			$.messager.confirm('删除提示', '确认删除此记录?', function(r){
                if (r){
                    $.get("purchase/supp/delete/"+row.id,function(){
                    	supplierGird.datagrid("reload");
                    }).error(function(){
                    		$.messager.alert("提示","删除失败，被关联无法删除!请先删除关联的数据!");
                    });
                }
            });
		}else{
			alert("请选择要操作的行");
		}
	});
	
	$("#supplierToolbar a.all").click(function() {
		
		$("#suppSearch").searchbox("setValue","");
		
		$('#supplierGird').datagrid("load",{});
	});
	
	$('#suppSearch').searchbox({
		searcher :doSearch,
		width : 300,
		menu : '#suppSearchitem',
		prompt : '请输入查找关键字'
	});
});

function doSearch(value,name){
	var param = {};
	param[name] = value;
	$('#supplierGird').datagrid('load', param);
}