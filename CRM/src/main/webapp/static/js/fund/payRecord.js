$(function() {
	
	var types = {PURCHASE:"采购",RETURN:"退货款"};
	var grid;
	grid = $("#payRecordGird").datagrid({
		fit:true,
		border:false,
		pagination:true,
		fitColumns:true,
	    url:'fund/payRecord/list',
	    rownumbers:true,
	    idField:'id',
	    toolbar:"#payRecordToolbar",
	    columns:[[
			{title:'ID',field:'id',width:50},
	        {title:'关联退货单编号',field:'returns',width:130,formatter:function(value,row,index){
	        	if(value.returnId == null){
	        		return "<span style='color:lightgray'>此记录为采购单付款记录</span>";
	        	} else {
	        		return value.returnId;
	        	}
	        }},
	        {title:'关联采购单编号',field:'purchase',width:130,formatter:function(value,row,index){
	        	if(value.chaseId == null){
	        		return "<span style='color:lightgray'>此记录为退货单付款记录</span>";
	        	} else {
	        		return value.chaseId;
	        	}
	        }},
	        {title:'付款金额',field:'money',width:100},
	        {title:'付款类型',field:'type',width:150,formatter:function(value,row,index){
	        	return types[value];
	        }},
	        {title:'付款时间',field:'date',width:150,formatter:function(value,row,index){
	        	return new Date(value).format("yyyy-MM-dd hh:mm:ss");
	        }}
	    ]]
	});
	
$('#payRecordToolbar #search').searchbox({width : 300});
	
	$("#payRecordToolbar #selectType").combobox({
		valueField: 'id',
		textField: 'text',
		data:[{
		    "id":"",
		    "text":"请选择类别"
		},{
		    "id":"PURCHASE",
		    "text":"采购"
		},{
		    "id":"RETURN",
		    "text":"退货款"
		}],
		onChange: function(before,after){
			var value = $("#payRecordToolbar #search").searchbox('getValue');
			var name = $("#payRecordToolbar #search").searchbox('getName');
			doSearch(value,name);
	    }
	});
	
	// 为工具栏的按钮绑定事件
	$("#payRecordToolbar").on("click","a.detail",function(){
		var row = grid.datagrid("getSelected");
		if(!row){
			$.messager.alert('提示','请选择要查看的记录！');
		} else {
			if(row.type == "PURCHASE"){
				var dialog = $("<div/>").dialog({
					title: "付款记录详情-采购单",
					width: 550,
					modal: true,
					left: 400,
					top: 100,
					href:'purchase/detail/look/' + row.id,
					onClose: function(){
						//关闭窗户后，立即销毁窗口
						$(this).dialog("destroy");
					}
				});
			} else {
				var dialog = $("<div/>").dialog({
					title: "付款记录详情-退货单",
					width: 550,
					modal: true,
					left: 400,
					top: 100,
					href:'return/return/returnDetail/' + row.id,
					onClose: function(){
						//关闭窗户后，立即销毁窗口
						$(this).dialog("destroy");
					}
				});
			}
		}
	})
})

function doSearch(value,name){
	var param = {};
	param[name] = value;
	var typeVal = $("#payRecordToolbar #selectType").combobox("getValue")
	param["type"] = typeVal;
	$('#payRecordGird').datagrid('load', param);
}