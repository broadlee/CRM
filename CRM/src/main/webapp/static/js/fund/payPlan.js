$(function() {
	var types = {PURCHASE:"采购",RETURN:"退货款"};
	var grid;
	grid = $("#payPlanGird").datagrid({
		fit:true,
		border:false,
		singleSelect:true,
		pagination:true,
		fitColumns:true,
	    url:'fund/payPlan/list',
	    rownumbers:true,
	    idField:'id',
	    toolbar:"#payPlanToolbar",
	    columns:[[
			{title:'ID',field:'id',width:50},
	        {title:'退货单编号',field:'returns',width:130,formatter:function(value,row,index){
	        	if(value == null){
	        		return "<span style='color:lightgray'>此计划为采购单付款计划</span>";
	        	} else {
	        		return value.returnId;
	        	}
	        }},
	        {title:'采购单编号',field:'purchase',width:130,formatter:function(value,row,index){
	        	if(value == null){
	        		return "<span style='color:lightgray'>此计划为退货单付款计划</span>";
	        	} else {
	        		return value.chaseId;
	        	}
	        }},
	        {title:'计划付款金额',field:'money',width:100},
	        {title:'付款类型',field:'type',width:150,formatter:function(value,row,index){
	        	return types[value];
	        }},
	        {title:'是否过期',field:'lateTime',width:150,formatter:function(value,row,index){
	        	if(new Date(value).format("yyyy-MM-dd hh:mm:ss") < new Date().format("yyyy-MM-dd hh:mm:ss")){
	        		return "已过期";
	        	} else {
	        		return "未过期";
	        	}
	        }},
	        {title:'创建时间',field:'createDate',width:150,formatter:function(value,row,index){
	        	return new Date(value).format("yyyy-MM-dd hh:mm:ss");
	        }}
	    ]]
	});
	
	$('#payPlanToolbar #search').searchbox({
		searcher : function(value, name) {
			if(value == ""){
				$('#payPlanGird').datagrid("load",{});
			} else {
				var param = {};
				param[name]=value;
				$('#payPlanGird').datagrid("load",param);
			}
		},
		width : 300,
		menu : '#searchitem',
		prompt : '请输入查找关键字'
	});
	
	// 为工具栏的按钮绑定事件
	$("#payPlanToolbar").on("click","a.submit",function(){
		var row = grid.datagrid("getSelected");
		if(!row){
			$.messager.alert('提示','请选择要付款的计划！');
		} else {
			 if(new Date(row.lateTime).format("yyyy-MM-dd hh:mm:ss") < new Date().format("yyyy-MM-dd hh:mm:ss")){
				$.messager.alert('提示','该付款计划已过期!');
			}else {
				if(row.returns != null){
					var dialog = $("<div/>").dialog({
						title: "确认付款",
						width: 550,
						modal: true,
						left: 400,
						top: 100,
						href:'return/return/returnDetail/'+row.returns.id,
						onClose: function(){
							//关闭窗户后，立即销毁窗口
							$(this).dialog("destroy");
						},
						buttons:[
							{
								iconCls:"icon-ok",
								text:"保存",
								handler:function(){
									$.messager.confirm('提示','确认已付款？',function(data){
										if(data){
											if(row.purchase != null ){
												var oId = row.purchase.id;
											} else {
												var oId = "";
											}
											
											if(row.returns != null){
												var rId = row.returns.id;
											} else {
												var rId = "";
											}
											
											var param = '&money=' + row.money + '&date=' + new Date().format("yyyy-MM-dd hh:mm:ss") + '&type=' + row.type
											 + "&rId=" + rId + "&oId=" + oId + "&planId=" + row.id;
											
											//校验表单数据
											$.post("fund/payRecord/save",param,function(rs){
												if(rs.err){
													$.messager.alert('提示',rs.err);
												}else{
													$.messager.alert('提示','付款完成');
													//关闭当前窗口
													dialog.dialog("close");
													grid.datagrid("reload");
												}
											});
										}
										//关闭当前窗口
										dialog.dialog("close");
										grid.datagrid("reload");
									});
								}
							},
							{
								iconCls:"icon-cancel",
								text:"取消",
								handler:function(){
									dialog.dialog("close");
								}
							}
						]
					});
				} else {
					var dialog = $("<div/>").dialog({
						title: "确认付款",
						width: 550,
						modal: true,
						left: 400,
						top: 100,
						href:'purchase/detail/look/'+row.purchase.id,
						onClose: function(){
							//关闭窗户后，立即销毁窗口
							$(this).dialog("destroy");
						},
						buttons:[
							{
								iconCls:"icon-ok",
								text:"保存",
								handler:function(){
									$.messager.confirm('提示','确认已付款？',function(data){
										if(data){
											if(row.purchase != null ){
												var oId = row.purchase.id;
											} else {
												var oId = "";
											}
											
											if(row.returns != null){
												var rId = row.returns.id;
											} else {
												var rId = "";
											}
											
											var param = '&money=' + row.money + '&date=' + new Date().format("yyyy-MM-dd hh:mm:ss") + '&type=' + row.type
											 + "&rId=" + rId + "&oId=" + oId + "&planId=" + row.id;
											
											//校验表单数据
											$.post("fund/payRecord/save",param,function(rs){
												if(rs.err){
													$.messager.alert('提示',rs.err);
												}else{
													$.messager.alert('提示','付款完成');
													//关闭当前窗口
													dialog.dialog("close");
													grid.datagrid("reload");
												}
											});
										}
										//关闭当前窗口
										dialog.dialog("close");
										grid.datagrid("reload");
									});
								}
							},
							{
								iconCls:"icon-cancel",
								text:"取消",
								handler:function(){
									dialog.dialog("close");
								}
							}
						]
					});
				}
			}
		}
	})
})