$(function() {
	var types = {SUBMONEY:"订金",PATMENT:"回款",REPAIR:"维修"};
	var grid;
	grid = $("#receivePlanGird").datagrid({
		fit:true,
		border:false,
		singleSelect:true,
		pagination:true,
		fitColumns:true,
	    url:'fund/receivePlan/list',
	    rownumbers:true,
	    idField:'id',
	    toolbar:"#receivePlanToolbar",
	    columns:[[
			{title:'ID',field:'id',width:50},
	        {title:'订单编号',field:'orders',width:130,formatter:function(value,row,index){
	        	if(value == null){
	        		return "<span style='color:lightgray'>此计划为维修单回款计划</span>";
	        	} else {
	        		return value.orderId;
	        	}
	        }},
	        {title:'维修单编号',field:'repair',width:130,formatter:function(value,row,index){
	        	if(value == null){
	        		return "<span style='color:lightgray'>此计划为订单回款计划</span>";
	        	} else {
	        		return value.repairId;
	        	}
	        }},
	        {title:'计划回款金额',field:'money',width:100},
	        {title:'回款类型',field:'type',width:150,formatter:function(value,row,index){
	        	return types[value];
	        }},
	        {title:'是否过期',field:'lateTime',width:150,formatter:function(value,row,index){
	        	if(new Date(value).format("yyyy-MM-dd hh:mm:ss") < new Date().format("yyyy-MM-dd hh:mm:ss")){
	        		return "已过期";
	        	} else {
	        		return "未过期";
	        	}
	        }},
	        {title:'创建时间',field:'createDate',width:150,formatter:function(value,row,index){
	        	return new Date(value).format("yyyy-MM-dd hh:mm:ss");
	        }}
	    ]]
	});
	
	$('#receivePlanToolbar #search').searchbox({
		searcher : function(value, name) {
			if(value == ""){
				$('#receivePlanGird').datagrid("load",{});
			} else {
				var param = {};
				param[name]=value;
				$('#receivePlanGird').datagrid("load",param);
			}
		},
		width : 300,
		menu : '#searchitem',
		prompt : '请输入查找关键字'
	});
	
	// 为工具栏的按钮绑定事件
	$("#receivePlanToolbar").on("click","a.submit",function(){
		var row = grid.datagrid("getSelected");
		if(!row){
			$.messager.alert('提示','请选择要回款的计划！');
		} else {
//				 if(row.orders != null){
					 var dialog = $("<div/>").dialog({
							title: "确认回款-订单",
							width: 550,
							modal: true,
							left: 400,
							top: 100,
							href:'order/order/orderDetails/' + row.orders.id,
							onClose: function(){
								//关闭窗户后，立即销毁窗口
								$(this).dialog("destroy");
							},
							buttons:[
								{
									iconCls:"icon-ok",
									text:"确认",
									handler:function(){
										$.messager.confirm('提示','确认已回款？',function(data){
											if(data){
												
												if(row.orders != null ){
													var oId = row.orders.id;
												} else {
													var oId = "";
												}
												
												if(row.repair != null){
													var rId = row.repair.id;
												} else {
													var rId = "";
												}
												
												var param = '&money=' + row.money + '&date=' + new Date().format("yyyy-MM-dd hh:mm:ss") + '&type=' + row.type
												 + "&rId=" + rId + "&oId=" + oId + "&planId=" + row.id;
												
												//校验表单数据
												$.post("fund/receiveRecord/save",param,function(rs){
													if(rs.err){
														$.messager.alert('提示',rs.err);
													}else{
														$.messager.alert('提示','回款完成');
														//关闭当前窗口
														dialog.dialog("close");
														grid.datagrid("reload");
													}
												});
											}
										});
									}
								},
								{
									iconCls:"icon-cancel",
									text:"取消",
									handler:function(){
										dialog.dialog("close");
									}
								}
							]
						});
				 /* else {
					 var dialog = $("<div/>").dialog({
							title: "确认回款-维修单",
							width: 550,
							modal: true,
							left: 400,
							top: 100,
							href:'repair/repairDetails/' + row.repair.id,
							onClose: function(){
								//关闭窗户后，立即销毁窗口
								$(this).dialog("destroy");
							},
							buttons:[
								{
									iconCls:"icon-ok",
									text:"确认",
									handler:function(){
										$.messager.confirm('提示','确认已回款？',function(data){
											if(data){
												//校验表单数据
												$.post("fund/receiveRecord/repairSave",{id:$("#repairId").val()},function(rs){
													if(rs.err){
														$.messager.alert('提示',rs.err);
													}else{
														$.messager.alert('提示','回款完成');
														//关闭当前窗口
														dialog.dialog("close");
														grid.datagrid("reload");
													}
												});
											}
										});
									}
								},
								{
									iconCls:"icon-cancel",
									text:"取消",
									handler:function(){
										dialog.dialog("close");
									}
								}
							]
						});
//				 }*/
		}
	})
	
	
	
})