$(function() {
	
	var types = {SUBMONEY:"订金",PATMENT:"回款",REPAIR:"维修"};
	var grid;
	grid = $("#receiveRecordGird").datagrid({
		fit:true,
		border:false,
		pagination:true,
		fitColumns:true,
	    url:'fund/receiveRecord/list',
	    rownumbers:true,
	    idField:'id',
	    toolbar:"#receiveRecordToolbar",
	    columns:[[
			{title:'ID',field:'id',width:50},
	        {title:'关联订单编号',field:'orders',width:130,formatter:function(value,row,index){
	        	if(value.orderId == null){
	        		return "<span style='color:lightgray'>此记录为维修单回款记录</span>";
	        	} else {
	        		return value.orderId;
	        	}
	        }},
	        {title:'关联维修单编号',field:'repair',width:130,formatter:function(value,row,index){
	        	if(value.repairId == null){
	        		return "<span style='color:lightgray'>此记录为订单回款记录</span>";
	        	} else {
	        		return value.repairId;
	        	}
	        }},
	        {title:'回款金额',field:'money',width:100},
	        {title:'回款类型',field:'type',width:150,formatter:function(value,row,index){
	        	return types[value];
	        }},
	        {title:'回款时间',field:'date',width:150,formatter:function(value,row,index){
	        	return new Date(value).format("yyyy-MM-dd hh:mm:ss");
	        }}
	    ]]
	});
	
	$('#receiveRecordToolbar #search').searchbox({width : 300});
	
	$("#receiveRecordToolbar #selectType").combobox({
		valueField: 'id',
		textField: 'text',
		data:[{
		    "id":"",
		    "text":"请选择类别"
		},{
		    "id":"SUBMONEY",
		    "text":"订金"
		},{
		    "id":"PATMENT",
		    "text":"回款"
		},{
		    "id":"REPAIR",
		    "text":"维修",
		}],
		onChange: function(before,after){
			var value = $("#receiveRecordToolbar #search").searchbox('getValue');
			var name = $("#receiveRecordToolbar #search").searchbox('getName');
			doSearch(value,name);
	    }
	});
	
	// 为工具栏的按钮绑定事件
	$("#receiveRecordToolbar").on("click","a.detail",function(){
		var row = grid.datagrid("getSelected");
		if(!row){
			$.messager.alert('提示','请选择要查看的记录！');
		} else {
			if(row.type == "SUBMONEY" || row.type == "PATMENT"){
				var dialog = $("<div/>").dialog({
					title: "付款记录详情-订单",
					width: 550,
					modal: true,
					left: 400,
					top: 100,
					href:'order/order/orderDetails/'+row.id,
					onClose: function(){
						//关闭窗户后，立即销毁窗口
						$(this).dialog("destroy");
					}
				});
			} else {
				var dialog = $("<div/>").dialog({
					title: "付款记录详情-维修",
					width: 550,
					modal: true,
					left: 400,
					top: 100,
					href:'repair/repairDetails/'+row.id,
					onClose: function(){
						//关闭窗户后，立即销毁窗口
						$(this).dialog("destroy");
					}
				});
			}
		}
	})
})

function doSearch(value,name){
	var param = {};
	param[name] = value;
	var typeVal = $("#receiveRecordToolbar #selectType").combobox("getValue")
	param["type"] = typeVal;
	console.log(param);
	$('#receiveRecordGird').datagrid('load', param);
}