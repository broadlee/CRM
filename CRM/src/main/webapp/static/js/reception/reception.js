$(function() {
$(".open-form").on("click",function() {
	var star = $(this);
	if (star.children("i.iconfont").css('color') != 'rgb(255, 0, 0)') {
		var id = $(this).parent().siblings("input[type='hidden']").val();
var dialog = $("<div/>").dialog({
	title : "选择产品",
	width : 450,
	height : 250,
	modal : true,
	href : 'reception/form/' + id,
	onClose : function() {
		// 关闭窗户后，立即销毁窗口
		$(this).dialog("destroy");
	},
buttons : [{
			iconCls : "icon-ok",
			text : "保存",
			handler : function() {
				var id = $("#receptionForm #id").val();
				var name = $("#receptionForm #name").val();
				var num = $("#receptionForm #num").val();
				$('#orderDg').datagrid('appendRow',
						{
							id : id,
							name : name,
							num : num
						});
				var input = $("<input type='hidden' name='id"
						+ id
						+ "' value="
						+ id
						+ '.'
						+ num
						+ ">");
				var form = $("#userForm");
				// 将该输入框插入到 form中
				form.append(input);
				// 关闭弹窗
				dialog.dialog("close");
				star.children("i.iconfont").css({
					color : '#ff0000'
				})
			}
		},
			{
				iconCls : "icon-cancel",
				text : "取消",
				handler : function() {
					dialog.dialog("close");
				}
			} ]
		});
	}

});

	$('#pp').pagination(
			{
				onSelectPage : function(pageNumber, pageSize) {
					$(this).pagination('loading');
					$(this).pagination('loaded');
					$('#content').panel(
							'refresh',
							'reception/list?page=' + pageNumber + '&pageSize='
									+ pageSize);
				}
			});

});

function submitForm() {
	var numArray ="";
	$("#userForm").children("input[type=hidden]").each(function(){
		var text = $(this).val();
		numArray += text+",";
	});
	$('#userForm').form('submit',{
		url : "reception/userForm?numArray="+numArray
	});
	$('#userForm').form('clear');
	$('#orderDg').datagrid('loadData',{total:0,rows:[]});  
	$("#content").find("i.iconfont").css({
		color:'#333333'
	});
	//找到form表单对应id的隐藏input
	var input = $("#userForm").children("input[type=hidden]");
	//移除
	input.remove();
}
function clearForm() {
	$('#userForm').form('clear');
}
