$(function(){
	$('#orderDg').datagrid({
		fit:true,
		border:false,
		singleSelect:true,
		pagination:true,
	    idField:'id',
	    columns:[[
	    	{field:'id',title:'ID',hidden:"hidden"},
	        {field:'name',title:'产品名称',width:170,align:'center'},
	        {field:'num',title:'数量',width:80,align:'center'},
	        {field:'edit',title:'操作',width:80,align:'center',formatter:function(value,row,index){
	        	return "<button onclick='del("+row.id+")' class='del' style='border-radius: 4px;border: 1px solid #0a73ab;background: #0a73ab;color: #fff;outline: none;'>删除</button>";
	        }}
	    ]],
	    onDblClickCell: function(index,field,value){
//	    	alert(field);
	    }
	});
});
//移除已选产品
function del(event){
	$.messager.confirm("提示","确认删除？",function(data){
		if(data){
			//找到当前这列的产品id
			var data = $('#orderDg').datagrid("getData");
			for (var i = 0; i < data.rows.length; i++) {
				if(data.rows[i].id==event){
					var rowIndex = $('#orderDg').datagrid("getRowIndex",data.rows[i]);
					$('#orderDg').datagrid('deleteRow',rowIndex);
				}
			}
			var id = event;
			var form = $("#userForm");
			//找到form表单对应id的隐藏input
			var input = $(form).find("input[name='id"+id+"']");
			//移除
			input.remove();
			//找到显示列表中的对应id，改变收藏颜色
			var li = $("#content").find("ul input[value='"+id+"']");
			li.siblings("li").find("i.iconfont").css({
				color:'#333333'
			});
		}
	})
}


