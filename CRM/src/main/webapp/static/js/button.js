/**
 * 
 */

$(function() {

	$("button").mouseenter(function() {
		$(this).css("background", "#fff").css("color", "#0a73ab")
	}).mouseleave(function() {
		$(this).css("background", "#0a73ab").css("color", "#fff")
	}).mousedown(function() {
		$(this).css("background", "#0a73ab").css("color", "#fff")
	}).mouseup(function() {
		$(this).css("background", "#fff").css("color", "#0a73ab")
	})	

})
