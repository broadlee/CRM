$(function() {
	
	//获取session
	var process = {UNCHECK:"待检测",UNCONFIRM:"待客户确认",PEPAIRING:"维修中",HANDLE:"已交付"};
	var grid;
	grid = $("#repairGird").datagrid({
		fit:true,
		border:false,
		singleSelect:true,
		pagination:true,
		fitColumns:true,
	    url:'repair/list',
	    rownumbers:true,
	    idField:'id',
	    toolbar:"#repairToolbar",
	    columns:[[
			{title:'ID',field:'id',width:50},
			{title:'维修单号',field:'repairId',width:130},
	        {title:'客户名称',field:'customer',width:100,formatter:function(value,row,index){
	        		return value.name;
	        }},
	        {title:'维修产品',field:'product',width:100,formatter:function(value,row,index){
        		return value.name;
	        }},
	        {title:'维修主题',field:'theme',width:180},
	        {title:'进度',field:'process',width:70,formatter:function(value,row,index){
	        		return process[value];
	        }},
	        {title:'被指派维修人',field:'member',width:100,formatter:function(value,row,index){
	        		return value.name;
	        }},
	        {title:'创建时间',field:'date',width:150,formatter:function(value,row,index){
	        	return new Date(value).format("yyyy-MM-dd hh:mm:ss");
	        }},
	        {title:'收取费用',field:'money',width:50}
	    ]]
	});
	
	$("#repairToolbar #search").searchbox({width:250});
	
	$("#repairToolbar a.all").click(function() {
		$("#repairToolbar #selectProcess").combobox("setValue","");
		$("#repairToolbar #search").searchbox("setValue","");
		$('#repairGird').datagrid("load",{});
	})
	
	$("#repairToolbar #selectProcess").combobox({
		valueField: 'id',
		textField: 'text',
		data:[{
		    "id":"",
		    "text":"请选择进度"
		},{
		    "id":"UNCHECK",
		    "text":"待检测"
		},{
		    "id":"UNCONFIRM",
		    "text":"待客户确认"
		},{
		    "id":"PEPAIRING",
		    "text":"维修中"
		},{
		    "id":"HANDLE",
		    "text":"已交付",
		}],
		onChange: function(before,after){
			var value = $("#repairToolbar #search").searchbox('getValue');
			var name = $("#repairToolbar #search").searchbox('getName');
			doSearch(value,name);
	    }
	});
	
	// 为工具栏的按钮绑定事件
	$("#repairToolbar").on("click","a.add",function(){
		
		var dialog = $("<div/>").dialog({
			title: "添加维修单",
			width: 550,
			modal: true,
			left: 400,
			top: 130,
			href:'repair/form',
			onClose: function(){
				//关闭窗户后，立即销毁窗口
				$(this).dialog("destroy");
			},
			buttons:[
				{
					iconCls:"icon-ok",
					text:"保存",
					handler:function(){
						if($("#repairForm #selectCus").val() != "" && $("#repairForm #product").val() != "" 
							&& $("#repairForm #theme").val() != "" && $("#repairForm #selectMem").val() != ""){
							switch ($("#repairForm #process").val()) {
								case "待检测":
									$("#repairForm #process").textbox({value:"UNCHECK"});
									break;
								case "待客户确认":
									$("#repairForm #process").textbox({value:"UNCONFIRM"});
									break;
								case "维修中":
									$("#repairForm #process").textbox({value:"PEPAIRING"});
									break;
								case "已交付":
									$("#repairForm #state").textbox({value:"HANDLE"});
									break;
								default:
									break;
							}
							var form = $("#repairForm");
							console.log(form.serialize());
							//校验表单数据
							if(form.form("validate")){
								$.post("repair/save",form.serialize(),function(rs){
									if(rs.err){
										$.messager.alert('提示',rs.err);
									}else{
										$.messager.alert('提示','添加成功');
										//关闭当前窗口
										dialog.dialog("close");
										grid.datagrid("reload");
									}
								});
							}
						} else {
							$.messager.alert('提示','请填好所有必填信息!');
						}
					}
				},
				{
					iconCls:"icon-cancel",
					text:"取消",
					handler:function(){
						dialog.dialog("close");
					}
				}
			]
		});
	}).on("click","a.edit",function(){
		var row = grid.datagrid("getSelected");
		if(!row){
			$.messager.alert('提示','请选择要编辑的行！');
		} else {
			if(row.process != "UNCHECK"){
				$.messager.alert('提示','只可编辑 <待检测> 的服务');
			} else {
				var dialog = $("<div/>").dialog({
					title: "编辑维修单",
					width: 550,
					modal:true,
					left: 400,
					top: 100,
					href:'repair/form/'+row.id,
					onClose: function(){
						//关闭窗户后，立即销毁窗口
						$(this).dialog("destroy");
					},
					buttons:[
						{
							iconCls:"icon-ok",
							text:"保存",
							handler:function(){
								if($("#repairForm #theme").val() != ""){
									switch ($("#repairForm #process").val()) {
										case "待检测":
											$("#repairForm #process").textbox({value:"UNCHECK"});
											break;
										case "待客户确认":
											$("#repairForm #process").textbox({value:"UNCONFIRM"});
											break;
										case "维修中":
											$("#repairForm #process").textbox({value:"PEPAIRING"});
											break;
										case "已交付":
											$("#repairForm #process").textbox({value:"HANDLE"});
											break;
										default:
											break;
									}
									var form = $("#repairForm");
									//校验表单数据
									if(form.form("validate")){
										$.post("repair/save",form.serialize(),function(rs){
											if(rs.err){
												$.messager.alert('提示',rs.err);
											}else{
												$.messager.alert('提示','编辑成功');
												//关闭当前窗口
												dialog.dialog("close");
												grid.datagrid("reload");
											}
										});
									}
								} else {
									$.messager.alert('提示','请填好所有必填信息!');
								}
							}
						},
						{
							iconCls:"icon-cancel",
							text:"取消",
							handler:function(){
								dialog.dialog("close");
							}
						}
					]
				});
			}
		}
	}).on("click","a.delete",function(){
		var row = grid.datagrid("getSelected");
		if(!row){
			$.messager.alert('提示','请选择要删除的行！');
		} else{
			if(row.process != "HANDLE"){
				$.messager.alert('提示','不可删除正在执行中的维修单！');
			} else {
				$.messager.confirm('删除提示', '确认删除此记录?', function(r){
	                if (r){
	                    $.get("repair/delete/"+row.id,function(){
	                    	$.messager.alert('提示','删除成功');
	                    	grid.datagrid("reload");
	                    }).error(function(){
	                    	$.messager.alert("提示","系统错误,删除失败!");
	                    });
	                }
	            });
			}
		}
	}).on("click","a.see",function(){
		var row = grid.datagrid("getSelected");
		if(!row){
			$.messager.alert('提示','请选择行！');
		} else {
			var dialog = $("<div/>").dialog({
				title: "维修单详情",
				width: 550,
				modal:true,
				left: 400,
				top: 100,
				href:'repair/form/'+row.id,
				onClose: function(){
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				}
			});
		}
	}).on("click","a.check",function(){
		var row = grid.datagrid("getSelected");
		if(!row){
			$.messager.alert('提示','请选择要反馈的行！');
		} else {
			if(row.process != "UNCHECK"){
				$.messager.alert('提示','只可反馈 <待检测> 的维修单');
			} else {
				var dialog = $("<div/>").dialog({
					title: "反馈检测结果",
					width: 550,
					modal:true,
					left: 400,
					top: 100,
					href:'repair/form/'+row.id,
					onClose: function(){
						//关闭窗户后，立即销毁窗口
						$(this).dialog("destroy");
					},
					buttons:[
						{
							iconCls:"icon-ok",
							text:"保存",
							handler:function(){
								if($("#repairForm #backResult").val() != ""){
									switch ($("#repairForm #process").val()) {
										case "待检测":
											$("#repairForm #process").textbox({value:"UNCHECK"});
											break;
										case "待客户确认":
											$("#repairForm #process").textbox({value:"UNCONFIRM"});
											break;
										case "维修中":
											$("#repairForm #process").textbox({value:"PEPAIRING"});
											break;
										case "已交付":
											$("#repairForm #process").textbox({value:"HANDLE"});
											break;
										default:
											break;
									}
									var form = $("#repairForm");
									//校验表单数据
									if(form.form("validate")){
										$.post("repair/save",form.serialize(),function(rs){
											if(rs.err){
												$.messager.alert('提示',rs.err);
											}else{
												$.messager.alert('提示','反馈成功');
												//关闭当前窗口
												dialog.dialog("close");
												grid.datagrid("reload");
											}
										});
									}
								} else {
									$.messager.alert('提示','请填好所有必填信息!');
								}
							}
						},
						{
							iconCls:"icon-cancel",
							text:"取消",
							handler:function(){
								dialog.dialog("close");
							}
						}
					]
				});
			}
		}
	}).on("click","a.confirm",function(){
		var row = grid.datagrid("getSelected");
		if(!row){
			$.messager.alert('提示','请选择一条维修单！');
		} else {
			if(row.process != "UNCONFIRM"){
				$.messager.alert('提示','只可确认 <待客户确认> 的维修单');
			} else {
				var dialog = $("<div/>").dialog({
					title: "客户确认",
					width: 550,
					modal:true,
					left: 400,
					top: 100,
					href:'repair/form/'+row.id,
					onClose: function(){
						//关闭窗户后，立即销毁窗口
						$(this).dialog("destroy");
					},
					buttons:[
						{
							iconCls:"icon-ok",
							text:"保存",
							handler:function(){
								if($("#repairForm #money").val() != ""){
									switch ($("#repairForm #process").val()) {
										case "待检测":
											$("#repairForm #process").textbox({value:"UNCHECK"});
											break;
										case "待客户确认":
											$("#repairForm #process").textbox({value:"UNCONFIRM"});
											break;
										case "维修中":
											$("#repairForm #process").textbox({value:"PEPAIRING"});
											break;
										case "已交付":
											$("#repairForm #process").textbox({value:"HANDLE"});
											break;
										default:
											break;
									}
									var form = $("#repairForm");
									console.log(form.serialize());
									//校验表单数据
									if(form.form("validate")){
										$.post("repair/save",form.serialize(),function(rs){
											if(rs.err){
												$.messager.alert('提示',rs.err);
											}else{
												$.messager.alert('提示','客户确认成功');
												//关闭当前窗口
												dialog.dialog("close");
												grid.datagrid("reload");
											}
										});
									}
								} else {
									$.messager.alert('提示','请填好所有必填信息!');
								}
							}
						},
						{
							iconCls:"icon-cancel",
							text:"取消",
							handler:function(){
								dialog.dialog("close");
							}
						}
					]
				});
			}
		}
	});
});

function doSearch(value,name){
	var param = {};
	param[name] = value;
	param["process"] = $("#repairToolbar #selectProcess").combobox("getValue");
	$('#repairGird').datagrid('load', param);
}


