$(function() {
	$("#selectCus").combobox({
		url: "customers/selectList",
		method: "post",
		valueField:'id',
	    textField:'text'
	});
	
	$("#product").combobox({
		url: "product/selectList",
		method: "post",
		valueField: 'id',
		textField: 'text'
	});
	
	$("#selectMem").combobox({
		url: "permission/member/selectList",
		method: "post",
		valueField: 'id',
		textField: 'text'
	});
	
	var title = $(".panel-title").text().substr(2,$(".panel-title").text().length);
	
	if(title == "添加维修单"){
		$("#repairForm #createDate").textbox({disabled:true});
		$("#repairForm #money").textbox({readonly:true});
		$("#repairForm #backResult").textbox({readonly:true});
	} else if(title == "编辑维修单"){
		$("#repairForm #money").textbox({readonly:true});
		$("#repairForm #backResult").textbox({readonly:true});
	} else if(title == "反馈检测结果"){
		$("#repairForm #repairId").textbox({readonly:true});
		$("#repairForm #selectCus").combobox({readonly:true});
		$("#repairForm #product").combobox({readonly:true});
		$("#repairForm #theme").textbox({readonly:true});
		$("#repairForm #process").textbox({readonly:true});
		$("#repairForm #createDate").textbox({readonly:true});
		$("#repairForm #selectMem").combobox({readonly:true});
		$("#repairForm #money").textbox({readonly:true});
		$("#repairForm #backResult").textbox({required:true});
	}  else if(title == "客户确认"){
		$("#repairForm #repairId").textbox({readonly:true});
		$("#repairForm #selectCus").combobox({readonly:true});
		$("#repairForm #product").combobox({readonly:true});
		$("#repairForm #theme").textbox({readonly:true});
		$("#repairForm #process").textbox({readonly:true});
		$("#repairForm #createDate").textbox({readonly:true});
		$("#repairForm #selectMem").combobox({readonly:true});
		$("#repairForm #money").textbox({required:true});
		$("#repairForm #backResult").textbox({readonly:true});
	} else if(title == "维修单详情"){
		$("#repairForm #repairId").textbox({readonly:true});
		$("#repairForm #selectCus").combobox({readonly:true});
		$("#repairForm #product").combobox({readonly:true});
		$("#repairForm #theme").textbox({readonly:true});
		$("#repairForm #process").textbox({readonly:true});
		$("#repairForm #createDate").textbox({readonly:true});
		$("#repairForm #selectMem").combobox({readonly:true});
		$("#repairForm #money").textbox({readonly:true});
		$("#repairForm #backResult").textbox({readonly:true});
	}
})