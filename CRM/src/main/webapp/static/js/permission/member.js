$(function(){
	var grid;
	grid = $("#memberGird").datagrid({
		fit:true,
		border:false,
		singleSelect:true,
		pagination:true,
	    url:'permission/member/list',
	    idField:'id',
	    toolbar:"#memberToolbar",
	    columns:[[
	        {title:'帐号',field:'account',width:180},
	        {field:'roles',title:'关联角色',width:200,formatter:function(value,row,index){
	        		var roleNames = [];
	        		$.each(value,function(){
	        			roleNames.push(this.roleName);
	        		});
	        		return roleNames.join(",");
	        }},
	        {field:'enable',title:'状态',width:80,formatter:function(value,row,index){
	        		return value ? "可用":"禁用";
	        }}
	    ]]
	});
	

	// 为工具栏的按钮绑定事件
	$("#memberToolbar").on("click","a.add",function(){
		var dialog = $("<div/>").dialog({
			title: "新增用户",
			width: 450,
			height: 250,
			modal:true,
			href:'permission/member/form',
			onClose: function(){
				//关闭窗户后，立即销毁窗口
				$(this).dialog("destroy");
			},
			buttons:[
				{
					iconCls:"icon-ok",
					text:"保存",
					handler:function(){
						var form = $("#memberForm");
						//校验表单数据
						if(form.form("validate")){
							$.post("permission/member/save",form.serialize(),function(rs){
								if(rs.err){
									alert(rs.err);
								}else{
									//关闭当前窗口
									dialog.dialog("close");
									grid.datagrid("reload");
								}
							});
						}
					}
				},
				{
					iconCls:"icon-cancel",
					text:"取消",
					handler:function(){
						alert(2);
					}
				}
			]
		});
	}).on("click","a.edit",function(){
		var row = grid.datagrid("getSelected");
		if(row){
			var dialog = $("<div/>").dialog({
				title: "编辑用户",
				width: 450,
				height: 250,
				modal:true,
				href:'permission/member/form/'+row.id,
				onClose: function(){
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				},
				buttons:[
					{
						iconCls:"icon-ok",
						text:"保存",
						handler:function(){
							var form = $("#memberForm");
							//校验表单数据
							if(form.form("validate")){
								$.post("permission/member/save",form.serialize(),function(rs){
									if(rs.err){
										alert(rs.err);
									}else{
										//关闭当前窗口
										dialog.dialog("close");
										grid.datagrid("reload");
									}
								});
							}
						}
					},
					{
						iconCls:"icon-cancel",
						text:"取消",
						handler:function(){
							dialog.dialog("close");
						}
					}
				]
			});
		}
	}).on("click","a.delete",function(){
		var row = grid.datagrid("getSelected");
		if(row){
			$.messager.confirm('删除提示', '确认删除此记录?', function(r){
                if (r){
                    $.get("permission/member/delete/"+row.id,function(){
                    		grid.datagrid("reload");
                    }).error(function(){
                    		$.messager.alert("提示","系统错误,删除失败!");
                    });
                }
            });
		}
	});
});