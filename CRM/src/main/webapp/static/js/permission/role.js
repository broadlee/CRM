$(function(){
	var grid;
	grid = $("#roleGird").datagrid({
		fit:true,
		border:false,
		singleSelect:true,
	    url:'permission/role/list',
	    idField:'id',
	    toolbar:"#roleToolbar",
	    columns:[[
	        {title:'名称',field:'roleName',width:180},
	        {field:'enable',title:'状态',width:80,formatter:function(value,row,index){
	        		return value ? "可用":"禁用";
	        }}
	    ]]
	});
	

	// 为工具栏的按钮绑定事件
	$("#roleToolbar").on("click","a.add",function(){
		var dialog = $("<div/>").dialog({
			title: "新增角色",
			width: 450,
			height: 250,
			modal:true,
			href:'permission/role/form',
			onClose: function(){
				//关闭窗户后，立即销毁窗口
				$(this).dialog("destroy");
			},
			buttons:[
				{
					iconCls:"icon-ok",
					text:"保存",
					handler:function(){
						var form = $("#roleForm");
						//校验表单数据
						if(form.form("validate")){
							$.post("permission/role/save",form.serialize(),function(rs){
								if(rs.err){
									alert(rs.err);
								}else{
									//关闭当前窗口
									dialog.dialog("close");
									grid.datagrid("reload");
								}
							});
						}
					}
				},
				{
					iconCls:"icon-cancel",
					text:"取消",
					handler:function(){
						alert(2);
					}
				}
			]
		});
	}).on("click","a.edit",function(){
		var row = grid.datagrid("getSelected");
		if(row){
			var dialog = $("<div/>").dialog({
				title: "编辑角色",
				width: 450,
				height: 250,
				modal:true,
				href:'permission/role/form/'+row.id,
				onClose: function(){
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				},
				buttons:[
					{
						iconCls:"icon-ok",
						text:"保存",
						handler:function(){
							var form = $("#roleForm");
							//校验表单数据
							if(form.form("validate")){
								$.post("permission/role/save",form.serialize(),function(rs){
									if(rs.err){
										alert(rs.err);
									}else{
										//关闭当前窗口
										dialog.dialog("close");
										grid.datagrid("reload");
									}
								});
							}
						}
					},
					{
						iconCls:"icon-cancel",
						text:"取消",
						handler:function(){
							dialog.dialog("close");
						}
					}
				]
			});
		}
	}).on("click","a.delete",function(){
		var row = grid.datagrid("getSelected");
		console.log(row);
		if(row){
			$.messager.confirm('删除提示', '确认删除此记录?', function(r){
                if (r){
                    $.get("permission/role/delete/"+row.id,function(){
                    		grid.datagrid("reload");
                    }).error(function(){
                    		$.messager.alert("提示","系统错误,删除失败!");
                    });
                }
            });
		}
	}).on("click","a.grant",function(){
		var row = grid.datagrid("getSelected");
		var roleGrantTree;
		if(row){
			var dialog = $("<div/>").dialog({
				title: "为"+row.roleName+"角色授权",
				width: 450,
				height: 450,
				modal:true,
				href:'permission/role/grant/'+row.id,
				onClose: function(){
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				},
				onLoad:function(){
					 roleGrantTree = $('#roleGrantTree',this);
					//初始化资源树
					roleGrantTree.tree({
						checkbox:true
					});
				},
				buttons:[
					{
						iconCls:"icon-ok",
						text:"保存权限",
						handler:function(){
							//获取 tree 组件中节点为打勾和实心的
							var nodes =  roleGrantTree.tree('getChecked', ['checked','indeterminate']);
							var roleIds = [];
							$.each(nodes,function(){
								roleIds.push(this.id);
							});
							var parma = "roleId="+roleGrantTree.data("roleId");
							if(roleIds.length > 0){
								parma += "&rid="+roleIds.join("&rid=");
							}
							console.log(parma);
							$.post("permission/role/grant",parma,function(){
								dialog.dialog("close");
								$.messager.alert("提示","授权成功!");
							});
						}
					},
					{
						iconCls:"icon-cancel",
						text:"取消",
						handler:function(){
							dialog.dialog("close");
						}
					}
				]
			});
		}
	});
});