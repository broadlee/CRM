/**
 * 订单详情编辑
 */
$(function(){
	var orderDetailEditGird=$("#orderDetailEditGird");
	var order=$("#orderDetailOrderEdit").val();
	$("#orderDetailEditToolbar").on("click","a.edit",function(){
		var row = orderDetailEditGird.datagrid("getSelected");
		if(row){
			var dialog = $("<div/>").dialog({
				title: "编辑",
				width: 650,
				height: 300,
				maximizable:true,
				modal:true,
				href:'order/order/detailEditFrom/'+order+"/"+row.id,
				onClose: function(){
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				},
				buttons:[
					{
						iconCls:"icon-ok",
						text:"保存",
						handler:function(){
							var form = $("#detailEditFrom");
							//校验表单数据
							if(form.form("validate")){
								$.post("order/order/detailEditFrom/save",form.serialize(),function(rs){
									if(rs.msg){
										$.messager.alert("提示",rs.msg);
									}else{
										//关闭当前窗口
										dialog.dialog("close");
										orderDetailEditGird.datagrid("reload");
										$('#orderXiaGird').datagrid('reload');
									}
								});
							}
						}
					},
					{
						iconCls:"icon-cancel",
						text:"取消",
						handler:function(){
							dialog.dialog("close");
						}
					}
				]
			});
		}else{
			$.messager.alert("提示","请选择一个产品");
		}
	}).on("click","a.add",function(){
			var dialog = $("<div/>").dialog({
				maximizable:true,
				title: "添加",
				width: 650,
				height: 300,
				width: 650,
				height: 300,
				modal:true,
				href:'order/order/detailEditFrom/'+order,
				onClose: function(){
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				},
				buttons:[
					{
						iconCls:"icon-ok",
						text:"保存",
						handler:function(){
							var form = $("#detailEditFrom");
							//校验表单数据
							if(form.form("validate")){
								$.post("order/order/detailEditFrom/save",form.serialize(),function(rs){
									if(rs.msg){
										$.messager.alert("提示",rs.msg);
									}else{
										//关闭当前窗口
										dialog.dialog("close");
										orderDetailEditGird.datagrid("reload");
										$('#orderXiaGird').datagrid('reload');
									}
								});
							}
						}
					},
					{
						iconCls:"icon-cancel",
						text:"取消",
						handler:function(){
							dialog.dialog("close");
						}
					}
				]
			});
	}).on("click","a.delete",function(){
		var row = orderDetailEditGird.datagrid("getSelected");
		if(row){
			$.messager.confirm('删除提示', '确认删除此产品?', function(r){
                if (r){
                    $.get("order/order/orderDetail/delete/"+row.id,function(){
                    	orderDetailEditGird.datagrid("reload");
                    	$('#orderXiaGird').datagrid('reload');
                    }).error(function(){
                    		$.messager.alert("提示","系统错误,删除失败!");
                    });
                }
            });
		}
	});
});