/*
*  datagrid 获取正在编辑状态的行，使用如下：
*  $('#dg').datagrid('getEditingRowIndexs'); //获取当前datagrid中在编辑状态的行编号列表
*/
$.extend($.fn.datagrid.methods, {
    getEditingRowIndexs: function(jq) {
        var rows = $.data(jq[0], "datagrid").panel.find('.datagrid-row-editing');
        var indexs = [];
        rows.each(function(i, row) {
            var index = row.sectionRowIndex;
            if (indexs.indexOf(index) == -1) {
                indexs.push(index);
            }
        });
        return indexs;
    }
});

/**
 * 以下是行编辑
 */
var editGrid=$('#dg');//表格对象

var editIndex = undefined;// 正在编辑的行的索引

// 结束正在被编辑的行，编辑之前先对组件进行验证
function endEditing() {
	if (editIndex == undefined) {
		return true;
	}
	if (editGrid.datagrid('validateRow', editIndex)) { // 验证
		/*		//获取编辑的单元格
				var ed = editGrid.datagrid('getEditor', {index:editIndex,field:'p_id'});
				//获取编辑的单元格的文本
				var editCellValue=$(ed.target).combobox('getText');*/

		editGrid.datagrid('endEdit', editIndex);// 结束编辑

		/*		//将编辑的单元格文本显示到结束编辑后的单元格中
				var rows=editGrid.datagrid('getRows');
				rows[editIndex]['p_id']=editCellValue;
				//刷新单元格内容
				editGrid.datagrid('refreshRow', editIndex);	*/

		editIndex = undefined;

		return true;// 验证通过
	} else {
		return false;// 验证不通过
	}
}
// 单击取消编辑
function onClickRow() {
	endEditing();
}
// 单击单元格的事件
function onClickCell(index/* 行索引 */, field/* json中的一个属性 */) {
	if (editIndex != index) {// 如果正在编辑的行与单击的单元格所在行不同
		if (endEditing()) {// 先将原来编辑的行结束
			editGrid.datagrid('selectRow', index)// 选择新行
			.datagrid('beginEdit', index);// 新行处于编辑状态
			var ed = editGrid.datagrid('getEditor', {
				index : index,
				field : field
			});// 获取单元格编辑器
			if (ed) {
				($(ed.target).data('textbox') ? $(ed.target).textbox('textbox')
						: $(ed.target)).focus();
			}
			editIndex = index;
		} else {
			setTimeout(function() {
				editGrid.datagrid('selectRow', editIndex);
			}, 0);
		}
	}
}

// 结束编辑
function onEndEdit(index, row) {
	/*
	 * var ed = $(this).datagrid('getEditor', { index: index, field: 'productid'
	 * //根据实际情况进行改动 }); row.productname = $(ed.target).combobox('getText');
	 */
}

function onSelect(index, row) {
	if (editIndex != index) {
		editGrid.datagrid('validateRow', editIndex);
		editGrid.datagrid('endEdit', editIndex);// 结束编辑
	}
	editIndex = undefined;
}

// 追加一行
function append() {
	if (endEditing()) {
		// 追加新行
		editGrid.datagrid('appendRow', {
			p_id : '',
			price:'0.0',
			count : '1',
			cost : '10',
			subtotal : '0.0'
		});//指定默认值
		// 返回最后一行的索引
		editIndex = editGrid.datagrid('getRows').length - 1;
		// 选择最后一行，将最后一行变成编辑状态
		editGrid.datagrid('beginEdit', editIndex);

	}
}

//删除选中的行
function removeit() {
	//
	var rows = editGrid.datagrid('getSelections');
	//数据库删除成功则删除页面   再重新加载
	for (var i = 0; i < rows.length; i++) {
		var row = rows[i];
		var id = row.usid;

		//发送到数据库  删除
	/*	$.get("deleteUsers?userid=" + id, function(result) {

		});*/
		//删除页面
		var index = editGrid.datagrid('getRowIndex', row);
		editGrid.datagrid('deleteRow', index);
		//重新计算金额
		var edit=editGrid.datagrid('getEditingRowIndexs');
		//alert(edit[0]);
		if(edit[0]!=undefined){
			var subtotal = editGrid.datagrid('getEditor', {index:edit[0],field:'subtotal'});
			var subtotal1=$(subtotal.target).textbox('getValue');
			setTotal(subtotal1)
		}else{
			var rows=editGrid.datagrid('getRows');
			var total=0;		
			for(var i=0;i<rows.length;i++){
				
				 total+=parseFloat(rows[i]['subtotal']);				
			}
			$("#orderTotal").textbox("setValue",total);
		}
	
	}

}

// 获取添加、修改和删除的所在json对象
function getChanges() {
	var inserted = editGrid.datagrid('getChanges', "inserted");
	var updated = editGrid.datagrid('getChanges', "updated");
	console.log(JSON.stringify(inserted));
	console.log(JSON.stringify(updated));
}


//////////////////////////////////////////////////////////////////////
//判断是否有重复的商品
function addIfProduct(p_id){
	var result=true;
	//获取正在编辑的行
	var edit=editGrid.datagrid('getEditingRowIndexs');
	//获取当前的编辑器控件
	var product=editGrid.datagrid('getEditor', {index:edit[0],field:'p_id'});
	$(product.target).combogrid({
		onHidePanel:function(){
			//获取所有的行对象
			var rows=editGrid.datagrid('getRows');
			for(var i=0;i<rows.length;i++){
				//比较每一行的数据
				if(rows[i]['p_id']==p_id){
					result=false;
					$.messager.alert('提示','请不要重复添加！'+i+"----"+edit[0]);
					//将其值设计为空
					$(product.target).combogrid('setValue', ''); 
					//获取价格编辑的单元格
	            	var price = editGrid.datagrid('getEditor', {index:edit[0],field:'price'});
	            	//设置价格为0.0
					$(price.target).textbox('setValue',"0.0");	
					//获取小计的单元格									
	            	var subtotal = editGrid.datagrid('getEditor', {index:edit[0],field:'subtotal'});
	            	//设计小计价格为0
					$(subtotal.target).textbox('setValue',"0.0");
					break;
				
				}
			}
			
		}
		
	});	

	return result;
}
/**
 * 设置总金额
 */
function setTotal(subtotal){
	//获取所有的行对象
	var rows=editGrid.datagrid('getRows');
	var total=subtotal;
	//获取正在编辑的行
	var edit=editGrid.datagrid('getEditingRowIndexs');
	
	for(var i=0;i<rows.length;i++){
		//如果是编辑行就不加
		if(i!=edit[0]){
			 total+=parseFloat(rows[i]['subtotal']);
		}
			
	}
	$("#orderTotal").textbox("setValue",total);
}

/**
 *  保存数据
 */
function save(){
	if(endEditing()){//先判断是否能结束编辑
		var form = $("#orderForm");
		if(form.form("validate")){
			$.post("order/order/save",form.serialize(),function(rs){				
				if(rs.msg=="false"){
					$.messager.alert("校验失败！系统异常");
				}else{
					//添加订单详情
					var inserted = editGrid.datagrid('getChanges', "inserted");
					var data=inserted;
					for(var i=0;i<data.length;i++){
						data[i]["order"]=rs.msg;
						
					}
					console.log(JSON.stringify(data));
						$.ajax({url:"order/order/addDetails",
							data:{"detail":JSON.stringify(data)},
							type:"post",
							success:function(data){
								if(data=="true"){
								
									$.messager.alert("提示","添加成功！");
									//重置表单数据
									form.form("reset");
									//清除表格数据
									var item = editGrid.datagrid('getRows');  
						            if (item) {  
						                for (var i = item.length - 1; i >= 0; i--) {  
						                    var index = $('#dg').datagrid('getRowIndex', item[i]);  
						                    editGrid.datagrid('deleteRow', index);  
						                }  
						            }  
									//显示订单列表
									if ($('#tabs').tabs("exists", "订单列表")) {
										
										$('#tabs').tabs("select", "订单列表")
										//刷新
										$("#orderGird").datagrid("reload");
									} else {
										$('#tabs').tabs('add', {
											title : "订单列表",
											href : "order/order",
											closable : true
										});
									}
									
									
								}else{
									$.messager.alert("提示","添加失败！系统异常");
								}
							}				
					});	
					
				}		
			});
		}else{
			$.messager.alert("提示","请检查表单内容！");
		}
	}
	
}