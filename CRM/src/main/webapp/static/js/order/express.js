$(function(){
	var grid;
	grid = $("#expressGird").datagrid({
		fit:true,
		border:false,
		title:'物流公司列表',
		width:"100%",
		pagination:true,
		rownumbers:true,
		singleSelect:true,
		pageList:[8,16,32,64],
		pageSize:8,
	    url:'order/express/lists',
	    idField:'id',
	    method:'get',
	    toolbar:"#expressToolbar",
	    columns:[[
	        {title:'公司名称',field:'name',width:200},
	        {title:'标识',field:'identify',width:220}
	       ]]
	    
	});
	

	// 为工具栏的按钮绑定事件
	$("#expressToolbar").on("click","a.add",function(){
		var dialog = $("<div/>").dialog({
			title: "新增物流公司",
			width: 450,
			height: 200,
			modal:true,
			href:'order/express/form',
			onClose: function(){
				//关闭窗户后，立即销毁窗口
				$(this).dialog("destroy");
			},
			buttons:[
				{
					iconCls:"icon-ok",
					text:"保存",
					handler:function(){
						var form = $("#expressForm");
						//校验表单数据
						if(form.form("validate")){
							$.post("order/express/save",form.serialize(),function(rs){
								if(rs.err){
									alert(rs.err);
								}else{
									//关闭当前窗口
									dialog.dialog("close");
									grid.datagrid("reload");
								}
							});
						}
					}
				},
				{
					iconCls:"icon-cancel",
					text:"取消",
					handler:function(){
						//关闭当前窗口
						dialog.dialog("close");
						grid.datagrid("reload");
					}
				}
			]
		});
	}).on("click","a.edit",function(){
		var row = grid.datagrid("getSelected");
		if(row){
			var dialog = $("<div/>").dialog({
				title: "编辑物流公司",
				width: 450,
				height: 250,
				modal:true,
				href:'order/express/form/'+row.id,
				onClose: function(){
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				},
				buttons:[
					{
						iconCls:"icon-ok",
						text:"保存",
						handler:function(){
							var form = $("#expressForm");
							//校验表单数据
							if(form.form("validate")){
								$.post("order/express/save",form.serialize(),function(rs){
									if(rs.err){
										alert(rs.err);
									}else{
										//关闭当前窗口
										dialog.dialog("close");
										grid.datagrid("reload");
									}
								});
							}
						}
					},
					{
						iconCls:"icon-cancel",
						text:"取消",
						handler:function(){
							dialog.dialog("close");
						}
					}
				]
			});
		}
	}).on("click","a.delete",function(){
		var row = grid.datagrid("getSelected");
		if(row){
			$.messager.confirm('删除提示', '确认删除此记录?', function(r){
                if (r){
                    $.get("order/express/delete/"+row.id,function(){
                    		grid.datagrid("reload");
                    }).error(function(){
                    		$.messager.alert("提示","系统错误,删除失败!");
                    });
                }
            });
		}
	});
	
});