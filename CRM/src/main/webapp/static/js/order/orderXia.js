$(function(){
	var states = {XIA:"已下单",PAY:"已付款",SEND:"已发货",DONE:"已完成",CANCEL:"已取消"};
	var grid;
	grid = $("#orderXiaGird").datagrid({
		fit:true,
		border:false,
		width:"100%",
		pagination:true,
		rownumbers:true,
		singleSelect:true,
		fitColumns:true,
		remoteSore:true,
		nowrap:false,
		pageList:[8,16,32,64],
		pageSize: 16,
	    url:'order/order/lists?state=XIA',
	    idField:'id',
	    method:'get',
	    toolbar:"#orderXiaToolbar",
	    columns:[[
	        {title:'订单编号',field:'orderId',width:280},
	        {title:'主题',field:'theme',width:220},
	        {title:'客户',field:'customer',width:200,formatter:function(value,row,index){
	        	if(row.customer!=null){
	        		return row.customer.name;
	        	}
	        }},
	        {title:'创建人',field:'m_id',width:200,formatter:function(value,row,index){
	        	if(row.member!=null){
	        		return row.member.name;
	        	}
	        }},
	        {title:'销售机会',field:'chance',width:200,formatter:function(value,row,index){
	        	if(row.chance!=null){
	        		return row.chance.chanceInfo;
	        	}else{
	        		return "无";
	        	}
	        }},
	        {title:'下单时间',field:'date',width:200,sortable:true,order:'desc'},
	        {title:'订单状态',field:'state',width:200,formatter:function(value,row,index){
	        		return states[value];
	        }},
	        {title:'总金额/元',field:'total',width:200,sortable:true,order:'desc'},
	        {title:'物流单号',field:'shipment',width:200,formatter:function(value,row,index){
	        	if(row.shipment){
	        		return value;	        	
	        	}else{
	        		return '暂无';
	        	}
	        }},	        
	        {title:'备注',field:'remark',width:250}
	    ]]
	});
	

	// 为工具栏的按钮绑定事件
	$("#orderXiaToolbar").on("click","a.add",function(){
		//跳转到新增订单页面
		if ($('#tabs').tabs("exists", "新增订单")) {
			$('#tabs').tabs("select", "新增订单")
		} else {
			$('#tabs').tabs('add', {
				title : "新增订单",
				href : "order/order/toAdd",
				closable : true
			});
		}
	}).on("click","a.edit",function(){
		var row = grid.datagrid("getSelected");
		if(row){
			var dialog = $("<div/>").dialog({
				title: "编辑订单",
				minimizable:true,
				maximizable:true,
				width: 900,
				height: 450,
				modal:true,
				href:'order/order/toEdit/'+row.id,
				onClose: function(){
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				},
				buttons:[
					{
						iconCls:"icon-ok",
						text:"保存",
						handler:function(){
							var form = $("#orderEditForm");
							//校验表单数据
							if(form.form("validate")){
								$.post("order/order/orderEdit",form.serialize(),function(rs){
									if(rs.msg){
										alert(rs.err);
									}else{
										//关闭当前窗口
										dialog.dialog("close");
										grid.datagrid("reload");
									}
								});
							}
						}
					},
					{
						iconCls:"icon-cancel",
						text:"取消",
						handler:function(){
							dialog.dialog("close");
						}
					}
				]
			});
		}
	}).on("click","a.editDetail",function(){
		var row = grid.datagrid("getSelected");
		if(row){
			var dialog = $("<div/>").dialog({
				title: "编辑订单详情("+row.orderId+")",
				minimizable:true,
				maximizable:true,
				width: 900,
				height: 450,
				modal:true,
				href:'order/order/toEditDetail/'+row.id,
				onClose: function(){
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				}
				
			});
		}
	}).on("click","a.cancel",function(){
		var row = grid.datagrid("getSelected");
		if(row){
			$.messager.confirm('取消提示', '取消该订单将删除一条回款计划，真的要取消吗?', function(r){
                if (r){
                    $.get("order/order/cancel/"+row.id,function(){
                    		grid.datagrid("reload");
                    }).error(function(){
                    		$.messager.alert("提示","系统错误,操作失败!");
                    });
                }
            });
		}
	}).on("click","a.customerInfo",function(){
		var row = grid.datagrid("getSelected");
		if(row){
			var dialog = $("<div/>").dialog({
				title: "客户信息",
				minimizable:true,
				maximizable:true,
				width:500,
				height:400,
				modal:true,
				href:'order/order/customerInfo/'+row.customer.id,
				onClose: function(){
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				},
				buttons:[
					{
						iconCls:"icon-ok",
						text:"打印",
						handler:function(){				
				
							var printHtml = document.getElementById("dam-orderDetails").innerHTML;

							var wind = window.open("",'newwindow', 'height=600, width=800, top=100, left=300, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=n o, status=no');

							wind.document.body.innerHTML = printHtml;

							wind.print();
							return false; 
			
			
						}
					},
					{
						iconCls:"icon-cancel",
						text:"取消",
						handler:function(){
							dialog.dialog("close");
						}
					}
				]
			});
		}
	}).on("click","a.orderDetails",function(){
		var row = grid.datagrid("getSelected");
		if(row){
			var dialog = $("<div/>").dialog({
				title: "订单明细",
				minimizable:true,
				maximizable:true,
				width:550,
				height:600,
				modal:true,
				href:'order/order/orderDetails/'+row.id,
				onClose: function(){
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				},
				buttons:[
					{
						iconCls:"icon-ok",
						text:"打印",
						handler:function(){				
				
							var printHtml = document.getElementById("dam-orderDetails").innerHTML;

							var wind = window.open("",'newwindow', 'height=600, width=800, top=100, left=300, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=n o, status=no');

							wind.document.body.innerHTML = printHtml;

							wind.print();
							return false; 
			
			
						}
					},
					{
						iconCls:"icon-cancel",
						text:"取消",
						handler:function(){
							dialog.dialog("close");
						}
					}
				]
			});
		}
	});
	
});