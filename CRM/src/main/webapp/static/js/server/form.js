$(function() {
	$("#selectCus").combobox({
		url: "customers/selectList",
		method: "post",
		valueField:'id',
	    textField:'text'
	});
	
	$("#satis").combobox({
		valueField: 'id',
		textField: 'text',
		data:[{
		    "id":5,
		    "text":"☆☆☆☆☆"
		},{
		    "id":4,
		    "text":"☆☆☆☆"
		},{
		    "id":3,
		    "text":"☆☆☆",
		},{
		    "id":2,
		    "text":"☆☆"
		},{
		    "id":1,
		    "text":"☆"
		}]
	});
	
	var beforeTime = $("#serverForm #assignDate").val();
	$("#selectMem").combobox({
		url: "permission/member/selectList",
		method: "post",
		valueField:'id',
	    textField:'text',
	    onChange: function(newValue,oldValue){
	    	if(newValue != oldValue){
	    		$("#serverForm #assignDate").textbox({value:new Date().format("yyyy-MM-dd hh:mm:ss")});
	    		$("#serverForm #assignDate").textbox({disabled:false});
	    	} else {
	    		$("#serverForm #assignDate").textbox({value:beforeTime});
	    		$("#serverForm #assignDate").textbox({disabled:false});
	    	}
	    }
	});
	
	var title = $(".panel-title").text().substr(2,$(".panel-title").text().length);
	if(title == "创建服务"){
		$("#serverForm #answer").textbox({readonly:true});
		$("#serverForm #result").textbox({readonly:true});
		$("#serverForm #satis").combobox({readonly:true});
		$("#serverForm #createDate").textbox({disabled:true});
		$("#serverForm #assignDate").textbox({readonly:true});
	} else if(title == "编辑/分配服务") {
		$("#serverForm #answer").textbox({readonly:true});
		$("#serverForm #result").textbox({readonly:true});
		$("#serverForm #satis").combobox({readonly:true});
		$("#serverForm #assignDate").textbox({disabled:true});
	} else if(title == "处理服务") {
		$("#serverForm #selectCus").combobox({readonly:true});
		$("#serverForm #type").combobox({readonly:true});
		$("#serverForm #selectMem").combobox({readonly:true});
		$("#serverForm #question").textbox({readonly:true});
		$("#serverForm #answer").textbox({required:true});
		$("#serverForm #result").textbox({readonly:true});
		$("#serverForm #satis").combobox({readonly:true});
	} else if(title == "反馈服务"){
		$("#serverForm #selectCus").combobox({readonly:true});
		$("#serverForm #type").combobox({readonly:true});
		$("#serverForm #selectMem").combobox({readonly:true});
		$("#serverForm #question").textbox({readonly:true});
		$("#serverForm #answer").textbox({readonly:true});
		$("#serverForm #dealDate").textbox({disabled:false});
		$("#serverForm #dealer").textbox({disabled:false});
		$("#serverForm #result").textbox({required:true});
		$("#serverForm #satis").combobox({required:true});
	} else if(title == "服务详情"){
		$("#serverForm #selectCus").combobox({readonly:true});
		$("#serverForm #type").combobox({readonly:true});
		$("#serverForm #selectMem").combobox({readonly:true});
		$("#serverForm #question").textbox({readonly:true});
		$("#serverForm #answer").textbox({readonly:true});
		$("#serverForm #dealDate").textbox({readonly:true});
		$("#serverForm #dealer").textbox({readonly:true});
		$("#serverForm #result").textbox({readonly:true});
		$("#serverForm #satis").combobox({readonly:true});
		$("#serverForm #dealDate").textbox({disabled:false});
		$("#serverForm #dealer").textbox({disabled:false});
		$("#serverForm #result").textbox({required:false});
		$("#serverForm #satis").combobox({required:false});
	}
})