$(function() {
	
	//获取session
	var loginName = $("#sessionName").val();
	var types = {ASK:"咨询",COMPLAIN:"投诉",ADVICE:"建议"};
	var state = {NEW:"新创建",ASSIGNED:"已分配",DEALED:"已处理",SAVED:"已归档"};
	var grid;
	grid = $("#serverGird").datagrid({
		fit:true,
		border:false,
		singleSelect:true,
		pagination:true,
		fitColumns:true,
	    url:'server/list',
	    rownumbers:true,
	    idField:'id',
	    toolbar:"#serverToolbar",
	    columns:[[
			{title:'ID',field:'id',width:50},
	        {title:'客户名称',field:'customer',width:100,formatter:function(value,row,index){
	        		return value.name;
	        }},
	        {title:'服务类型',field:'type',width:100,formatter:function(value,row,index){
	        		return types[value];
	        }},
	        {title:'问题描述',field:'question',width:180},
	        {title:'状态',field:'state',width:100,formatter:function(value,row,index){
	        		return state[value];
	        }},
	        {title:'创建人',field:'creator',width:100,formatter:function(value,row,index){
	        		return value.name;
	        }},
	        {title:'创建时间',field:'createDate',width:150,formatter:function(value,row,index){
	        	return new Date(value).format("yyyy-MM-dd hh:mm:ss");
	        }}
	    ]]
	});
	
	$("#serverToolbar #search").searchbox({width:250});
	
	$("#serverToolbar a.all").click(function() {
		
		$("#serverToolbar #selectState").combobox("setValue","");
		$("#serverToolbar #selectType").combobox("setValue","");
		$("#search").searchbox("setValue","");
		
		$('#serverGird').datagrid("load",{});
	})
	
	$("#serverToolbar #selectState").combobox({
		valueField: 'id',
		textField: 'text',
		data:[{
		    "id":"",
		    "text":"请选择状态"
		},{
		    "id":"NEW",
		    "text":"新创建"
		},{
		    "id":"ASSIGNED",
		    "text":"已分配"
		},{
		    "id":"DEALED",
		    "text":"已处理",
		},{
		    "id":"SAVED",
		    "text":"已归档"
		}],
		onChange: function(before,after){
			var value = $("#serverToolbar #search").searchbox('getValue');
			var name = $("#serverToolbar #search").searchbox('getName');
			doSearch(value,name);
	    }
	});
	
	$("#serverToolbar #selectType").combobox({
		valueField: 'id',
		textField: 'text',
		data:[{
		    "id":"",
		    "text":"请选择类别"
		},{
		    "id":"ASK",
		    "text":"咨询"
		},{
		    "id":"COMPLAIN",
		    "text":"投诉"
		},{
		    "id":"ADVICE",
		    "text":"建议",
		}],
		onChange: function(before,after){
			var value = $("#serverToolbar #search").searchbox('getValue');
			var name = $("#serverToolbar #search").searchbox('getName');
			doSearch(value,name);
	    }
	});
	
	// 为工具栏的按钮绑定事件
	$("#serverToolbar").on("click","a.add",function(){
		
		var dialog = $("<div/>").dialog({
			title: "创建服务",
			width: 550,
			modal: true,
			left: 400,
			top: 100,
			href:'server/form',
			onClose: function(){
				//关闭窗户后，立即销毁窗口
				$(this).dialog("destroy");
			},
			buttons:[
				{
					iconCls:"icon-ok",
					text:"保存",
					handler:function(){
						if($("#serverForm #selectCus").val() != "" && $("#serverForm #question").val() != ""){
							switch ($("#serverForm #state").val()) {
								case "新创建":
									$("#serverForm #state").textbox({value:"NEW"});
									break;
								case "已分配":
									$("#serverForm #state").textbox({value:"ASSIGNED"});
									break;
								case "已处理":
									$("#serverForm #state").textbox({value:"DEALED"});
									break;
								case "已归档":
									$("#serverForm #state").textbox({value:"SAVED"});
									break;
								default:
									break;
							}
							var form = $("#serverForm");
							console.log(form.serialize());
							//校验表单数据
							if(form.form("validate")){
								$.post("server/save",form.serialize(),function(rs){
									if(rs.err){
										$.messager.alert('提示',rs.err);
									}else{
										$.messager.alert('提示','创建成功');
										//关闭当前窗口
										dialog.dialog("close");
										grid.datagrid("reload");
									}
								});
							}
						} else {
							$.messager.alert('提示','请填好所有必填信息!');
						}
					}
				},
				{
					iconCls:"icon-cancel",
					text:"取消",
					handler:function(){
						dialog.dialog("close");
					}
				}
			]
		});
	}).on("click","a.edit",function(){
		var row = grid.datagrid("getSelected");
		if(!row){
			$.messager.alert('提示','请选择要编辑/分配的行！');
		} else {
			if(row.state != "ASSIGNED" && row.state != "NEW"){
				$.messager.alert('提示','不可编辑 <'+state[row.state]+'> 的服务');
			} else {
				var dialog = $("<div/>").dialog({
					title: "编辑/分配服务",
					width: 550,
					modal:true,
					left: 400,
					top: 100,
					href:'server/form/'+row.id,
					onClose: function(){
						//关闭窗户后，立即销毁窗口
						$(this).dialog("destroy");
					},
					buttons:[
						{
							iconCls:"icon-ok",
							text:"保存",
							handler:function(){
								if($("#serverForm #question").val() != ""){
									switch ($("#serverForm #state").val()) {
										case "新创建":
											$("#serverForm #state").textbox({value:"NEW"});
											break;
										case "已分配":
											$("#serverForm #state").textbox({value:"ASSIGNED"});
											break;
										case "已处理":
											$("#serverForm #state").textbox({value:"DEALED"});
											break;
										case "已归档":
											$("#serverForm #state").textbox({value:"SAVED"});
											break;
										default:
											break;
									}
									var form = $("#serverForm");
									console.log(form.serialize());
									//校验表单数据
									if(form.form("validate")){
										$.post("server/save",form.serialize(),function(rs){
											if(rs.err){
												$.messager.alert('提示',rs.err);
											}else{
												$.messager.alert('提示','编辑成功');
												//关闭当前窗口
												dialog.dialog("close");
												grid.datagrid("reload");
											}
										});
									}
								} else {
									$.messager.alert('提示','请填好所有必填信息!');
								}
							}
						},
						{
							iconCls:"icon-cancel",
							text:"取消",
							handler:function(){
								dialog.dialog("close");
							}
						}
					]
				});
			}
		}
	}).on("click","a.delete",function(){
		var row = grid.datagrid("getSelected");
		if(!row){
			$.messager.alert('提示','请选择要删除的行！');
		} else{
			if(row.creator.name != loginName){
				$.messager.alert('提示','非你创建,不可删除！');
			} else {
				$.messager.confirm('删除提示', '确认删除此记录?', function(r){
	                if (r){
	                    $.get("server/delete/"+row.id,function(){
	                    	$.messager.alert('提示','删除成功');
	                    	grid.datagrid("reload");
	                    }).error(function(){
	                    	$.messager.alert("提示","系统错误,删除失败!");
	                    });
	                }
	            });
			}
		}
	}).on("click","a.deal",function(){
		var row = grid.datagrid("getSelected");
		if(!row){
			$.messager.alert('提示','请选择要处理的行！');
		} else {
			if(row.state != "ASSIGNED"){
				$.messager.alert("提示","暂未分配,不可处理!");
			} else {
				if(row.dealer.name != loginName){
					$.messager.alert("提示","未分配你处理,不可处理!");
				} else {
					var dialog = $("<div/>").dialog({
						title: "处理服务",
						width: 550,
						modal:true,
						left: 400,
						top: 100,
						href:'server/form/'+row.id,
						onClose: function(){
							//关闭窗户后，立即销毁窗口
							$(this).dialog("destroy");
						},
						buttons:[
							{
								iconCls:"icon-ok",
								text:"保存",
								handler:function(){
									switch ($("#serverForm #state").val()) {
										case "新创建":
											$("#serverForm #state").textbox({value:"NEW"});
											break;
										case "已分配":
											$("#serverForm #state").textbox({value:"ASSIGNED"});
											break;
										case "已处理":
											$("#serverForm #state").textbox({value:"DEALED"});
											break;
										case "已归档":
											$("#serverForm #state").textbox({value:"SAVED"});
											break;
										default:
											break;
									}
									var form = $("#serverForm");
									//校验表单数据
									if(form.form("validate")){
										$.post("server/save",form.serialize(),function(rs){
											if(rs.err){
												$.messager.alert('提示',rs.err);
											}else{
												$.messager.alert('提示','处理成功');
												//关闭当前窗口
												dialog.dialog("close");
												grid.datagrid("reload");
											}
										});
									}
								}
							},
							{
								iconCls:"icon-cancel",
								text:"取消",
								handler:function(){
									dialog.dialog("close");
								}
							}
						]
					});
				}
			}
		}
	}).on("click","a.back",function(){
		var row = grid.datagrid("getSelected");
		if(!row){
			$.messager.alert('提示','请选择要反馈的行！');
		} else {
			if(row.state != "DEALED"){
				$.messager.alert('提示','只可反馈已处理服务！');
			} else {
				var dialog = $("<div/>").dialog({
					title: "反馈服务",
					width: 550,
					modal:true,
					left: 400,
					top: 100,
					href:'server/form/'+row.id,
					onClose: function(){
						//关闭窗户后，立即销毁窗口
						$(this).dialog("destroy");
					},
					buttons:[
						{
							iconCls:"icon-ok",
							text:"保存",
							handler:function(){
								switch ($("#serverForm #state").val()) {
									case "新创建":
										$("#serverForm #state").textbox({value:"NEW"});
										break;
									case "已分配":
										$("#serverForm #state").textbox({value:"ASSIGNED"});
										break;
									case "已处理":
										$("#serverForm #state").textbox({value:"DEALED"});
										break;
									case "已归档":
										$("#serverForm #state").textbox({value:"SAVED"});
										break;
									default:
										break;
								}
								var form = $("#serverForm");
								console.log(form.serialize());
								//校验表单数据
								if(form.form("validate")){
									$.post("server/save",form.serialize(),function(rs){
										if(rs.err){
											$.messager.alert('提示',rs.err);
										}else{
											if($("#serverForm #satis").val() >= 3){
												$.messager.alert('提示','反馈成功,满意度达标,已存档！');
											} else {
												$.messager.alert('提示','反馈成功,满意度不达标,已重新分配！');
											}
											//关闭当前窗口
											dialog.dialog("close");
											grid.datagrid("reload");
										}
									});
								}
							}
						},
						{
							iconCls:"icon-cancel",
							text:"取消",
							handler:function(){
								dialog.dialog("close");
							}
						}
					]
				});
			}
		}
	}).on("click","a.see",function(){
		var row = grid.datagrid("getSelected");
		if(!row){
			$.messager.alert('提示','请选择行！');
		} else {
			var dialog = $("<div/>").dialog({
				title: "服务详情",
				width: 550,
				modal:true,
				left: 400,
				top: 100,
				href:'server/form/'+row.id,
				onClose: function(){
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				}
			});
		}
	});
});

function doSearch(value,name){
	var param = {};
	param[name] = value;
	param["type"] = $("#serverToolbar #selectType").combobox("getValue");
	param["state"] = $("#serverToolbar #selectState").combobox("getValue");
	$('#serverGird').datagrid('load', param);
}


