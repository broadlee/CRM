$(function(){
	var state={TUING:"退货中",TUED:"已完成"};
	var grid=$('#returnDataGrid').datagrid({
		fit:true,
		title:"退货列表",
		border:false,
		singleSelect:true,
		pagination:true,
		fitColumns:true,
		nowrap:false,
		pageList:[8,16,32,64],
		pageSize:8,
		method:'get',
	    url:'return/return/list',
	    toolbar:'#returnToolBar',
	    idField:'id',
	    columns:[[
	        {field:'returnId',title:'退货单号',width:200,align:'center'},
	        {field:'date',title:'退货时间',width:100,align:'center',sortable:true,order:"desc"},
	        {field:'orders',title:'订单编号',width:200,align:'center',formatter:function(value,row,index){
	        	
	        	return row.orders.orderId;
	        }},
	        {field:'theme',title:'订单主题',width:100,align:'center',formatter:function(value,row,index){
	        	return row.orders.theme;
	        }},
	        {field:'customer',title:'客户',width:100,align:'center',formatter:function(value,row,index){
	        	if(row.orders.customer!=null){
	        		return row.orders.customer.name;
	        		}else{
	        			return "暂没客户";
	        		}
	        	
	        }},
	        {field:'returned',title:'应退金额/元',width:100,align:'center',sortable:true,order:'desc'},
	        {field:'refuned',title:'已退金额/元',width:100,align:'center',sortable:true,order:'desc'},
	        {field:'state',title:'状态',width:100,align:'center',formatter:function(value,row,index){
	        	return state[value];
	        }},
	    ]]
	});
	$("#returnToolBar").on("click","a.returnDetails",function(){
		var row = grid.datagrid("getSelected");
		if(row){
			var dialog = $("<div/>").dialog({
				title: "退货单明细",
				minimizable:true,
				maximizable:true,
				width:550,
				height:600,
				modal:true,
				href:'return/return/returnDetail/'+row.id,
				onClose: function(){
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				},
				buttons:[
					{
						iconCls:"icon-ok",
						text:"打印",
						handler:function(){				
				
							var printHtml = document.getElementById("dam-orderDetails").innerHTML;

							var wind = window.open("",'newwindow', 'height=600, width=800, top=100, left=300, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=n o, status=no');

							wind.document.body.innerHTML = printHtml;

							wind.print();
							return false; 
			
			
						}
					},
					{
						iconCls:"icon-cancel",
						text:"取消",
						handler:function(){
							dialog.dialog("close");
						}
					}
				]
			});
		}
	}).on("click","a.customerInfo",function(){
		var row = grid.datagrid("getSelected");
		if(row){
			var dialog = $("<div/>").dialog({
				title: "客户信息",
				minimizable:true,
				maximizable:true,
				width:500,
				height:400,
				modal:true,
				href:'order/order/customerInfo/'+row.orders.customer.id,
				onClose: function(){
					//关闭窗户后，立即销毁窗口
					$(this).dialog("destroy");
				},
				buttons:[
					{
						iconCls:"icon-ok",
						text:"打印",
						handler:function(){				
				
							var printHtml = document.getElementById("dam-orderDetails").innerHTML;

							var wind = window.open("",'newwindow', 'height=600, width=800, top=100, left=300, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=n o, status=no');

							wind.document.body.innerHTML = printHtml;

							wind.print();
							return false; 
			
			
						}
					},
					{
						iconCls:"icon-cancel",
						text:"取消",
						handler:function(){
							dialog.dialog("close");
						}
					}
				]
			});
		}
	}).on("click","a.delete",function(){
		var row = grid.datagrid("getSelected");
		if(row&&row.state=="TUED"){
			$.messager.confirm('删除提示', '确认删除此记录?', function(r){
                if (r){
                    $.get("return/return/delete/"+row.id,function(){
                    		grid.datagrid("reload");
                    }).error(function(){
                    		$.messager.alert("提示","系统错误,删除失败!");
                    });
                }
            });
		}else{
			$.messager.alert("提示","退货中的商品不能删除！");
		}
	});
});