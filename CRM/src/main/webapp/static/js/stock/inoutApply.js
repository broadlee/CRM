$(function() {
	var state = {NODEAL:"未处理",DEAL:"已通过",REJECT:"已拒绝"};
	var grid;
	grid = $("#applyGird").datagrid({
		fit:true,
		border:false,
		singleSelect:true,
		pagination:true,
		fitColumns:true,
	    url:'stock/inoutApply/list',
	    rownumbers:true,
	    idField:'id',
	    toolbar:"#applyToolbar",
	    columns:[[
			{title:'ID',field:'id',width:50},
	        {title:'关联订单编号',field:'orders',width:100,formatter:function(value,row,index){
	        	return value==null?"":value.orderId;
	        }},
	        {title:'关联采购单编号',field:'purchase',width:180,formatter:function(value,row,index){
	        	return value==null?"":value.chaseId;
	        }},
	        {title:'状态',field:'state',width:100,formatter:function(value,row,index){
	        		return state[value];
	        }}
	    ]]
	});
	
	$("#applyToolbar #pass").on("click",function(){
		var row = $("#applyGird").datagrid("getSelected");
		if(row){
			var data = {};
			data["rowid"]=row.id;
			data["rowtype"]="pass";
			$('#applyGird').datagrid('load', data);
		}else{
			$.messager.alert("提示","请选择一条出/入库记录");
		}
	});
	
	$("#applyToolbar #refuse").on("click",function(){
		var row = $("#applyGird").datagrid("getSelected");
		if(row){
			$('#applyGird').datagrid('load', {
				"rowid":row.id,
				"rowtype":"refuse"
			});
		}else{
			$.messager.alert("提示","请选择一条出/入库记录");
		}
	});
	
	$('#applyToolbar #search').searchbox({
		searcher : function(value,name){
			doSearch(value,name);
		},
		width : 300,
		menu : '#searchitem',
		prompt : '请输入查找关键字'
	});
	
	$("#applyToolbar #selectState").combobox({
		valueField: 'id',
		textField: 'text',
		data:[{
		    "id":"",
		    "text":"请选择状态"
		},{
		    "id":"NODEAL",
		    "text":"未处理"
		},{
		    "id":"DEAL",
		    "text":"已通过"
		},{
		    "id":"REJECT",
		    "text":"已拒绝",
		}],
		onChange: function(before,after){
			var value = $("#applyToolbar #search").searchbox('getValue');
			var name = $("#applyToolbar #search").searchbox('getName');
			doSearch(value,name);
	    }
	});
})

function doSearch(value,name){
	var param = {};
	param[name] = value;
	param["state"] = $("#applyToolbar #selectState").combobox("getValue");
	$('#applyGird').datagrid('load', param);
}

