$(function() {
	var grid;
	grid = $("#inoutRecordGird").datagrid({
		fit:true,
		border:false,
		singleSelect:true,
		pagination:true,
		fitColumns:true,
	    url:'stock/inoutRecord/list',
	    rownumbers:true,
	    idField:'id',
	    toolbar:"#inoutRecordToolbar",
	    columns:[[
			{title:'ID',field:'id',width:50},
	        {title:'关联出/入库单编号',field:'inout',width:100,formatter:function(value,row,index){
	        	return value.inoutId;
	        }},
	        {title:'处理人',field:'member',width:180,formatter:function(value,row,index){
	        	return value.name;
	        }},
	        {title:'处理时间',field:'date',width:150,formatter:function(value,row,index){
	        	return new Date(value).format("yyyy-MM-dd hh:mm:ss");
	        }}
	    ]]
	});
	
	$('#inoutRecordToolbar #search').searchbox({
		searcher : function(value, name) {
			$('#inoutRecordGird').datagrid('load',{
				'name': name,
				'value':value
			});
		},
		width : 300,
		menu : '#searchitem',
		prompt : '请输入查找关键字'
	});
})
