$(function() {
	var grid;
	grid = $("#stockGird").datagrid({
		fit:true,
		border:false,
		singleSelect:true,
		pagination:true,
		fitColumns:true,
	    url:'stock/stockList/list',
	    rownumbers:true,
	    idField:'id',
	    toolbar:"#stockToolbar",
	    columns:[[
			{title:'ID',field:'id',width:50},
	        {title:'产品名称',field:'name',width:100},
	        {title:'库存数量',field:'stock',width:180}
	    ]]
	});
	
	$('#stockToolbar #show').on("click",function(){
		var row = $("#stockGird").datagrid("getSelected");
		if(row){
			$('#formGrid').datagrid({
				url:"stock/stockList/form/"+row.name,
				pagination: true,  
	            rownumbers: true, 
				columns:[[
					{title:'产品名称',field:'name',width:100,align:'center'},
					{title:'订单id',field:'orderId',width:100,align:'center'},
					{title:'采购id',field:'chaseId',width:100,align:'center'},
				    {title:'售出数量',field:'count',width:100,align:'center'},
				    {title:'采购数量',field:'stock',width:100,align:'center'},
				    {title:'处理时间',field:'date',width:190,align:'center'}
				]]
			});
			 $('#dd').window('open');
		}else{
			$.messager.alert("提示","请选择一条库存记录");
		}
	});
	
	$('#stockToolbar #search').searchbox({
		searcher : function(value, name) {
			$('#stockGird').datagrid('load',{
				'value': value
			});
		},
		width : 300,
		menu : '#searchitem',
		prompt : '请输入查找关键字'
	});
})
